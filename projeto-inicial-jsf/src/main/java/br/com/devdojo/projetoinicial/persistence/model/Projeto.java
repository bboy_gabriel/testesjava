package br.com.devdojo.projetoinicial.persistence.model;

import javax.persistence.Entity;

/**
 * Created by William Suane on 9/28/2016.
 */
@Entity
public class Projeto extends AbstractEntity {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
