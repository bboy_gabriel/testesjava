package br.com.devdojo.projetoinicialrest.persistence.model.enums;

/**
 * Created by William Suane on 11/15/2016.
 */
public enum Condition {
    LIKE("like"), EQUAL("=");
    private String condition;

    Condition(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }
}
