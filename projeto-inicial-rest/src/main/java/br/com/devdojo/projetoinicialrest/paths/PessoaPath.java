package br.com.devdojo.projetoinicialrest.paths;

import br.com.devdojo.projetoinicialrest.annotations.Transactional;
import br.com.devdojo.projetoinicialrest.exceptions.custom.ObjectNotFoundException;
import br.com.devdojo.projetoinicialrest.persistence.daointerfaces.DAO;
import br.com.devdojo.projetoinicialrest.persistence.model.Pessoa;
import br.com.devdojo.projetoinicialrest.persistence.model.enums.Condition;
import br.com.devdojo.projetoinicialrest.utils.StringUtils;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Arrays.asList;

/**
 * Created by William Suane on 11/15/2016.
 */
@Path("/pessoa")
public class PessoaPath {
    private List<Pessoa> pessoas;
    private static ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    @Inject
    private DAO<Pessoa> dao;

    {
        pessoas = asList(new Pessoa("William Suane"), new Pessoa("Goku"), new Pessoa("Genji"));
    }


    @GET
    @Path("oi")
//    @Produces("application/json")
    public Response oi() {
//        System.out.println(Thread.currentThread().getName());
        Pessoa p = new Pessoa("DevDojo person");
        return Response.ok(new Gson().toJson(p)).build();
//        return Response.status(Response.Status.OK).entity(Thread.currentThread().getName()).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("oiAsync")
    @Produces("application/json")
    public void oi(@Suspended AsyncResponse asyncResponse) {
//        System.out.println(Thread.currentThread().getName());
        executorService.execute(() -> {
            Pessoa p = new Pessoa("DevDojo person");
            Response res = Response.status(Response.Status.OK).entity(p).type(MediaType.APPLICATION_JSON_TYPE).build();
            asyncResponse.resume(res);
        });

    }

    @GET
    @Path("listar")
    @Produces("application/json")
    public Response listar() {
        this.pessoas = dao.listAll();
        return Response.ok(new Gson().toJson(pessoas)).build();
    }

    @GET
    @Path("buscar/{nome}")
    @Produces("application/json")
    public Response buscar(@PathParam("nome") String nome) throws ObjectNotFoundException {
//        pessoas = dao.findByHQLQuery("findPessoaByName", Collections.singletonList(nome), 0);
       pessoas = dao.findByAttributes(ImmutableMap.of("nome", StringUtils.like(nome)), Collections.singletonList(Condition.LIKE));
//        if (index < 0)
//            throw new ObjectNotFoundException();
        return Response.ok(new Gson().toJson(pessoas)).build();
    }

    @POST
    @Path("salvar")
    @Consumes("application/json")
    @Produces("application/json")
    @Transactional
    public Response salvar(Pessoa pessoa) {
        Pessoa saved = dao.save(pessoa);
        return Response.ok(new Gson().toJson(saved)).build();
    }

}
