package com.springboot.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloController {

	@RequestMapping("/hello/{name}")
	String hello(@PathVariable String name) {

		return "Hi " + name + " !";

	}
	
	@RequestMapping("/helloWord/{name}")
	String helloWord(@PathVariable String name) {

		return "Hi world " + name + " !";

	}
}