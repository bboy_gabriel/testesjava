package com.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.model.UsuarioModel;
import com.springboot.repository.UsuarioRepository;


@RestController
class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@RequestMapping("/usuario/list")
	public ResponseEntity<?> listarUsuarios(){
		//return new ResponseEntity<>("opa",HttpStatus.OK);
		return new ResponseEntity<>(usuarioRepository.findAll(),HttpStatus.OK);
	}
	
	@RequestMapping("/usuario/save")
	public ResponseEntity<?> listarUsuarios(@RequestBody UsuarioModel usuario){
		//return new ResponseEntity<>("opa",HttpStatus.OK);
		return new ResponseEntity<>(usuarioRepository.save(usuario),HttpStatus.OK);
	}
}