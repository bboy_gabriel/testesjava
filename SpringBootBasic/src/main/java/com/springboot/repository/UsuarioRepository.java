package com.springboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.model.UsuarioModel;


@Repository
public interface UsuarioRepository 
extends CrudRepository<UsuarioModel, Long> 
{
	
}
