import { error } from '@angular/compiler/src/util';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpProgressEvent, HttpRequest, HttpResponse, HttpSentEvent, HttpUserEvent } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, finalize, tap } from 'rxjs/operators';
import { MessagesService } from '../services/message/message.service';
import swal from 'sweetalert2';
import { getTokenHeadersFromHttpClient, tokenGetter } from 'src/app/app.module';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private injector: Injector,
        private router: Router,
        private messageService: MessagesService,
        private spinner: NgxSpinnerService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any> | HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
            let requisicao = !req.url.includes('login') ? req.clone({
                setHeaders: {Authorization: `Bearer `+ tokenGetter()}
            }): req;     
            return next.handle(requisicao)  
            .pipe(tap(data =>{   
                this.spinner.show();          
                if(data instanceof HttpResponse)
                    this.spinner.hide();
                of(data)
            },
            error =>{
                return of(error);
            } ))          
            .pipe(catchError((error: HttpErrorResponse) => {
                this.injector.get(MessagesService)
                let isErroResp = error instanceof HttpErrorResponse;
                if (isErroResp && error.status === 401) {
                    this.messageService.errorSession("Sessão expirada. Logar novamente.", { objmessage: null });
                    this.router.navigate(['/'])                    
                } else if(isErroResp && error.status !== null){
                    this.messageService.errorSession(error.error.mensagem || error.error.message, { objmessage: null });
                }                
                this.spinner.hide();
                return throwError(error);
            }) as any, finalize (()=>{
                    //ação final
                    //this.spinner.hide();
            }));
    }
}