import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { getTokenHeadersFromHttpClient } from 'src/app/app.module';
import { environment } from 'src/environments/environment';
import { FornecedorSAP } from '../../../model/tabelas/fornecedorsap.model';
import { SolicitacaoPagamento } from '../../../model/pagamento/solicitacao-pagamento.model';

@Injectable()
export class PagamentoService {
    api_ref: string = environment.API_URL;

    constructor(private httpClient: HttpClient) { }

    public solicitarPagamento(solicitacao: SolicitacaoPagamento): Observable<any> {
        return this.httpClient.post(`${this.api_ref}/api/solicitacoesPagamentos/`, solicitacao);
    }

    public reenviarSolicitacao(solicitacao: SolicitacaoPagamento): Observable<any> {
        return this.httpClient.put<SolicitacaoPagamento>(`${this.api_ref}/api/solicitacoesPagamentos/`, solicitacao);
    }

    public consultaSolicitacao(params): Observable<any> {
        return this.httpClient.put<SolicitacaoPagamento>(`${this.api_ref}/api/solicitacoesPagamentos/consultarSolicitacaoSAP/${params}`, params);
    }

    public consultaSolicitacaoPorID(params): Observable<any> {
        return this.httpClient.get<SolicitacaoPagamento>(`${this.api_ref}/api/solicitacoesPagamentos/${params}`);
    }

    public consultarPagamento(params): Observable<any> {
        return this.httpClient.get<SolicitacaoPagamento[]>(`${this.api_ref}/api/solicitacoesPagamentos`);
    }

    public tipoDepJudicial(): Observable<any> {
        return this.httpClient.get<FornecedorSAP[]>(`${this.api_ref}/api/fornecedoresSap`);
    }

    public obterDomicilioBancario(codigoFornecedorSAP): Observable<any> {
        return this.httpClient.get(`${this.api_ref}/api/sap/obterDomicilioBancario/${codigoFornecedorSAP}`);
    }

    public consultaSolicitacaoPagamento(paginaFiltro:any): Observable<any> {
        return this.httpClient.post<any>(`${this.api_ref}/api/solicitacoesPagamentos/consulta/`, paginaFiltro);
    }

}
