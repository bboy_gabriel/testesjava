import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { OrgaoGestor } from '../../../model/tabelas/orgaogestor.model';

@Injectable({
  providedIn: 'root'
})
export class OrgaoGestorService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public ConsultaOrgaoGestor(): Observable<any> {
    return this.httpClient.get<OrgaoGestor[]>(`${this.api_ref}/api/orgaoGestor`);
  }

  public cadastrar(orgao: OrgaoGestor): Observable<any> {
    return this.httpClient.post<OrgaoGestor>(`${this.api_ref}/api/orgaoGestor`, orgao);
  }

  public consultaPorId(id): Observable<any> {
    return this.httpClient.get<OrgaoGestor>(`${this.api_ref}/api/orgaoGestor/${id}`);
  }

  public alterar(params): Observable<any> {
    return this.httpClient.put<OrgaoGestor>(`${this.api_ref}/api/orgaoGestor`, params);
  }

  public remover(id): Observable<any> {
    return this.httpClient.delete<OrgaoGestor>(`${this.api_ref}/api/orgaoGestor/${id}`);
  }

}
