import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ContaRazaoProvisao } from '../../../model/tabelas/conta-razao-provisao';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContaRazaoProvisaoService {

  api_ref: string = environment.API_URL;
  
  constructor(private httpClient: HttpClient) { }

  public consultar(): Observable<any> {
    return this.httpClient.get<ContaRazaoProvisao[]>(`${this.api_ref}/api/contaRazaoProvisao`);
  }

  public cadastrar(conta: ContaRazaoProvisao): Observable<any> {
    return this.httpClient.post<ContaRazaoProvisao>(`${this.api_ref}/api/contaRazaoProvisao`, conta);
  }

  public consultarPorId(id: number): Observable<any> {
    return this.httpClient.get<ContaRazaoProvisao>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }

  public alterar(conta: ContaRazaoProvisao): Observable<any> {
    return this.httpClient.put<ContaRazaoProvisao>(`${this.api_ref}/api/ENDPOINT`, conta);
  }

  public remover(id: number): Observable<any> {
    return this.httpClient.delete<ContaRazaoProvisao>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }
}
