import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { FornecedorSAP } from '../../../model/tabelas/fornecedorsap.model';



@Injectable({
  providedIn: 'root'
})
export class FornecedorSapService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public consultaFornecedoresSAP(): Observable<any> {
    return this.httpClient.get<FornecedorSAP[]>(`${this.api_ref}/api/fornecedoresSap`)
  }

  public cadastrar(fornecedor: FornecedorSAP): Observable<any> {
    return this.httpClient.post<FornecedorSAP>(`${this.api_ref}/api/fornecedoresSap`, (fornecedor));

  }

  public consultaPorId(id): Observable<any> {
    return this.httpClient.get<FornecedorSAP>(`${this.api_ref}/api/fornecedoresSap/${id}`);

  }

  public alterar(params): Observable<any> {
    return this.httpClient.put<FornecedorSAP>(`${this.api_ref}/api/fornecedoresSap`, params);

  }

  public remover(id): Observable<any> {
    return this.httpClient.delete(`${this.api_ref}/api/fornecedoresSap/${id}`);
  }

}
