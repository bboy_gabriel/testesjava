import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { ElementoPEP } from '../../../model/tabelas/elementoPEP.model';

@Injectable({
  providedIn: 'root'
})
export class ElementoPepService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public consultaElementosPEP(): Observable<any> {
    return this.httpCliente.get<ElementoPEP[]>(`${this.api_ref}/api/elementosPep`);
  }

  public cadastrar(elemento: ElementoPEP): Observable<any> {
    return this.httpCliente.post<ElementoPEP>(`${this.api_ref}/api/elementosPep`, (elemento));
  }

  public consultaPorId(id): Observable<any> {
    return this.httpCliente.get<ElementoPEP>(`${this.api_ref}/api/elementosPep/${id}`);
  }

  public alterarPorId(params): Observable<any> {
    return this.httpCliente.put<ElementoPEP>(`${this.api_ref}/api/elementosPep`, params);
  }

  public remover(id): Observable<any> {
    return this.httpCliente.delete<ElementoPEP>(`${this.api_ref}/api/elementosPep/${id}`);
  }

}
