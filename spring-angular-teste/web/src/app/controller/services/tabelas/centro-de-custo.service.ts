import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { CentroDeCusto } from '../../../model/tabelas/centroDeCusto.model';

@Injectable({
  providedIn: 'root'
})
export class CentroDeCustoService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public consultar(): Observable<any> {
    return this.httpCliente.get<CentroDeCusto[]>(`${this.api_ref}/api/centrosDeCusto`);
  }

  public cadastrar(centro: CentroDeCusto): Observable<any> {
    return this.httpCliente.post<CentroDeCusto>(`${this.api_ref}/api/centrosDeCusto`, (centro))
  }

  public consultaPorId(id): Observable<any> {
    return this.httpCliente.get<CentroDeCusto>(`${this.api_ref}/api/centrosDeCusto/${id}`)
  }

  public alterar(params): Observable<any> {
    return this.httpCliente.put<CentroDeCusto>(`${this.api_ref}/api/centrosDeCusto`, params)
  }

  public remover(id): Observable<any> {
    return this.httpCliente.delete<CentroDeCusto>(`${this.api_ref}/api/centrosDeCusto/${id}`)
  }

}
