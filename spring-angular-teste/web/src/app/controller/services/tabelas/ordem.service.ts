import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { Ordem } from '../../../model/tabelas/ordem.model';

@Injectable({
  providedIn: 'root'
})

export class OrdemService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public ConsultaOrdem(): Observable<any> {
    return this.httpCliente.get<Ordem[]>(`${this.api_ref}/api/ordens`);
  }

  public cadastrar(ordem: Ordem): Observable<any> {
    return this.httpCliente.post<Ordem[]>(`${this.api_ref}/api/ordens`, (ordem));
  }

  public consultaPorId(id): Observable<any> {
    return this.httpCliente.get<Ordem[]>(`${this.api_ref}/api/ordens/${id}`);
  }

  public alterar(params): Observable<any> {
    return this.httpCliente.put<Ordem[]>(`${this.api_ref}/api/ordens/`, params);
  }

  public remover(id): Observable<any> {
    return this.httpCliente.delete<Ordem[]>(`${this.api_ref}/api/ordens/${id}`)
  }

}
