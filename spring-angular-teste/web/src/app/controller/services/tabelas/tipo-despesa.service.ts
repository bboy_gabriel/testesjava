import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { TipoDespesa } from '../../../model/tabelas/tipodespesa.model';

@Injectable({
  providedIn: 'root'
})
export class TipoDespesaService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public consultaTipoDespesa(): Observable<any> {
    return this.httpClient.get<TipoDespesa[]>(`${this.api_ref}/api/tipoDeDespesas`);
  }

  public cadastrar(tipoDepesa: TipoDespesa): Observable<any> {
    return this.httpClient.post<TipoDespesa>(`${this.api_ref}/api/tipoDeDespesas`, (tipoDepesa));
  }

  public consultaPorId(id): Observable<any> {
    return this.httpClient.get<TipoDespesa>(`${this.api_ref}/api/tipoDeDespesas/${id}`);
  }

  public alterar(params): Observable<any> {
    return this.httpClient.put<TipoDespesa>(`${this.api_ref}/api/tipoDeDespesas`, params);
  }

  public remover(id): Observable<any> {
    return this.httpClient.delete<TipoDespesa>(`${this.api_ref}/api/tipoDeDespesas/${id}`);
  }

}
