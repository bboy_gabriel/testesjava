import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { ContaRazaoLiberacaoLitigante } from '../../../model/tabelas/conta-razao-liberacao-litigante';

@Injectable({
  providedIn: 'root'
})
export class ContaRazaoLiberacaoLitiganteService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public consultar(): Observable<any> {
    return this.httpClient.get<ContaRazaoLiberacaoLitigante[]>(`${this.api_ref}/api/contaRazaoLiberacaoLitigante`);
  }
  public cadastrar(conta): Observable<any> {
    return this.httpClient.post<ContaRazaoLiberacaoLitigante>(`${this.api_ref}/api/ENDPOINT`, conta);
  }
  public consultarPorId(id: number): Observable<any> {
    return this.httpClient.get<ContaRazaoLiberacaoLitigante>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }
  public alterar(conta): Observable<any> {
    return this.httpClient.put<ContaRazaoLiberacaoLitigante>(`${this.api_ref}/api/ENDPOINT`, conta);
  }
  public remover(id: number): Observable<any> {
    return this.httpClient.delete<ContaRazaoLiberacaoLitigante>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }
}
