import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ContaRazaoEstornoProvisao } from '../../../model/tabelas/razao-estorno-provisao.model';

@Injectable({
  providedIn: 'root'
})
export class ContaRazaoEstornoProvisaoService {

  api_ref: string = environment.API_URL;
  
  constructor(private httpClient: HttpClient) { }

  public consultar(): Observable<any> {
    return this.httpClient.get<ContaRazaoEstornoProvisao[]>(`${this.api_ref}/api/contaRazaoEstornoProvisao`);
  }

  public cadastrar(conta: ContaRazaoEstornoProvisao): Observable<any> {
    return this.httpClient.post<ContaRazaoEstornoProvisao>(`${this.api_ref}/api/contaRazaoEstornoProvisao`, conta);
  }

  public consultarPorId(id: number): Observable<any> {
    return this.httpClient.get<ContaRazaoEstornoProvisao>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }

  public alterar(conta: ContaRazaoEstornoProvisao): Observable<any> {
    return this.httpClient.put<ContaRazaoEstornoProvisao>(`${this.api_ref}/api/ENDPOINT`, conta);
  }

  public remover(id: number): Observable<any> {
    return this.httpClient.delete<ContaRazaoEstornoProvisao>(`${this.api_ref}/api/ENDPOINT/${id}`);
  }
}

