import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Processo } from 'src/app/model/processo/processo.model';
import { LancamentoContabil } from 'src/app/model/provisao/lancamento-contabil.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProvisaoEstornoService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  incluirProvisao(provisao: LancamentoContabil): Observable<any> {
    return this.httpCliente.post(`${this.api_ref}/api/lancamentoProvisao/estorno`, provisao)
  }
}