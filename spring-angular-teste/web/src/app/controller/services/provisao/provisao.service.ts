import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LancamentoContabil } from 'src/app/model/provisao/lancamento-contabil.model';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProvisaoService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  incluirProvisao(provisao: LancamentoContabil): Observable<any> {
    return this.httpClient.post(`${this.api_ref}/api/lancamentoProvisao/provisao`, provisao)
  }

}
