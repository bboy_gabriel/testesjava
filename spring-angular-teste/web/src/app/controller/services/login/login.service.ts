import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { User } from '../../../model/user/user.model';

@Injectable()
export class LoginService {

  user: User
  api_ref: string = environment.API_URL
  logado: boolean = false;

  constructor(private httpClient: HttpClient) { }

  isLogado(): boolean {
    return this.logado;
  }

  login(username: string, password: string): Observable<any> {
    let input = new FormData();

    input.append('username', username);
    input.append('password', password);
    return this.httpClient.post<HttpResponse<any>>(`${this.api_ref}/login`, input, { observe: 'response' });
  }
}
