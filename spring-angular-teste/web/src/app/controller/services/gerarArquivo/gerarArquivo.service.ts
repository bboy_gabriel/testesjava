import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InformacaoContabil } from 'src/app/model/deposito-judicial/lancamentoComtabilVO.model';
import { LancamentoContabilRelatorioVO } from 'src/app/model/provisao/lancamento-contabil-relatorio.model';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
  })
export class GerarArquivoService {

  api_ref: string = environment.API_URL

  constructor(private httpClient: HttpClient) { }

public consultarLancamentosCiclo(dataLancamentoMensal: any): Observable<InformacaoContabil[]> {
    return this.httpClient.get<InformacaoContabil[]>(`${this.api_ref}/api/lancamentoProvisao/ciclo/${dataLancamentoMensal}`);
}

public atualizarDataGeracaoArquivoCSV(lancamentoContabilRelatorioVO: LancamentoContabilRelatorioVO): Observable<any> {
    return this.httpClient.post<InformacaoContabil>(`${this.api_ref}/api/lancamentoProvisao/dataCSV`, (lancamentoContabilRelatorioVO));
}


}
