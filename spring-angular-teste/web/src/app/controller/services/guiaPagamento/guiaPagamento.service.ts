import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { GuiaPagamentoModel } from 'src/app/model/guiaPagamento/guiaPagamento.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GuiaPagamentoService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public consultar(): Observable<any> {
    return this.httpCliente.get<GuiaPagamentoModel[]>(`${this.api_ref}/api/ENDPOINT`);
  }

  public cadastrar(guiaPagamento: GuiaPagamentoModel): Observable<any> {
    return this.httpCliente.post<GuiaPagamentoModel>(`${this.api_ref}/api/ENDPOINT`, (guiaPagamento))
  }

  public consultaPorId(id): Observable<any> {
    return this.httpCliente.get<GuiaPagamentoModel>(`${this.api_ref}/api/ENDPOINT/${id}`)
  }

  public alterar(params): Observable<any> {
    return this.httpCliente.put<GuiaPagamentoModel>(`${this.api_ref}/api/ENDPOINT`, params)
  }

  public remover(id): Observable<any> {
    return this.httpCliente.delete<GuiaPagamentoModel>(`${this.api_ref}/api/ENDPOINT/${id}`)
  }

}