import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ReuModel } from 'src/app/model/guiaPagamento/reu.model';
import { environment } from 'src/environments/environment';
import { AutorModel } from './../../../model/guiaPagamento/autor.model';

@Injectable({
  providedIn: 'root'
})
export class GuiaPagamentoProcessosService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public consultarAutor(): Observable<any> {
    return this.httpCliente.get<AutorModel[]>(`${this.api_ref}/api/ENDPOINT`);
  }

  public consultarReu(): Observable<any> {
    return this.httpCliente.get<ReuModel[]>(`${this.api_ref}/api/ENDPOINT`);
  }

}