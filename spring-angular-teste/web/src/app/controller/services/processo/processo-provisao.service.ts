import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Processo } from 'src/app/model/processo/processo.model';
import { environment } from 'src/environments/environment';
import { ContaRazaoProvisao } from 'src/app/model/tabelas/conta-razao-provisao';

@Injectable({
    providedIn: 'root'
})
export class ProcessoProvisaoService {

    api_ref: string = environment.API_URL;

    constructor(private httpCliente: HttpClient) { }

    public consultarProcesso(numero: Processo): Observable<any> {
        let processo = { "numeroProcesso": numero }
        return this.httpCliente.post<Processo[]>(`${this.api_ref}/api/processos/registroProvisao/valida`, (processo))
    }
  
    public consultarTipoProcesso(codigoTipoProcesso: string): Observable<any> {
        return this.httpCliente.get<ContaRazaoProvisao[]>(`${this.api_ref}/api/contaRazaoProvisao/tipoProcesso/${codigoTipoProcesso}`);
    }

}