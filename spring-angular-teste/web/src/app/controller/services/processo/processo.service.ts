import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { Processo } from '../../../model/processo/processo.model';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class ProcessoService {

  api_ref: string = environment.API_URL;

  constructor(private httpCliente: HttpClient) { }

  public consultaProcesso(numero: Processo): Observable<any> {
    let processo = { "numeroProcesso": numero }
    return this.httpCliente.post<Processo[]>(`${this.api_ref}/api/processos`, (processo))
  }

}
