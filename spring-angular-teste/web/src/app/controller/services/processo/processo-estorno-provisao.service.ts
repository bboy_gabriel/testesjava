import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Processo } from 'src/app/model/processo/processo.model';
import { ContaRazaoProvisao } from 'src/app/model/tabelas/conta-razao-provisao';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProcessoEstornoProvisaoService {

    api_ref: string = environment.API_URL;

    constructor(private httpCliente: HttpClient, private http: Http) { }

    public consultarProcesso(numero: Processo): Observable<any> {
        let processo = { "numeroProcesso": numero }
        return this.httpCliente.post<Processo[]>(`${this.api_ref}/api/processos`, (processo))
    }

    /*     public consultarTipoProcesso(codigoTipoProcesso: string): Observable<any> {
            return this.httpCliente.get<ContaRazaoEstornoProvisao[]>(`${this.api_ref}/api/contaRazaoEstornoProvisao/${codigoTipoProcesso}`);
        } */

    public consultarTipoProcesso(codigoTipoProcesso: string): Observable<any> {
        return this.httpCliente.get<ContaRazaoProvisao[]>(`${this.api_ref}/api/contaRazaoProvisao/tipoProcesso/${codigoTipoProcesso}`);
    }

    public consultarTotalProvisao(numero: Processo): Observable<any> {
        let processo = { "numeroProcesso": numero }
        return this.httpCliente.post<Processo[]>(`${this.api_ref}/api/lancamentoProvisao/totalDisponivel`, (processo))
    }
}