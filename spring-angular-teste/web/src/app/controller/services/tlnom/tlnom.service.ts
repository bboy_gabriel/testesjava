import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TlnomService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public consultaMatricula(matricula): Observable<any> {
    return this.httpClient.get(`${this.api_ref}/api/tlnomEmprg/valida/${matricula}`);
  }
}
