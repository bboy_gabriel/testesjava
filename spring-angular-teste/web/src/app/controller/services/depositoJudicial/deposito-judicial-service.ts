import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { InformacaoContabil } from 'src/app/model/deposito-judicial/lancamentoComtabilVO.model';
import { environment } from 'src/environments/environment';
import { ConsultaDepositoJudicial } from '../../../model/deposito-judicial/consultaDepositoJudicialVO.model';
import { DepositoJudicial } from '../../../model/deposito-judicial/depositoJudicialVO.model';

@Injectable({
  providedIn: 'root'
})
export class DepositoJuducialService {


  api_ref: string = environment.API_URL;
  constructor(private httpClient: HttpClient) { }

  public consultaDepositoJudicial(params: string): Observable<any> {
    return this.httpClient.get<ConsultaDepositoJudicial>(`${this.api_ref}/api/depositosJudiciais/informacao/${params}`);

  }

  public consultaTotalDepositado(params: string): Observable<any> {
    return this.httpClient.get<DepositoJudicial[]>(`${this.api_ref}/api/depositosJudiciais/totalDepositado/${params}`);

  }

  public consultaTotalProvisao(params: string): Observable<any> {
    let processo = { "numeroProcesso": params }
    return this.httpClient.post<InformacaoContabil[]>(`${this.api_ref}/api/lancamentoProvisao/processo/`, (processo));

  }

}