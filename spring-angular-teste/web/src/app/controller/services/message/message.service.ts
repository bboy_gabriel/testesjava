import { Injectable } from '@angular/core';
import swal, { SweetAlertResult } from 'sweetalert2';

@Injectable()

export class MessagesService {

    constructor() { }

    success(message: string) {

        swal({
            type: 'success',
            html:
                `<span style="font-size: 14px; color: #ffffff"><b>${message}</b></span>`,
            width: '100%',
            animation: false,
            toast: true,
            background: '#4ec87f',
            showConfirmButton: true,
            confirmButtonColor: '#009a3f',
            confirmButtonText: 'OK',
            position: 'top-end',
            timer: 3000
        });

    }

    error(message: string, error) {
        let detalhe1: string = (error.objmessage == null || error.objmessage === undefined) ? "" : error.objmessage.message;
        swal({
            type: 'error',
            html: `<span style='color:#DFDFDF;font-weight: 400;'>${message}</span>
             </br>
             <span style='color:#AFAFAF;font-weight: 400;font-size: 12px; align-text:left;'>${detalhe1}</span>`,
            width: '100%',
            animation: false,
            toast: true,
            background: '#6d2e2e',
            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonColor: '#bd2130',
            position: 'top-end',
        });
    }

    errorSession(message: string, error) {
        let detalhe1: string = (error.objmessage == null || error.objmessage === undefined) ? "" : error.objmessage.message;
        swal({
            type: 'error',
            html: `<span style='color:#DFDFDF;font-weight: 400;'>${message}</span>
             </br>
             <span style='color:#AFAFAF;font-weight: 400;font-size: 12px; align-text:left;'>${detalhe1}</span>`,
            width: '100%',
            animation: false,
            toast: true,
            background: '#6d2e2e',
            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonColor: '#bd2130',
            position: 'top-end',
            timer: 5000,
        });
    }

    info(message: any) {

        return swal({
            title: message,
            type: 'info',
            position: 'center',
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonColor: '#0078b0',
        });

    }

    infoFormated(message: any) {

        return swal({
            title: message,
            type: 'success',
            position: 'center',
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonText: 'OK',
            confirmButtonColor: '#0078b0',
        });

    }

    warning(message: any) {

        return swal({
            html:
                `<span style="font-size: 14px; color: #ffffff"><b>${message}</b></span>`,
            title: message,
            type: 'warning',
            position: 'center',
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonText: 'OK',
            confirmButtonColor: '#bd2130',
            timer: 4000,
        });

    }

    warningFormated(message: any) {

        return swal({
            html:
                `<span style="font-size: 25px; color: black"><b>${message}</b></span>`,
            text: message,
            type: 'warning',
            position: 'center',
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonText: 'OK',
            confirmButtonColor: '#0078b0',
        });
    }

    delete(message: any) {
        return swal({
            title: 'Você deseja excluir esse item?',
            text: "Essa função não pode ser desfeita.",
            type: 'warning',
            position: 'center',
            showCancelButton: true,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancelar',
            confirmButtonColor: '#bd2130',
            cancelButtonColor: '#bd2130'
        }).then((result) => {
            if (result.value) {
                swal(
                    'Excluido',
                    'Esse item foi excluído.',
                    'success'
                );
            }
        });
    }

    infoActionButton(message: any) {

        return swal({
            title: message,
            type: 'info',
            position: 'center',
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true,
            confirmButtonColor: '#0078b0',

        }).then((value) => {
            if (value) {
                window.location.reload();
            }
        });

    }

    insertConfirmation(): Promise<SweetAlertResult> {
        return this.confirmation("insert");
    }

    updateConfirmation(): Promise<SweetAlertResult> {
        return this.confirmation("update");
    }

    deleteConfirmation(): Promise<SweetAlertResult> {
        return this.confirmation("delete");
    }

    private confirmation(type): Promise<SweetAlertResult> {

        var message: string = '';
        var cor: string = '';

        switch (type) {
            case 'insert':
                message = 'Deseja realmente salvar o registro?';
                break;

            case 'update':
                message = 'Deseja realmente salvar a alteração?';
                break;

            case 'delete':
                message = 'Deseja realmente excluir?';
                break;
        }

        return swal({
            title: message,
            text: "Esta operação não tem retorno.",
            type: 'warning',
            position: 'center',
            showConfirmButton: true,
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonColor: '#0078b0',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Não',
            confirmButtonText: 'Sim',
        });

    }

/*     loading(message: any) {

        return swal({
            html:
                `<span style="font-size: 14px; color: #ffffff">${message}</span>`,
            title: message,
            imageUrl: './assets/img/spinner.gif',
            imageWidth: 50,
            imageHeight: 50,
            imageAlt: 'Custom image',
            animation: true,
            showConfirmButton: false,
            timer: 3000,
        });

    } */
    
    createLoading(message: any) {

        return swal({
            html:
                `<span style="font-size: 14px; color: #ffffff">${message}</span>`,
            title: message,
            imageUrl: './assets/img/spinner.gif',
            imageWidth: 50,
            imageHeight: 50,
            imageAlt: 'Custom image',
            animation: true,
            showConfirmButton: false,  
            backdrop: false          
        });

    }
    getAlertLoad() {

        const ipAPI = 'https://api.ipify.org?format=json'

        swal.queue([{
            title: 'Your public IP',
            confirmButtonText: 'Show my public IP',
            text:
                'Your public IP will be received ' +
                'via AJAX request',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return fetch(ipAPI)
                    .then(response => response.json())
                    .then(data => swal.insertQueueStep(data.ip))
                    .catch(() => {
                        swal.insertQueueStep({
                            type: 'error',
                            title: 'Unable to get your public IP'
                        })
                    })
            }
        }]);
    }

}
