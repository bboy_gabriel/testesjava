import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
 * Servico utilado para tranferir dados entre componentes
 * Injeta-se no construturo do componentes:
 * constructor(private data: DataService) { }
 * 
 * Como enviar uma nova mensagem:
 * 
 * constructor(private data: DataService) { }
 *  
 * enviarMensagemNova() {
 *      this.data.changeMessage("Hello from Sibling")    
 *  }
 *  Como ler a mensagem no componente de destino
 *  message: any
 *  constructor(private data: DataService) { }
 *  ngOnInit() {
 *   this.data.currentMessage.subscribe(message => this.message = message)
 *  } 
 */
@Injectable()
export class DataTransferService {

  private messageSource = new BehaviorSubject('empty');
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  enviarDados(message: any) {
    this.messageSource.next(message)
  }

}