import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { getTokenHeaders } from 'src/app/app.module';
import { environment } from 'src/environments/environment';
import { ErrorHandler } from '../../../app.error-handler';

@Injectable({
  providedIn: 'root'
})

export class GenericService {

  urlService: string = undefined;
  api_ref: string = environment.API_URL;

  constructor(private http: Http) { }

  public consultar(): Observable<any[]> {
    return this.http.get(`${this.api_ref}/urlService`)
      .map((resposta: any) => resposta.json());
  }

  public consultaPorId(id): Observable<any> {
    return this.http.get(`${this.api_ref}/urlService/${id}`)
      .map((resposta: any) => resposta.json());
  }

  public create(item: any): Observable<any> {
    return this.http.post(`${this.api_ref}/urlService`, (item))
      .map(res => console.log(res))
      .catch(ErrorHandler.handleError);
  }

  public update(params): Observable<any> {
    return this.http.put(`${this.api_ref}/urlService`, params)
      .map((resposta: any) => resposta.json());
  }

  public delete(id): Observable<any> {
    return this.http.delete(`${this.api_ref}/urlService/${id}`)
      .map((resposta: any) => resposta.json());
  }

}
