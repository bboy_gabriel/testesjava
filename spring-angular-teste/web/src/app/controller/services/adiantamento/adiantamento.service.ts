import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AdiantamentoService {

  api_ref: string = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  public solicitarAdiantamento(params: any): Observable<any> {
    return this.httpClient.post(`${this.api_ref}/api/adiantamentos`, params);
  }

  public consultarAdiantamentos(): Observable<any> {
    return this.httpClient.get(`${this.api_ref}/api/adiantamentos`);
  }

  public consultaPorId(id): Observable<any> {
    return this.httpClient.get(`${this.api_ref}/api/adiantamentos/${id}`);
  }

  public consultaPorUsuario(matricula): Observable<any> {
    return this.httpClient.get(`${this.api_ref}/api/adiantamentos/usuarios/${matricula}`);
  }

  public consultaAdiantamento(params): Observable<any> {
    return this.httpClient.put(`${this.api_ref}/api/adiantamentos/confirmaAdiantamentos`, params);
  }

}
