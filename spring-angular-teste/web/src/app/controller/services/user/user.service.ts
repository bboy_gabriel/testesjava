import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';
import { readLocalStorage, tokenGetter } from '../../../app.module';

@Injectable()
export class UserService {
  api_ref: string = environment.API_URL;
  constructor(private httpClient: Http) {
  }

  public setUserData(): any {
    const token = tokenGetter();
    return this.httpClient.post(`${this.api_ref}/usuarios`, token);
  }

  public getUserData(): any {
    const token = tokenGetter();
    const user = readLocalStorage('user_data');
    return JSON.parse(user);
  }

}