import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmarAdiantamentoComponent } from './view/adiantamento/confirmar-adiantamento/confirmar-adiantamento.component';
import { ConsultarAdiantamentoComponent } from './view/adiantamento/consultar-adiantamento/consultar-adiantamento.component';
import { SolicitarAdiantamentoComponent } from './view/adiantamento/solicitar-adiantamento/solicitar-adiantamento.component';
import { ConsultarDepComponent } from './view/depJudicial/consultar-dep-judicial/consultar-dep.component';
import { ConsultarTotalDepositadoComponent } from './view/depJudicial/consultar-total-depositado/consultar-total-depositado.component';
import { LevantamentoDepJudComponent } from './view/depJudicial/levantamento-dep-jud/levantamento-dep-jud.component';
import { AutenticacaoGuard } from './view/login/autenticacaoGuard';
import { LoginComponent } from './view/login/login.component';
import { ConsultarPagamentoComponent } from './view/pagamento/consultar-pagamento/consultar-pagamento.component';
import { ConsultaSolicitacaoPagamentoComponent } from './view/pagamento/consultar-solicitacao/consulta-solicitacao.component';
import { SolicitarPagamentoComponent } from './view/pagamento/solicitar-pagamento/solicitar-pagamento.component';
import { RegistrarProvisaoComponent } from './view/provisao/registrar-provisao/registrar-provisao.component';
import { ProvisaoResolver } from './view/provisao/registrar-provisao/provisao-resolver';
import { ProvisaoEstornoResolver } from './view/provisao/estornar-provisao/provisao-resolver'
import { HomeComponent } from './view/shared/components/home/home.component';
import { MenuComponent } from './view/shared/components/menu/menu.component';
import { ContaRazaoEstornoProvisaoComponent } from './view/tabelas/conta-razao-estorno-provisao/conta-razao-estorno-provisao.component';
import { ContaRazaoLiberacaoLitiganteComponent } from './view/tabelas/conta-razao-liberacao-litigante/conta-razao-liberacao-litigante.component';
import { ContaRazaoProvisaoComponent } from './view/tabelas/conta-razao-provisao/conta-razao-provisao.component';
import { TabelaCentroCustoComponent } from './view/tabelas/tabela-centro-custo/tabela-centro-custo.component';
import { TabelaElementoPepComponent } from './view/tabelas/tabela-elemento-pep/tabela-elemento-pep.component';
import { TabelaFornecedorSapComponent } from './view/tabelas/tabela-fornecedor-sap/tabela-fornecedor-sap.component';
import { TabelaOrdemComponent } from './view/tabelas/tabela-ordem/tabela-ordem.component';
import { TabelaOrgaoGestorComponent } from './view/tabelas/tabela-orgao-gestor/tabela-orgao-gestor.component';
import { TabelaTipoDespesaComponent } from './view/tabelas/tabela-tipo-despesa/tabela-tipo-despesa.component';
import { GerarArquivoCSVComponent } from './view/relatorios/gerar-arquivo-csv/gerar-arquivo-csv.component';
import { DetalharLancamentoContabilComponent } from './view/depJudicial/detalhar-lancamento-contabil/detalhar-lancamento-contabil.component';
import { EstornarProvisaoComponent } from './view/provisao/estornar-provisao/estornar-provisao.component';
import { GuiaPagamento } from './view/guiaPagamento/guiaPagamento.module';
import { IncluirInformacoesResolver } from './view/guiaPagamento/incluir-informacoes/incluir-informacoes-resolver';

/*  Rotas da Aplicação */
const rotas: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'menu', component: MenuComponent, canActivate: [AutenticacaoGuard],
    children: [
      { path: 'home', component: HomeComponent },
      //pagamento
      { path: 'solicitacao-pagamento', component: SolicitarPagamentoComponent },
      { path: 'solicitacao-pagamento/:id', component: SolicitarPagamentoComponent },
      { path: 'acompanhar-solicitacao', component: ConsultarPagamentoComponent },
      { path: 'consulta-solicitacao-pagamento', component: ConsultaSolicitacaoPagamentoComponent },
      //adiantamento
      { path: 'solicitar-adiantamento', component: SolicitarAdiantamentoComponent },
      { path: 'solicitar-adiantamento/:id', component: SolicitarAdiantamentoComponent },
      { path: 'confirmar-adiantamento', component: ConfirmarAdiantamentoComponent },
      { path: 'consultar-adiantamento', component: ConsultarAdiantamentoComponent },
      //provisao
      { path: 'registrar-provisao', component: RegistrarProvisaoComponent, resolve: { message: ProvisaoResolver } },
      { path: 'estornar-provisao', component: EstornarProvisaoComponent, resolve: { message: ProvisaoEstornoResolver } },
      //tabelas
      { path: 'tabela-centro-custo', component: TabelaCentroCustoComponent },
      { path: 'tabela-elemento-pep', component: TabelaElementoPepComponent },
      { path: 'tabela-fornecedor-sap', component: TabelaFornecedorSapComponent },
      { path: 'tabela-ordem', component: TabelaOrdemComponent },
      { path: 'tabela-tipo-despesa', component: TabelaTipoDespesaComponent },
      { path: 'tabela-orgao-gestor', component: TabelaOrgaoGestorComponent },
      { path: 'conta-razao-estorno-provisao', component: ContaRazaoEstornoProvisaoComponent },
      { path: 'conta-razao-liberacao-litigante', component: ContaRazaoLiberacaoLitiganteComponent },
      { path: 'conta-razao-provisao', component: ContaRazaoProvisaoComponent },
      //relatorio
      { path: 'gerar-arquivo-csv', component: GerarArquivoCSVComponent },
      //guia pagamento
      { path: 'incluir-informacoes', loadChildren: () => GuiaPagamento, resolve: { message: IncluirInformacoesResolver }},
      //depositoJudicial
      {
        path: 'consultar-dep', component: LevantamentoDepJudComponent,
        children: [
          { path: '', component: ConsultarDepComponent },
          { path: 'consultar-total-depositado', component: ConsultarTotalDepositadoComponent },
          { path: 'consultar-total-depositado/:id', component: ConsultarTotalDepositadoComponent },
          { path: 'detalhar-lancamento-contabil', component: DetalharLancamentoContabilComponent },
          { path: 'detalhar-lancamento-contabil/:id', component: DetalharLancamentoContabilComponent },
        ]
      },
    ]
  },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(rotas, { useHash: true })
  ],
  exports: [
    RouterModule
  ],
})

export class AppRoutingModule { }
