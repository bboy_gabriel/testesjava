import { HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Headers, HttpModule, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { AuthInterceptor } from './controller/interceptors/auth.interceptor';
import { DataTransferService } from './controller/services/generic/data-transfer.service';
import { MessagesService } from './controller/services/message/message.service';
import { UserService } from './controller/services/user/user.service';
import { AdiantamentoComponent } from './view/adiantamento/adiantamento.module';
import { DepJudicialComponent } from './view/depJudicial/dep.judicial.module';
import { LevantamentoDepJudComponent } from './view/depJudicial/levantamento-dep-jud/levantamento-dep-jud.component';
import { PagamentoComponent } from './view/pagamento/pagamento.module';
import { ProvisaoEstornoResolver } from './view/provisao/estornar-provisao/provisao-resolver';
import { ProvisaoComponent } from './view/provisao/provisao.module';
import { ProvisaoResolver } from './view/provisao/registrar-provisao/provisao-resolver';
import { RelatorioComponent } from './view/relatorios/relatorios.module';
import { ComponentsComponent } from './view/shared/components/components.module';
import { TabelasModule } from './view/tabelas/tabelas.module';
import { IncluirInformacoesResolver } from './view/guiaPagamento/incluir-informacoes/incluir-informacoes-resolver';

export function tokenGetter() {
  return sessionStorage.getItem('access_token');
}

export function tokenSetter(token: any) {
  return sessionStorage.setItem('access_token', token);
}

export function writeLocalStorge(key: string, value: string) {
  return sessionStorage.setItem(key, value);
}

export function readLocalStorage(key: string) {
  return sessionStorage.getItem(key);
}

export function getTokenHeaders(): RequestOptions {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('access_token'));
  const options = new RequestOptions({ headers: headers });
  return options;
}

export function getTokenHeadersFromHttpClient(): any {
  const headers = new Headers();
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('access_token')
    })
  };
  return httpOptions;
}

@NgModule({
  declarations: [
    AppComponent,
    LevantamentoDepJudComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    ComponentsComponent,
    PagamentoComponent,
    DepJudicialComponent,
    AdiantamentoComponent,
    ProvisaoComponent,
    RelatorioComponent,
    HttpModule,
    HttpClientModule,
    TabelasModule,
    NgxSpinnerModule
  ],
  providers: [UserService, MessagesService,
    ProvisaoResolver,
    ProvisaoEstornoResolver,
    IncluirInformacoesResolver,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    DataTransferService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
