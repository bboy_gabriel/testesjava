export class StringHelper {

  static retiraSimbolos(str) {
    return str.replace('-', '').replace('/', '').replace('.', '').replace('.', '').trim();
  }

  static retiraEspacos(str) {
    if (this.isNullOrEmpty(str))
      return "";
    return str.replace(/^\s+|\s+$/g, "").replace(/^\s+/, "").replace(/\s+$/, "").trim();
  }

  static getNumbersCaracters(str: string) {
    let numeros = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let retorno = "";
    for (let i = 0; i < str.length; i++) {
      if (numeros.indexOf(str[i]) >= 0)
        retorno = retorno + str[i];
    }
    return retorno;
  }

  static isNullOrEmpty(str: string) {
    if (str == null)
      return true;
    if (str == undefined)
      return true;
    if (str.length == 0)
      return true;

    return false;
  }

  static isStringFill(str: string) {
    return this.isNullOrEmpty(str) == false;
  }

}