import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { debounceTime, switchMap, map, first } from "rxjs/operators";
import { of } from "rxjs";


@Injectable({ providedIn: 'root' })
export class CnpjValidatorService {

    constructor() { }

    checkCnpjTaken(current: string) {

        return (control: AbstractControl) => {
            if (/^([0-9])\1*$/.test(control.value))
                return of({ cnpjInvalid: true });

            if (current !== control.value) {

                let validator = CnpjValidatorService.isValid(control.value);
                return of(validator);

            }

            return of(false);
        }

    }

    static isValid(cnpj: string) {

        if (cnpj.length < 14) {
            return { minlength: true };
        }

        // A seguir é realizado o cálculo verificador.
        const cnpjArr: string[] = cnpj.split('').reverse().slice(2);

        cnpjArr.unshift(this.buildDigit(cnpjArr).toString());
        cnpjArr.unshift(this.buildDigit(cnpjArr).toString());

        if (cnpj !== cnpjArr.reverse().join('')) {
            return { cnpjInvalid: true };
        }

        return true;
    }

    static buildDigit(arr: string[]): number {

        const digit = arr
            .map((val, idx) => parseInt(val) * ((idx % 8) + 2))
            .reduce((total, current) => total + current) % 11;

        if (digit < 2) {
            return 0;
        }

        return 11 - digit;
    }

}