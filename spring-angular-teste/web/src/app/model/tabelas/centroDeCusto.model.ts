export class CentroDeCusto {
    constructor (
    public id: number,
    public codigo: string,
    public descricao: string,
    ) {}
}