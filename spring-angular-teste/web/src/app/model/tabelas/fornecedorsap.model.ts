export class FornecedorSAP {
    constructor (
    public id: number,
    public codigo: number,
    public descricao: string,
    public cnpj: string ) {}
}
