export class ContaRazaoLiberacaoLitigante {
    constructor (
        public id: number,
        public contaDebitoPrevisao: number,
        public contaCreditoPrevisao: number,
        public codigoTipoProcesso: string,
        public tipoProcessoProvisaoEnum: string,
        ) {}
}
