export class TipoDeDespesa {
    codigo: string
    descricao:  string
    id: number
    indicadorPagamentoEnum: string
}