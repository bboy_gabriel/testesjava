export class TipoDespesa {
    constructor(
        public id: number,
        public codigo: number,
        public descricao: string,
        public indicadorPagamentoEnum: string,
        public indicadorProcesso?: any,
        public indicadorInfoContabeis?: any
    ){}    

}
