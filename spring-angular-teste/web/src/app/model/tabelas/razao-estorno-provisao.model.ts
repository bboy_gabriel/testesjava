export class ContaRazaoEstornoProvisao {
    constructor (
        public id: number,
        public contaCredito: number,
        public contaDebito: number,
        public codigoTipoProcesso: string,
        public tipoProcessoProvisaoEnum: string
        ) {}
}
