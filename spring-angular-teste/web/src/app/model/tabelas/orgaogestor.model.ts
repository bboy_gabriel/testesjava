export class OrgaoGestor {
    constructor(  
        public id: number,
        public codigo: number,
        public descricao: string,
        public situacaoOrgaoGestorEnum: string,
    ){}

}
