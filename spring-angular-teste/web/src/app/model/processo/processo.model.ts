export class Processo {

    constructor (
    public codigoProcesso: number,
	public numeroProcesso: string,
	public tipoProcesso: string,
	public codigoTipoProcesso: number,
	public codigoTipoAcao: number,
	public valorPedido: number,
	public valorEstimado: number,
	public codigoOrgaoJulgador: number,
	public codigoMunicipio: number,
	public descricaoVara: string,
	public descricaoAcao: string,
	public idRisco: string,
	public codigoMatrizAdvogado: string,
	public idPrincipal: string,
	public codigoOrgaoReq: string,
	public idStatus: string,
	public mensagemOpcional: string,
	public siglaEstado: string,
	public codigoEscritorioTerceiro: number,
	public numeroProcessoSijus: number,
	public numeroDigVerfSijus: number,
	public dataAutuacao: string,
	public idRelevante: string,
	public codigoEscritorioChesf: string,
	public descricaoOrgaoJulgAdm: string,
	public dataAnoPrcs: number,
	public idComplexidade: number,
	public descricaoCriterioValorRisco: string,
	public dataCliente: string,
	public idSituacaoCliente: string,
	public idMigracaoSijus: string,
	public idFiscal: string,
	public idSolEncerramento: string,
	public dataAjuizamento: string,
	public valorSentenca: number,
	public valorTotalProvisao: number,
	public nomeApelidoParte: string,
	public idLocad: number,
    public tipEncr: string,
    ) {}

}