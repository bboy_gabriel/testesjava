import { ElementoPEP } from '../tabelas/elementoPEP.model';
import { Ordem } from '../tabelas/ordem.model';
import { OrgaoGestor } from '../tabelas/orgaogestor.model';
import { CentroDeCusto } from './../tabelas/centroDeCusto.model';
import { TipoDeDespesa } from './../tabelas/tipoDeDespesa.model';
import { AutorModel } from './autor.model';
import { ReuModel } from './reu.model';

export class GuiaPagamentoModel {

    constructor(
        public id: number,
        public processo: string,
        public processoNaJustica: string,
        public matriculaSolicitante: string,
        public nomeSolicitante: string,
        public descricaoProcesso: string,
        public varaCoTribunal: string,
        public autor: AutorModel,
        public cpfCnpjAutor: string,
        public reu: ReuModel,
        public cpfCnpjReu: string,
        public dataHoraSolicitacao: string,
        public prazo: string,
        public orgaoGestor: OrgaoGestor,
        public centroDeCusto: CentroDeCusto,
        public elementoPep: ElementoPEP,
        public ordemInterna: Ordem,
        public descricaoDasCustas: string,
        public tipoDeDespesaCustas: TipoDeDespesa,
        public valorDasCustas: number,
        public descricaoDoDepositoJudicial: string,
        public tipoDeDespesaDepositoJudicial: TipoDeDespesa,
        public valorDepositoJudicial: number,
    ) { }
}
