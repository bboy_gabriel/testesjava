
export class AutorModel {

    constructor(
        public id: number,
        public nome: string,
        public cnjCpf: string,
    ) { }
}
