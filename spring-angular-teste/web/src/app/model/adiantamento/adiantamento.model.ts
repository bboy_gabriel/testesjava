export class Adiantamento {

    constructor(
        public id: number,
        public areaLotacao: string,
        public dataCorrente: string,
        public dataSolicitacao: string,
        public dataVencimento: string,
        public indicadorDespesaEnum: string,
        public loginSolicitacao: string,
        public matriculaSap: string,
        public motivoDespesa: string,
        public nomeSAP: string,
        public statusAdiantamento: any,
        public valorAdiantamento: number,
        public numProcesso?: string,
        public docSAP?: string,
        public matricula?: string
    ) { }
}
