export class TipoDespesaVO {

    public descricaoCompleta: string;


    constructor(
        public id: number,
        public codigo: number,
        public descricao: string,
        public indicadorPagamentoEnum: string,
        public indicadorProcesso?: any,
        public indicadorInfoContabeis?: any
    ){
        this.descricaoCompleta = `${this.codigo} - ${this.descricao}`
    }    

}