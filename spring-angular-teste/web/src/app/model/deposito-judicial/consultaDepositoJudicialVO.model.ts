export class ConsultaDepositoJudicial {
    totalDepositado: number
    totalPagoLitigante: number
    saldoDepositado: number
    totalProvisionado: number
    totalPagoChesf: number
}