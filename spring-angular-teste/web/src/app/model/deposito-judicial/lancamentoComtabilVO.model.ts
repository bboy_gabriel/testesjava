export class InformacaoContabil {
    id: number
    tipoLancamento: string
    tipoLancamentoContabilDescricao: string
    numeroProcesso: number
    dataLancamento: Date
    valor: number
    contaCredito: number
    contaDebito: number
    centroDeCusto: any
    elementoPep: any
    ordem: any
    historico: string
    dataGeracaoCSV: string
    constructor(){
    }
}