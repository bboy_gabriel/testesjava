import { DomicilioBancario } from '../pagamento/domicilioBancario.model';

export class DepositoJudicial {
    id: number
    processo: string
    tipoDepositoJudicialEnum : string
    codigoDepositoJudicial: number
    dataDeposito: Date
    domicilioBancario: DomicilioBancario
    transacaoBancaria: string
    valorLitigante: number
    valorChesf: number
    valorCorrecaoChesf: number    
    valorDeposito: number
    usuario: string
    documentoSap: string
    constructor(){
    }
}