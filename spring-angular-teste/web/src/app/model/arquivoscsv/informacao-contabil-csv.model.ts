export class InformacaoContabilCSV {
    contaRazao: number
    montante: number
    texto: string
    centroDeCusto: string
    ordem: number
    ordemVazio: string
    centroDeLucro: string
    elementoPEP: string
    sociedadeParceira: string
    tipoMovimento: string
    constructor(){
        
    }
}