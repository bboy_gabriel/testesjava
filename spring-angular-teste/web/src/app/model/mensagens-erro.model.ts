export class MensagensError {
    constructor(
        public id: number,
        public idSolicitacao: number,
        public mensagens: string,
        public dataInsercao: string,
    ) { }
}