import { MensagensError } from '../mensagens-erro.model';
import { OrgaoGestor } from '../tabelas/orgaogestor.model';
import { TipoDespesaVO } from '../tipoDespesaVO.model';
import { InformacaoContabil } from './informacaoContabil.model';


export class SolicitacaoPagamento {

        constructor(

                public id: number,
                public codigoStatus: string,
                public dataEmissao: string,
                public dataVencimento: string,
                public documentoSap: string,
                public domicilioBancario: any,
                public formaPagamentoEnum: string,                
                public fornecedorSap: any,
                public informacoesContabeis: InformacaoContabil[],
                public listaMensagensErroGeraPagamento: MensagensError,
                public orgaoGestor: OrgaoGestor,
                public processo: string,
                public statusPagamentoEnum: string,
                public tipoDeDespesa: TipoDespesaVO,
                public usuario: string,
                public valor: number,
                public loginSolicitacao: string,
                public tipoDepositoJudicialEnum?: string,
                public codigoBarra?: string,
                public dataInsercao?: string,
                public codigoDepositoJudicial?: number,
                public codigTipoPagamento?: number,
                public formaPagamentoEnumDesc?: string,
                public arquivoPagamento?: any           
                ) { }
}
