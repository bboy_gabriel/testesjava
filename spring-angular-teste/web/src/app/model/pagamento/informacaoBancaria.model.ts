export class InformacaoBancaria {

    constructor ( 
        public id: number,
        public bancoAgencia: string,
        public bancoConta: string,
        public bancoNome: string,
        public bancoDomicilio: string,
    ){ }
}
