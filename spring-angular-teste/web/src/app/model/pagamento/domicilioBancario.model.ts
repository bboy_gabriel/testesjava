export class DomicilioBancario{

    id: number
    bancoAgencia: string
    bancoConta: string
    bancoNome: string
    bancoDomicilio: number
}