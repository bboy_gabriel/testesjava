import { InformacaoContabil } from './informacao-contabil.model';
import { ContaRazaoProvisao } from '../tabelas/conta-razao-provisao';

export class Provisao {
    constructor (
        public id: number,
        public processo: number,
        public tipoProcesso: string,
        public dataLancamento: string,
        public valor: number,
        public contaRazaoCreditoDebito: ContaRazaoProvisao,
        public historico: string,
        public informacoesContabeis: InformacaoContabil[] = []
    ){}
}
