export class ContaRazao {

    public contasCreditoDebito: string;

    constructor (
        public id: number,
        public contaCredito: number,
        public contaDebito: number,
        public codigoTipoProcesso: string,
        public tipoProcessoProvisaoEnum: string,
        ) {
            this.contasCreditoDebito = `Crédito: ${this.contaCredito} - Débito: ${this.contaDebito}`
        }
}
