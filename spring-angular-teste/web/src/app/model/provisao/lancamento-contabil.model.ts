import { ElementoPEP } from '../tabelas/elementoPEP.model';
import { CentroDeCusto } from '../tabelas/centroDeCusto.model';
import { Ordem } from '../tabelas/ordem.model';

export class LancamentoContabil {
    
        public id: number;
        public numeroProcesso: number;
        public dataLancamento: string;
        public valor: number;
        public contaDebito: number;
        public contaCredito: number;
        public historico: string;
        public centroDeCusto: CentroDeCusto;
        public elementoPep?: ElementoPEP;
        public ordem?: Ordem;
        constructor(){}        
    
}
