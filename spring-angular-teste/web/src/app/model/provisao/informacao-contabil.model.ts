import { CentroDeCusto } from '../tabelas/centroDeCusto.model';
import { ElementoPEP } from '../tabelas/elementoPEP.model';
import { Ordem } from '../tabelas/ordem.model';

export class InformacaoContabil {


    public centroDescricao: string;
    public pepDescricao: string;
    public ordemDescricao: string;
    public removido: boolean = false;

    constructor(
        public id: number,
        public centroDeCusto: CentroDeCusto,
        public pep: ElementoPEP,
        public ordem: Ordem,
    ) {
        this.setDescricoes();
    }

    setDescricoes() {
        this.centroDescricao = this.centroDeCusto ? this.centroDeCusto.descricao : '-';
        this.pepDescricao = this.pep ? this.pep.descricao : '-';
        this.ordemDescricao = this.ordem ? this.ordem.descricao : '-';
    }
}
