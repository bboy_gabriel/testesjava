import { CentroDeCusto } from '../../../model/tabelas/centroDeCusto.model';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/controller/services/user/user.service';
import { TipoDespesa } from '../../../model/tabelas/tipodespesa.model';
import { MessagesService } from '../../../controller/services/message/message.service';
import { PagamentoService } from '../../../controller/services/pagamento/pagamento.service';
import { ProcessoService } from '../../../controller/services/processo/processo.service';
import { CentroDeCustoService } from '../../../controller/services/tabelas/centro-de-custo.service';
import { ElementoPepService } from '../../../controller/services/tabelas/elemento-pep.service';
import { FornecedorSapService } from '../../../controller/services/tabelas/fornecedor-sap.service';
import { OrdemService } from '../../../controller/services/tabelas/ordem.service';
import { OrgaoGestorService } from '../../../controller/services/tabelas/orgao-gestor.service';
import { TipoDespesaService } from '../../../controller/services/tabelas/tipo-despesa.service';
import { ElementoPEP } from '../../../model/tabelas/elementoPEP.model';
import { FornecedorSAP } from '../../../model/tabelas/fornecedorsap.model';
import { Ordem } from '../../../model/tabelas/ordem.model';
import { OrgaoGestor } from '../../../model/tabelas/orgaogestor.model';
import { Processo } from '../../../model/processo/processo.model';
import { SolicitacaoPagamento } from '../../../model/pagamento/solicitacao-pagamento.model';
import { TipoDespesaVO } from '../../../model/tipoDespesaVO.model';
import { Observable, of } from 'rxjs';
import { InformacaoContabil } from 'src/app/model/pagamento/informacaoContabil.model';

@Component({
  selector: 'app-solicitar-pagamento',
  templateUrl: './solicitar-pagamento.component.html',
  styleUrls: ['./solicitar-pagamento.component.css'],
  providers: [PagamentoService, ProcessoService]
})
export class SolicitarPagamentoComponent implements OnInit {

  public buscarSolicitacoes: Array<SolicitacaoPagamento>;

  fornecedoressap: FornecedorSAP[];
  tipoDespesas: TipoDespesa[];
  listaDespesasVO: TipoDespesaVO[] = [];
  orgaogestores: OrgaoGestor[];
  centrosDeCustos: CentroDeCusto[];
  ordens: Ordem[];
  elementosPEP: ElementoPEP[];

  infoContabil: {};
  dataAtual: String;
  dataFutura: String;
  dataVencimentoLimite: String;
  solicitacaoForm: FormGroup;
  solicitacao: SolicitacaoPagamento = {} as SolicitacaoPagamento;
  processoAtual: Processo;

  dataSource: any;
  dataSourceBancario: any;
  informacaoBancariaLista: any;
  centro: CentroDeCusto;
  elemento: ElementoPEP;
  ordem: Ordem;
  valorContabil = 0;
  teste: any;
  valorTotalSolicitado: number;
  infoContabilColumnDisplays = ['centroDescricao', 'pepDescricao', 'ordemDescricao', 'valor', 'excluir'];
  infoContabilDataSource = [];
  bancoDisplayedColumns = ['radioButton', 'bancoAg', 'conta', 'nome'];
  bancoDataSource: any;

  arquivoPagamento: any = { nome: '', tipo: '', file: '', valor: null };
  elementoBancario: any;
  dataBancario: any;

  statusPagamentoAtual: String;
  user: any;

  idRecebido: any;

  habilitaBotao: Boolean = false;

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  bancoNulo = {
    "id": null,
    "bancoAgencia": null,
    "bancoConta": null,
    "bancoNome": null,
    "bancoDomicilio": "0001"
  }

  informacaoBancariaEmpyt = {
    "id": null,
    "bancoAgencia": "",
    "bancoConta": "",
    "bancoNome": "",
    "bancoDomicilio": ""
  }

  tipoDepositos = [
    { "id": "2", "descricao": "PRINCIPAL" },
    { "id": "1", "descricao": "RECURSAL" },
  ];

  tipoPagamento: any;

  pagamento = [
    { "tipo": "C", "descricaoPagamento": "Cheque" },
    { "tipo": "B", "descricaoPagamento": "Código de Barras" },
    { "tipo": "I", "descricaoPagamento": "Conciliação Automática" },
    { "tipo": "R", "descricaoPagamento": "Crédito em Conta" },
    { "tipo": "M", "descricaoPagamento": "Monitoração Contas a Pagar" }
  ];

  @Input('titulo') titulo: string = 'Solicitação de Pagamento';
  @Input('disabledFrom') disabledFrom: boolean = false;
  @Input('formData') formData: SolicitacaoPagamento;
  @Input('voltarFunction') voltarFunction: Function;
  imteDetalhe: SolicitacaoPagamento = null;
  habilitarInfoContabeis: boolean = false;
  isProcessoObrigatorio: boolean = false;
  processoCadastrado: boolean = false;
  observerYear: Observable<any> = of(new Date().getFullYear());

  @ViewChild(MatSort) sort: MatSort;
  constructor(private router: Router,
    private editedRouter: ActivatedRoute,
    private fb: FormBuilder,
    private pagamentoService: PagamentoService,
    private processoService: ProcessoService,
    private message: MessagesService,
    private centroService: CentroDeCustoService,
    private elementoService: ElementoPepService,
    private fornecedorService: FornecedorSapService,
    private ordemService: OrdemService,
    private orgaoGestorService: OrgaoGestorService,
    private tipoDespesaService: TipoDespesaService,
    private userService: UserService) {

    //formulario
    this.solicitacaoForm = this.fb.group({
      id: this.fb.control(''),
      usuario: this.fb.control(null),
      valor: this.fb.control('', [this.minLength(2), this.maxLength(15), this.required]),
      tipoDeDespesa: this.fb.group({
        id: this.fb.control('', [this.required]),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
        indicadorPagamentoEnum: this.fb.control(''),
        descricaoCompleta: this.fb.control(''),
        indicadorProcesso: this.fb.control(''),
        indicadorInfoContabeis: this.fb.control(''),
      }, [this.required]),
      tipoDepositoJudicialEnum: this.fb.control(null),
      tipoPagamentoEnum: this.fb.control(null),
      fornecedorSap: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
        cnpj: this.fb.control(''),
      }, [this.required]),
      dataEmissao: this.fb.control('', [this.required]),
      dataVencimento: this.fb.control('', [this.required]),
      orgaoGestor: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
        situacaoOrgaoGestorEnum: this.fb.control(''),
      }),
      processo: this.fb.control(''),
      informacoesContabeis: this.fb.array([]),
      formaPagamentoEnum: this.fb.control('', [this.required]),
      domicilioBancario: this.fb.group({
        id: this.fb.control(''),
        bancoAgencia: this.fb.control(''),
        bancoConta: this.fb.control(''),
        bancoNome: this.fb.control(''),
        bancoDomicilio: this.fb.control('', [this.required]),
      }),
      codigoBarra: this.fb.control(null, [this.minLength(48), this.maxLength(48)]),
    });

  }

  ngOnInit() {
    this.editedRouter.params.subscribe(res => {
      this.idRecebido = res.id;
    });
    this.loadSelects();
    this.dataAtual = this.dataAtualFormatada();
    this.dataFutura = this.dataFuturaFormatadoa();
    this.elementoBancario = this.informacaoBancariaEmpyt;
    this.user = JSON.parse(this.userService.getUserData());
    this.solicitacaoForm.controls.usuario.setValue(this.user.matricula);
    this.montarForm();
    if (this.disabledFrom)
      this.solicitacaoForm.disable({ onlySelf: this.disabledFrom });
    this.preencherFormDetalhe();
  }

  dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  //valida a data maxima
  dataFuturaFormatadoa() {
    return '2100' + '-' + '12' + "-" + '31';
  }

  getDataEmissao(data) {
    if (data > this.dataAtualFormatada()) {
      this.solicitacaoForm.controls.dataEmissao.reset();
      this.message.error("Data de emissão não pode ser maior que hoje.", {});
    }
  }

  getDataVencimento(data) {
    if (data < this.dataAtualFormatada()) {
      this.solicitacaoForm.controls.dataVencimento.reset();
      this.message.error("Data de vencimento não pode ser menor que hoje.", {});
    }
    if (data > this.dataFuturaFormatadoa()) {
      this.solicitacaoForm.controls.dataVencimento.reset();
      this.message.error("Forneça uma data válida.", {});
    }
  }

  voltar() {
    if (this.voltarFunction) {
      this.voltarFunction.call(this, 'voltar')
    } else {
      this.router.navigate(['menu']);
    }
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let arquivo = event.target.files[0];
      reader.readAsDataURL(arquivo);
      reader.onload = () => {
        this.arquivoPagamento = {
          nome: arquivo.name,
          tipo: arquivo.type,
          file: reader.result.toString().replace("data:image/png;base64,", "")
        };
      };
    }
  }

  clearFile() {
    this.arquivoPagamento = {
      nome: '',
      tipo: '',
      file: ''
    };
  }

  incluirInformacaoContabil() {
    this.validacaoValorSolicitado();
  }

  onClickRemove(elemento: InformacaoContabil) {
    let posicao = this.solicitacao.informacoesContabeis.indexOf(elemento);
    let filterDatasource: InformacaoContabil[] = [];
    this.removeNumbers(posicao);
    this.solicitacao.informacoesContabeis.splice(posicao, 1);
    filterDatasource = this.solicitacao.informacoesContabeis
    this.dataSource = new MatTableDataSource(filterDatasource);
  }

  removeNumbers(i: number) {
    const control = <FormArray>this.solicitacaoForm.controls['informacoesContabeis'];
    control.value.splice(i, 1);
  }

  onSelectionChange(data) {
    this.elementoBancario = data;
    this.solicitacaoForm.controls.domicilioBancario.setValue(this.elementoBancario);
  }

  incluirInformacaoBancaria() {
    if (this.dataBancario != null) {
      this.elementoBancario = this.dataBancario;
    }
  }

  loadSelects() {
    this.fornecedorService.consultaFornecedoresSAP()
      .subscribe((fornecedoressap: FornecedorSAP[]) => {
        this.fornecedoressap = fornecedoressap;
      }, error => console.log(error));

    this.tipoDespesaService.consultaTipoDespesa()
      .subscribe((despesas: TipoDespesa[]) => {
        this.tipoDespesas = despesas;
        this.montarListaDespesaVO();
      }, error => console.log(error));

    this.orgaoGestorService.ConsultaOrgaoGestor()
      .subscribe((orgaogestores: OrgaoGestor[]) => {
        this.orgaogestores = orgaogestores;
      }, error => console.log(error));

    this.centroService.consultar()
      .subscribe((centroCusto: CentroDeCusto[]) => {
        this.centrosDeCustos = centroCusto;
      }, error => console.log(error));

    this.elementoService.consultaElementosPEP()
      .subscribe((elementos: ElementoPEP[]) => {
        this.elementosPEP = elementos;
      }, error => console.log(error));

    this.ordemService.ConsultaOrdem()
      .subscribe((ordem: Ordem[]) => {
        this.ordens = ordem;
      }, error => console.log(error));
  }

  enviarSolicitacao() {
    let listaInfoContabeis = this.solicitacao.informacoesContabeis;
    this.solicitacao = this.solicitacaoForm.getRawValue();
    this.incluiCodigoDeposito();
    if (this.solicitacao) {
      if (listaInfoContabeis && this.habilitarInfoContabeis) {
        this.solicitacao.informacoesContabeis = listaInfoContabeis;
      } else {
        this.solicitacao.informacoesContabeis = [];
      }
      this.solicitacao.arquivoPagamento = this.arquivoPagamento;
      delete this.solicitacao.tipoDeDespesa.descricaoCompleta;
      this.pagamentoService.solicitarPagamento(this.solicitacao)
        .subscribe(response => {
          if (response.documentoSap !== null && response.documentoSap !== undefined) {
            this.message.infoFormated("Sua solicitação foi enviada com sucesso! N° do Doc: " + response.documentoSap + ".");
            this.router.navigate(['menu']);
          } else {
            let ultimaMensagem = response.listaMensagensErroGeraPagamento.length - 1;
            this.message.warningFormated("Pendência da solicitação: " + response.listaMensagensErroGeraPagamento[ultimaMensagem].mensagens + ".");
            this.router.navigate(['menu']);
          }
        },
          error => {
            this.message.error("Sua solicitação não foi enviada! N° do Doc: ", null);
          });
    }

  }

  reenviarSolicitacao() {
    this.montarSolicitacaoParaReenviar(this.solicitacao)
    if (this.solicitacao) {
      delete this.solicitacao.tipoDeDespesa.descricaoCompleta;

      if (!this.habilitarInfoContabeis && this.solicitacao.informacoesContabeis && this.solicitacao.informacoesContabeis[0]) {
        this.solicitacao.informacoesContabeis = [];
      }

      this.pagamentoService.reenviarSolicitacao(this.solicitacao)
        .subscribe(response => {
          if (response.documentoSap !== null && response.documentoSap !== undefined) {
            this.message.infoFormated("Sua solicitação foi atualizada com sucesso! N° do Doc: " + response.documentoSap + ".");
            this.router.navigate(['menu']);
          } else {
            let ultimaMensagem = response.listaMensagensErroGeraPagamento.length - 1;

            this.message.warningFormated("Pendência da solicitação: " + response.listaMensagensErroGeraPagamento[ultimaMensagem].mensagens + ".");
            this.router.navigate(['menu/acompanhar-solicitacao']);
          }
        },
          error => {
            this.message.error("Sua solicitação não foi enviada! N° do Doc: ", null);
          });
    }

  }

  consultaProcesso(nProcesso: any): Processo {

    if (nProcesso) {
      this.processoService.consultaProcesso(nProcesso).subscribe(
        response => {
          this.processoAtual = response;
          this.validaValorSolicitacao(this.processoAtual)
          this.solicitacaoForm.controls.processo.setValue(this.processoAtual.numeroProcessoSijus.toString() +
            this.processoAtual.numeroDigVerfSijus.toString());
        },
        error => {
          this.message.error("Processo não encontrado no PROJUR ", error)
          this.solicitacaoForm.controls.processo.setValue('');
        });
    } return this.processoAtual;
  }

  recuperaProcesso(): any {
    let valor: string = this.solicitacaoForm.value.processo;
    if (valor) {
      let processo: Processo = this.consultaProcesso(valor)
    }
    this.isIndicadorProcessoObrigatorio();
  }

  validaValorSolicitacao(processo: Processo): any {

    if (processo.idStatus != 'A') {
      this.message.info("Processo encerrado ou não disponivel!");
    }
    if (this.getValorSolicitacao() > processo.valorPedido) {
      this.message.warning("Valor solicitado não pode ultrapassar o valor do processo." + this.getFormattedPrice(processo.valorPedido));
      this.solicitacaoForm.controls.valor.setValue("0");
    }

  }

  getFormattedPrice(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

  getValorSolicitacao(): number {
    if (this.solicitacaoForm.controls.valor.value) {
      return this.solicitacaoForm.controls.valor.value;
    }
    return 0;
  }

  isBoleto(): boolean {
    return this.solicitacaoForm.value.formaPagamentoEnum == 'B';
  }

  changeFornecedorSap() {
    this.elementoBancario = this.bancoNulo;
    this.changePagamento();
  }

  isEditar(): boolean {
    return this.idRecebido;
  }

  changePagamento() {
    let data = this.getFormaPagamento();
    if (data == 'R' && this.getCodigoFornecedor()) {
      this.solicitacaoForm.controls.codigoBarra.disable();
      this.solicitacaoForm.controls.domicilioBancario.setValue(this.informacaoBancariaEmpyt);
      this.pagamentoService.obterDomicilioBancario(this.getCodigoFornecedor()).subscribe(response => {
        this.informacaoBancariaLista = response;
        this.dataSourceBancario = new MatTableDataSource(this.informacaoBancariaLista);
      });

    } else {
      this.elementoBancario = this.bancoNulo;
      if (data == 'B') {

        this.solicitacaoForm.controls.domicilioBancario.setValue(this.bancoNulo);
        this.solicitacaoForm.controls.codigoBarra.enable();
        this.solicitacaoForm.controls.codigoBarra.reset();

      } else {
        this.solicitacaoForm.controls.codigoBarra.reset();
        this.solicitacaoForm.controls.codigoBarra.disable();
        this.solicitacaoForm.controls.domicilioBancario.setValue(this.bancoNulo);
      }
    }
  }

  isCreditoEmConta(): boolean {
    return 'R' == this.solicitacaoForm.value.formaPagamentoEnum;
  }

  indicadorGarantia(): boolean {
    if (this.solicitacaoForm.value.tipoDeDespesa && this.solicitacaoForm.value.tipoDeDespesa.indicadorPagamentoEnum == 'G'
      || (this.formData && this.formData.tipoDeDespesa.indicadorPagamentoEnum === 'G')) {
      this.solicitacaoForm.controls.tipoDepositoJudicialEnum.enable();
      return true;
    } else {
      this.solicitacaoForm.controls.tipoDepositoJudicialEnum.disable();
      return false;
    }
  }

  tipoDePagamento(): boolean {
    if ( this.solicitacaoForm.value.tipoDeDespesa && this.solicitacaoForm.value.tipoDeDespesa.indicadorPagamentoEnum === 'P'
    || (this.formData && this.formData.tipoDeDespesa.indicadorPagamentoEnum === 'P')) {
      this.solicitacaoForm.controls.tipoPagamentoEnum.enable();
      return true;
    } else {
      this.solicitacaoForm.controls.tipoPagamentoEnum.disable();
      return false;
    }
  }

  validaStatusProcesso(): boolean {
    if (this.processoAtual) {
      return (this.processoAtual.idStatus == 'A' && (this.processoAtual.valorPedido > this.valorSolicitado()));
    }
  }

  valorSolicitado(): number {

    if (this.solicitacaoForm.value.valor) {
      return this.solicitacaoForm.value.valor;
    } return 0;
  }

  validacaoValorSolicitado(): any {
    let valorInput = this.valorContabil + this.getCustoTotal()

    let verificaZero = Number(valorInput)
    if (verificaZero <= 0.00) {
      this.message.warning("Valor total não pode ser menor ou igual a zero!");
    }

    //valor do formulário < ao valor da soma dos itens da tabela
    if (this.solicitacaoForm.value.valor < this.getCustoTotal()
      || (this.solicitacaoForm.value.valor < valorInput)) {
      this.message.warning("Valor total não pode ultrapassar o valor solicitado!");
    }

    //valor do formulário > ou = ao valor da soma dos itens da tabela
    else if ((this.solicitacaoForm.value.valor >= this.getCustoTotal()
      || (this.solicitacaoForm.value.valor >= valorInput))) {
      this.adicionarLista();
    }
    this.limparInfoContabil();
  }

  adicionarLista() {
    let valor: number = 0;
    let novoInfoContabil = new InformacaoContabil(null,
      this.centro,
      this.elemento,
      this.valorContabil,
      this.ordem);
    this.solicitacao.informacoesContabeis = this.solicitacao.informacoesContabeis ? this.solicitacao.informacoesContabeis : [];
    this.solicitacao.informacoesContabeis.push(novoInfoContabil);
    this.dataSource = new MatTableDataSource(this.solicitacao.informacoesContabeis);
    this.message.success("Informação contábil adicionada.");
  }

  limparInfoContabil() {
    this.valorContabil = 0;
    this.centro = undefined
    this.elemento = undefined;
    this.ordem = undefined;
  }

  getCustoTotal(): number {
    // contabiliza todas as inserções de valores, e gera um total
    if (this.solicitacao.informacoesContabeis) {
      this.valorTotalSolicitado = this.solicitacao.informacoesContabeis.map(t => t.valor).reduce((acc, value) => acc + value, 0);
      return this.valorTotalSolicitado;
    } else {
      this.valorTotalSolicitado = 0;
      return this.valorTotalSolicitado;
    }
  }

  getCodigoFornecedor() {
    let fornecedor = this.solicitacaoForm.controls.fornecedorSap.value;
    if (fornecedor.codigo == "" || fornecedor.codigo == null) {
      this.message.warning("Necessário selecionar um fornecedor SAP!");
    } else {
      return fornecedor.codigo;
    }
  }

  getFormaPagamento() {
    return this.solicitacaoForm.value.formaPagamentoEnum;
  }

  possuiValor(): boolean {
    if ((this.centro || this.ordem || this.elemento) && this.valorContabil) {
      return true;
    }
    return false;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  montarListaDespesaVO() {
    if (this.tipoDespesas) {
      let lista: any[] = [];
      this.tipoDespesas.forEach(value => {
        lista.push(new TipoDespesaVO(value.id, value.codigo, value.descricao, value.indicadorPagamentoEnum, value.indicadorProcesso, value.indicadorInfoContabeis));
      });
      this.listaDespesasVO = lista
    }
  }

  obrigatoriedadeProcesso() {
    this.solicitacaoForm.controls.tipoDepositoJudicialEnum.reset();
    this.isIndicadorProcessoObrigatorio()
    this.gerarTipoDePagamentoLista();
  }

  gerarTipoDePagamentoLista(){
    if(this.solicitacaoForm.controls.tipoDeDespesa.value.codigo !== 'OU12'){
      this.tipoPagamento = [
        { "id": "1", "descricao": "Processual" },
        { "id": "2", "descricao": "Administrativo" },
      ];
    
     }else{
       this.tipoPagamento = [
        { "id": "1", "descricao": "Servidão Administrativa" },
        { "id": "2", "descricao": "Desapropriação Amigável" },
        { "id": "3", "descricao": "Cessão de Posse" },
        { "id": "4", "descricao": "Somente Benfeitorias" },
        { "id": "5", "descricao": "Despesas com Cartório" },
      ];
     }
    }

  isIndicadorInfoContabeisObrigatorio(): boolean {
    return this.habilitarInfoContabeis = this.solicitacaoForm.controls.tipoDeDespesa.value.indicadorInfoContabeis === 'S';
  }

  indicadorPreenchimentoContabil(): boolean {
    if (this.solicitacao.informacoesContabeis) {
      return this.solicitacao.informacoesContabeis.length > 0;
    }
  }

  habilitaEnvio(): boolean {
    if (this.isIndicadorInfoContabeisObrigatorio()) {
      return this.indicadorPreenchimentoContabil();
    } return true;
  }

  isIndicadorProcessoObrigatorio() {
    if (this.solicitacaoForm.controls.tipoDeDespesa.value.indicadorProcesso === 'S') {
      this.isProcessoObrigatorio = true;
      this.solicitacaoForm.controls.processo.setValidators(this.required);
      this.solicitacaoForm.updateValueAndValidity();
      if (this.solicitacaoForm.value.processo == "") {
        this.processoCadastrado = false;
      } else {
        this.processoCadastrado = true;
      }
    } if (this.solicitacaoForm.controls.tipoDeDespesa.value.indicadorProcesso === 'N') {
      this.solicitacaoForm.controls.processo.clearValidators();
      this.solicitacaoForm.updateValueAndValidity();
      this.processoCadastrado = true;
      this.isProcessoObrigatorio = false;
    }
    return this.isProcessoObrigatorio;
  }

  montarDespesaVO(despesa): TipoDespesaVO {
    return new TipoDespesaVO(despesa.id, despesa.codigo, despesa.descricao, despesa.indicadorPagamentoEnum, despesa.indicadorProcesso, despesa.indicadorInfoContabeis)
  }

  adicionarmontarFormEdicao(response: SolicitacaoPagamento) {
    this.solicitacao = response;
    this.solicitacao.informacoesContabeis = response.informacoesContabeis.map((item: InformacaoContabil) => {
      return new InformacaoContabil(item.id,
        item.centroDeCusto,
        item.pep,
        item.valor,
        item.ordem);
    })
    this.dataSource = new MatTableDataSource(this.solicitacao.informacoesContabeis);
  }

  montarForm() {
    if (this.idRecebido) {
      this.pagamentoService.consultaSolicitacaoPorID(this.idRecebido).subscribe(response => {
        this.adicionarmontarFormEdicao(response);
        response.tipoDeDespesa = this.montarDespesaVO(response.tipoDeDespesa)
        this.habilitarInfoContabeis = response.tipoDeDespesa && response.tipoDeDespesa.indicadorInfoContabeis === 'S';
        this.obrigatoriedadeProcesso();
        this.isProcessoObrigatorio = response.tipoDeDespesa && response.tipoDeDespesa.indicadorProcesso === 'S';
        this.solicitacao = response;
        
        this.solicitacaoForm.patchValue(response);
        
        this.isIndicadorProcessoObrigatorio();
        if (response.domiciolioBancario) {
          this.changePagamento();
          this.solicitacaoForm.controls.domicilioBancario.setValue(response.domiciolioBancario);
        }
        else {
          this.changePagamento();
          this.solicitacaoForm.controls.domicilioBancario.setValue(this.bancoNulo)
        }

      })
    }
  }

  montarSolicitacaoParaReenviar(solicitacao: SolicitacaoPagamento): SolicitacaoPagamento {
    solicitacao.dataEmissao = this.solicitacaoForm.controls.dataEmissao.value;
    solicitacao.dataVencimento = this.solicitacaoForm.controls.dataVencimento.value;
    solicitacao.domicilioBancario = this.solicitacaoForm.controls.domicilioBancario.value;
    solicitacao.formaPagamentoEnum = this.solicitacaoForm.controls.formaPagamentoEnum.value;
    solicitacao.fornecedorSap = this.solicitacaoForm.controls.fornecedorSap.value;
    solicitacao.orgaoGestor = this.solicitacaoForm.controls.orgaoGestor.value;
    if (this.solicitacaoForm.controls.processo) {
      solicitacao.processo = this.solicitacaoForm.controls.processo.value;
    }
    solicitacao.tipoDeDespesa = this.solicitacaoForm.controls.tipoDeDespesa.value;
    solicitacao.usuario = this.solicitacaoForm.controls.usuario.value;
    solicitacao.valor = this.solicitacaoForm.controls.valor.value;
    solicitacao.tipoDepositoJudicialEnum = this.solicitacaoForm.controls.tipoDepositoJudicialEnum.value;
    if (this.solicitacaoForm.controls.codigoBarra) {
      solicitacao.codigoBarra = this.solicitacaoForm.controls.codigoBarra.value;
    }
    return solicitacao;
  }

  incluiCodigoDeposito(): void {
    this.tipoDepositos.forEach(item => {
      if (item.descricao === this.solicitacaoForm.controls.tipoDepositoJudicialEnum.value) {
        this.solicitacao.codigoDepositoJudicial = (Number(item.id));
      }
    })
  }

  incluiCodigoTipoPagamento(): void {
    this.tipoPagamento.forEach(item => {
      if (item.descricao === this.solicitacaoForm.controls.tipoPagamentoEnum.value) {
        this.solicitacao.codigTipoPagamento = (Number(item.id));
      }
    })
  }

  preencherFormDetalhe() {
    if (this.disabledFrom) {
      let listaCentroCustos = this.formData.informacoesContabeis.map((ele: any) => {
        return new InformacaoContabil(
          ele.id,
          ele.centroDeCusto,
          ele.pep, ele.valor, ele.ordem);
      });

      this.formData.tipoDeDespesa = new TipoDespesaVO(this.formData.id, this.formData.tipoDeDespesa.codigo, this.formData.tipoDeDespesa.descricao,
        this.formData.tipoDeDespesa.indicadorPagamentoEnum, this.formData.tipoDeDespesa.indicadorProcesso, this.formData.tipoDeDespesa.indicadorInfoContabeis);;
      this.dataSource = new MatTableDataSource(listaCentroCustos);
    }
  }

  public setFormData(item: SolicitacaoPagamento): void {
    this.formData = item;
    this.preencherFormDetalhe();
  }

  isTed() {
    return this.solicitacaoForm.controls.tipoDeDespesa.value.codigo == 'JU01';
  }

  getDescricaoDepositoJudicial(codigo: string) {
    const tipoDeposito = this.tipoDepositos.filter(dep => dep.id == codigo)[0]
    if (!tipoDeposito) return '';
    return tipoDeposito.descricao;
  }

  getDescricaoTipoPagamento(codigo:string){
    this.gerarTipoDePagamentoLista();
    const tipoPagamento = this.tipoPagamento.filter(dep=>dep.id == codigo)[0]
    if(!tipoPagamento) return '';
    return tipoPagamento.descricao;
  }

}