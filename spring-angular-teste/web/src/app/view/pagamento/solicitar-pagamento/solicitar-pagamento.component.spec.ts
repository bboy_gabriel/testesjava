import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarPagamentoComponent } from './solicitar-pagamento.component';

describe('SolicitarPagamentoComponent', () => {
  let component: SolicitarPagamentoComponent;
  let fixture: ComponentFixture<SolicitarPagamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarPagamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarPagamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
