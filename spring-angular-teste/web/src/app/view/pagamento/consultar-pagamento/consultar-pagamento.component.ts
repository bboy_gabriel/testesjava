import { HttpErrorResponse } from '@angular/common/http';
import { error } from '@angular/compiler/src/util';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { UserService } from 'src/app/controller/services/user/user.service';
import { fromMatPaginator, paginateRows } from 'src/app/core/utils/data.paginator';
import { MensagensError } from 'src/app/model/mensagens-erro.model';
import { SolicitacaoPagamento } from '../../../model/pagamento/solicitacao-pagamento.model';
import { TipoDespesaVO } from 'src/app/model/tipoDespesaVO.model';
import { PagamentoService } from '../../../controller/services/pagamento/pagamento.service';
import { OrgaoGestorService } from '../../../controller/services/tabelas/orgao-gestor.service';
import { OrgaoGestor } from '../../../model/tabelas/orgaogestor.model';
import { TipoDespesaService } from './../../../controller/services/tabelas/tipo-despesa.service';
import { TipoDespesa } from '../../../model/tabelas/tipodespesa.model';

@Component({
  selector: 'consultar-pagamento',
  templateUrl: './consultar-pagamento.component.html',
  styleUrls: ['./consultar-pagamento.component.css'],
  providers: [PagamentoService]
})
export class ConsultarPagamentoComponent implements OnInit {

  public buscarSolicitacoes: Array<SolicitacaoPagamento>;
  public buscarSolicitacoesAux: Array<SolicitacaoPagamento>;
  public orgaogestores: OrgaoGestor[];
  public listaMensagensErroGeraPagamento: MensagensError;
  consultarForm: FormGroup;
  response: any;
  errorMessage: any;
  statusPagamentoAtual: String;
  user: any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedRows$: Observable<any>;
  totalRows$: Observable<number>;
  tipodespesas: TipoDespesa[];
  listaDespesasVO: TipoDespesaVO[];

  constructor(private router: Router,
    private pagamentoService: PagamentoService,
    private messageService: MessagesService,
    private orgaoGestorService: OrgaoGestorService,
    private userService: UserService,
    private tipoDespesaService: TipoDespesaService) { }

  reenviar(item: SolicitacaoPagamento) {
    this.router.navigate(['menu/solicitacao-pagamento', item.id])
  }

  confirmar(item: SolicitacaoPagamento) {
    this.statusPagamentoAtual = item.statusPagamentoEnum;
    this.pagamentoService.consultaSolicitacao(item.id)
      .subscribe(
        response => {
          this.exibeMensagemSucesso(response)
        },
        error => this.msgErro(error, 'Falha ao tentar confirmar a solicitação.')
      );
  }

  msgErro(error, msg) {
    this.messageService.error(msg, error);
  }

  exibeMensagemSucesso(param) {
    if (param.statusPagamentoEnum !== undefined) {
      if (param.statusPagamentoEnum.toUpperCase() !== this.statusPagamentoAtual.toUpperCase()) {
        this.messageService.infoActionButton("Sua solicitação foi atualizada para: " + param.statusPagamentoEnum);
      } else {
        this.messageService.success("Não houve alterações de status da sua solicitação");
      }
    } else {
      this.messageService.error("Ocorreu um erro interno", error);
    }
  }

  paginatorTable(lista): void {
    const pageEvents$: Observable<PageEvent> = fromMatPaginator(this.paginator);
    const rows$ = of(lista);
    this.totalRows$ = rows$.pipe(map(rows => rows.length));
    this.displayedRows$ = rows$.pipe(paginateRows(pageEvents$));
  }

  //Deve ser passado o valor referente ao ID do usuario em this.pagamentoService.consultarPagamento('')
  ngOnInit() {
    this.user = JSON.parse(this.userService.getUserData());
    this.pagamentoService.consultarPagamento(this.user.matricula)
      .subscribe((buscarSolicitacoes: SolicitacaoPagamento[]) => {
        this.buscarSolicitacoes = buscarSolicitacoes;
        this.buscarSolicitacoesAux = this.buscarSolicitacoes;
        this.paginatorTable(this.buscarSolicitacoes);
        if (this.buscarSolicitacoes.length == 0) {
          this.messageService.error("Não foi encontrada nenhuma solicitação", error);
        }
      }, error => {
        this.messageService.error("Erro ao consultar " + "solicitações", error);
      });

    this.orgaoGestorService.ConsultaOrgaoGestor()
      .subscribe((orgaogestores: OrgaoGestor[]) => {
        this.orgaogestores = orgaogestores;
      }, (error: HttpErrorResponse) => console.log(error));

    this.tipoDespesaService.consultaTipoDespesa()
      .subscribe((tipodespesas: TipoDespesa[]) => {
        this.tipodespesas = tipodespesas;
        this.montarListaDespesaVO();
      }, error => console.log(error));


    this.consultarForm = new FormGroup(
      {
        orgao: new FormControl(),
        situacaoSelect: new FormControl(),
        despesaSelect: new FormControl(),
        processo: new FormControl()
      }
    );
  }

  situacao = [
    { "id": "1", "tipoSituacao": "Aguardando Aprovação" },
    { "id": "2", "tipoSituacao": "Aprovado" },
    { "id": "3", "tipoSituacao": "Compensado" },
    { "id": "4", "tipoSituacao": "Estornado" },
    { "id": "9", "tipoSituacao": "Pendente" },
  ]

  applyFilter() {
    const listaAplicativoFiltrada: Array<SolicitacaoPagamento> = [];
    this.buscarSolicitacoes = this.buscarSolicitacoesAux;

    this.buscarSolicitacoes.filter(item => {
      //valida orgao
      if (this.consultarForm.value.orgao !== null && this.verificaNullEmpty(this.consultarForm.value.orgao.descricao)) {
        if (this.verificaNullEmpty(this.consultarForm.value.processo)) {
          if (this.consultarForm.value.situacaoSelect !== null && this.verificaNullEmpty(this.consultarForm.value.situacaoSelect.id)) {
            if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
              //Filtra os 4
              if (String(item.processo).includes(String(this.consultarForm.value.processo))
                && String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)
                && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
                listaAplicativoFiltrada.push(item);
              }
            } else {
              //Filtra por Orgao, Processo e Situacao
              if (String(item.processo).includes(String(this.consultarForm.value.processo))
                && String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)) {
                listaAplicativoFiltrada.push(item);
              }
            }
          } else {
            if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
              //Filtra Por Orgao Processo e Tipo Despesa
              if (String(item.processo).includes(String(this.consultarForm.value.processo))
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)
                && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
                listaAplicativoFiltrada.push(item);
              }
            } else {
              //Filta por Orgao e Processo
              if (String(item.processo).includes(String(this.consultarForm.value.processo))
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)) {
                listaAplicativoFiltrada.push(item);
              }
            }
          }
        } else {
          if (this.consultarForm.value.situacaoSelect !== null && this.verificaNullEmpty(this.consultarForm.value.situacaoSelect.id)) {

            if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
              //Filtra por Orgao, Situacao Tipo Despesa
              if (String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)
                && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
                listaAplicativoFiltrada.push(item);
              }
            } else {
              //Filtra Por Orgao e Situacao
              if (String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
                && String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)) {
                listaAplicativoFiltrada.push(item);
              }
            }
          } else if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
            //Filtra por Orgao e Despesa
            if (String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)
              && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
              listaAplicativoFiltrada.push(item);
            }
          } else {
            //Filtra Por Orgao
            if (String(item.orgaoGestor.descricao) === String(this.consultarForm.value.orgao.descricao)) {
              listaAplicativoFiltrada.push(item);
            }
          }
        }
      } else if (this.verificaNullEmpty(this.consultarForm.value.processo)) {
        if (this.consultarForm.value.situacaoSelect !== null && this.verificaNullEmpty(this.consultarForm.value.situacaoSelect.id)) {
          if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
            //Filtra por Processo, Situacao e Despesa
            if (String(item.processo).includes(String(this.consultarForm.value.processo))
              && String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
              && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
              listaAplicativoFiltrada.push(item);
            }
          } else {
            //Filtra por Processo e Situacao
            if (String(item.processo).includes(String(this.consultarForm.value.processo))
              && String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)) {
              listaAplicativoFiltrada.push(item);
            }
          }
        } else {
          if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
            //Filtra por Processo e Despesa
            if (String(item.processo).includes(String(this.consultarForm.value.processo))
              && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
              listaAplicativoFiltrada.push(item);
            }
          } else {
            //Filtra por Processo
            if (String(item.processo).includes(String(this.consultarForm.value.processo))) {
              listaAplicativoFiltrada.push(item);
            }
          }
        }
      } else if (this.consultarForm.value.situacaoSelect !== null && this.verificaNullEmpty(this.consultarForm.value.situacaoSelect.id)) {

        if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
          //Filtra por Situacao e Despesa
          if (String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)
            && String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
            listaAplicativoFiltrada.push(item);
          }
        } else {
          //Filtra Por Situacao
          if (String(item.codigoStatus) === String(this.consultarForm.value.situacaoSelect.id)) {
            listaAplicativoFiltrada.push(item);
          }
        }
      } else if (this.consultarForm.value.despesaSelect !== null && this.verificaNullEmpty(this.consultarForm.value.despesaSelect.codigo)) {
        //Filtra Por Tipo de Despesa
        if (String(item.tipoDeDespesa.codigo) === String(this.consultarForm.value.despesaSelect.codigo)) {
          listaAplicativoFiltrada.push(item);
        }
      } else {
        listaAplicativoFiltrada.push(item);
      }

    });
    this.paginatorTable(listaAplicativoFiltrada);
    this.buscarSolicitacoes = listaAplicativoFiltrada;

  }

  public selectSolicitacao(solicitacao: SolicitacaoPagamento) {
    this.listaMensagensErroGeraPagamento = solicitacao.listaMensagensErroGeraPagamento;
  }

  voltar() {
    this.router.navigate(['menu']);
  }

  verificaNullEmpty(item: any) {
    let inValid = /\s/;
    if (item !== null && !inValid.test(String(item)) && String(item).length != 0) {
      return true;
    }
    return false;
  }

  montarListaDespesaVO() {
    if (this.tipodespesas) {
      let lista: any[] = [];
      this.tipodespesas.forEach(value => {
        lista.push(new TipoDespesaVO(value.id, value.codigo, value.descricao, value.indicadorPagamentoEnum));
      });
      this.listaDespesasVO = lista
    }
  }

}
