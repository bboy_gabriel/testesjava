import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarPagamentoComponent } from './consultar-pagamento.component';

describe('ConsultarPagamentoComponent', () => {
  let component: ConsultarPagamentoComponent;
  let fixture: ComponentFixture<ConsultarPagamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarPagamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarPagamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
