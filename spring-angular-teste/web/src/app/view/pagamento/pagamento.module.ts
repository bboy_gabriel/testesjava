import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSelectModule, MatTooltipModule, MatPaginatorIntl } from '@angular/material';

import { NgxCurrencyModule } from "ngx-currency";

import { SolicitarPagamentoComponent } from './solicitar-pagamento/solicitar-pagamento.component';
import { ConsultarPagamentoComponent } from './consultar-pagamento/consultar-pagamento.component';
import { ComponentsComponent } from '../shared/components/components.module';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { PaginatorText } from 'src/app/core/utils/paginator.text';
import { ConsultaSolicitacaoPagamentoComponent } from './consultar-solicitacao/consulta-solicitacao.component';
import { DataTransferService } from 'src/app/controller/services/generic/data-transfer.service';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    SolicitarPagamentoComponent,
    ConsultarPagamentoComponent,
    ConsultaSolicitacaoPagamentoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ComponentsComponent,
    ReactiveFormsModule,
    FormsModule,
    NgxCurrencyModule,
    /* ----interface---- */
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgSelectModule,
    MatSelectModule,
    MatTooltipModule

  ],
  providers: [
    MessagesService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: MatPaginatorIntl, useClass: PaginatorText },
    DataTransferService
  ],
  exports: [
    SolicitarPagamentoComponent,
    ConsultarPagamentoComponent,
    ConsultaSolicitacaoPagamentoComponent
  ]
})
export class PagamentoComponent { }
