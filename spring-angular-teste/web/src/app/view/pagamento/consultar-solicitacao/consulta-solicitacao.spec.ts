import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsultaSolicitacaoPagamentoComponent } from './consulta-solicitacao.component';




describe('ConsultarPagamentoComponent', () => {
  let component: ConsultaSolicitacaoPagamentoComponent;
  let fixture: ComponentFixture<ConsultaSolicitacaoPagamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaSolicitacaoPagamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaSolicitacaoPagamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
