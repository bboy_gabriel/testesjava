import { error } from '@angular/compiler/src/util';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { UserService } from 'src/app/controller/services/user/user.service';
import { fromMatPaginator, paginateRows } from 'src/app/core/utils/data.paginator';
import { MensagensError } from 'src/app/model/mensagens-erro.model';
import { SolicitacaoPagamento } from '../../../model/pagamento/solicitacao-pagamento.model';
import { TipoDespesaVO } from 'src/app/model/tipoDespesaVO.model';
import { PagamentoService } from '../../../controller/services/pagamento/pagamento.service';
import { OrgaoGestorService } from '../../../controller/services/tabelas/orgao-gestor.service';
import { OrgaoGestor } from '../../../model/tabelas/orgaogestor.model';
import { TipoDespesaService } from './../../../controller/services/tabelas/tipo-despesa.service';
import { TipoDespesa } from '../../../model/tabelas/tipodespesa.model';
import { SolicitarPagamentoComponent } from '../solicitar-pagamento/solicitar-pagamento.component';

@Component({
  selector: 'consulta-solicitacao-pagamento',
  templateUrl: './consulta-solicitacao.html',
  styleUrls: ['./consulta-solicitacao.css'],
  providers: [PagamentoService]
})
export class ConsultaSolicitacaoPagamentoComponent implements OnInit {

  public buscarSolicitacoes: Array<SolicitacaoPagamento>;
  public buscarSolicitacoesAux: Array<SolicitacaoPagamento>;
  public orgaogestores: OrgaoGestor[];
  public listaMensagensErroGeraPagamento: MensagensError;
  consultarForm: FormGroup;
  response: any;
  errorMessage: any;
  statusPagamentoAtual: String;
  user: any;
  paginaBusca: Object = {busca: null, buscarTodos: true, 
    orgaoGestor: null, processo: null, tipoDeDespesa: null, statusPagamentoEnum: null}

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedRows$: Observable<any>;
  totalRows$: Observable<number>;
  tipodespesas: TipoDespesa[];
  listaDespesasVO: TipoDespesaVO[];
  LISTA_VIEW = ['CONSULTA','DETALHE']  
  VIEW = this.LISTA_VIEW[0];
  solicitacaoSelecionada: SolicitacaoPagamento;
  voltarCallback: Function = this.executarCallBackVoltar();

  @ViewChild('detalheComponent') detalheComponent: SolicitarPagamentoComponent;
  
  constructor(private router: Router,
    private pagamentoService: PagamentoService,
    private messageService: MessagesService,
    private orgaoGestorService: OrgaoGestorService,
    private userService: UserService,
    private tipoDespesaService: TipoDespesaService) { }

  ngOnInit() {
    this.user = JSON.parse(this.userService.getUserData());
    this.consultarSolicitacao(true)
    this.popularSelect();
    this.consultarForm = new FormGroup(
      {
        orgaoGestor: new FormControl(),
        statusPagamentoEnum: new FormControl(),
        tipoDeDespesa: new FormControl(),
        processo: new FormControl()
      }
    );
  }

  executarCallBackVoltar(): Function {
    let fn = (paramentors)=>{
      this.VIEW = this.LISTA_VIEW[0];     
      this.solicitacaoSelecionada = undefined;
      this.consultarSolicitacao(true);
    }        
    return fn.bind(this)
  }
  
  reenviar(item: SolicitacaoPagamento) {
    this.router.navigate(['menu/solicitacao-pagamento', item.id])
  }

  
  consultarSolicitacao(todos: boolean) {
    this.paginaBusca = todos? {busca: null, buscarTodos: true}: this.paginaBusca;
    this.pagamentoService.consultaSolicitacaoPagamento(this.paginaBusca)
      .subscribe(
        response => {
          this.buscarSolicitacoes = response.content;
          this.buscarSolicitacoesAux = this.buscarSolicitacoes;
          this.paginatorTable(response.content);
        }
      );
  }

  detalhar(item: SolicitacaoPagamento) {    
    this.solicitacaoSelecionada = item;    
    this.VIEW = this.LISTA_VIEW[1];
    this.detalheComponent.setFormData(item);
  }

  msgErro(error, msg) {
    this.messageService.error(msg, error);
  }

  exibeMensagemSucesso(param) {
    if (param.statusPagamentoEnum !== undefined) {
      if (param.statusPagamentoEnum.toUpperCase() !== this.statusPagamentoAtual.toUpperCase()) {
        this.messageService.infoActionButton("Sua solicitação foi atualizada para: " + param.statusPagamentoEnum);
      } else {
        this.messageService.success("Não houve alterações de status da sua solicitação");
      }
    } else {
      this.messageService.error("Ocorreu um erro interno", error);
    }
  }

  paginatorTable(lista): void {
    const pageEvents$: Observable<PageEvent> = fromMatPaginator(this.paginator);
    const rows$ = of(lista);
    this.totalRows$ = rows$.pipe(map(rows => rows.length));
    this.displayedRows$ = rows$.pipe(paginateRows(pageEvents$));
  }
  
  popularSelect(){
    this.tipoDespesaService.consultaTipoDespesa()
    .subscribe((despesas: TipoDespesa[]) => {
      this.tipodespesas = despesas as TipoDespesaVO[];
      this.montarListaDespesaVO();
    });

  this.orgaoGestorService.ConsultaOrgaoGestor()
    .subscribe((orgaogestores: OrgaoGestor[]) => {
      this.orgaogestores = orgaogestores;
    });
  }

  situacao = [
    { "id": "1", "tipoSituacao": "Aguardando Aprovação" },
    { "id": "2", "tipoSituacao": "Aprovado" },
    { "id": "3", "tipoSituacao": "Compensado" },
    { "id": "4", "tipoSituacao": "Estornado" },
    { "id": "9", "tipoSituacao": "Pendente" },
  ]

  applyFilter(key: any) {
    
    let listaAplicativoFiltrada: Array<SolicitacaoPagamento> = [];
    this.buscarSolicitacoes = this.buscarSolicitacoesAux;
    listaAplicativoFiltrada = this.filtrar(key);
    this.paginatorTable(listaAplicativoFiltrada);
    this.buscarSolicitacoes = listaAplicativoFiltrada;
  }

  
 
  filtrar(key: any){
    if(!this.consultarForm.value[key])
      return this.buscarSolicitacoes;
    return this.buscarSolicitacoes.filter(item =>{
      let value = '';
      let valueIten = '';
      switch (key) {
        case 'orgaoGestor':
            value = this.consultarForm.value.orgaoGestor.descricao;
            valueIten = item['orgaoGestor']['descricao'];
            break;
        case 'tipoDeDespesa':
            value = this.consultarForm.value.tipoDeDespesa.descricao;
            valueIten = item['tipoDeDespesa']['descricao'];
            break;
        case 'statusPagamentoEnum':
            value = this.consultarForm.value.statusPagamentoEnum.tipoSituacao;
            valueIten = item['statusPagamentoEnum']
            break;
        case 'processo':
            value = this.consultarForm.value.processo;
            valueIten = item['processo'];
            break;
        }
       return String(valueIten).includes(value)});
  }

  public selectSolicitacao(solicitacao: SolicitacaoPagamento) {
    this.listaMensagensErroGeraPagamento = solicitacao.listaMensagensErroGeraPagamento;
  }

  voltar() {
    this.router.navigate(['menu']);
  }

  verificaNullEmpty(item: any) {
    let inValid = /\s/;
    if (item !== null && !inValid.test(String(item)) && String(item).length != 0) {
      return true;
    }
    return false;
  }

  montarListaDespesaVO() {
    if (this.tipodespesas) {
      let lista: any[] = [];
      this.tipodespesas.forEach(value => {
        lista.push(new TipoDespesaVO(value.id, value.codigo, value.descricao, value.indicadorPagamentoEnum));
      });
      this.listaDespesasVO = lista
    }
  }

}
