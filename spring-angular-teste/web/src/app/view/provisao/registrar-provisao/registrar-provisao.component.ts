import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandler } from 'src/app/app.error-handler';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { ProcessoProvisaoService } from 'src/app/controller/services/processo/processo-provisao.service';
import { UserService } from 'src/app/controller/services/user/user.service';
import { Processo } from 'src/app/model/processo/processo.model';
import { ContaRazao } from 'src/app/model/provisao/conta-provisao.model';
import { LancamentoContabil } from 'src/app/model/provisao/lancamento-contabil.model';
import { Provisao } from 'src/app/model/provisao/provisao.model';
import { CentroDeCusto } from 'src/app/model/tabelas/centroDeCusto.model';
import { ContaRazaoProvisao } from 'src/app/model/tabelas/conta-razao-provisao';
import { ElementoPEP } from 'src/app/model/tabelas/elementoPEP.model';
import { Ordem } from 'src/app/model/tabelas/ordem.model';
import { ProvisaoService } from '../../../controller/services/provisao/provisao.service';
import { InformacaoContabil } from '../../../model/provisao/informacao-contabil.model';

@Component({
  selector: 'registrar-provisao',
  templateUrl: './registrar-provisao.component.html',
  styleUrls: ['./registrar-provisao.component.css']
})
export class RegistrarProvisaoComponent implements OnInit {

  //Tabela Informação Contábil
  centrosDeCustos: CentroDeCusto[];
  elementosPEP: ElementoPEP[];
  ordens: Ordem[];

  centro: CentroDeCusto;
  elemento: ElementoPEP;
  ordem: Ordem;

  dataSource: any;
  infoContabilColumnDisplays = ['centroDescricao', 'pepDescricao', 'ordemDescricao', 'excluir'];
  infoContabil: {};
  infoContabilDataSource = [];

  //Formulário
  registrarProvisaoForm: FormGroup;
  user: any;
  processoAtual: Processo;
  dataAtual: String;
  mesAtual: String;
  valorContabil: string = '0';
  valorTotalSolicitado: number;
  habilitaBotao: Boolean = false;

  //Modelo
  provisao: Provisao = {} as Provisao;

  //ContaRazaoProvisa
  contasRazaoProvisao: ContaRazaoProvisao[];
  contasCreditoDebito: ContaRazao[];
  mostrarContabil: boolean = false;

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  @ViewChild(MatSort) sort: MatSort;
  constructor(private router: Router,
    private fb: FormBuilder,
    private message: MessagesService,
    private userService: UserService,
    private provisaoService: ProvisaoService,
    private processoProvisaoService: ProcessoProvisaoService,
    private activate: ActivatedRoute) {

    //formulario
    this.registrarProvisaoForm = this.fb.group({
      id: this.fb.control(''),
      usuario: this.fb.control(null),
      processo: this.fb.control('', [this.required]),
      tipoLancamento: this.fb.control('', [this.required]),
      dataLancamento: this.fb.control('', [this.required]),
      valor: this.fb.control('', [this.minLength(2), this.maxLength(15), this.required]),
      contaRazaoCreditoDebito: this.fb.group({
        id: this.fb.control(''),
        contaDebito: this.fb.control('', this.required),
        contaCredito: this.fb.control('', this.required),
        codigoTipoProcesso: this.fb.control(''),
        tipoProcessoProvisaoEnum: this.fb.control(''),
        contasCreditoDebito: this.fb.control(''),
      }),
      historico: this.fb.control('', [this.minLength(2), this.maxLength(50)]),
      informacoesContabeis: this.fb.array([])
    });
  }

  ngOnInit() {
    this.dataAtual = this.dataAtualFormatada();
    this.mesAtual = this.mesAtualFormatado();
    this.user = JSON.parse(this.userService.getUserData());
    this.registrarProvisaoForm.controls.usuario.setValue(this.user.matricula);
    this.activate.data.subscribe(response => {
      this.centrosDeCustos = response.message[0];
      this.elementosPEP = response.message[1];
      this.ordens = response.message[2];
    })
  }

  //valida o mês atual
  mesAtualFormatado() {
    var data: Date = new Date(),
      mes = (data.getMonth() + 1).toString(),
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + '01';
  }

  //valida a data atual
  dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  //valida se a data está no intervalo do mês corrente e dia atual
  validacaoMesCorrente(data) {
    if (data < this.mesAtualFormatado()) {
      this.registrarProvisaoForm.controls.dataLancamento.reset();
      this.message.error("A data não pode ser inferior ao mês corrente.", {});
    } if (data > this.dataAtualFormatada()) {
      this.registrarProvisaoForm.controls.dataLancamento.reset();
      this.message.error("A data não pode ser superior ao dia corrente.", {});
    }
  }

  incluirInformacaoContabil() {
    this.adicionarLista();
    this.limparInfoContabil();
  }

  onClickRemove(elemento: InformacaoContabil) {
    let posicao = this.provisao.informacoesContabeis.indexOf(elemento);
    let filterDatasource: InformacaoContabil[] = [];
    this.removeNumbers(posicao);
    this.provisao.informacoesContabeis.splice(posicao, 1);
    filterDatasource = this.provisao.informacoesContabeis
    this.dataSource = new MatTableDataSource(filterDatasource);
  }

  adicionarLista() {
    if (!this.provisao.informacoesContabeis || this.provisao.informacoesContabeis.length === 0) {
      let novoInfoContabil = new InformacaoContabil(null, this.centro, this.elemento, this.ordem);
      this.provisao.informacoesContabeis = [];
      this.provisao.informacoesContabeis.push(novoInfoContabil);
      this.dataSource = new MatTableDataSource(this.provisao.informacoesContabeis);
      this.message.success("Informação contábil adicionada.");
    }
  }

  possuiInfoContabil(): boolean {
    if (this.provisao.informacoesContabeis) {
      return this.provisao.informacoesContabeis.length === 1;
    }
  }

  validaInformacaoContabil(): boolean {
    if (this.centro && !this.possuiInfoContabil()) {
      return true;
    }
    return false;
  }

  limparInfoContabil() {
    this.valorContabil = undefined;
    this.centro = undefined
    this.elemento = undefined;
    this.ordem = undefined;
  }

  removeNumbers(i: number) {
    const control = <FormArray>this.registrarProvisaoForm.controls['informacoesContabeis'];
    control.value.splice(i, 1);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //verifica se o processo é valido
  validarProcessoSIJUS(nProcesso: any): Processo {
    if (nProcesso) {
      this.processoProvisaoService.consultarProcesso(nProcesso).subscribe(
        response => {
          this.processoAtual = response;
          if (this.processoAtual.idStatus !== "A") {
            this.message.info("Processo encerrado ou não disponivel!");
            this.limparCamposValidacaoProcesso();
          }
          if (this.processoAtual.idRisco !== "1") {
            this.message.info("Processo não permite incluir provisão.");
            this.limparCamposValidacaoProcesso();
          }
          else {
            if (this.processoAtual.mensagemOpcional) {
              this.message.info(this.processoAtual.mensagemOpcional + ".");
            }
            this.mostrarContabil = true;
            this.registrarProvisaoForm.controls.tipoLancamento.setValue(this.processoAtual.tipoProcesso)
            this.registrarProvisaoForm.controls.historico.setValue("Projur_FIN Provisão – SIJUS " + nProcesso);;
            this.consultarContasRazaoProvisao()
          }
        },
        error => {
          this.mostrarContabil = false;
          this.message.error("Processo encerrado ou não encontrado no PROJUR.", error)
          this.limparCamposValidacaoProcesso();
        });
    } return this.processoAtual;
  }

  //consulta as contas na tabela conta razão provisão
  consultarContasRazaoProvisao(): any {
    this.processoProvisaoService.consultarTipoProcesso(this.processoAtual.codigoTipoProcesso.toString())
      .subscribe((response: ContaRazaoProvisao[]) => {
        this.contasRazaoProvisao = response;
        this.montarContaRazao();
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar as contas.", {});
      });
  }

  montarContaRazao() {
    if (this.contasRazaoProvisao) {
      let lista: ContaRazao[] = [];
      this.contasRazaoProvisao.forEach(value => {
        lista.push(new ContaRazao(value.id, value.contaCredito, value.contaDebito, value.codigoTipoProcesso, value.tipoProcessoProvisaoEnum));
      });
      this.contasCreditoDebito = lista
    }
  }

  //limpa os itens do formulário
  limparCamposValidacaoProcesso() {
    this.registrarProvisaoForm.controls.historico.reset();
    this.registrarProvisaoForm.controls.processo.reset();
    this.registrarProvisaoForm.controls.tipoLancamento.reset();
  }

  recuperaProcesso(): any {
    let valor: string = this.registrarProvisaoForm.value.processo;
    if (valor) {
      let processo: Processo = this.validarProcessoSIJUS(valor)
    }
    else {
      this.registrarProvisaoForm.controls.tipoLancamento.reset();

    }
  }

  getValorRegistro(): number {
    if (this.registrarProvisaoForm.controls.valor.value) {
      return this.registrarProvisaoForm.controls.valor.value;
    }
    return 0;
  }

  validarValorRegistro() {
    if (this.getValorRegistro() <= 0) {
      this.message.info("O valor da provisão não pode ser igual ou inferior a zero");
      this.registrarProvisaoForm.controls.valor.reset();
    }
  }

  getFormattedPrice(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

  enviarProvisao() {
    let infoContabil = this.provisao.informacoesContabeis;
    this.provisao = this.registrarProvisaoForm.getRawValue();
    if (this.provisao) {
      if (infoContabil) {
        this.provisao.informacoesContabeis = infoContabil;
      } else {
        this.provisao.informacoesContabeis = [];
      }
      let lancamento = new LancamentoContabil();

      lancamento.numeroProcesso = this.provisao.processo
      lancamento.dataLancamento = this.provisao.dataLancamento
      lancamento.valor = this.provisao.valor
      lancamento.contaDebito = this.provisao.contaRazaoCreditoDebito.contaDebito
      lancamento.contaCredito = this.provisao.contaRazaoCreditoDebito.contaCredito
      lancamento.historico = this.provisao.historico
      if (this.provisao.informacoesContabeis[0].pep != undefined) {
        lancamento.elementoPep = this.provisao.informacoesContabeis[0].pep
      } else {
        lancamento.elementoPep = null;
      }
      if (this.provisao.informacoesContabeis[0].ordem != undefined) {
        lancamento.ordem = this.provisao.informacoesContabeis[0].ordem;
      } else {
        lancamento.ordem = null;
      }
      lancamento.centroDeCusto = this.provisao.informacoesContabeis[0].centroDeCusto
      this.provisaoService.incluirProvisao(lancamento).subscribe(response => {
        this.message.infoFormated("Provisão incluída com sucesso!")
        this.router.navigate(['menu']);
      }, error => {
        this.message.error("Provisão não incluida!", null)
      })
    }
  }

  voltar() {
    this.router.navigate(['menu']);
  }

}
