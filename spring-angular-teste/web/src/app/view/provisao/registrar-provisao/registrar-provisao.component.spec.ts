import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarProvisaoComponent } from './registrar-provisao.component';

describe('RegistrarProvisaoComponent', () => {
  let component: RegistrarProvisaoComponent;
  let fixture: ComponentFixture<RegistrarProvisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarProvisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarProvisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
