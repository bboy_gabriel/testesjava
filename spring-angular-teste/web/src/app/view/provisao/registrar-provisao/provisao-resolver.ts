import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CentroDeCustoService } from '../../../controller/services/tabelas/centro-de-custo.service';
import { ElementoPepService } from '../../../controller/services/tabelas/elemento-pep.service';
import { OrdemService } from '../../../controller/services/tabelas/ordem.service';
import { Observable, forkJoin, of } from 'rxjs';

@Injectable()
export class ProvisaoResolver implements Resolve<any> {
  constructor(private centroService: CentroDeCustoService,
    private elementoService: ElementoPepService,
    private ordemService: OrdemService) {}
 
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|any {
    const centro = this.centroService.consultar();
    const pep = this.elementoService.consultaElementosPEP();
    const ordem = this.ordemService.ConsultaOrdem();
    return forkJoin([centro, pep, ordem]);    
    
  }
}