import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstornarProvisaoComponent } from './estornar-provisao.component';

describe('EstornarProvisaoComponent', () => {
  let component: EstornarProvisaoComponent;
  let fixture: ComponentFixture<EstornarProvisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstornarProvisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstornarProvisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
