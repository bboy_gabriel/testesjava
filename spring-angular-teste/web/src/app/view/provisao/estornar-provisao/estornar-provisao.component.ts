import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandler } from 'src/app/app.error-handler';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { ProcessoEstornoProvisaoService } from 'src/app/controller/services/processo/processo-estorno-provisao.service';
import { ProvisaoEstornoService } from 'src/app/controller/services/provisao/provisao-estorno.service';
import { UserService } from 'src/app/controller/services/user/user.service';
import { Processo } from 'src/app/model/processo/processo.model';
import { ContaRazao } from 'src/app/model/provisao/conta-provisao.model';
import { InformacaoContabil } from 'src/app/model/provisao/informacao-contabil.model';
import { EstornoProvisao } from 'src/app/model/provisao/provisao-estorno.model';
import { CentroDeCusto } from 'src/app/model/tabelas/centroDeCusto.model';
import { ElementoPEP } from 'src/app/model/tabelas/elementoPEP.model';
import { Ordem } from 'src/app/model/tabelas/ordem.model';
import { ContaRazaoEstornoProvisao } from 'src/app/model/tabelas/razao-estorno-provisao.model';
import { ProcessoService } from 'src/app/controller/services/processo/processo.service';
import { LancamentoContabil } from 'src/app/model/provisao/lancamento-contabil.model';

@Component({
  selector: 'estornar-provisao',
  templateUrl: './estornar-provisao.component.html',
  styleUrls: ['./estornar-provisao.component.css'],
  providers: [ProcessoService]
})
export class EstornarProvisaoComponent implements OnInit {

  //Tabela Informação Contábil
  centrosDeCustos: CentroDeCusto[];
  elementosPEP: ElementoPEP[];
  ordens: Ordem[];

  centro: CentroDeCusto;
  elemento: ElementoPEP;
  ordem: Ordem;

  dataSource: any;
  infoContabilColumnDisplays = ['centroDescricao', 'pepDescricao', 'ordemDescricao', 'excluir'];
  infoContabil: {};
  infoContabilDataSource = [];

  //Formulário
  estornoProvisaoForm: FormGroup;
  user: any;
  processoAtual: Processo;
  dataAtual: String;
  mesAtual: String;
  valorContabil: string = '0';
  valorTotalSolicitado: number;
  habilitaBotao: Boolean = false;

  //Modelo
  provisaoEstorno: EstornoProvisao = {} as EstornoProvisao;

  //ContaEstornoProvisao
  valorSomaProvisao: Processo;
  valorTotais: number;
  contaRazaoEstornoProvisao: ContaRazaoEstornoProvisao[];
  contasCreditoDebito: ContaRazao[];
  mostrarContabil: boolean = false;

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  @ViewChild(MatSort) sort: MatSort;
  constructor(private router: Router,
    private fb: FormBuilder,
    private message: MessagesService,
    private userService: UserService,
    private provisaoEstornoService: ProvisaoEstornoService,
    private processoEstornoProvisaoService: ProcessoEstornoProvisaoService,
    private processoService: ProcessoService,
    private activate: ActivatedRoute) {

    //formulario
    this.estornoProvisaoForm = this.fb.group({
      id: this.fb.control(''),
      usuario: this.fb.control(null),
      processo: this.fb.control('', [this.required]),
      tipoLancamento: this.fb.control('', [this.required]),
      dataLancamento: this.fb.control('', [this.required]),
      valor: this.fb.control('', [this.minLength(2), this.maxLength(15), this.required]),
      contaRazaoCreditoDebito: this.fb.group({
        id: this.fb.control(''),
        contaDebito: this.fb.control('', this.required),
        contaCredito: this.fb.control('', this.required),
        codigoTipoProcesso: this.fb.control(''),
        tipoProcessoProvisaoEnum: this.fb.control(''),
        contasCreditoDebito: this.fb.control(''),
      }),
      historico: this.fb.control('', [this.minLength(2), this.maxLength(50), this.required]),
      informacoesContabeis: this.fb.array([])
    });
  }

  ngOnInit() {
    this.dataAtual = this.dataAtualFormatada();
    this.mesAtual = this.mesAtualFormatado();
    this.user = JSON.parse(this.userService.getUserData());
    this.estornoProvisaoForm.controls.usuario.setValue(this.user.matricula);
    this.activate.data.subscribe(response => {
      this.centrosDeCustos = response.message[0];
      this.elementosPEP = response.message[1]
      this.ordens = response.message[2];
    })
  }

  //valida o mês atual
  mesAtualFormatado() {
    var data: Date = new Date(),
      mes = (data.getMonth() + 1).toString(),
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + '01';
  }

  //valida a data atual
  dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  //valida se a data está no intervalo do mês corrente e dia atual
  validacaoMesCorrente(data) {
    if (data < this.mesAtualFormatado()) {
      this.estornoProvisaoForm.controls.dataLancamento.reset();
      this.message.error("A data não pode ser inferior ao mês corrente.", {});
    } if (data > this.dataAtualFormatada()) {
      this.estornoProvisaoForm.controls.dataLancamento.reset();
      this.message.error("A data não pode ser superior ao dia corrente.", {});
    }
  }

  incluirInformacaoContabil() {
    this.adicionarLista();
    this.limparInfoContabil();
  }

  onClickRemove(elemento: InformacaoContabil) {
    let posicao = this.provisaoEstorno.informacoesContabeis.indexOf(elemento);
    let filterDatasource: InformacaoContabil[] = [];
    this.removeNumbers(posicao);
    this.provisaoEstorno.informacoesContabeis.splice(posicao, 1);
    filterDatasource = this.provisaoEstorno.informacoesContabeis
    this.dataSource = new MatTableDataSource(filterDatasource);
  }

  adicionarLista() {
    if (!this.provisaoEstorno.informacoesContabeis || this.provisaoEstorno.informacoesContabeis.length === 0) {
      let novoInfoContabil = new InformacaoContabil(null, this.centro, this.elemento, this.ordem);
      this.provisaoEstorno.informacoesContabeis = [];
      this.provisaoEstorno.informacoesContabeis.push(novoInfoContabil);
      this.dataSource = new MatTableDataSource(this.provisaoEstorno.informacoesContabeis);
      this.message.success("Informação contábil adicionada.");
    }
  }

  possuiInfoContabil(): boolean {
    if (this.provisaoEstorno.informacoesContabeis) {
      return this.provisaoEstorno.informacoesContabeis.length === 1;
    }
  }

  validaInformacaoContabil(): boolean {
    if (this.centro && !this.possuiInfoContabil()) {
      return true;
    }
    return false;
  }

  limparInfoContabil() {
    this.centro = undefined
    this.elemento = undefined;
    this.ordem = undefined;
  }

  removeNumbers(i: number) {
    const control = <FormArray>this.estornoProvisaoForm.controls['informacoesContabeis'];
    control.value.splice(i, 1);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  //Validar processo SIJUS
  validarProcessoSIJUS(nProcesso: any): Processo {
    if (nProcesso) {
      this.processoService.consultaProcesso(nProcesso).subscribe(
        response => {
          this.processoAtual = response;
          if (this.processoAtual.idStatus !== "A") {
            this.message.info("Processo encerrado ou não disponivel!");
            this.limparCamposValidacaoProcesso();
          }
          if (this.processoAtual.idRisco !== "1") {
            this.message.info("Processo não permite incluir provisão.");
            this.limparCamposValidacaoProcesso();
          }
          else {
            if (this.processoAtual.mensagemOpcional) {
              this.message.info(this.processoAtual.mensagemOpcional + ".");
            }
            this.processoEstornoProvisaoService.consultarTotalProvisao(nProcesso).subscribe(
              response => {
                this.valorSomaProvisao = response;
                this.valorTotais = this.valorSomaProvisao.valorTotalProvisao
              })
            this.mostrarContabil = true;
            this.estornoProvisaoForm.controls.tipoLancamento.setValue(this.processoAtual.tipoProcesso)
            this.estornoProvisaoForm.controls.historico.setValue("Projur_FIN Estorno Provisão – SIJUS " + nProcesso);;
            this.consultarContasRazaoProvisao()
          }
        },
        error => {
          this.mostrarContabil = false;
          this.message.error("Processo encerrado ou não encontrado no PROJUR.", error)
          this.limparCamposValidacaoProcesso();
        });
    } return this.processoAtual;
  }

  consultarContasRazaoProvisao(): any {
    this.processoEstornoProvisaoService.consultarTipoProcesso(this.processoAtual.codigoTipoProcesso.toString())
      .subscribe((response: ContaRazaoEstornoProvisao[]) => {
        this.contaRazaoEstornoProvisao = response;
        this.montarContaRazao();
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar as contas.", {});
      });
  }

  montarContaRazao() {
    if (this.contaRazaoEstornoProvisao) {
      let lista: ContaRazao[] = [];
      this.contaRazaoEstornoProvisao.forEach(value => {
        lista.push(new ContaRazao(value.id, value.contaCredito, value.contaDebito, value.codigoTipoProcesso, value.tipoProcessoProvisaoEnum));
      });
      this.contasCreditoDebito = lista
    }
  }

  limparCamposValidacaoProcesso() {
    this.estornoProvisaoForm.controls.historico.reset();
    this.estornoProvisaoForm.controls.processo.reset();
    this.estornoProvisaoForm.controls.tipoLancamento.reset();
  }

  recuperaProcesso(): any {
    let valor: string = this.estornoProvisaoForm.value.processo;
    if (valor) {
      let processo: Processo = this.validarProcessoSIJUS(valor)
    }
    else {
      this.estornoProvisaoForm.controls.tipoLancamento.reset();

    }
  }

  getValorRegistro(): number {
    if (this.estornoProvisaoForm.controls.valor.value) {
      return this.estornoProvisaoForm.controls.valor.value;
    }
    return 0;
  }

  validarValorRegistro() {
    if (this.getValorRegistro() <= 0) {
      this.message.info("O valor da provisão não pode ser igual ou inferior a zero.");
      this.estornoProvisaoForm.controls.valor.reset();
    }
    if (this.getValorRegistro() > this.valorTotais) {
      this.message.info("O valor informado é maior que o total provisionado para o processo.");
      this.estornoProvisaoForm.controls.valor.reset();
    }
  }

  getFormattedPrice(price: number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price);
  }

  enviarProvisao() {
    let infoContabil = this.provisaoEstorno.informacoesContabeis;
    this.provisaoEstorno = this.estornoProvisaoForm.getRawValue();
    if (this.provisaoEstorno) {
      if (infoContabil) {
        this.provisaoEstorno.informacoesContabeis = infoContabil;
      } else {
        this.provisaoEstorno.informacoesContabeis = [];
      }
      let lancamento = new LancamentoContabil();
      lancamento.numeroProcesso = this.provisaoEstorno.processo
      lancamento.dataLancamento = this.provisaoEstorno.dataLancamento
      lancamento.valor = this.provisaoEstorno.valor
      lancamento.contaDebito = this.provisaoEstorno.contaRazaoCreditoDebito.contaDebito
      lancamento.contaCredito = this.provisaoEstorno.contaRazaoCreditoDebito.contaCredito
      lancamento.historico = this.provisaoEstorno.historico
      if (this.provisaoEstorno.informacoesContabeis[0].pep != undefined) {
        lancamento.elementoPep = this.provisaoEstorno.informacoesContabeis[0].pep
      } else {
        lancamento.elementoPep = null;
      }
      if (this.provisaoEstorno.informacoesContabeis[0].ordem != undefined) {
        lancamento.ordem = this.provisaoEstorno.informacoesContabeis[0].ordem;
      } else {
        lancamento.ordem = null;
      }
      lancamento.centroDeCusto = this.provisaoEstorno.informacoesContabeis[0].centroDeCusto

      this.provisaoEstornoService.incluirProvisao(lancamento).subscribe(response => {
        this.message.infoFormated("Estorno de Provisão incluído com sucesso.")
        this.router.navigate(['menu']);
      }, error => {
        this.message.error("Estorno de Provisão não incluído.", null)
      })
    }
  }

  voltar() {
    this.router.navigate(['menu']);
  }

}
