import { Component, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/controller/services/user/user.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { InformacaoContabil } from 'src/app/model/deposito-judicial/lancamentoComtabilVO.model';
import { GerarArquivoService } from 'src/app/controller/services/gerarArquivo/gerarArquivo.service';
import { InformacaoContabilCSV } from 'src/app/model/arquivoscsv/informacao-contabil-csv.model';
import { LancamentoContabilRelatorioVO } from 'src/app/model/provisao/lancamento-contabil-relatorio.model';

@Component({
  selector: 'gerar-arquivo-csv',
  templateUrl: './gerar-arquivo-csv.component.html',
  styleUrls: ['./gerar-arquivo-csv.component.css']
})
export class GerarArquivoCSVComponent implements OnInit {

  //tabela
  displayedColumns: string[] = ['dataLancamento', 'numeroProcesso', 'tipoLancamento', 'valor', 'dataGeracaoCSV'];
  informacoesContabeis: InformacaoContabil[];
  informacoesContabeisCSV: InformacaoContabilCSV[];
  informacoesContabeisCSVData: InformacaoContabil[];
  gerarArquivoDataSource = new MatTableDataSource<InformacaoContabil>();
  gerarArquivoForm: FormGroup;
  showTabela: boolean = false;

  //data
  dataFutura: string;
  dataBuscada: Date;

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  constructor(private router: Router,
    private message: MessagesService,
    private fb: FormBuilder,
    private routerActive: ActivatedRoute,
    private userService: UserService,
    private gerarAquivoServie: GerarArquivoService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.gerarArquivoDataSource = new MatTableDataSource<InformacaoContabil>(new Array<InformacaoContabil>());
    this.dataFutura = this.dataFuturaFormatadoa();
    this.gerarArquivoForm = this.fb.group({
      id: this.fb.control(''),
      usuario: this.fb.control(null),
      dataLancamento: this.fb.control('', [this.required]),
    });
  }

  //valida a data maxima
  dataFuturaFormatadoa() {
    return '2100' + '-' + '12' + "-" + '31';
  }

  //valida se a data está no intervalo do mês corrente e dia atual
  validacaoMesCorrente(data) {
    if (data > this.dataFuturaFormatadoa()) {
      this.gerarArquivoForm.controls.dataLancamento.reset();
      this.message.error("Forneça uma data válida.", {});
      this.showTabela = false;
    }
    else {
      this.buscarInformacoesContabeis(data);
    }
  }

  //verifica se a data é válida e exibe a tabela
  buscarInformacoesContabeis(data) {
    this.gerarAquivoServie.consultarLancamentosCiclo(data).subscribe(
      response => {
        if (response.length) {
          this.informacoesContabeis = response;
          this.dataBuscada = data;
          this.showTabela = true;
          setTimeout(() => {
            this.gerarArquivoDataSource = new MatTableDataSource<InformacaoContabil>(this.informacoesContabeis);
            this.gerarArquivoDataSource.paginator = this.paginator;
          });
        } else {
          this.showTabela = false;
          this.gerarArquivoDataSource = new MatTableDataSource<InformacaoContabil>(this.informacoesContabeis);
          this.gerarArquivoDataSource.paginator = this.paginator;
          this.message.error("Não foram encontrados registros.", "");
        }
      },
      erro => {
        this.message.error("Forneça uma data válida.", erro);
      }
    )
  }

  voltar() {
    this.router.navigate(['menu']);
  }

  options: any = {
    fieldSeparator: ';',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: false,
    showTitle: false,
    title: 'Relatório CSV',
    useBom: true,
    noDownload: false,
    headers: false
  };

  gerarCSV() {
    this.informacoesContabeisCSV = new Array<InformacaoContabilCSV>();
    const lancamentoContabilRelatorioVO = new LancamentoContabilRelatorioVO;
    const IDs = new Array<number>();

    for (let index = 0; index < this.informacoesContabeis.length; index++) {
      const element = this.informacoesContabeis[index];
      IDs.push(element.id)
    }

    lancamentoContabilRelatorioVO.dataGeracao = this.dataAtualFormatada();
    lancamentoContabilRelatorioVO.listaIds = IDs;

    this.gerarAquivoServie.atualizarDataGeracaoArquivoCSV(lancamentoContabilRelatorioVO).subscribe(
      response => {
        this.informacoesContabeisCSVData = response;
        this.montaCSV(this.informacoesContabeisCSVData);
        this.buscarInformacoesContabeis(this.dataBuscada);
      }
    )
  }

  montaCSV(lista: InformacaoContabil[]) {
    console.log(lista);
    for (let index = 0; index < lista.length; index++) {
      const itemCredito = new InformacaoContabilCSV;
      const itemDebito = new InformacaoContabilCSV;
      const element = lista[index];

      if (element.contaDebito !== null && element.contaDebito !== undefined) {
        itemDebito.contaRazao = element.contaDebito;
      } else {
        itemDebito.contaRazao = 0;
      }
      if (element.valor !== null && element.valor !== undefined) {
        itemDebito.montante = element.valor;
      } else {
        itemDebito.montante = 0;
      }
      if (element.historico !== null && element.historico !== undefined) {
        itemDebito.texto = element.historico;
      } else {
        itemDebito.texto = ' ';
      }
      if (element.centroDeCusto !== null && element.centroDeCusto !== undefined && element.centroDeCusto.codigo !== null && element.centroDeCusto.codigo !== undefined) {
        itemDebito.centroDeCusto = element.centroDeCusto.codigo;
      } else {
        itemDebito.centroDeCusto = ' ';
      }
      if (element.ordem !== null && element.ordem !== undefined && element.ordem.codigo !== null && element.ordem.codigo !== undefined) {
        itemDebito.ordem = element.ordem.codigo;
      } else {
        itemDebito.ordemVazio = ' ';
      }
      itemDebito.centroDeLucro = ' ';

      if (element.elementoPep !== null && element.elementoPep !== undefined && element.elementoPep.codigo !== null && element.elementoPep.codigo !== undefined) {
        itemDebito.elementoPEP = element.elementoPep.codigo;
      } else {
        itemDebito.elementoPEP = ' ';
      }
      itemDebito.sociedadeParceira = ' ';
      itemDebito.tipoMovimento = "Z09";

      if (element.contaCredito !== null && element.contaCredito !== undefined) {
        itemCredito.contaRazao = element.contaCredito;
      } else {
        itemCredito.contaRazao = 0;
      }
      if (element.valor !== null && element.valor !== undefined) {
        itemCredito.montante = (element.valor * -1);
      } else {
        itemCredito.montante = 0;
      }
      if (element.historico !== null && element.historico !== undefined) {
        itemCredito.texto = element.historico;
      } else {
        itemCredito.texto = ' ';
      }
      if (element.centroDeCusto !== null && element.centroDeCusto !== undefined && element.centroDeCusto.codigo !== null && element.centroDeCusto.codigo !== undefined) {
        itemCredito.centroDeCusto = element.centroDeCusto.codigo;
      } else {
        itemCredito.centroDeCusto = ' ';
      }
      if (element.ordem !== null && element.ordem !== undefined && element.ordem.codigo !== null && element.ordem.codigo !== undefined) {
        itemCredito.ordem = element.ordem.codigo;
      } else {
        itemCredito.ordemVazio = ' ';
      }
      itemCredito.centroDeLucro = ' ';

      if (element.elementoPep !== null && element.elementoPep !== undefined && element.elementoPep.codigo !== null && element.elementoPep.codigo !== undefined) {
        itemCredito.elementoPEP = element.elementoPep.codigo;
      } else {
        itemCredito.elementoPEP = ' ';
      }
      itemCredito.sociedadeParceira = ' ';
      itemCredito.tipoMovimento = "Z09";

      this.informacoesContabeisCSV.push(itemDebito);
      this.informacoesContabeisCSV.push(itemCredito);

    }
    const file = new ngxCsv(this.informacoesContabeisCSV, 'Arquivo CSV_' + this.dataAtualFormatadaCSV(), this.options);
  }

  dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  dataAtualFormatadaCSV() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return mesF + anoF;
  }

}

