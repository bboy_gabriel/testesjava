import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerarArquivoCSVComponent } from './gerar-arquivo-csv.component';

describe('GerarArquivoCSVComponent', () => {
  let component: GerarArquivoCSVComponent;
  let fixture: ComponentFixture<GerarArquivoCSVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerarArquivoCSVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerarArquivoCSVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
