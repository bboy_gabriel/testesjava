import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatPaginatorIntl, MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxCurrencyModule } from "ngx-currency";
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { PaginatorText } from 'src/app/core/utils/paginator.text';
import { ComponentsComponent } from '../shared/components/components.module';
import { GerarArquivoCSVComponent } from './gerar-arquivo-csv/gerar-arquivo-csv.component';

registerLocaleData(localePt);

@NgModule({
    declarations: [
        GerarArquivoCSVComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ComponentsComponent,
        ReactiveFormsModule,
        FormsModule,
        NgxCurrencyModule,
        /* ----interface---- */
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        NgSelectModule,
        MatSelectModule,
        MatTooltipModule,
        HttpClientModule,

    ],
    providers: [
        MessagesService,
        { provide: LOCALE_ID, useValue: 'pt-BR' },
        { provide: MatPaginatorIntl, useClass: PaginatorText }
    ],
    exports: [
    ]
})
export class RelatorioComponent { }
