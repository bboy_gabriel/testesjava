import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmarAdiantamentoComponent } from './confirmar-adiantamento.component';

describe('ConfirmarAdiantamentoComponent', () => {
  let component: ConfirmarAdiantamentoComponent;
  let fixture: ComponentFixture<ConfirmarAdiantamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmarAdiantamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmarAdiantamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
