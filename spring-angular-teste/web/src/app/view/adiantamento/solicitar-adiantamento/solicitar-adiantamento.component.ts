import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProcessoService } from 'src/app/controller/services/processo/processo.service';
import { Processo } from '../../../model/processo/processo.model';
import { AdiantamentoService } from '../../../controller/services/adiantamento/adiantamento.service';
import { MessagesService } from '../../../controller/services/message/message.service';
import { TlnomService } from '../../../controller/services/tlnom/tlnom.service';
import { UserService } from '../../../controller/services/user/user.service';
import { Adiantamento } from '../../../model/adiantamento/adiantamento.model';
import { Telenom } from '../../../model/telenom.model';

@Component({
  selector: 'solicitar-adiantamento',
  templateUrl: './solicitar-adiantamento.component.html',
  styleUrls: ['./solicitar-adiantamento.component.css'],
  providers: [AdiantamentoService, ProcessoService, TlnomService]
})

export class SolicitarAdiantamentoComponent implements OnInit {

  matriculaAtual: string;
  adiantamentoForm: FormGroup;
  dataAtual: string;
  dataVencimentoLimite: string;
  processoAtual: Processo;
  telenom: Telenom;
  user: any;
  idSolicitacao: any;

  indicadorDespesa = [
    { id: '1', tipo: 'P', descricao: 'Processual' },
    { id: '2', tipo: 'A', descricao: 'Administrativa' }
  ]

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;
  isConsultar: boolean;
  textoHeaderAdiantamento: string;

  constructor(private router: Router,
    private fb: FormBuilder,
    private param: ActivatedRoute,
    private adiantamentoService: AdiantamentoService,
    private tlnomService: TlnomService,
    private message: MessagesService,
    private processoService: ProcessoService,
    private userService: UserService) {

    this.adiantamentoForm = this.fb.group({
      id: this.fb.control(''),
      matriculaSap: this.fb.control('', this.required),
      areaLotacao: this.fb.control('', this.required),
      nomeSAP: this.fb.control('', this.required),
      loginSolicitacao: this.fb.control(''),
      indicadorDespesaEnum: this.fb.control('', this.required),
      dataSolicitacao: this.fb.control('', this.required),
      dataVencimento: this.fb.control('', this.required),
      valorAdiantamento: this.fb.control('', this.required),
      motivoDespesa: this.fb.control('', this.required),
      numProcesso: this.fb.control('', this.required),
      matricula: this.fb.control('', this.required),
    })
  }

  ngOnInit() {
    this.textoHeaderAdiantamento = "Solicitação de Adiantamento";
    this.param.params.subscribe(
      res => {
        if(res.id){
          this.isConsultar = true;
          this.idSolicitacao = res.id;
          this.montarForm(this.idSolicitacao);
          this.desabilitarForm();
          this.textoHeaderAdiantamento = "Consultar Adiantamento";
        } else {
          this.isConsultar = false;
        }
      },
      error => {
        this.isConsultar = false;
        this.message.warningFormated("Falha ao consultar adiantamento");
      }
    );
    this.dataAtual = this.dataAtualFormatada();
    this.user = JSON.parse(this.userService.getUserData());
    this.adiantamentoForm.controls.matricula.setValue(this.user.matricula);
  }

  habilitaProcesso(): boolean {
    if (this.adiantamentoForm.value.indicadorDespesaEnum == 'P') {
      this.adiantamentoForm.controls.numProcesso.enable();
      return true;
    } else {
      this.adiantamentoForm.controls.numProcesso.disable();
      this.adiantamentoForm.controls.numProcesso.reset();
      return false;
    }
  }

  getDataEmissao(data) {
    if (data > this.dataAtualFormatada()) {
      this.adiantamentoForm.controls.dataSolicitacao.reset();
      this.message.error("Data de solicitação não pode ser maior que hoje.", {});
    }
  }

  getDataVencimento(data) {
    if (data < this.dataAtualFormatada()) {
      this.adiantamentoForm.controls.dataVencimento.reset();
      this.message.error("Data de vencimento não pode ser menor que hoje.", {});
    }
  }

  desabilitarForm(){
    if(this.isConsultar){
      this.adiantamentoForm.disable();
    }
  }

  dataAtualFormatada() {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return anoF + "-" + mesF + "-" + diaF;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  enviarAdiantamento() {
    let adiantamento: Adiantamento = this.adiantamentoForm.value;
    adiantamento.motivoDespesa = adiantamento.motivoDespesa.toUpperCase();
    if (adiantamento) {
      this.adiantamentoService.solicitarAdiantamento(adiantamento)
        .subscribe(response => {
          if (response.id) {
            this.message.infoFormated("Sua solicitação foi enviada com sucesso! " + "Nº Doc SAP: " + response.docSAP);
            this.router.navigate(['menu']);
          } else {
            this.message.warningFormated(response.msgErro + ".");
          }
        },
          error => {
            this.message.error("Sua solicitação não foi enviada!", error.mensagem);
          });
    }
  }

  montarForm(id : any) {
    this.adiantamentoService.consultaPorId(id).subscribe(response => {
      if(response){
        this.adiantamentoForm.patchValue(response);
      } else {
        this.message.warningFormated("Adiantamento não existe");
      }
    })
  }

  consultaProcesso(nProcesso: any): Processo {
    if (nProcesso) {
      this.processoService.consultaProcesso(nProcesso).subscribe(
        response => {
          this.processoAtual = response;
        },
        error => {
          this.message.error("Processo não encontrado no PROJUR ", error)
          this.adiantamentoForm.controls.numProcesso.reset();
        });
    } return this.processoAtual;
  }

  recuperaProcesso(): any {
    let valor: string = this.adiantamentoForm.value.numProcesso;
    if (valor != '' && valor != undefined) {
      let processo: Processo = this.consultaProcesso(valor)
    }
  }

  recuperaMatricula(): void {
    let matricula = this.adiantamentoForm.value.matriculaSap;
    if (matricula) {
      this.consultaMatricula(matricula)
    }
  }

  consultaMatricula(matricula: any): any {
    this.tlnomService.consultaMatricula(matricula).subscribe(response => {
      if (response.statusMatricula) {
        this.telenom = response;
        this.preencheTelenom();
      } else {
        this.adiantamentoForm.controls.matriculaSap.reset();
        this.adiantamentoForm.controls.nomeSAP.reset();
        this.adiantamentoForm.controls.areaLotacao.reset();
        this.message.error("Matricula não encontrada no SAP ", []);
      }

    }, error => {
      this.message.error("Matricula não encontrada no SAP ", error);
    })
  }

  preencheTelenom() {
    this.adiantamentoForm.controls.areaLotacao.setValue(this.telenom.areaLotacao);
    this.adiantamentoForm.controls.nomeSAP.setValue(this.telenom.nomeEmpregado);
    let user = JSON.parse(this.userService.getUserData());
    this.adiantamentoForm.controls.loginSolicitacao.setValue(user.username);
  }

  voltar() {
    if(this.isConsultar){
      this.router.navigate(['menu/consultar-adiantamento']);
    } else {
      this.router.navigate(['menu']);
    }
  }

}
