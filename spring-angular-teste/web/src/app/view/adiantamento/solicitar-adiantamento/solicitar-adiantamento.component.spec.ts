import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarAdiantamentoComponent } from './solicitar-adiantamento.component';

describe('SolicitarAdiantamentoComponent', () => {
  let component: SolicitarAdiantamentoComponent;
  let fixture: ComponentFixture<SolicitarAdiantamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarAdiantamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarAdiantamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
