import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarAdiantamentoComponent } from './consultar-adiantamento.component';

describe('ConfirmarAdiantamentoComponent', () => {
  let component: ConsultarAdiantamentoComponent;
  let fixture: ComponentFixture<ConsultarAdiantamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarAdiantamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarAdiantamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
