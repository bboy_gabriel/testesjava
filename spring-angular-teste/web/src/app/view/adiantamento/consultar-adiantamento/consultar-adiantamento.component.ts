import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/controller/services/user/user.service';
import { fromMatPaginator, paginateRows } from 'src/app/core/utils/data.paginator';
import { Adiantamento } from '../../../model/adiantamento/adiantamento.model';
import { error } from 'util';
import { AdiantamentoService } from '../../../controller/services/adiantamento/adiantamento.service';
import { MessagesService } from '../../../controller/services/message/message.service';

@Component({
  selector: 'consultar-adiantamento',
  templateUrl: './consultar-adiantamento.component.html',
  styleUrls: ['./consultar-adiantamento.component.css'],
  providers: [AdiantamentoService]
})

export class ConsultarAdiantamentoComponent implements OnInit {

  public confirmarSolicitacoesAdiantamento: Array<Adiantamento>;
  public confirmarSolicitacoesAdiantamentoAux: Array<Adiantamento>;
  confirmarAdiantamentoForm: FormGroup;
  response: any;
  errorMessage: any;
  statusAdiantamentoAtual: String;
  user: any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedRows$: Observable<any>;
  totalRows$: Observable<number>;

  constructor(private router: Router,
    private adiantamentoService: AdiantamentoService,
    private messageService: MessagesService,
    private fb: FormBuilder,
    private userService: UserService) {

    this.confirmarAdiantamentoForm = this.fb.group({
      matriculaSap: this.fb.control(''),
      situacaoSelect: this.fb.control(''),
    })

  }

  situacao = [
    { "id": "1", "tipoSituacao": "Aguardando Aprovação" },
    { "id": "2", "tipoSituacao": "Aprovado" },
    { "id": "3", "tipoSituacao": "Compensado" },
    { "id": "4", "tipoSituacao": "Estornado" },
    { "id": "5", "tipoSituacao": "Prestação de Contas Efetuada" },
    { "id": "6", "tipoSituacao": "Conciliado" },
  ]

  msgErro(error, msg) {
    this.messageService.error(msg, error);
  }

  ngOnInit() {
    this.user = JSON.parse(this.userService.getUserData());
    this.consultarAdiantamentos(this.user.matricula);
  }

  paginatorTable(lista): void {
    const pageEvents$: Observable<PageEvent> = fromMatPaginator(this.paginator);
    const rows$ = of(lista);
    this.totalRows$ = rows$.pipe(map(rows => rows.length));
    this.displayedRows$ = rows$.pipe(paginateRows(pageEvents$));
  }

  consultarAdiantamentos(item: ConsultarAdiantamentoComponent) {
    this.adiantamentoService.consultarAdiantamentos()
      .subscribe(response => {
        this.confirmarSolicitacoesAdiantamento = response
        this.confirmarSolicitacoesAdiantamento = this.confirmarSolicitacoesAdiantamento
        this.confirmarSolicitacoesAdiantamentoAux = this.confirmarSolicitacoesAdiantamento;
        if (this.confirmarSolicitacoesAdiantamento !== null
          && this.confirmarSolicitacoesAdiantamento !== undefined) {
          this.paginatorTable(this.confirmarSolicitacoesAdiantamento);
          if (this.confirmarSolicitacoesAdiantamento.length == 0) {
            this.messageService.error("Não foi encontrada nenhuma solicitação", error);
          }
        }
      },
        error => {
          this.messageService.error("Não foi possível consultar!", error);
        })
  }

  consultarAdiantamentoId(item: Adiantamento) {
    this.router.navigate(['menu/solicitar-adiantamento', item.id])
  }

  exibeMensagemSucesso(param) {
    if (param.statusAdiantamento.desc !== undefined) {
      if (param.statusAdiantamento.desc.toUpperCase() !== this.statusAdiantamentoAtual.toUpperCase()) {
        this.messageService.infoActionButton("Sua solicitação foi atualizada para: " + param.statusAdiantamento.desc);
      } else {
        this.messageService.success("Não houve alterações de status da sua solicitação");
      }
    } else {
      this.messageService.error("Ocorreu um erro interno", error);
    }
  }

  applyFilter() {
    var listaSolicitacoesAdiantementoFiltrada: Array<Adiantamento> = [];
    this.confirmarSolicitacoesAdiantamento = this.confirmarSolicitacoesAdiantamentoAux;
    let inValid = /\s/;

    this.confirmarSolicitacoesAdiantamento.filter(item => {
      if (this.confirmarAdiantamentoForm.value.matriculaSap !== null
        && (this.confirmarAdiantamentoForm.value.situacaoSelect == '' || this.confirmarAdiantamentoForm.value.situacaoSelect == null)) {
        if (String(item.matriculaSap).includes(
          String(this.confirmarAdiantamentoForm.value.matriculaSap))) {
          listaSolicitacoesAdiantementoFiltrada.push(item);
        }
        //Se a matrícula não estiver preenchida
        //filtra pela situação
      }

      if ((this.confirmarAdiantamentoForm.value.situacaoSelect !== '' && this.confirmarAdiantamentoForm.value.situacaoSelect !== null)
        && this.confirmarAdiantamentoForm.value.matriculaSap == '') {
        this.confirmarSolicitacoesAdiantamento = this.confirmarSolicitacoesAdiantamentoAux;
        if (String(item.statusAdiantamento).includes(
          String(this.confirmarAdiantamentoForm.value.situacaoSelect.id))) {
          listaSolicitacoesAdiantementoFiltrada.push(item);
        }
        //Se a matrícula não estiver preenchida
        //filtra pela situação
      }

      if ((this.confirmarAdiantamentoForm.value.situacaoSelect !== '' && this.confirmarAdiantamentoForm.value.situacaoSelect !== null)
        && this.confirmarAdiantamentoForm.value.matriculaSap != '') {
        this.confirmarSolicitacoesAdiantamento = this.confirmarSolicitacoesAdiantamentoAux;
        if (String(item.matriculaSap).includes(String(this.confirmarAdiantamentoForm.value.matriculaSap))
          && String(item.statusAdiantamento.id) === String(this.confirmarAdiantamentoForm.value.situacaoSelect.id)) {
          listaSolicitacoesAdiantementoFiltrada.push(item);
        }
        //Se a matrícula não estiver preenchida
        //filtra pela situação
      }

      if ((this.confirmarAdiantamentoForm.value.situacaoSelect != '' && this.confirmarAdiantamentoForm.value.situacaoSelect != null)
        && this.confirmarAdiantamentoForm.value.matriculaSap == '' && this.confirmarAdiantamentoForm.value.situacaoSelect.id !== null &&
        this.confirmarAdiantamentoForm.value.situacaoSelect.id !== null &&
        !inValid.test(String(this.confirmarAdiantamentoForm.value.situacaoSelect.id))
        && String(this.confirmarAdiantamentoForm.value.situacaoSelect.id).length != 0) {
        if (String(item.statusAdiantamento.id) === String(
          this.confirmarAdiantamentoForm.value.situacaoSelect.id)) {
          listaSolicitacoesAdiantementoFiltrada.push(item);
        }
        //Se nenhum dos campos estiverem preenchidos
        //Não filtra e rotorna a tabela 
      }
    })

    this.paginatorTable(listaSolicitacoesAdiantementoFiltrada);
    this.confirmarSolicitacoesAdiantamento = listaSolicitacoesAdiantementoFiltrada;
  }

  voltar() {
    this.router.navigate(['menu']);
  }

}
