import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoginService } from '../../controller/services/login/login.service';
import { MessagesService } from '../../controller/services/message/message.service';
import { tokenSetter, writeLocalStorge } from '../../app.module';
import { UserService } from 'src/app/controller/services/user/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  router: Router


  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private inject: Injector,
    private messagem: MessagesService,
    private userService: UserService) {

    this.router = this.inject.get(Router);
  }

  ngOnInit() {
    tokenSetter(null);
    this.loginForm = this.fb.group({
      username: this.fb.control('', [Validators.required]),
      password: this.fb.control('', [Validators.required])
    })
  }

  fazerLogin() {

    this.loginService.login(this.loginForm.value.username, this.loginForm.value.password)
      .subscribe(response => {
        const token: string = response.headers.get('Authorization');
        tokenSetter(token.replace('Bearer ', ''));
        this.userService.setUserData()
          .subscribe((data: any) => {
            writeLocalStorge('user_data', JSON.stringify(data._body));
            this.router.navigate(['menu']);
          })
      },
        err => {
          this.loginForm.controls.username.setValue(null);
          this.loginForm.controls.password.setValue(null);
        });

    
  }

}
