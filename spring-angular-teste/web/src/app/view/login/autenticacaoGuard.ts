import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { tokenGetter } from '../../app.module';
import { LoginService } from '../../controller/services/login/login.service';
import { Router } from '@angular/router';

@Injectable()
export class AutenticacaoGuard implements CanActivate {

    constructor(private autenticacao: LoginService, private router:Router) { }

    canActivate(): boolean {
        if(tokenGetter() !== null  && tokenGetter() !== undefined && tokenGetter() !== '' && tokenGetter() !== 'null'){
            return true;
        }else{
            this.router.navigate(['/'])
        }
        //return true;
    }
}