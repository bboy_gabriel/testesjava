import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule, MatSelectModule, MatTooltipModule } from '@angular/material';

import { NgxCurrencyModule } from "ngx-currency";

import { ComponentsComponent } from '../shared/components/components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { ConsultarDepComponent } from './consultar-dep-judicial/consultar-dep.component';
import { ConsultarTotalDepositadoComponent } from './consultar-total-depositado/consultar-total-depositado.component';
import { RouterModule } from '@angular/router';
import { DetalharLancamentoContabilComponent } from './detalhar-lancamento-contabil/detalhar-lancamento-contabil.component';

registerLocaleData(localePt);

@NgModule({
    declarations: [
        ConsultarDepComponent,
        ConsultarTotalDepositadoComponent,
        DetalharLancamentoContabilComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ComponentsComponent,
        ReactiveFormsModule,
        FormsModule,
        NgxCurrencyModule,
        RouterModule,
        /* ----interface---- */
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        NgSelectModule,
        MatSelectModule,
        MatTooltipModule

    ],
    providers: [
        MessagesService,
        { provide: LOCALE_ID, useValue: 'pt-BR' }
    ],
    exports: [
        ConsultarDepComponent,
        ConsultarTotalDepositadoComponent,
    ]
})
export class DepJudicialComponent { }
