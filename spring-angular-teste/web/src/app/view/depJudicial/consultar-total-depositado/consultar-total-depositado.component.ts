import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DepositoJuducialService } from 'src/app/controller/services/depositoJudicial/deposito-judicial-service';
import { DepositoJudicial } from '../../../model/deposito-judicial/depositoJudicialVO.model';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'consultar-total-depositado',
  templateUrl: './consultar-total-depositado.component.html',
  styleUrls: ['./consultar-total-depositado.component.css']
})

export class ConsultarTotalDepositadoComponent implements OnInit {
  displayedColumns: string[] = ['radio', 'dataDeposito', 'tipoDepositoJudicialEnum', 'documentoSap', 'valorDeposito'];
  totalDepositado: DepositoJudicial[];
  detalharDeposito: DepositoJudicial;
  depJudiciarioDataSource = new MatTableDataSource<DepositoJudicial>();  
  detalheForm: FormGroup;

  processo: string;
  selecionado: null;

  constructor(private router: Router,
    private routerActive: ActivatedRoute,
    private depositoJudicialService: DepositoJuducialService,
    private message: MessagesService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.detalheForm = new FormGroup(
      {
        id: new FormControl(), 
        processo: new FormControl(), 
        tipoDepositoJudicialEnum: new FormControl(), 
        codigoDepositoJudicial: new FormControl(), 
        dataDeposito: new FormControl(), 
        domicilioBancario :new FormControl(), 
        transacaoBancaria: new FormControl(), 
        valorLitigante: new FormControl(), 
        valorChesf: new FormControl(), 
        valorCorrecaoChesf: new FormControl(),
        valorDeposito: new FormControl(), 
        usuario: new FormControl(), 
        documentoSap: new FormControl()
      }
    );
    this.routerActive.params.subscribe(res => {
      this.processo = res.id;
    });
    this.depositoJudicialService.consultaTotalDepositado(this.processo).subscribe(
      response => {        
        this.totalDepositado = response;
        this.depJudiciarioDataSource = new MatTableDataSource<DepositoJudicial>(this.totalDepositado);        
        this.depJudiciarioDataSource.paginator = this.paginator;
      },
      error => {
        this.message.error("Dados não encontrados.", error)
      });
    
  }

  voltar() {
    this.router.navigate(['menu/consultar-dep/']);
  }

  onSelectionChange(data) {
    this.detalharDeposito = data;
    this.selecionado = data;
    this.detalheForm.patchValue(this.detalharDeposito);
    this.trataValoresNulos();
  }

  consultaDOC(): boolean {
    return this.detalheForm.controls.documentoSap.value === '9999999999';
  }

  trataValoresNulos(): void {
    if(this.detalheForm.controls.valorLitigante.value == null){
      this.detalheForm.controls.valorLitigante.setValue(0);
    }
    if(this.detalheForm.controls.valorChesf.value == null){
      this.detalheForm.controls.valorChesf.setValue(0);
    }
    if(this.detalheForm.controls.valorDeposito.value == null){
      this.detalheForm.controls.valorDeposito.setValue(0);
    } 
  }
}