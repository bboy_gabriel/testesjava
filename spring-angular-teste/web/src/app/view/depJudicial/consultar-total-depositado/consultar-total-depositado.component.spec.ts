import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarTotalDepositadoComponent } from './consultar-total-depositado.component';

describe('ConsultarTotalDepositadoComponent', () => {
  let component: ConsultarTotalDepositadoComponent;
  let fixture: ComponentFixture<ConsultarTotalDepositadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarTotalDepositadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarTotalDepositadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
