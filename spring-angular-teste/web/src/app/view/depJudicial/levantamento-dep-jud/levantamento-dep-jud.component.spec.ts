import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevantamentoDepJudComponent } from './levantamento-dep-jud.component';

describe('LevantamentoDepJudComponent', () => {
  let component: LevantamentoDepJudComponent;
  let fixture: ComponentFixture<LevantamentoDepJudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevantamentoDepJudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevantamentoDepJudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
