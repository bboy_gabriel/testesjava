import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DepositoJuducialService } from 'src/app/controller/services/depositoJudicial/deposito-judicial-service';
import { DepositoJudicial } from '../../../model/deposito-judicial/depositoJudicialVO.model';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { FormGroup, FormControl } from '@angular/forms';
import { InformacaoContabil } from 'src/app/model/deposito-judicial/lancamentoComtabilVO.model';

@Component({
  selector: 'detalhar-lancamento-contabil',
  templateUrl: './detalhar-lancamento-contabil.component.html',
  styleUrls: ['./detalhar-lancamento-contabil.component.css']
})
export class DetalharLancamentoContabilComponent implements OnInit {
  displayedColumns: string[] = ['radio', 'dataLancamento', 'tipoLancamento', 'valor'];
  totalLancamentoContabil: InformacaoContabil[];
  detalharInformacaoContabil: InformacaoContabil;
  lancamentoContabilDataSource = new MatTableDataSource<InformacaoContabil>();
  detalheForm: FormGroup;

  processo: string;
  selecionado: null;

  constructor(private router: Router,
    private routerActive: ActivatedRoute,
    private depositoJudicialService: DepositoJuducialService,
    private message: MessagesService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.detalheForm = new FormGroup(
      {
        id: new FormControl(),
        tipoLancamento: new FormControl(),
        numeroProcesso: new FormControl(),
        dataLancamento: new FormControl(),
        tipoLancamentoContabilDescricao: new FormControl(),
        valor: new FormControl(),
        contaCredito: new FormControl(),
        contaDebito: new FormControl(),
        centroDeCusto: new FormControl(),
        elementoPep: new FormControl(),
        ordem: new FormControl(),
        historico: new FormControl(),
        dataGeracaoCSV: new FormControl()
      }
    );
    this.routerActive.params.subscribe(res => {
      this.processo = res.id;
    });
    this.depositoJudicialService.consultaTotalProvisao(this.processo).subscribe(
      response => {
        this.totalLancamentoContabil = response;
        this.lancamentoContabilDataSource = new MatTableDataSource<InformacaoContabil>(this.totalLancamentoContabil);
        this.lancamentoContabilDataSource.paginator = this.paginator;
      },
      error => {
        this.message.error("Dados não encontrados.", error)
      });

  }

  voltar() {
    this.router.navigate(['menu/consultar-dep/']);
  }

  onSelectionChange(data) {
    this.detalharInformacaoContabil = data;
    this.detalharInformacaoContabil.elementoPep = data.elementoPep.codigo;
    this.detalharInformacaoContabil.ordem = data.ordem.codigo;
    this.detalharInformacaoContabil.centroDeCusto = data.centroDeCusto.codigo;
    this.selecionado = data;
    this.detalheForm.patchValue(this.detalharInformacaoContabil);
  }
}