import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalharLancamentoContabilComponent } from './detalhar-lancamento-contabil.component';

describe('DetalharLancamentoContabilComponent', () => {
  let component: DetalharLancamentoContabilComponent;
  let fixture: ComponentFixture<DetalharLancamentoContabilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalharLancamentoContabilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalharLancamentoContabilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
