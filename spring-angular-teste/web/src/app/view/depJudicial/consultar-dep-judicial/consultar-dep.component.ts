import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultaDepositoJudicial } from '../../../model/deposito-judicial/consultaDepositoJudicialVO.model';
import { DepositoJuducialService } from 'src/app/controller/services/depositoJudicial/deposito-judicial-service';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'consultar-dep',
  templateUrl: './consultar-dep.component.html',
  styleUrls: ['./consultar-dep.component.css'],
  providers: [DepositoJuducialService]
})
export class ConsultarDepComponent implements OnInit {

  showDepJudicial: Boolean = false;
  depositoJudicial: ConsultaDepositoJudicial;
  depositoForm: FormGroup;

  constructor(private router: Router,
    private message: MessagesService,
    private depositoJudicialService: DepositoJuducialService) { }

  ngOnInit() {
    this.depositoForm = new FormGroup(
      {
        processo: new FormControl(),
        totalDepositado: new FormControl(),
        totalPagoLitigante: new FormControl(),
        saldoDepositado: new FormControl(),
        totalProvisionado: new FormControl(),
        totalPagoChesf: new FormControl(),
      }
    );
  }

  consultarDepJud() {
    let matricula = this.depositoForm.value.processo;
    if (this.depositoForm.value.processo == null) {
      this.message.error("Forneça um processo para consultar.", {});
    }else{
      this.depositoJudicialService.consultaDepositoJudicial(matricula).subscribe(
        response => {
          this.depositoJudicial = response;
          this.depositoForm.patchValue(response);
          this.showDepJudicial = true;
        },
        error => {
          this.message.error("Processo não encontrado no PROJUR ", error)
          this.depositoForm.reset();
          this.showDepJudicial = false;
        });
    }
  }

  consultarTotalDep() {
    this.router.navigate(['menu/consultar-dep/consultar-total-depositado', this.depositoForm.value.processo])
  }

  consultarLancamentoContabil() {
    this.router.navigate(['menu/consultar-dep/detalhar-lancamento-contabil', this.depositoForm.value.processo])
  }

  voltar() {
    this.router.navigate(['menu/']);
  }

}
