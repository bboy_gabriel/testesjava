import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarDepComponent } from './consultar-dep.component';

describe('ConsultarDepComponent', () => {
  let component: ConsultarDepComponent;
  let fixture: ComponentFixture<ConsultarDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
