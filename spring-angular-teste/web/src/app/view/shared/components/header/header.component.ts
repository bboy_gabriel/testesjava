import { getTokenHeaders } from 'src/app/app.module';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/controller/services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: any = {};
  
  constructor(private userService: UserService) { }
  
  ngOnInit() {
    this.user = JSON.parse(this.userService.getUserData());
  }

  getData(){
    let data = new Date;
    return data.toLocaleDateString()
  }

  sair() {
    window.localStorage.clear();
  }

}
