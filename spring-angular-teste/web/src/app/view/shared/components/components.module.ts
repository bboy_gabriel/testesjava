import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LoginComponent } from '../../login/login.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginService } from '../../../controller/services/login/login.service';
import { AutenticacaoGuard } from '../../login/autenticacaoGuard';

@NgModule({
  declarations: [
    HomeComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatExpansionModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [LoginService, AutenticacaoGuard],
  exports: [
    HomeComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    /* ---interface--- */
    MatSidenavModule,
    MatExpansionModule
  ]
})
export class ComponentsComponent { }
