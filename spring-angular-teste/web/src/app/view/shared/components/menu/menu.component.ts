import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  showFiller = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  logoff() {
    this.router.navigate(['login']);
    window.localStorage.clear();
  }

  solicitacaoPagamento() {
    this.router.navigate(['solicitacao-pagamento']);
  }

}
