import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaRazaoLiberacaoLitiganteComponent } from './conta-razao-liberacao-litigante.component';

describe('ContaRazaoLiberacaoLitiganteComponent', () => {
  let component: ContaRazaoLiberacaoLitiganteComponent;
  let fixture: ComponentFixture<ContaRazaoLiberacaoLitiganteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaRazaoLiberacaoLitiganteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaRazaoLiberacaoLitiganteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
