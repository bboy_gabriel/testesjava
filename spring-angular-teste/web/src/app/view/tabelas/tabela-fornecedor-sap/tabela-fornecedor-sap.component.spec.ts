import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaFornecedorSapComponent } from './tabela-fornecedor-sap.component';

describe('TabelaFornecedorSapComponent', () => {
  let component: TabelaFornecedorSapComponent;
  let fixture: ComponentFixture<TabelaFornecedorSapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaFornecedorSapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaFornecedorSapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
