import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { FornecedorSapService } from './../../../controller/services/tabelas/fornecedor-sap.service';
import { TabelaGenericaComponent, exportarConfigButtons } from '../tabela-generica/tabela-generica.component';
import { FornecedorSAP } from 'src/app/model/tabelas/fornecedorsap.model';


@Component({
  selector: 'tabela-fornecedor-sap',
  templateUrl: './tabela-fornecedor-sap.component.html',
  styleUrls: ['./tabela-fornecedor-sap.component.css']
})

export class TabelaFornecedorSapComponent implements OnInit {

  tableForm: FormGroup;
  fornecedoresSAP: FornecedorSAP[] = undefined;

  isEditar: boolean = false;
  habilitaSalvar: boolean = false;
  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  botoesCrud = exportarConfigButtons(true, true, true, false);
  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private fornecedorSapService: FornecedorSapService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(8), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
      cnpj: this.fb.control('', [this.minLength(14), this.maxLength(14), Validators.required]),
    });
  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultaFornecedoreSAP();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  consultaFornecedoreSAP() {
    this.fornecedorSapService.consultaFornecedoresSAP()
      .subscribe((response: FornecedorSAP[]) => {
        this.fornecedoresSAP = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar.", {});
      });
  }

  preencheEditar() {
    return this.tableForm.setValue(this.selecionado);
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    if(this._validarFornecedorSAP(element)){
      this.fornecedorSapService.alterar(element).subscribe(() => {
        this.message.success("Atualizado com sucesso.")
        this.tabelaGene.executarCallBackAlteracao()
        this.consultaFornecedoreSAP();
        this.tableForm.reset();
      }, error => {
        this.message.error(error.error.mensagem, {})
        this.tableForm.reset();
      });
    }else{
      this.message.error("Código não pode ser menor que 1", {});
    }
  }

  salvar(item) {
    if(this._validarFornecedorSAP(item)){
      this.fornecedorSapService.cadastrar(item).subscribe(() => {
        this.message.success("Cadastrado com sucesso.")
        this.consultaFornecedoreSAP();
        this.tableForm.reset();
      }, error => {
        this.message.error(error.error.mensagem, {})
        this.tableForm.reset();
      });
    }else{
      this.message.error("Código não pode ser menor que 1", {});
    }
  }

  _validarFornecedorSAP(item){
    if(item.codigo < 1){
      return false;
    }
    return true;
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.fornecedorSapService.remover(this.selecionado.id).subscribe(() => {
          this.consultaFornecedoreSAP();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
        this.consultaFornecedoreSAP(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(
      new TableColumnConfig('', 'radio', 2),
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 2),
      new TableColumnConfig('CNPJ', 'cnpj', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaFornecedorSapEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  selecionaFornecedorSapExclusao(event: any) {
    this.selecionado = event;
    this.remover();
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }

}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}
