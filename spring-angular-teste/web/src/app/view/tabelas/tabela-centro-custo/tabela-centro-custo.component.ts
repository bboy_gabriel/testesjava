import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { CentroDeCustoService } from '../../../controller/services/tabelas/centro-de-custo.service';
import { CentroDeCusto } from '../../../model/tabelas/centroDeCusto.model';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';

@Component({
  selector: 'tabela-centro-custo',
  templateUrl: './tabela-centro-custo.component.html',
  styleUrls: ['./tabela-centro-custo.component.css']
})

export class TabelaCentroCustoComponent implements OnInit {

  tableForm: FormGroup;
  centrosDeCusto: CentroDeCusto[] = undefined;

  isEditar: boolean = false;
  habilitaSalvar: boolean = false;
  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private centroDeCustoService: CentroDeCustoService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(10), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
    })
  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultaCentros();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  consultaCentros() {
    this.centroDeCustoService.consultar()
      .subscribe(response => {
        this.centrosDeCusto = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error(error.error.mensagem, {});
      });
  }

  alterar(element) {
    if(this._validarDescricao(element)){
      this.centroDeCustoService.alterar(element).subscribe(() => {
        this.message.success("Atualizado com sucesso.")
        this.tabelaGene.executarCallBackAlteracao()
        this.consultaCentros();
        this.tableForm.reset();
      }, error => {
        this.message.error(error.error.mensagem, {});
        this.tableForm.reset();
      });
    }else{
      this.message.error("Código do centro de custo deve iniciar com H", {});
    }
  }

  salvar(item) {
    if(this._validarDescricao(item)){
      this.centroDeCustoService.cadastrar(item).subscribe(() => {
        this.message.success("Cadastrado com sucesso.")
        this.consultaCentros();
        this.tableForm.reset();
      }, error => {
        this.message.error(error.error.mensagem, {});
        this.tableForm.reset();
      });
    }else{
      this.message.error("Código do centro de custo deve iniciar com H", {});
    }
  }

  _validarDescricao(item){
    if(item.codigo.substring(0,1) !== 'H'){
      return false;
    }
    return true;
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.centroDeCustoService.remover(this.selecionado.id).subscribe(() => {
          this.consultaCentros();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
        this.consultaCentros(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(      
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 2),                 
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaCentroCustoEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  selecionaCentroCustoExclusao(event: any) {
    this.selecionado = event;
    this.remover();
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }
}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}

