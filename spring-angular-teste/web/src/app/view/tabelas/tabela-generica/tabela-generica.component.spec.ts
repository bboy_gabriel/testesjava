import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaGenericaComponent } from './tabela-generica.component';

describe('TabelaGenericaComponent', () => {
  let component: TabelaGenericaComponent;
  let fixture: ComponentFixture<TabelaGenericaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaGenericaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaGenericaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
