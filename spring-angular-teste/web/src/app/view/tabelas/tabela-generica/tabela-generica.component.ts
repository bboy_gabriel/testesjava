import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { TableColumnConfig } from '../tabela-fornecedor-sap/tabela-fornecedor-sap.component';

export class ConfigButtons {
  constructor(
    public sair: boolean = true,
    public incluir: boolean = false,
    public editar: boolean = false,
    public remover: boolean = false){
  }
} 

export function exportarConfigButtons (sair: boolean = true, incluir: boolean = true, editar: boolean = true, remover: boolean = true)  {
  return new ConfigButtons(sair, incluir, editar, remover);
}

@Component({
  selector: 'tabela-generica',
  templateUrl: './tabela-generica.component.html',
  styleUrls: ['./tabela-generica.component.css']
})
export class TabelaGenericaComponent implements OnInit {

  dataTable: MatTableDataSource<any> = new MatTableDataSource<any>([]);

  @Output()
  changeEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  removeEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  includeEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  _dados: Array<any> = new Array();
  @Input()
  set dados(dataList) {
    this._dados = dataList;
    this.dataTable = new MatTableDataSource<any>(this._dados);
    this.ngOnInit();
  }

  _tableConfig: Array<TableColumnConfig> = new Array();
  @Input()
  set tableConfig(config) {
    this._tableConfig = config;
    this.getColumnName();
  }

  @Input()
  configButtons: ConfigButtons = new ConfigButtons();
  
  columnNameList: Array<string> = new Array();
  isEditar: boolean = false;
  @Output() enviar = new EventEmitter();
 
  selecionado = null;
  incluir: any;

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  ngOnInit() {
    this.dataTable.paginator = this.paginator;
  }

  private getColumnName() {
    this.columnNameList = this._tableConfig.map(columnConfig => columnConfig.key);
  }

  capturaSelecionado(selecionado) {
    this.selecionado = selecionado;
    this._tableConfig = this._tableConfig;
    this.enviar.emit(this.selecionado);
  }

  include() {
    this.includeEvent.emit(this.incluir);  
    this.selecionado = null;
    this.changeEvent.emit({data: null, canEdit: false});  
  }

  change() {
    if (this.selecionado) {
      this.isEditar = true;
      this.changeEvent.emit({data: this.selecionado, canEdit: true});   
    }
  }

  remover() {
    if (this.selecionado) {
      this.removeEvent.emit(this.selecionado); 
      this.selecionado = null; 
    }
  }

  voltar() {
    this.router.navigate(['menu']);
  }

  executarCallBackAlteracao(): void{
    this.selecionado = null;
    this.isEditar = false;    
  }

}

