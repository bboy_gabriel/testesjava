import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaOrdemComponent } from './tabela-ordem.component';

describe('TabelaOrdemComponent', () => {
  let component: TabelaOrdemComponent;
  let fixture: ComponentFixture<TabelaOrdemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaOrdemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaOrdemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
