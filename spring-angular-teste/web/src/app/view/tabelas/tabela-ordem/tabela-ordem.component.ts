import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { Ordem } from '../../../model/tabelas/ordem.model';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { OrdemService } from './../../../controller/services/tabelas/ordem.service';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';

@Component({
  selector: 'tabela-ordem',
  templateUrl: './tabela-ordem.component.html',
  styleUrls: ['./tabela-ordem.component.css']
})

export class TabelaOrdemComponent implements OnInit {

  tableForm: FormGroup;
  ordens: Ordem[] = undefined;

  isEditar: boolean = false;
  habilitaSalvar: boolean = false;
  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private ordemService: OrdemService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(12), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
    })
  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultarOrdens();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  consultarOrdens() {
    this.ordemService.ConsultaOrdem()
      .subscribe((response: Ordem[]) => {
        this.ordens = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar.", {});
      });
  }

  preencheEditar() {
    return this.tableForm.setValue(this.selecionado);
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    this.ordemService.alterar(element).subscribe(() => {
      this.message.success("Atualizado com sucesso.")
      this.tabelaGene.executarCallBackAlteracao()
      this.consultarOrdens();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  salvar(item) {
    this.ordemService.cadastrar(item).subscribe(() => {
      this.message.success("Cadastrado com sucesso.")
      this.consultarOrdens();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.ordemService.remover(this.selecionado.id).subscribe(() => {
          this.consultarOrdens();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
          this.consultarOrdens(),
          result.dismiss === swal.DismissReason.cancel
          ) {
            swal(
              'Cancelado',
              '',
              'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(      
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaOrdemInternaEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  selecionaOrdemInternaExclusao(event: any) {
    this.selecionado = event;
    this.remover();
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }
}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}