import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaTipoDespesaComponent } from './tabela-tipo-despesa.component';

describe('TabelaTipoDespesaComponent', () => {
  let component: TabelaTipoDespesaComponent;
  let fixture: ComponentFixture<TabelaTipoDespesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaTipoDespesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaTipoDespesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
