import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { TipoDespesa } from '../../../model/tabelas/tipodespesa.model';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { TipoDespesaService } from './../../../controller/services/tabelas/tipo-despesa.service';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';

@Component({
  selector: 'tabela-tipo-despesa',
  templateUrl: './tabela-tipo-despesa.component.html',
  styleUrls: ['./tabela-tipo-despesa.component.css']
})

export class TabelaTipoDespesaComponent implements OnInit {

  tableForm: FormGroup;
  tiposDespesas: TipoDespesa[] = undefined;

  isEditar: boolean = false;
  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  indicadoresPagamento = [
    { "id": "1", "descricao": "PAGAMENTO", "codigo": "P" },
    { "id": "2", "descricao": "GARANTIA", "codigo": "G" },
  ];

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private tipoDespesaService: TipoDespesaService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(4), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
      indicadorPagamentoEnum: this.fb.control('', [Validators.required]),
    });

  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultarTiposDespesa();
  }

  consultarTiposDespesa() {
    this.tipoDespesaService.consultaTipoDespesa()
      .subscribe((response: TipoDespesa[]) => {
        this.tiposDespesas = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar.", {});
      });
  }

  preencheEditar() {
    return this.tableForm.setValue(this.selecionado);
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    this.tipoDespesaService.alterar(element).subscribe((objetoAlterado) => {
      this.message.success("Atualizado com sucesso.")
      this.tabelaGene.executarCallBackAlteracao()
      this.consultarTiposDespesa();
      this.tableForm.reset();
      this.consultarTiposDespesa();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
      this.consultarTiposDespesa();
    });
  }

  salvar(item) {
    this.tipoDespesaService.cadastrar(item).subscribe(() => {
      this.message.success("Cadastrado com sucesso.")
      this.consultarTiposDespesa();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.tipoDespesaService.remover(this.selecionado.id).subscribe(() => {
          this.consultarTiposDespesa();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
        this.consultarTiposDespesa(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(      
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 3),
      new TableColumnConfig('Indicador', 'indicadorPagamentoEnum', 2),
      new TableColumnConfig('Indicador de Processo Obrigatório', 'indicadorProcesso', 2),
      new TableColumnConfig('Indicador de Informação Contábil', 'indicadorInfoContabeis', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaTipoDespesaEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  selecionaTipoDespesaRemover(event: any) {
    this.selecionado = event;
    this.remover();
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }

}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}
