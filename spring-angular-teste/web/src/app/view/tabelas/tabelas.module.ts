import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntl, MatPaginatorModule, MatSortModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';
import { PaginatorText } from 'src/app/core/utils/paginator.text';
import { MessagesService } from '../../controller/services/message/message.service';
import { TabelaCentroCustoComponent } from './tabela-centro-custo/tabela-centro-custo.component';
import { TabelaElementoPepComponent } from './tabela-elemento-pep/tabela-elemento-pep.component';
import { TabelaFornecedorSapComponent } from './tabela-fornecedor-sap/tabela-fornecedor-sap.component';
import { TabelaGenericaComponent } from './tabela-generica/tabela-generica.component';
import { TabelaOrdemComponent } from './tabela-ordem/tabela-ordem.component';
import { TabelaOrgaoGestorComponent } from './tabela-orgao-gestor/tabela-orgao-gestor.component';
import { TabelaTipoDespesaComponent } from './tabela-tipo-despesa/tabela-tipo-despesa.component';
import { ContaRazaoProvisaoComponent } from './conta-razao-provisao/conta-razao-provisao.component';
import { ContaRazaoEstornoProvisaoComponent } from './conta-razao-estorno-provisao/conta-razao-estorno-provisao.component';
import { ContaRazaoLiberacaoLitiganteComponent } from './conta-razao-liberacao-litigante/conta-razao-liberacao-litigante.component';


@NgModule({
  declarations: [
    TabelaCentroCustoComponent,
    TabelaElementoPepComponent,
    TabelaFornecedorSapComponent,
    TabelaTipoDespesaComponent,
    TabelaOrdemComponent,
    TabelaOrgaoGestorComponent,
    TabelaGenericaComponent,
    ContaRazaoProvisaoComponent,
    ContaRazaoEstornoProvisaoComponent,
    ContaRazaoLiberacaoLitiganteComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    NgSelectModule,
    /* ----interface---- */
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTooltipModule
  ],

  providers: [
    MessagesService,
    { provide: MatPaginatorIntl, useClass: PaginatorText }
  ],

  exports: [
    TabelaCentroCustoComponent,
    TabelaElementoPepComponent,
    TabelaFornecedorSapComponent,
    TabelaTipoDespesaComponent,
    TabelaOrdemComponent,
    TabelaOrgaoGestorComponent,
  ]
})
export class TabelasModule { }
