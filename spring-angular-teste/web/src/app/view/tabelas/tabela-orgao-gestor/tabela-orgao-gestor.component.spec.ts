import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaOrgaoGestorComponent } from './tabela-orgao-gestor.component';

describe('TabelaOrgaoGestorComponent', () => {
  let component: TabelaOrgaoGestorComponent;
  let fixture: ComponentFixture<TabelaOrgaoGestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaOrgaoGestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaOrgaoGestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
