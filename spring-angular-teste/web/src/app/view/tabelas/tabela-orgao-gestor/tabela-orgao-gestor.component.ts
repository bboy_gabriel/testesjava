import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { OrgaoGestorService } from 'src/app/controller/services/tabelas/orgao-gestor.service';
import { OrgaoGestor } from '../../../model/tabelas/orgaogestor.model';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';

@Component({
  selector: 'tabela-orgao-gestor',
  templateUrl: './tabela-orgao-gestor.component.html',
  styleUrls: ['./tabela-orgao-gestor.component.css']
})

export class TabelaOrgaoGestorComponent implements OnInit {

  tableForm: FormGroup;
  orgaos: OrgaoGestor[] = undefined;

  isEditar: boolean = false;

  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;
  situacoes = [
    { "id": "1", "descricao": "Ativo", "codigo": "A" },
    { "id": "2", "descricao": "Inativo", "codigo": "I" },
  ]

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private orgaoGestorService: OrgaoGestorService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(12), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
      situacaoOrgaoGestorEnum: this.fb.control('', [Validators.required]),
    });

  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultarOrgaos();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  recebeSelecionado(resposta) {
    this.selecionado = resposta;
  }

  consultarOrgaos(): any {
    this.orgaoGestorService.ConsultaOrgaoGestor()
      .subscribe((response: OrgaoGestor[]) => {
        this.orgaos = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar.", {});
      });
  }

  preencheEditar() {
    return this.tableForm.setValue(this.selecionado);
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    this.orgaoGestorService.alterar(element).subscribe(() => {
      this.message.success("Atualizado com sucesso.")
      this.tabelaGene.executarCallBackAlteracao()
      this.consultarOrgaos();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  salvar(item) {
    this.orgaoGestorService.cadastrar(item).subscribe(() => {
      this.message.success("Cadastrado com sucesso.")
      this.consultarOrgaos();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.orgaoGestorService.remover(this.selecionado.id).subscribe(() => {
          this.consultarOrgaos();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
        this.consultarOrgaos(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(      
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 2),
      new TableColumnConfig('Situação', 'situacaoOrgaoGestorEnum', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaOrgaoGestorEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }

  selecionaOrgaoGestorRemover(event: any) {
    this.selecionado = event;
    this.remover();
  }
}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}

