import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaRazaoEstornoProvisaoComponent } from './conta-razao-estorno-provisao.component';

describe('ContaRazaoEstornoProvisaoComponent', () => {
  let component: ContaRazaoEstornoProvisaoComponent;
  let fixture: ComponentFixture<ContaRazaoEstornoProvisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaRazaoEstornoProvisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaRazaoEstornoProvisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
