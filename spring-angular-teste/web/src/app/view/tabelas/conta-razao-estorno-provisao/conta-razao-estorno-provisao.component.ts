import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorHandler } from 'src/app/app.error-handler';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import swal from 'sweetalert2';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';
import { ContaRazaoEstornoProvisaoService } from 'src/app/controller/services/tabelas/estornoProvisao.service';
import { ContaRazaoEstornoProvisao } from '../../../model/tabelas/razao-estorno-provisao.model';

@Component({
  selector: 'conta-razao-estorno-provisao',
  templateUrl: './conta-razao-estorno-provisao.component.html',
  styleUrls: ['./conta-razao-estorno-provisao.component.css']
})
export class ContaRazaoEstornoProvisaoComponent implements OnInit {

  tableForm: FormGroup;
  contasRazaoEstornoProvisao: ContaRazaoEstornoProvisao[] = undefined;

  isEditar: boolean = false;

  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  tipoProvisoes = [
    { "id": "1", "descricao": "Administrativo", "codigo": "1" },
    { "id": "2", "descricao": "Civil", "codigo": "2" },
    { "id": "3", "descricao": "Trabalhista", "codigo": "3" },
    { "id": "4", "descricao": "Ambiental", "codigo": "4" },
    { "id": "5", "descricao": "Fiscal", "codigo": "5" },
    { "id": "6", "descricao": "Fundiário", "codigo": "6" },
    { "id": "7", "descricao": "Regulatório", "codigo": "7" }
  ]

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private contaRazaoEstornoProvisaoService: ContaRazaoEstornoProvisaoService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      contaDebito: this.fb.control('', [this.minLength(1), this.maxLength(10), Validators.required]),
      contaCredito: this.fb.control('', [this.minLength(1), this.maxLength(10), Validators.required]),
      tipoProcessoProvisaoEnum: this.fb.control('', [Validators.required]),
    });

  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultarContasRazaoEstornoProvisao();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  recebeSelecionado(resposta) {
    this.selecionado = resposta;
  }

  consultarContasRazaoEstornoProvisao(): any {
    this.contaRazaoEstornoProvisaoService.consultar()
      .subscribe((response: ContaRazaoEstornoProvisao[]) => {
        this.contasRazaoEstornoProvisao = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error("Erro ao consultar.", {});
      });
  }

  preencheEditar() {
    return this.tableForm.setValue(this.selecionado);
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    this.contaRazaoEstornoProvisaoService.alterar(element).subscribe(() => {
      this.message.success("Atualizado com sucesso.")
      this.tabelaGene.executarCallBackAlteracao()
      this.consultarContasRazaoEstornoProvisao();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  salvar(item) {
    this.contaRazaoEstornoProvisaoService.cadastrar(item).subscribe(() => {
      this.message.success("Cadastrado com sucesso.")
      this.consultarContasRazaoEstornoProvisao();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.contaRazaoEstornoProvisaoService.remover(this.selecionado.id).subscribe(() => {
          this.consultarContasRazaoEstornoProvisao();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
          );
        }, error => {
          this.message.error(error.error.mensagem, {})
        });
      } else if (
        this.consultarContasRazaoEstornoProvisao(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(
      new TableColumnConfig('Conta de Débito', 'contaDebito', 2),
      new TableColumnConfig('Conta de Crédito', 'contaCredito', 2),
      new TableColumnConfig('Tipo Processo', 'tipoProcessoProvisaoEnum', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaContaRazaoEstornoProvisaoEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }

  selecionaContaRazaoEstornoProvisaoRemover(event: any) {
    this.selecionado = event;
    this.remover();
  }
}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}

