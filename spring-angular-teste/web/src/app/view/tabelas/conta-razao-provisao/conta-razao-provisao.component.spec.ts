import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaRazaoProvisaoComponent } from './conta-razao-provisao.component';

describe('ContaRazaoProvisaoComponent', () => {
  let component: ContaRazaoProvisaoComponent;
  let fixture: ComponentFixture<ContaRazaoProvisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaRazaoProvisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaRazaoProvisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
