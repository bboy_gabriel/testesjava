import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import swal from 'sweetalert2';
import { ErrorHandler } from '../../../app.error-handler';
import { ElementoPepService } from './../../../controller/services/tabelas/elemento-pep.service';
import { ElementoPEP } from '../../../model/tabelas/elementoPEP.model';
import { TabelaGenericaComponent } from '../tabela-generica/tabela-generica.component';

@Component({
  selector: 'tabela-elemento-pep',
  templateUrl: './tabela-elemento-pep.component.html',
  styleUrls: ['./tabela-elemento-pep.component.css']
})

export class TabelaElementoPepComponent implements OnInit {

  tableForm: FormGroup;
  elementosPep: ElementoPEP[] = undefined;

  isEditar: boolean = false;
  habilitaSalvar: boolean = false;
  selecionado: any
  tableConfig: Array<TableColumnConfig> = new Array();

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  @ViewChild('tabelaGene') tabelaGene: TabelaGenericaComponent;

  constructor(
    private fb: FormBuilder,
    private elementoPepService: ElementoPepService,
    private message: MessagesService) {

    this.tableForm = this.fb.group({
      id: this.fb.control(null),
      codigo: this.fb.control('', [this.minLength(1), this.maxLength(24), Validators.required]),
      descricao: this.fb.control('', [this.minLength(1), this.maxLength(50), Validators.required]),
    })
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  ngOnInit() {
    this.tableConfig = this.createTableColumns();
    this.consultarElementosPep();
  }

  consultarElementosPep() {
    this.elementoPepService.consultaElementosPEP()
      .subscribe((response: ElementoPEP[]) => {
        this.elementosPep = response;
      }, error => {
        ErrorHandler.handleError(error);
        this.message.error(error.error.mensagem, {});
      });
  }

  setAlterar() {
    this.isEditar = true;
  }

  alterar(element) {
    this.elementoPepService.alterarPorId(element).subscribe(() => {
      this.message.success("Atualizado com sucesso.")
      this.tabelaGene.executarCallBackAlteracao()
      this.consultarElementosPep();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  salvar(item) {
    this.elementoPepService.cadastrar(item).subscribe(() => {
      this.message.success("Cadastrado com sucesso.")
      this.consultarElementosPep();
      this.tableForm.reset();
    }, error => {
      this.message.error(error.error.mensagem, {})
      this.tableForm.reset();
    });
  }

  remover() {
    return swal({
      title: 'Você deseja excluir esse item ' + this.selecionado.codigo + '?',
      text: "Essa função não pode ser desfeita.",
      type: 'warning',
      position: 'center',
      showCancelButton: true,
      showConfirmButton: true,
      showCloseButton: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0078b0',
      cancelButtonColor: '#bd2130'
    }).then((result) => {
      if (result.value) {
        this.elementoPepService.remover(this.selecionado.id).subscribe(() => {
          this.consultarElementosPep();
          swal(
            'Excluido',
            'Esse item foi excluído.' + this.selecionado.codigo,
            'success',
            );
          }, error => {
            this.message.error(error.error.mensagem, {})
          });
        } else if (
        this.consultarElementosPep(),
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelado',
          '',
          'error',
        );
      }
    });
  }

  private createTableColumns() {
    return new Array(      
      new TableColumnConfig('Código', 'codigo', 2),
      new TableColumnConfig('Descrição', 'descricao', 2),
    );
  }

  incluirElement(element: any) {
    this.tableForm.reset(element)
  }

  selecionaElementoPepEdicao(event: any) {
    this.isEditar = event.canEdit;
    this.fillForm(event.data);
  }

  selecionaElementoPepExclusao(event: any) {
    this.selecionado = event;
    this.remover();
  }

  private fillForm(element: any) {
    this.tableForm.patchValue(element);
  }
}

export class TableColumnConfig {
  description: string;
  key: string;
  size: number;

  constructor(description: string, key: string, size: number) {
    this.description = description;
    this.key = key;
    this.size = size;
  }
}

