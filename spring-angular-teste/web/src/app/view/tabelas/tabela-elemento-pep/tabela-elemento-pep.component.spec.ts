import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaElementoPepComponent } from './tabela-elemento-pep.component';

describe('TabelaElementoPepComponent', () => {
  let component: TabelaElementoPepComponent;
  let fixture: ComponentFixture<TabelaElementoPepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaElementoPepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaElementoPepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
