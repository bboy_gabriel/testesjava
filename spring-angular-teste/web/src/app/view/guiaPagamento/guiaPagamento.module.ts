import { registerLocaleData, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatPaginatorIntl, MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxCurrencyModule } from "ngx-currency";
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { PaginatorText } from 'src/app/core/utils/paginator.text';
import { ComponentsComponent } from '../shared/components/components.module';
import { IncluirInformacoesComponent } from './incluir-informacoes/incluir-informacoes.component';
import { GuiaPagamentoRoutingModule } from './guia-pagamento-routing.module';

registerLocaleData(localePt);

@NgModule({
    declarations: [
        IncluirInformacoesComponent,
    ],
    imports: [
        ComponentsComponent,
        ReactiveFormsModule,
        FormsModule,
        NgxCurrencyModule,
        /* ----interface---- */
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        NgSelectModule,
        MatSelectModule,
        MatTooltipModule,
        HttpClientModule,
        GuiaPagamentoRoutingModule,
        CommonModule

    ],
    providers: [
        MessagesService,
        { provide: LOCALE_ID, useValue: 'pt-BR' },
        { provide: MatPaginatorIntl, useClass: PaginatorText }
    ],
    exports: [
        IncluirInformacoesComponent,
    ]
})
export class GuiaPagamento { }