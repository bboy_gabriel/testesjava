import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirInformacoesComponent } from './incluir-informacoes.component';

describe('IncluirInformacoesComponent', () => {
  let component: IncluirInformacoesComponent;
  let fixture: ComponentFixture<IncluirInformacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirInformacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirInformacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
