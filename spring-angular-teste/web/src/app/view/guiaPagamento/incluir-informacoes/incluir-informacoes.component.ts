import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MessagesService } from 'src/app/controller/services/message/message.service';
import { ProcessoService } from 'src/app/controller/services/processo/processo.service';
import { UserService } from 'src/app/controller/services/user/user.service';
import { AutorModel } from 'src/app/model/guiaPagamento/autor.model';
import { ReuModel } from 'src/app/model/guiaPagamento/reu.model';
import { Processo } from 'src/app/model/processo/processo.model';
import { CentroDeCusto } from 'src/app/model/tabelas/centroDeCusto.model';
import { ElementoPEP } from 'src/app/model/tabelas/elementoPEP.model';
import { Ordem } from 'src/app/model/tabelas/ordem.model';
import { OrgaoGestor } from 'src/app/model/tabelas/orgaogestor.model';
import { TipoDespesa } from 'src/app/model/tabelas/tipodespesa.model';

@Component({
  selector: 'incluir-informacoes',
  templateUrl: './incluir-informacoes.component.html',
  styleUrls: ['./incluir-informacoes.component.css']
})

export class IncluirInformacoesComponent implements OnInit {

  //Informação Contábil
  centrosDeCustos: CentroDeCusto[];
  elementosPEP: ElementoPEP[];
  ordens: Ordem[];
  orgaosGesto: OrgaoGestor[];
  tiposDespesas: TipoDespesa[];

  centro: CentroDeCusto;
  elemento: ElementoPEP;
  ordem: Ordem;

  //processos Autor e Reu
  autores: AutorModel[];
  reus: ReuModel[];

  //usuário
  userData: any;

  //Formulário
  incluirInformacoesForm: FormGroup;
  user: any;
  processoAtual: Processo;
  dataAtual: String;
  dataFutura: String;
  valorContabil: string = '0';

  //validadores
  maxLength = Validators.maxLength;
  minLength = Validators.minLength;
  required = Validators.required;

  //descrição do Depósito Judicial
  descricaoDepositoJudicial = [
    { id: '1', descricao: 'Depósito Recursal – RO' },
    { id: '2', descricao: 'Depósito Recursal – RR' },
    { id: '3', descricao: 'Depósito Recursal – AI' },
    { id: '4', descricao: 'Depósito Prévio' },
    { id: '5', descricao: 'Depósito Complementar' },
    { id: '6', descricao: 'Pagamento' },
    { id: '7', descricao: 'Honorários Periciais' },
    { id: '8', descricao: 'Honorários Advocatícios' }
  ]

  //descrição das Custas
  descricaoDasCustas = [
    { id: '1', descricao: 'Custas iniciais' },
    { id: '2', descricao: 'Custas recursais' },
    { id: '3', descricao: 'Custas Complementares' },
    { id: '4', descricao: 'Custas Finais' }
  ]

  //Autores e Reus MOC
  autoresMoc = [
    { id: '1', nome: 'Autor Teste 1', cnpjCpf: '1111' },
    { id: '2', nome: 'Autor Teste 2', cnpjCpf: '2222' },
    { id: '3', nome: 'Autor Teste 3', cnpjCpf: '3333' },
    { id: '4', nome: 'Autor Teste 4', cnpjCpf: '4444' }
  ]

  reusMoc = [
    { id: '1', nome: 'Réu Teste 1', cnpjCpf: '1111' },
    { id: '2', nome: 'Réu Teste 2', cnpjCpf: '2222' },
    { id: '3', nome: 'Réu Teste 3', cnpjCpf: '3333' },
    { id: '4', nome: 'Réu Teste 4', cnpjCpf: '4444' }
  ]

  @ViewChild(MatSort) sort: MatSort;
  constructor(private router: Router,
    private fb: FormBuilder,
    private message: MessagesService,
    private processoService: ProcessoService,
    private userService: UserService,
    private activate: ActivatedRoute) {

    //formulario
    this.incluirInformacoesForm = this.fb.group({
      id: this.fb.control(''),
      usuario: this.fb.control(null),
      //processo
      processo: this.fb.control('', [this.required]),
      processoNaJustica: this.fb.control('', [this.required]),
      matriculaSolicitante: this.fb.control('', [this.required]),
      nomeSolicitante: this.fb.control('', [this.required]),
      descricaoProcesso: this.fb.control('', [this.required]),
      //tribunal
      varaCoTribunal: this.fb.control('', [this.required]),
      autor: this.fb.control('', [this.required]),
      cpfCnpjAutor: this.fb.control('', [this.required]),
      reu: this.fb.control('', [this.required]),
      cpfCnpjReu: this.fb.control('', [this.required]),
      //data
      dataHoraSolicitacao: this.fb.control('', [this.required]),
      prazo: this.fb.control('', [this.required]),
      //informações contábeis
      orgaoGestor: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      centroDeCusto: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      elementoPep: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      ordemInterna: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      //custas
      descricaoDasCustas: this.fb.control('', [this.required]),
      tipoDeDespesaCustas: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      valorDasCustas: this.fb.control('', [this.required]),
      //depósito judicial
      descricaoDoDepositoJudicial: this.fb.control('', [this.required]),
      tipoDeDespesaDepositoJudicial: this.fb.group({
        id: this.fb.control(''),
        codigo: this.fb.control(''),
        descricao: this.fb.control(''),
      }),
      valorDepositoJudicial: this.fb.control('', [this.required]),
    });
  }

  ngOnInit() {
    this.dataAtual = this.dataAtualFormatada();
    this.dataFutura = this.dataFuturaFormatada();
    this.dataAtualFormatada();
    this.consultarServiços();
  }

  recuperarProcesso(): any {
    let valor: string = this.incluirInformacoesForm.value.processo;
    if (valor) {
      let processo: Processo = this.consultaProcesso(valor)
    }
  }

  consultaProcesso(nProcesso: any): Processo {
    if (nProcesso) {
      this.processoService.consultaProcesso(nProcesso).subscribe(
        response => {
          this.processoAtual = response;
          this.incluirInformacoesForm.controls.processo.setValue(this.processoAtual.numeroProcessoSijus.toString() +
            this.processoAtual.numeroDigVerfSijus.toString());
        },
        error => {
          this.message.error("Processo não encontrado no PROJUR ", error)
          this.incluirInformacoesForm.controls.processo.setValue('');
        });
    } return this.processoAtual;
  }

  getValorCustas(): number {
    if (this.incluirInformacoesForm.controls.valorDasCustas.value) {
      return this.incluirInformacoesForm.controls.valorDasCustas.value;
    }
    return 0;
  }

  validarValorCustas() {
    if (this.getValorCustas() <= 0) {
      this.message.info("O valor não pode ser igual ou inferior a zero");
      this.incluirInformacoesForm.controls.valorDasCustas.reset();
    }
  }

  getValorDeposito(): number {
    if (this.incluirInformacoesForm.controls.valorDepositoJudicial.value) {
      return this.incluirInformacoesForm.controls.valorDepositoJudicial.value;
    }
    return 0;
  }

  validarValorDeposito() {
    if (this.getValorDeposito() <= 0) {
      this.message.info("O valor não pode ser igual ou inferior a zero");
      this.incluirInformacoesForm.controls.valorDasCustas.reset();
    }
  }

  getDataPrazo(data) {
    if (data < this.dataAtualFormatada()) {
      this.incluirInformacoesForm.controls.prazo.reset();
      this.message.error("A data da solicitação não pode ser menor que hoje.", {});
    }
    if (data > this.dataFuturaFormatada()) {
      this.incluirInformacoesForm.controls.prazo.reset();
      this.message.error("Forneça uma data válida.", {});
    }
  }

  //data e hora
  dataAtualFormatada() {
    var data = new Date(),
      //data
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear(),
      //hora
      hora = data.getHours().toString(),
      minuto = data.getMinutes().toString();

    this.incluirInformacoesForm.controls.dataHoraSolicitacao.setValue(
      anoF + "-" + mesF + "-" + diaF + " - " + hora + ":" + minuto)
    return anoF + "-" + mesF + "-" + diaF;
  }

  //valida a data maxima
  dataFuturaFormatada() {
    return '2100' + '-' + '12' + "-" + '31';
  }

  //consultas
  consultarServiços() {
    this.consultardadosUsuario();
    this.activate.data.subscribe(response => {
      this.orgaosGesto = response.message[0];
      this.centrosDeCustos = response.message[1]
      this.elementosPEP = response.message[2];
      this.ordens = response.message[3];
      this.tiposDespesas = response.message[4]
      // this.autores = response.message[5];
      // this.reus = response.message[6];
    });
  }

  consultardadosUsuario() {
    let user = JSON.parse(this.userService.getUserData());
    this.incluirInformacoesForm.controls.nomeSolicitante.setValue(user.username);
    this.incluirInformacoesForm.controls.matriculaSolicitante.setValue(user.matricula);
  }

  changeAutor() {
    if (this.incluirInformacoesForm.controls.autor.value) {
      this.incluirInformacoesForm.controls.cpfCnpjAutor.setValue(
        this.incluirInformacoesForm.controls.autor.value.cnpjCpf)
    } else {
      this.incluirInformacoesForm.controls.autor.reset()
      this.incluirInformacoesForm.controls.cpfCnpjAutor.reset()
    }
  }

  changeReu() {
    if (this.incluirInformacoesForm.controls.reu.value) {
      this.incluirInformacoesForm.controls.cpfCnpjReu.setValue(
        this.incluirInformacoesForm.controls.reu.value.cnpjCpf)
    } else {
      this.incluirInformacoesForm.controls.reu.reset()
      this.incluirInformacoesForm.controls.cpfCnpjReu.reset()
    }
  }

  voltar() {
    this.router.navigate(['menu']);
  }

}
