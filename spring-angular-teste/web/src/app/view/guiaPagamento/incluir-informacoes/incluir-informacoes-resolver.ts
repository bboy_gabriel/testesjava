import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { GuiaPagamentoProcessosService } from 'src/app/controller/services/guiaPagamento/guiaPagamentoProcessos.service';
import { OrgaoGestorService } from 'src/app/controller/services/tabelas/orgao-gestor.service';
import { TipoDespesaService } from 'src/app/controller/services/tabelas/tipo-despesa.service';
import { CentroDeCustoService } from '../../../controller/services/tabelas/centro-de-custo.service';
import { ElementoPepService } from '../../../controller/services/tabelas/elemento-pep.service';
import { OrdemService } from '../../../controller/services/tabelas/ordem.service';

@Injectable()
export class IncluirInformacoesResolver implements Resolve<any> {

    constructor(
        private guiaPagamentoProcessosService: GuiaPagamentoProcessosService,
        private centroDeCustoService: CentroDeCustoService,
        private orgaoGestorService: OrgaoGestorService,
        private elementoPepService: ElementoPepService,
        private ordemService: OrdemService,
        private tipoDespesaService: TipoDespesaService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | any {

        const orgaosGesto = this.orgaoGestorService.ConsultaOrgaoGestor();
        const centrosDeCustos = this.centroDeCustoService.consultar();
        const elementosPEP = this.elementoPepService.consultaElementosPEP();
        const ordens = this.ordemService.ConsultaOrdem();
        const tiposDespesas = this.tipoDespesaService.consultaTipoDespesa();
        //const autores = this.guiaPagamentoProcessosService.consultarAutor();
        //const reus = this.guiaPagamentoProcessosService.consultarAutor();

        return forkJoin([orgaosGesto, centrosDeCustos, elementosPEP, ordens, tiposDespesas]);
    }

}