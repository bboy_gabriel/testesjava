import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncluirInformacoesComponent } from './incluir-informacoes/incluir-informacoes.component';


const routes: Routes = [
  { path:'', component: IncluirInformacoesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuiaPagamentoRoutingModule { }
