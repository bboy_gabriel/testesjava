package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.chesf.projurfinapi.enums.SituacaoOrgaoGestorEnum;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o VO referente ao recruso OrgaoGestor.
 *
 */
public class OrgaoGestorVO implements Serializable{

	private static final long serialVersionUID = -8431712559855854721L;

    private Long id;
	
    @NotNull(message = "Codigo nulo ou inválido!")
	private Long codigo;
	
    @NotEmpty(message = "Descrição nulo ou inválido!")
	private String descricao;
	
    @NotNull(message = "Situação orgão gestor nulo ou inválido!")
    private SituacaoOrgaoGestorEnum situacaoOrgaoGestorEnum;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the situacaoOrgaoGestorEnum
	 */
	public SituacaoOrgaoGestorEnum getSituacaoOrgaoGestorEnum() {
		return situacaoOrgaoGestorEnum;
	}

	/**
	 * @param situacaoOrgaoGestorEnum the situacaoOrgaoGestorEnum to set
	 */
	public void setSituacaoOrgaoGestorEnum(SituacaoOrgaoGestorEnum situacaoOrgaoGestorEnum) {
		this.situacaoOrgaoGestorEnum = situacaoOrgaoGestorEnum;
	}
    
}
