package br.com.chesf.projurfinapi.job;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.enums.StatusPagamentoEnum;
import br.com.chesf.projurfinapi.servico.SapServico;
import br.com.chesf.projurfinapi.servico.SolicitacaoPagamentoServico;

@Configuration
@EnableScheduling
public class JobRotina implements Serializable{
	
	private static final long serialVersionUID = -5226025339147320019L;
	
	@Autowired
	private SolicitacaoPagamentoServico solicitacaoPagamentoServico;
	
	@Autowired
	private SapServico sapServico;
	
	Logger logger = LoggerFactory.getLogger(JobRotina.class);
		
	// JOB AnalisarSeguroJob
    @Scheduled(fixedDelayString = "#{@jobConfirmaPagamento}")
	public void rotinaGerarPagamento() {
    	
    	logger.info("Iniciando o job para confirmação das solicitações de pagamento");
    	logger.info("Buscando as solicitações com status Aguardando aprovação e Aprovado. ");
    	
    	List<String> listaStatusReenvio = Arrays.asList(StatusPagamentoEnum.AGUARDANDO_APROVACAO.getId().toString(), StatusPagamentoEnum.APROVADO.getId().toString());
    	
		List<SolicitacaoPagamento> listaSolicitacaoPagamento = solicitacaoPagamentoServico.consultarSolicitacaoPagamentoStatusPagamento(listaStatusReenvio);
		logger.info("Foram retornadas " + listaSolicitacaoPagamento.size() + " solicitações.");
		for (SolicitacaoPagamento solicitacaoPagamento : listaSolicitacaoPagamento) {
			solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.getStatusPagamentoEnumPorID(new Long(solicitacaoPagamento.getCodigoStatus())));
			solicitacaoPagamentoServico.atualizarSolicitacaoPagamentoBase(sapServico.confirmaPagamento(solicitacaoPagamento));
		}
		
    	logger.info("Finalizando o job de solicitacao de pagamento.");
    	
	}    
        
}
