package br.com.chesf.projurfinapi.enums;

public enum SituacaoOrgaoGestorEnum {

	A(1, "Ativo"),
	I(2, "Inativo");
	
	private Integer id;
	private String desc;
	
	private SituacaoOrgaoGestorEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
