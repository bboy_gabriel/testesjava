package br.com.chesf.projurfinapi.servico;

import java.text.MessageFormat;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.Configuracao;

@Service
public class EmailService {	 
	
	private Logger logger = LoggerFactory.getLogger(EmailService.class);
	
	private Session session;
	
	@Autowired
	private ConfiguracaoServico configuracaoServico;
	
	
	@PostConstruct
	public void init() {
		 /* ler porta, host username, assuntoMsgSolPagamento, corpoMsgSolPagamento, chesfDomineoEmail e password da tabela tb_config
	     Properties props = new Properties();
	     props.put("mail.smtp.auth", "true");
	     props.put("mail.smtp.starttls.enable", "true");
	     props.put("mail.smtp.host", host);
	     props.put("mail.smtp.port", porta);
	     this.session = Session.getInstance(props,
	             new javax.mail.Authenticator() {
	    	 	@Override
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(username, password);
	            }
		});
		*/
	}
	
	public boolean enviarEmailSolicitacaoPagamento(String remetente, String numeroDocumentoSap) {
		Configuracao config = configuracaoServico.getDestinatarioChesf();
		if(config == null) {
			logger.info("Erro ao enviar e-mail: Destinatário não informado");
			return false;
		}
		boolean emailEnviado = this.enviarEmail(MessageFormat.format("assuntoMsgSolPagamento", numeroDocumentoSap), remetente, config.getValorPropriedade(), MessageFormat.format("corpoMsgSolPagamento", numeroDocumentoSap) );
		if(emailEnviado)
			logger.info("E-mail enviado para solicitação de pagamento. Remetente {0}, documento SAP {1}", remetente, numeroDocumentoSap);
		return emailEnviado;
	}
	
	public boolean enviarEmail(String assunto, String remetente, String destinatario, String mensagem) {
		 try {			 
	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(remetente));
	            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
	            message.setSubject(assunto);
	            message.setText(mensagem);	 
	            Transport.send(message);
	            return true;
	        } catch (MessagingException e) {
	        	logger.info("Erro ao enviar e-mail", e);
	        	return false;
	        }
	}

	public String getChesfDomineoEmail() {
		return "chesfDomineoEmail";
	}
 
}
