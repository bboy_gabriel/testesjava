package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.TipoDeDespesa;
import br.com.chesf.projurfinapi.entidade.vo.TipoDeDespesaVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.TipoDeDespesaRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer os serviços referentes ao recurso TipoDeDespesa.
 *
 */
@Service
public class TipoDeDespesaServico implements Serializable{

	private static final long serialVersionUID = 7697390730141394794L;
	
	@Autowired
	private TipoDeDespesaRepositorio tipoDeDespesaRepositorio;
	
	@Autowired
	private SolicitacaoPagamentoServico solicitacaoPagamentoServico;
	
	Logger logger = LoggerFactory.getLogger(TipoDeDespesaServico.class);
	
	private static final String TIPO_DESPESA = "Tipo de Despesa";
	private static final String CODIGO = "Código ";

	
	/**
	 * 
	 * @return List<TipoDeDespesaVO>
	 * 
	 * Metodo responsavel por consultar uma lista de recurso TipoDeDespesa, validar a lista e montar uma lista TipoDeDespesaVO.
	 * 
	 */
	public List<TipoDeDespesaVO> consultar(){
		try {
			List<TipoDeDespesa> listaBase = tipoDeDespesaRepositorio.findAll();
			validarListaTipoDeDespesaBase(listaBase);
			return montarListaTipoDeDespesaVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_TIPO_DE_DESPESA);
		}
	}
	
	/**
	 * 
	 * @return List<TipoDeDespesaVO>
	 * 
	 * Metodo responsavel por consultar uma lista de recurso TipoDeDespesa ordenada pelo codigo, validar a lista e montar uma lista TipoDeDespesaVO.
	 * 
	 */
	public List<TipoDeDespesaVO> consultarOrdenada(){
		try {
			List<TipoDeDespesa> listaBase = tipoDeDespesaRepositorio.findAll(new Sort(Sort.Direction.ASC, "codigo"));
			validarListaTipoDeDespesaBase(listaBase);
			return montarListaTipoDeDespesaVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_TIPO_DE_DESPESA);
		}
	}
	
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo privado responsavel por validar a lista de recursos TipoDeDespesa consultada.
	 * 
	 */
	private void validarListaTipoDeDespesaBase(List<TipoDeDespesa> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_TIPO_DE_DESPESA_NULA_OU_INVALIDA);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<TipoDeDespesaVO>
	 * 
	 * Metodo privado responsavel por montar uma lista TipoDeDespesaVO.
	 * 
	 */
	private List<TipoDeDespesaVO> montarListaTipoDeDespesaVO(List<TipoDeDespesa> listaBase){
		List<TipoDeDespesaVO> listaVO = new ArrayList<TipoDeDespesaVO>();
		for (TipoDeDespesa itemBase : listaBase) {
			TipoDeDespesaVO vo = new TipoDeDespesaVO();
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			vo.setIndicadorPagamentoEnum(itemBase.getIndicadorPagamentoEnum());
			vo.setIndicadorInfoContabeis(itemBase.getIndicadorInfoContabeis());
			vo.setIndicadorProcesso(itemBase.getIndicadorProcesso());
			listaVO.add(vo);
		}
		return listaVO;
	}

	
	public TipoDeDespesaVO consultarPorId(Long id) {
		Validador.validarId(id);
		TipoDeDespesa itemBase = tipoDeDespesaRepositorio.findOne(id);
		validarEntidadeBase(itemBase);
		return montarTipoDeDespesaVO(itemBase);
	}
	
	
	public TipoDeDespesaVO cadastrar(TipoDeDespesaVO tipoDeDespesaVO) {
		try {
			validarVO(tipoDeDespesaVO);
			TipoDeDespesa tipoDeDespesaBase = tipoDeDespesaRepositorio.save(montarTipoDeDespesaBase(tipoDeDespesaVO));
			validarEntidadeBase(tipoDeDespesaBase);			
			return montarTipoDeDespesaVO(tipoDeDespesaBase);
		} catch (ConstraintViolationException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_TIPO_DE_DESPESA);
		}
	}
	
	
	public TipoDeDespesaVO atualizar(TipoDeDespesaVO tipoDeDespesaVO) {
		verificarExistencia(tipoDeDespesaVO);
		try {
			//validarVO(tipoDeDespesaVO);
			TipoDeDespesa tipoDeDespesaBase = tipoDeDespesaRepositorio.save(montarTipoDeDespesaBase(tipoDeDespesaVO));
			validarEntidadeBase(tipoDeDespesaBase);			
			return montarTipoDeDespesaVO(tipoDeDespesaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_TIPO_DE_DESPESA);
		}
	}
	
	public void remover(Long id) {
		Validador.validarId(id);
		
		List<SolicitacaoPagamento> lstSolic = null;

		TipoDeDespesa tipoDeDespesa = new TipoDeDespesa();
		tipoDeDespesa.setId(id);
		lstSolic = solicitacaoPagamentoServico.consultarSolicitacaoPorTipoDeDespesa(tipoDeDespesa); 
		if(lstSolic != null && !lstSolic.isEmpty()) {
			logger.info("Tipo de Despesa: " + lstSolic.get(0).getTipoDeDespesa().getDescricao() + " está sendo usado e não pode ser excluído!");
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO, TIPO_DESPESA);
		}else {
			tipoDeDespesaRepositorio.delete(id);				
		}
	}
	
	private void verificarExistencia(TipoDeDespesaVO tipoDeDespesaVO) {
		if(tipoDeDespesaRepositorio.existsByCodigoAndIdNotIn(tipoDeDespesaVO.getCodigo(), tipoDeDespesaVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, tipoDeDespesaVO.getCodigo().toString(), TIPO_DESPESA);
		}
	}
	
	
	private TipoDeDespesa montarTipoDeDespesaBase(TipoDeDespesaVO tipoDeDespesaVO) {
		TipoDeDespesa tipoDeDespesaBase = new TipoDeDespesa();
		tipoDeDespesaBase.setId(tipoDeDespesaVO.getId());
		tipoDeDespesaBase.setCodigo(tipoDeDespesaVO.getCodigo());
		tipoDeDespesaBase.setDescricao(tipoDeDespesaVO.getDescricao());
		tipoDeDespesaBase.setIndicadorPagamentoEnum(tipoDeDespesaVO.getIndicadorPagamentoEnum());
		return tipoDeDespesaBase;
	}
	

	private TipoDeDespesaVO montarTipoDeDespesaVO(TipoDeDespesa tipoDeDespesaBase) {
		TipoDeDespesaVO tipoDeDespesaVO = new TipoDeDespesaVO();
		tipoDeDespesaVO.setId(tipoDeDespesaBase.getId());
		tipoDeDespesaVO.setCodigo(tipoDeDespesaBase.getCodigo());
		tipoDeDespesaVO.setDescricao(tipoDeDespesaBase.getDescricao());
		tipoDeDespesaVO.setIndicadorPagamentoEnum(tipoDeDespesaBase.getIndicadorPagamentoEnum());
		tipoDeDespesaVO.setIndicadorProcesso(tipoDeDespesaBase.getIndicadorProcesso());
		tipoDeDespesaVO.setIndicadorInfoContabeis(tipoDeDespesaBase.getIndicadorInfoContabeis());
		return tipoDeDespesaVO;
	}
	
	
	private void validarVO(TipoDeDespesaVO tipoDeDespesaVO) {
		if(tipoDeDespesaVO == null) throw new NegocioException(MensagemEnum.TIPO_DE_DESPESA_NULO_OU_INVALIDO);
		TipoDeDespesa tipoDeDespesaBase = tipoDeDespesaRepositorio.findByCodigo(tipoDeDespesaVO.getCodigo());
		if(tipoDeDespesaBase != null) throw new NegocioException(MensagemEnum.CODIGO_TIPO_DESPESA_JA_EXISTE_EM_BASE);
	}

	
	/**
	 * 
	 * @param entidadeBase
	 * 
	 * Metodo privado responsavel por validar o retorno base no serviço getById
	 * 
	 */
	private void validarEntidadeBase(TipoDeDespesa entidadeBase) {
		if(entidadeBase == null) throw new NegocioException(MensagemEnum.TIPO_DE_DESPESA_NULO_OU_INVALIDO);
	}
	
}