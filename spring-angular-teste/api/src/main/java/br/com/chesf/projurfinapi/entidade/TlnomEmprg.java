package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TLNOM_EMPRG", schema = "TLNOM")
@SequenceGenerator(name = "SQ_TLNOM_EMPRG", sequenceName = "SQ_TLNOM_EMPRG", allocationSize = 1)
public class TlnomEmprg implements Serializable{

	private static final long serialVersionUID = 1355493776345953356L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TLNOM_EMPRG")
	@Column(name = "PK_EMPRG")
	private Long id;
	
	@Column(name = "NM_EMPRG_COMPL", length = 50)
	private String nomeEmprgCompl;
	
	@OneToOne(targetEntity = TlnomArelo.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_ARELO", referencedColumnName = "PK_ARELO")
	private TlnomArelo arelo;
	
	@Column(name = "FK_ESCRI", length = 2)
	private Long fkEscri;                  // Depois fazer a referencia da chave estrangeira
	
	@Column(name = "DS_EMPRG_LOCAL", length = 30)
	private String descricaoEmprgLocal;  
	
	@Column(name = "NR_EMPRG_RAMA1", length = 7)
	private String numeroEmprgRamal1;
	
	@Column(name = "NR_EMPRG_RAMA2", length = 7)
	private String numeroEmprgRamal2;
	
	@Column(name = "NR_EMPRG_SIT", length = 7)
	private String numeroEmprgSit;
	
	@Column(name = "FK_FUNCA", length = 5)
	private Long funca;
	
	@Column(name = "FK_CARGO", length = 4)
	private Long cargo;
	
	@Column(name = "FK_VIEMP", length = 2)
	private Long viemp;
	
	@Column(name = "FK_STFUN", length = 3)
	private Long Stfun;
	
	@Column(name = "NM_EMPRG_ABRV", length = 21)
	private String nomeEmprgAbrev;

	@Column(name = "CD_CCOM", length = 3)
	private String codigoCCom;
	
	@Column(name = "DT_ADMS")
	private LocalDate dataAdms;
	
	@Column(name = "QT_HORA_TRBL", length = 5)
	private Long quantidadeHorasTbrl;
	
	@Column(name = "NR_CPF_CGC", length = 14)
	private Long numeroCpfCgc;
	
	@Column(name = "CD_ESTB", length = 4)
	private String codigoEstb;
	
	@Column(name = "DT_DEMS")
	private LocalDate dataDems;
	
	@Column(name = "ID_CHEF", length = 1)
	private Long idChef;
	
	@Column(name = "DS_SITC", length = 200)
	private String descricaoSitc;
	
	@Column(name = "DT_ULTM_ATLZ")
	private LocalDate dataUltmAtlz;
	
	@Column(name = "CD_EMPRR", length = 3)
	private String codigoEmprr;
	
	@Column(name = "DT_NASC")
	private LocalDate dataNascimento;
	
	@Column(name = "NR_PLAC_VECL_EMPR_1", length = 7)
	private String numeroPlacVeclEmpr1;
	
	@Column(name = "NR_PLAC_VECL_EMPR_2", length = 7)
	private String numeroPlacVeclEmpr2;
	
	@Column(name = "NR_PLAC_VECL_EMPR_3", length = 7)
	private String numeroPlacVeclEmpr3;
	
	@Column(name = "ID_PERI", length = 1)
	private String idPeri;
	
	@Column(name = "QT_ANUN", length = 2)
	private Long quantidadeAnum;
	
	@Column(name = "DT_INIC_AFST")
	private LocalDate dataInicAfst;
	
	@Column(name = "CD_TIPO_SANG", length = 2)
	private String codigoTipoSang;
	
	@Column(name = "CD_FATR_RH", length = 1)
	private String codigoFatrRh;
	
	@Column(name = "NR_IDNT", length = 15)
	private Long numeroIdnt;
	
	@Column(name = "SG_ORGA_IDNT", length = 3)
	private String siglaOrgaIdnt;
	
	@Column(name = "SG_ESTD_IDNT", length = 2)
	private String siglaEstdIdnt;
	
	@Column(name = "NR_PIS_PASEP", length = 11)
	private Long numeroPisPasep;
	
	@Column(name = "NR_CARE_MOTR", length = 9)
	private Long numeroCareMotr;
	
	@Column(name = "NR_REGS_CARE_MOTR", length = 11)
	private Long numeroRegsCareMotr;
	
	@Column(name = "SG_CATG_CARE_MOTR", length = 3)
	private String siglaCatgCareMotr;
	
	@Column(name = "SG_ESTD_CARE_MOTR", length = 2)
	private String siglaEstdCareMotr;
	
	@Column(name = "NR_TITL_ELIT", length = 15)
	private Long numeroTitlElit;
	
	@Column(name = "NR_ZONA_TITL_ELIT", length = 4)
	private Long numeroZonaTitlElit;
	
	@Column(name = "NR_SECA_ELIT", length = 4)
	private Long numeroSecaElit;
	
	@Column(name = "SG_ESTD_TITL_ELIT", length = 2)
	private String siglaEstdTitlElit;
	
	@Column(name = "NR_RESR", length = 12)
	private Long numeroResr;
	
	@Column(name = "NR_REGI_MILT_RESR", length = 2)
	private Long numeroRegiMiltResr;
	
	@Column(name = "CD_CATG_RESR", length = 1)
	private Long codigoCatgResr;
	
	@Column(name = "SG_ESTD_RESR", length = 2)
	private String siglaEstdResr;
	
	@Column(name = "SG_ARMA_RESR", length = 1)
	private String siglaArmaResr;
	
	@Column(name = "NR_CARE_PRFS", length = 7)
	private Long numeroCarePrfs; 
	
	@Column(name = "NR_SERI_PRFS", length = 5)
	private Long numeroSeriPrfs;
	
	@Column(name = "SG_ESTD_CARE_PRFS", length = 2)
	private String siglaEstdCarePrfs;
	
	@Column(name = "NR_CONH_PRFS", length = 8)
	private String numeroConhPrfs;
	
	@Column(name = "SG_ORGA_CONH_PRFS", length = 7)
	private String siglaOrgaConhPrfs;
	
	@Column(name = "DT_REGS_CONH_PRFS")
	private LocalDate dataRegsConhPrfs;
	
	@Column(name = "NR_CPF", length = 11)
	private Long numeroCPF;
	
	@Column(name = "CD_VINL_EMPR", length = 2)
	private Long codigoVinilEmpr;
	
	@Column(name = "NM_LOGR_RESC", length = 33)
	private String nomeLogrResc;
	
	@Column(name = "NR_IMVL_RESC", length = 5)
	private String numeroImvlResc;
	
	@Column(name = "DS_COME_RESC", length = 8)
	private String descricaoComeResc;
	
	@Column(name = "NM_BARR_RESC", length = 15)
	private String nomeBarrResc;
	
	@Column(name = "NM_CIDD_RESC", length = 20)
	private String nomeCiddResc;
	
	@Column(name = "SG_ESTD_RESC", length = 2)
	private String siglaEstdResc;
	
	@Column(name = "NR_CEP_RESC", length = 8)
	private Long numeroCepResc;
	
	@Column(name = "NR_FONE_RESC", length = 15)
	private String numeroFoneResc;
	
	@Column(name = "DT_ULTM_RELT")
	private LocalDate dataUltmRelt;
	
	@Column(name = "NM_LOGN_REDE", length = 8)
	private String nomeLognRede;
	
	@Column(name = "CD_PLAN_ENQD_SALR", length = 1)
	private Long codigoPlanEnqdSalr;
	
	@Column(name = "CD_REGI", length = 1)
	private String codigoRegi;
	
	@Column(name = "CD_CLAS", length = 2)
	private Long codigoClas;
	
	@Column(name = "CD_NIVL", length = 3)
	private Long codigoNivl;
	
	@Column(name = "DS_HIER", length = 30)
	private String descricaoHier;
	
	@Column(name = "ID_TIPO_USUR")
	private Long idTipoUser;
	
	@Column(name = "NM_EMPR", length = 50)
	private String nomeEmpr;
	
	@Column(name = "NM_LOGN_CHEF", length = 8)
	private String nomeLognChef;
	
	@Column(name = "ID_CRIA_AD", length = 1)
	private String idCriaAD;
	
	@Column(name = "ID_SEXO", length = 1)
	private String idSexo;
	
	@Column(name = "DT_VENC_ASO")
	private LocalDate dataVencAso;
	
	@Column(name = "DS_MOTV_INVS", length = 25)
	private String descricaoMotvInvs;
	
	@Column(name = "SG_AREA_NIVEL1", length = 6)
	private String siglaAreaNivel1;
	
	@Column(name = "SG_AREA_NIVEL2", length = 6)
	private String siglaAreaNivel2;
	
	@Column(name = "SG_AREA_NIVEL3", length = 6)
	private String siglaAreaNivel3;
	
	@Column(name = "SG_AREA_NIVEL4", length = 6)
	private String siglaAreaNivel4;
	
	@Column(name = "DT_VIGN_CARGO")
	private LocalDate dataVignCargo;
	
	@Column(name = "DT_VIGN_FUNCAO")
	private LocalDate datavignFuncao;
	
	@Column(name = "DT_INIC_CCOM")
	private LocalDate dataInicCcom;
	
	@Column(name = "DT_FIM_CCOM")
	private LocalDate dataFimCcom;
	
	@Column(name = "CD_MOTV_CCOM", length = 3)
	private Long codigoMotvCcom;
	
	@Column(name = "CD_MOTV_AFST", length = 3)
	private Long codigoMotvAfst;
	
	@Column(name = "DT_INIC_SITFUN")
	private LocalDate dataInicSitfun;
	
	@Column(name = "DT_FIM_SITFUN")
	private LocalDate dataFimSitfun;
	
	@Column(name = "DT_INIC_GOZO_FERI")
	private LocalDate dataInicGozoFeri;
	
	@Column(name = "DT_FIM_GOZO_FERI")
	private LocalDate dataFimGozoFeri;
	
	@Column(name = "SG_ORGA_INVTD_INTRN", length = 6)
	private String siglaInvtdInTrn;
	
	@Column(name = "CD_INVTD_INTRN", length = 3)
	private String codigoInvtdIntrn;
	
	@Column(name = "DT_INIC_INVTD_INTRN")
	private LocalDate dataInicInvtdIntrn;
	
	@Column(name = "DT_FIM_INVTD_INTRN")
	private LocalDate dataFimInvtdIntrn;
	
	@Column(name = "SG_ORGA_INVTD_INTRN_EXCP", length = 6)
	private String siglaOrgaInvtdIntrnExcp;
	
	@Column(name = "CD_INVTD_INTRN_EXCP", length = 3)
	private String codigoInvtdIntrnExcp;
	
	@Column(name = "DT_INIC_INVTD_INTRN_EXCP")
	private LocalDate dataInicInvtdInternExcp;
	
	@Column(name = "DT_FIM_INVTD_INTRN_EXCP")
	private LocalDate dataFimInvtdIntrnExcp;
	
	@Column(name = "SG_ORGA_SUBI", length = 6)
	private String siglaOrgaSubi;
	
	@Column(name = "CD_INVTD_SUBI", length = 3)
	private String codigoInvtdSubi;
	
	@Column(name = "DT_INIC_SUBI")
	private LocalDate dataInicSubi;
	
	@Column(name = "DT_FIM_SUBI")
	private LocalDate dataFimSubi;
	
	@Column(name = "COD_EIXO", length = 10)
	private Long codigoEixo;
	
	@Column(name = "EIXO_DESC", length = 100)
	private String eixoDec;
	
	@Column(name = "COD_MACRO_PROCESSO", length = 10)
	private Long codigoMacroProcesso;
	
	@Column(name = "MACRO_PROCESSO", length = 100)
	private String macroProcesso;
	
	@Column(name = "COD_PROCESSO", length = 10)
	private Long codigoProcesso;
	
	@Column(name = "PROCESSO", length = 100)
	private String processo;
	
	@Column(name = "COD_FORMACAO", length = 10)
	private Long codigoFormacao;
	
	@Column(name = "FORMACAO", length = 100)
	private String formacao;
	
	@Column(name = "DT_FIM_AFST")
	private LocalDate dataFimAfst;
	
	@Column(name = "ID_SAP", length = 8)
	private String idSap;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nomeEmprgCompl
	 */
	public String getNomeEmprgCompl() {
		return nomeEmprgCompl;
	}

	/**
	 * @param nomeEmprgCompl the nomeEmprgCompl to set
	 */
	public void setNomeEmprgCompl(String nomeEmprgCompl) {
		this.nomeEmprgCompl = nomeEmprgCompl;
	}

	/**
	 * @return the arelo
	 */
	public TlnomArelo getArelo() {
		return arelo;
	}

	/**
	 * @param arelo the arelo to set
	 */
	public void setArelo(TlnomArelo arelo) {
		this.arelo = arelo;
	}

	/**
	 * @return the fkEscri
	 */
	public Long getFkEscri() {
		return fkEscri;
	}

	/**
	 * @param fkEscri the fkEscri to set
	 */
	public void setFkEscri(Long fkEscri) {
		this.fkEscri = fkEscri;
	}

	/**
	 * @return the descricaoEmprgLocal
	 */
	public String getDescricaoEmprgLocal() {
		return descricaoEmprgLocal;
	}

	/**
	 * @param descricaoEmprgLocal the descricaoEmprgLocal to set
	 */
	public void setDescricaoEmprgLocal(String descricaoEmprgLocal) {
		this.descricaoEmprgLocal = descricaoEmprgLocal;
	}

	/**
	 * @return the numeroEmprgRamal1
	 */
	public String getNumeroEmprgRamal1() {
		return numeroEmprgRamal1;
	}

	/**
	 * @param numeroEmprgRamal1 the numeroEmprgRamal1 to set
	 */
	public void setNumeroEmprgRamal1(String numeroEmprgRamal1) {
		this.numeroEmprgRamal1 = numeroEmprgRamal1;
	}

	/**
	 * @return the numeroEmprgRamal2
	 */
	public String getNumeroEmprgRamal2() {
		return numeroEmprgRamal2;
	}

	/**
	 * @param numeroEmprgRamal2 the numeroEmprgRamal2 to set
	 */
	public void setNumeroEmprgRamal2(String numeroEmprgRamal2) {
		this.numeroEmprgRamal2 = numeroEmprgRamal2;
	}

	/**
	 * @return the numeroEmprgSit
	 */
	public String getNumeroEmprgSit() {
		return numeroEmprgSit;
	}

	/**
	 * @param numeroEmprgSit the numeroEmprgSit to set
	 */
	public void setNumeroEmprgSit(String numeroEmprgSit) {
		this.numeroEmprgSit = numeroEmprgSit;
	}

	/**
	 * @return the funca
	 */
	public Long getFunca() {
		return funca;
	}

	/**
	 * @param funca the funca to set
	 */
	public void setFunca(Long funca) {
		this.funca = funca;
	}

	/**
	 * @return the cargo
	 */
	public Long getCargo() {
		return cargo;
	}

	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	/**
	 * @return the viemp
	 */
	public Long getViemp() {
		return viemp;
	}

	/**
	 * @param viemp the viemp to set
	 */
	public void setViemp(Long viemp) {
		this.viemp = viemp;
	}

	/**
	 * @return the stfun
	 */
	public Long getStfun() {
		return Stfun;
	}

	/**
	 * @param stfun the stfun to set
	 */
	public void setStfun(Long stfun) {
		Stfun = stfun;
	}

	/**
	 * @return the nomeEmprgAbrev
	 */
	public String getNomeEmprgAbrev() {
		return nomeEmprgAbrev;
	}

	/**
	 * @param nomeEmprgAbrev the nomeEmprgAbrev to set
	 */
	public void setNomeEmprgAbrev(String nomeEmprgAbrev) {
		this.nomeEmprgAbrev = nomeEmprgAbrev;
	}

	/**
	 * @return the codigoCCom
	 */
	public String getCodigoCCom() {
		return codigoCCom;
	}

	/**
	 * @param codigoCCom the codigoCCom to set
	 */
	public void setCodigoCCom(String codigoCCom) {
		this.codigoCCom = codigoCCom;
	}

	/**
	 * @return the dataAdms
	 */
	public LocalDate getDataAdms() {
		return dataAdms;
	}

	/**
	 * @param dataAdms the dataAdms to set
	 */
	public void setDataAdms(LocalDate dataAdms) {
		this.dataAdms = dataAdms;
	}

	/**
	 * @return the quantidadeHorasTbrl
	 */
	public Long getQuantidadeHorasTbrl() {
		return quantidadeHorasTbrl;
	}

	/**
	 * @param quantidadeHorasTbrl the quantidadeHorasTbrl to set
	 */
	public void setQuantidadeHorasTbrl(Long quantidadeHorasTbrl) {
		this.quantidadeHorasTbrl = quantidadeHorasTbrl;
	}

	/**
	 * @return the numeroCpfCgc
	 */
	public Long getNumeroCpfCgc() {
		return numeroCpfCgc;
	}

	/**
	 * @param numeroCpfCgc the numeroCpfCgc to set
	 */
	public void setNumeroCpfCgc(Long numeroCpfCgc) {
		this.numeroCpfCgc = numeroCpfCgc;
	}

	/**
	 * @return the codigoEstb
	 */
	public String getCodigoEstb() {
		return codigoEstb;
	}

	/**
	 * @param codigoEstb the codigoEstb to set
	 */
	public void setCodigoEstb(String codigoEstb) {
		this.codigoEstb = codigoEstb;
	}

	/**
	 * @return the dataDems
	 */
	public LocalDate getDataDems() {
		return dataDems;
	}

	/**
	 * @param dataDems the dataDems to set
	 */
	public void setDataDems(LocalDate dataDems) {
		this.dataDems = dataDems;
	}

	/**
	 * @return the idChef
	 */
	public Long getIdChef() {
		return idChef;
	}

	/**
	 * @param idChef the idChef to set
	 */
	public void setIdChef(Long idChef) {
		this.idChef = idChef;
	}

	/**
	 * @return the descricaoSitc
	 */
	public String getDescricaoSitc() {
		return descricaoSitc;
	}

	/**
	 * @param descricaoSitc the descricaoSitc to set
	 */
	public void setDescricaoSitc(String descricaoSitc) {
		this.descricaoSitc = descricaoSitc;
	}

	/**
	 * @return the dataUltmAtlz
	 */
	public LocalDate getDataUltmAtlz() {
		return dataUltmAtlz;
	}

	/**
	 * @param dataUltmAtlz the dataUltmAtlz to set
	 */
	public void setDataUltmAtlz(LocalDate dataUltmAtlz) {
		this.dataUltmAtlz = dataUltmAtlz;
	}

	/**
	 * @return the codigoEmprr
	 */
	public String getCodigoEmprr() {
		return codigoEmprr;
	}

	/**
	 * @param codigoEmprr the codigoEmprr to set
	 */
	public void setCodigoEmprr(String codigoEmprr) {
		this.codigoEmprr = codigoEmprr;
	}

	/**
	 * @return the dataNascimento
	 */
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	/**
	 * @param dataNascimento the dataNascimento to set
	 */
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	/**
	 * @return the numeroPlacVeclEmpr1
	 */
	public String getNumeroPlacVeclEmpr1() {
		return numeroPlacVeclEmpr1;
	}

	/**
	 * @param numeroPlacVeclEmpr1 the numeroPlacVeclEmpr1 to set
	 */
	public void setNumeroPlacVeclEmpr1(String numeroPlacVeclEmpr1) {
		this.numeroPlacVeclEmpr1 = numeroPlacVeclEmpr1;
	}

	/**
	 * @return the numeroPlacVeclEmpr2
	 */
	public String getNumeroPlacVeclEmpr2() {
		return numeroPlacVeclEmpr2;
	}

	/**
	 * @param numeroPlacVeclEmpr2 the numeroPlacVeclEmpr2 to set
	 */
	public void setNumeroPlacVeclEmpr2(String numeroPlacVeclEmpr2) {
		this.numeroPlacVeclEmpr2 = numeroPlacVeclEmpr2;
	}

	/**
	 * @return the numeroPlacVeclEmpr3
	 */
	public String getNumeroPlacVeclEmpr3() {
		return numeroPlacVeclEmpr3;
	}

	/**
	 * @param numeroPlacVeclEmpr3 the numeroPlacVeclEmpr3 to set
	 */
	public void setNumeroPlacVeclEmpr3(String numeroPlacVeclEmpr3) {
		this.numeroPlacVeclEmpr3 = numeroPlacVeclEmpr3;
	}

	/**
	 * @return the idPeri
	 */
	public String getIdPeri() {
		return idPeri;
	}

	/**
	 * @param idPeri the idPeri to set
	 */
	public void setIdPeri(String idPeri) {
		this.idPeri = idPeri;
	}

	/**
	 * @return the quantidadeAnum
	 */
	public Long getQuantidadeAnum() {
		return quantidadeAnum;
	}

	/**
	 * @param quantidadeAnum the quantidadeAnum to set
	 */
	public void setQuantidadeAnum(Long quantidadeAnum) {
		this.quantidadeAnum = quantidadeAnum;
	}

	/**
	 * @return the dataInicAfst
	 */
	public LocalDate getDataInicAfst() {
		return dataInicAfst;
	}

	/**
	 * @param dataInicAfst the dataInicAfst to set
	 */
	public void setDataInicAfst(LocalDate dataInicAfst) {
		this.dataInicAfst = dataInicAfst;
	}

	/**
	 * @return the codigoTipoSang
	 */
	public String getCodigoTipoSang() {
		return codigoTipoSang;
	}

	/**
	 * @param codigoTipoSang the codigoTipoSang to set
	 */
	public void setCodigoTipoSang(String codigoTipoSang) {
		this.codigoTipoSang = codigoTipoSang;
	}

	/**
	 * @return the codigoFatrRh
	 */
	public String getCodigoFatrRh() {
		return codigoFatrRh;
	}

	/**
	 * @param codigoFatrRh the codigoFatrRh to set
	 */
	public void setCodigoFatrRh(String codigoFatrRh) {
		this.codigoFatrRh = codigoFatrRh;
	}

	/**
	 * @return the numeroIdnt
	 */
	public Long getNumeroIdnt() {
		return numeroIdnt;
	}

	/**
	 * @param numeroIdnt the numeroIdnt to set
	 */
	public void setNumeroIdnt(Long numeroIdnt) {
		this.numeroIdnt = numeroIdnt;
	}

	/**
	 * @return the siglaOrgaIdnt
	 */
	public String getSiglaOrgaIdnt() {
		return siglaOrgaIdnt;
	}

	/**
	 * @param siglaOrgaIdnt the siglaOrgaIdnt to set
	 */
	public void setSiglaOrgaIdnt(String siglaOrgaIdnt) {
		this.siglaOrgaIdnt = siglaOrgaIdnt;
	}

	/**
	 * @return the siglaEstdIdnt
	 */
	public String getSiglaEstdIdnt() {
		return siglaEstdIdnt;
	}

	/**
	 * @param siglaEstdIdnt the siglaEstdIdnt to set
	 */
	public void setSiglaEstdIdnt(String siglaEstdIdnt) {
		this.siglaEstdIdnt = siglaEstdIdnt;
	}

	/**
	 * @return the numeroPisPasep
	 */
	public Long getNumeroPisPasep() {
		return numeroPisPasep;
	}

	/**
	 * @param numeroPisPasep the numeroPisPasep to set
	 */
	public void setNumeroPisPasep(Long numeroPisPasep) {
		this.numeroPisPasep = numeroPisPasep;
	}

	/**
	 * @return the numeroCareMotr
	 */
	public Long getNumeroCareMotr() {
		return numeroCareMotr;
	}

	/**
	 * @param numeroCareMotr the numeroCareMotr to set
	 */
	public void setNumeroCareMotr(Long numeroCareMotr) {
		this.numeroCareMotr = numeroCareMotr;
	}

	/**
	 * @return the numeroRegsCareMotr
	 */
	public Long getNumeroRegsCareMotr() {
		return numeroRegsCareMotr;
	}

	/**
	 * @param numeroRegsCareMotr the numeroRegsCareMotr to set
	 */
	public void setNumeroRegsCareMotr(Long numeroRegsCareMotr) {
		this.numeroRegsCareMotr = numeroRegsCareMotr;
	}

	/**
	 * @return the siglaCatgCareMotr
	 */
	public String getSiglaCatgCareMotr() {
		return siglaCatgCareMotr;
	}

	/**
	 * @param siglaCatgCareMotr the siglaCatgCareMotr to set
	 */
	public void setSiglaCatgCareMotr(String siglaCatgCareMotr) {
		this.siglaCatgCareMotr = siglaCatgCareMotr;
	}

	/**
	 * @return the siglaEstdCareMotr
	 */
	public String getSiglaEstdCareMotr() {
		return siglaEstdCareMotr;
	}

	/**
	 * @param siglaEstdCareMotr the siglaEstdCareMotr to set
	 */
	public void setSiglaEstdCareMotr(String siglaEstdCareMotr) {
		this.siglaEstdCareMotr = siglaEstdCareMotr;
	}

	/**
	 * @return the numeroTitlElit
	 */
	public Long getNumeroTitlElit() {
		return numeroTitlElit;
	}

	/**
	 * @param numeroTitlElit the numeroTitlElit to set
	 */
	public void setNumeroTitlElit(Long numeroTitlElit) {
		this.numeroTitlElit = numeroTitlElit;
	}

	/**
	 * @return the numeroZonaTitlElit
	 */
	public Long getNumeroZonaTitlElit() {
		return numeroZonaTitlElit;
	}

	/**
	 * @param numeroZonaTitlElit the numeroZonaTitlElit to set
	 */
	public void setNumeroZonaTitlElit(Long numeroZonaTitlElit) {
		this.numeroZonaTitlElit = numeroZonaTitlElit;
	}

	/**
	 * @return the numeroSecaElit
	 */
	public Long getNumeroSecaElit() {
		return numeroSecaElit;
	}

	/**
	 * @param numeroSecaElit the numeroSecaElit to set
	 */
	public void setNumeroSecaElit(Long numeroSecaElit) {
		this.numeroSecaElit = numeroSecaElit;
	}

	/**
	 * @return the siglaEstdTitlElit
	 */
	public String getSiglaEstdTitlElit() {
		return siglaEstdTitlElit;
	}

	/**
	 * @param siglaEstdTitlElit the siglaEstdTitlElit to set
	 */
	public void setSiglaEstdTitlElit(String siglaEstdTitlElit) {
		this.siglaEstdTitlElit = siglaEstdTitlElit;
	}

	/**
	 * @return the numeroResr
	 */
	public Long getNumeroResr() {
		return numeroResr;
	}

	/**
	 * @param numeroResr the numeroResr to set
	 */
	public void setNumeroResr(Long numeroResr) {
		this.numeroResr = numeroResr;
	}

	/**
	 * @return the numeroRegiMiltResr
	 */
	public Long getNumeroRegiMiltResr() {
		return numeroRegiMiltResr;
	}

	/**
	 * @param numeroRegiMiltResr the numeroRegiMiltResr to set
	 */
	public void setNumeroRegiMiltResr(Long numeroRegiMiltResr) {
		this.numeroRegiMiltResr = numeroRegiMiltResr;
	}

	/**
	 * @return the codigoCatgResr
	 */
	public Long getCodigoCatgResr() {
		return codigoCatgResr;
	}

	/**
	 * @param codigoCatgResr the codigoCatgResr to set
	 */
	public void setCodigoCatgResr(Long codigoCatgResr) {
		this.codigoCatgResr = codigoCatgResr;
	}

	/**
	 * @return the siglaEstdResr
	 */
	public String getSiglaEstdResr() {
		return siglaEstdResr;
	}

	/**
	 * @param siglaEstdResr the siglaEstdResr to set
	 */
	public void setSiglaEstdResr(String siglaEstdResr) {
		this.siglaEstdResr = siglaEstdResr;
	}

	/**
	 * @return the siglaArmaResr
	 */
	public String getSiglaArmaResr() {
		return siglaArmaResr;
	}

	/**
	 * @param siglaArmaResr the siglaArmaResr to set
	 */
	public void setSiglaArmaResr(String siglaArmaResr) {
		this.siglaArmaResr = siglaArmaResr;
	}

	/**
	 * @return the numeroCarePrfs
	 */
	public Long getNumeroCarePrfs() {
		return numeroCarePrfs;
	}

	/**
	 * @param numeroCarePrfs the numeroCarePrfs to set
	 */
	public void setNumeroCarePrfs(Long numeroCarePrfs) {
		this.numeroCarePrfs = numeroCarePrfs;
	}

	/**
	 * @return the numeroSeriPrfs
	 */
	public Long getNumeroSeriPrfs() {
		return numeroSeriPrfs;
	}

	/**
	 * @param numeroSeriPrfs the numeroSeriPrfs to set
	 */
	public void setNumeroSeriPrfs(Long numeroSeriPrfs) {
		this.numeroSeriPrfs = numeroSeriPrfs;
	}

	/**
	 * @return the siglaEstdCarePrfs
	 */
	public String getSiglaEstdCarePrfs() {
		return siglaEstdCarePrfs;
	}

	/**
	 * @param siglaEstdCarePrfs the siglaEstdCarePrfs to set
	 */
	public void setSiglaEstdCarePrfs(String siglaEstdCarePrfs) {
		this.siglaEstdCarePrfs = siglaEstdCarePrfs;
	}

	/**
	 * @return the numeroConhPrfs
	 */
	public String getNumeroConhPrfs() {
		return numeroConhPrfs;
	}

	/**
	 * @param numeroConhPrfs the numeroConhPrfs to set
	 */
	public void setNumeroConhPrfs(String numeroConhPrfs) {
		this.numeroConhPrfs = numeroConhPrfs;
	}

	/**
	 * @return the siglaOrgaConhPrfs
	 */
	public String getSiglaOrgaConhPrfs() {
		return siglaOrgaConhPrfs;
	}

	/**
	 * @param siglaOrgaConhPrfs the siglaOrgaConhPrfs to set
	 */
	public void setSiglaOrgaConhPrfs(String siglaOrgaConhPrfs) {
		this.siglaOrgaConhPrfs = siglaOrgaConhPrfs;
	}

	/**
	 * @return the dataRegsConhPrfs
	 */
	public LocalDate getDataRegsConhPrfs() {
		return dataRegsConhPrfs;
	}

	/**
	 * @param dataRegsConhPrfs the dataRegsConhPrfs to set
	 */
	public void setDataRegsConhPrfs(LocalDate dataRegsConhPrfs) {
		this.dataRegsConhPrfs = dataRegsConhPrfs;
	}

	/**
	 * @return the numeroCPF
	 */
	public Long getNumeroCPF() {
		return numeroCPF;
	}

	/**
	 * @param numeroCPF the numeroCPF to set
	 */
	public void setNumeroCPF(Long numeroCPF) {
		this.numeroCPF = numeroCPF;
	}

	/**
	 * @return the codigoVinilEmpr
	 */
	public Long getCodigoVinilEmpr() {
		return codigoVinilEmpr;
	}

	/**
	 * @param codigoVinilEmpr the codigoVinilEmpr to set
	 */
	public void setCodigoVinilEmpr(Long codigoVinilEmpr) {
		this.codigoVinilEmpr = codigoVinilEmpr;
	}

	/**
	 * @return the nomeLogrResc
	 */
	public String getNomeLogrResc() {
		return nomeLogrResc;
	}

	/**
	 * @param nomeLogrResc the nomeLogrResc to set
	 */
	public void setNomeLogrResc(String nomeLogrResc) {
		this.nomeLogrResc = nomeLogrResc;
	}

	/**
	 * @return the numeroImvlResc
	 */
	public String getNumeroImvlResc() {
		return numeroImvlResc;
	}

	/**
	 * @param numeroImvlResc the numeroImvlResc to set
	 */
	public void setNumeroImvlResc(String numeroImvlResc) {
		this.numeroImvlResc = numeroImvlResc;
	}

	/**
	 * @return the descricaoComeResc
	 */
	public String getDescricaoComeResc() {
		return descricaoComeResc;
	}

	/**
	 * @param descricaoComeResc the descricaoComeResc to set
	 */
	public void setDescricaoComeResc(String descricaoComeResc) {
		this.descricaoComeResc = descricaoComeResc;
	}

	/**
	 * @return the nomeBarrResc
	 */
	public String getNomeBarrResc() {
		return nomeBarrResc;
	}

	/**
	 * @param nomeBarrResc the nomeBarrResc to set
	 */
	public void setNomeBarrResc(String nomeBarrResc) {
		this.nomeBarrResc = nomeBarrResc;
	}

	/**
	 * @return the nomeCiddResc
	 */
	public String getNomeCiddResc() {
		return nomeCiddResc;
	}

	/**
	 * @param nomeCiddResc the nomeCiddResc to set
	 */
	public void setNomeCiddResc(String nomeCiddResc) {
		this.nomeCiddResc = nomeCiddResc;
	}

	/**
	 * @return the siglaEstdResc
	 */
	public String getSiglaEstdResc() {
		return siglaEstdResc;
	}

	/**
	 * @param siglaEstdResc the siglaEstdResc to set
	 */
	public void setSiglaEstdResc(String siglaEstdResc) {
		this.siglaEstdResc = siglaEstdResc;
	}

	/**
	 * @return the numeroCepResc
	 */
	public Long getNumeroCepResc() {
		return numeroCepResc;
	}

	/**
	 * @param numeroCepResc the numeroCepResc to set
	 */
	public void setNumeroCepResc(Long numeroCepResc) {
		this.numeroCepResc = numeroCepResc;
	}

	/**
	 * @return the numeroFoneResc
	 */
	public String getNumeroFoneResc() {
		return numeroFoneResc;
	}

	/**
	 * @param numeroFoneResc the numeroFoneResc to set
	 */
	public void setNumeroFoneResc(String numeroFoneResc) {
		this.numeroFoneResc = numeroFoneResc;
	}

	/**
	 * @return the dataUltmRelt
	 */
	public LocalDate getDataUltmRelt() {
		return dataUltmRelt;
	}

	/**
	 * @param dataUltmRelt the dataUltmRelt to set
	 */
	public void setDataUltmRelt(LocalDate dataUltmRelt) {
		this.dataUltmRelt = dataUltmRelt;
	}

	/**
	 * @return the nomeLognRede
	 */
	public String getNomeLognRede() {
		return nomeLognRede;
	}

	/**
	 * @param nomeLognRede the nomeLognRede to set
	 */
	public void setNomeLognRede(String nomeLognRede) {
		this.nomeLognRede = nomeLognRede;
	}

	/**
	 * @return the codigoPlanEnqdSalr
	 */
	public Long getCodigoPlanEnqdSalr() {
		return codigoPlanEnqdSalr;
	}

	/**
	 * @param codigoPlanEnqdSalr the codigoPlanEnqdSalr to set
	 */
	public void setCodigoPlanEnqdSalr(Long codigoPlanEnqdSalr) {
		this.codigoPlanEnqdSalr = codigoPlanEnqdSalr;
	}

	/**
	 * @return the codigoRegi
	 */
	public String getCodigoRegi() {
		return codigoRegi;
	}

	/**
	 * @param codigoRegi the codigoRegi to set
	 */
	public void setCodigoRegi(String codigoRegi) {
		this.codigoRegi = codigoRegi;
	}

	/**
	 * @return the codigoClas
	 */
	public Long getCodigoClas() {
		return codigoClas;
	}

	/**
	 * @param codigoClas the codigoClas to set
	 */
	public void setCodigoClas(Long codigoClas) {
		this.codigoClas = codigoClas;
	}

	/**
	 * @return the codigoNivl
	 */
	public Long getCodigoNivl() {
		return codigoNivl;
	}

	/**
	 * @param codigoNivl the codigoNivl to set
	 */
	public void setCodigoNivl(Long codigoNivl) {
		this.codigoNivl = codigoNivl;
	}

	/**
	 * @return the descricaoHier
	 */
	public String getDescricaoHier() {
		return descricaoHier;
	}

	/**
	 * @param descricaoHier the descricaoHier to set
	 */
	public void setDescricaoHier(String descricaoHier) {
		this.descricaoHier = descricaoHier;
	}

	/**
	 * @return the idTipoUser
	 */
	public Long getIdTipoUser() {
		return idTipoUser;
	}

	/**
	 * @param idTipoUser the idTipoUser to set
	 */
	public void setIdTipoUser(Long idTipoUser) {
		this.idTipoUser = idTipoUser;
	}

	/**
	 * @return the nomeEmpr
	 */
	public String getNomeEmpr() {
		return nomeEmpr;
	}

	/**
	 * @param nomeEmpr the nomeEmpr to set
	 */
	public void setNomeEmpr(String nomeEmpr) {
		this.nomeEmpr = nomeEmpr;
	}

	/**
	 * @return the nomeLognChef
	 */
	public String getNomeLognChef() {
		return nomeLognChef;
	}

	/**
	 * @param nomeLognChef the nomeLognChef to set
	 */
	public void setNomeLognChef(String nomeLognChef) {
		this.nomeLognChef = nomeLognChef;
	}

	/**
	 * @return the idCriaAD
	 */
	public String getIdCriaAD() {
		return idCriaAD;
	}

	/**
	 * @param idCriaAD the idCriaAD to set
	 */
	public void setIdCriaAD(String idCriaAD) {
		this.idCriaAD = idCriaAD;
	}

	/**
	 * @return the idSexo
	 */
	public String getIdSexo() {
		return idSexo;
	}

	/**
	 * @param idSexo the idSexo to set
	 */
	public void setIdSexo(String idSexo) {
		this.idSexo = idSexo;
	}

	/**
	 * @return the dataVencAso
	 */
	public LocalDate getDataVencAso() {
		return dataVencAso;
	}

	/**
	 * @param dataVencAso the dataVencAso to set
	 */
	public void setDataVencAso(LocalDate dataVencAso) {
		this.dataVencAso = dataVencAso;
	}

	/**
	 * @return the descricaoMotvInvs
	 */
	public String getDescricaoMotvInvs() {
		return descricaoMotvInvs;
	}

	/**
	 * @param descricaoMotvInvs the descricaoMotvInvs to set
	 */
	public void setDescricaoMotvInvs(String descricaoMotvInvs) {
		this.descricaoMotvInvs = descricaoMotvInvs;
	}

	/**
	 * @return the siglaAreaNivel1
	 */
	public String getSiglaAreaNivel1() {
		return siglaAreaNivel1;
	}

	/**
	 * @param siglaAreaNivel1 the siglaAreaNivel1 to set
	 */
	public void setSiglaAreaNivel1(String siglaAreaNivel1) {
		this.siglaAreaNivel1 = siglaAreaNivel1;
	}

	/**
	 * @return the siglaAreaNivel2
	 */
	public String getSiglaAreaNivel2() {
		return siglaAreaNivel2;
	}

	/**
	 * @param siglaAreaNivel2 the siglaAreaNivel2 to set
	 */
	public void setSiglaAreaNivel2(String siglaAreaNivel2) {
		this.siglaAreaNivel2 = siglaAreaNivel2;
	}

	/**
	 * @return the siglaAreaNivel3
	 */
	public String getSiglaAreaNivel3() {
		return siglaAreaNivel3;
	}

	/**
	 * @param siglaAreaNivel3 the siglaAreaNivel3 to set
	 */
	public void setSiglaAreaNivel3(String siglaAreaNivel3) {
		this.siglaAreaNivel3 = siglaAreaNivel3;
	}

	/**
	 * @return the siglaAreaNivel4
	 */
	public String getSiglaAreaNivel4() {
		return siglaAreaNivel4;
	}

	/**
	 * @param siglaAreaNivel4 the siglaAreaNivel4 to set
	 */
	public void setSiglaAreaNivel4(String siglaAreaNivel4) {
		this.siglaAreaNivel4 = siglaAreaNivel4;
	}

	/**
	 * @return the dataVignCargo
	 */
	public LocalDate getDataVignCargo() {
		return dataVignCargo;
	}

	/**
	 * @param dataVignCargo the dataVignCargo to set
	 */
	public void setDataVignCargo(LocalDate dataVignCargo) {
		this.dataVignCargo = dataVignCargo;
	}

	/**
	 * @return the datavignFuncao
	 */
	public LocalDate getDatavignFuncao() {
		return datavignFuncao;
	}

	/**
	 * @param datavignFuncao the datavignFuncao to set
	 */
	public void setDatavignFuncao(LocalDate datavignFuncao) {
		this.datavignFuncao = datavignFuncao;
	}

	/**
	 * @return the dataInicCcom
	 */
	public LocalDate getDataInicCcom() {
		return dataInicCcom;
	}

	/**
	 * @param dataInicCcom the dataInicCcom to set
	 */
	public void setDataInicCcom(LocalDate dataInicCcom) {
		this.dataInicCcom = dataInicCcom;
	}

	/**
	 * @return the dataFimCcom
	 */
	public LocalDate getDataFimCcom() {
		return dataFimCcom;
	}

	/**
	 * @param dataFimCcom the dataFimCcom to set
	 */
	public void setDataFimCcom(LocalDate dataFimCcom) {
		this.dataFimCcom = dataFimCcom;
	}

	/**
	 * @return the codigoMotvCcom
	 */
	public Long getCodigoMotvCcom() {
		return codigoMotvCcom;
	}

	/**
	 * @param codigoMotvCcom the codigoMotvCcom to set
	 */
	public void setCodigoMotvCcom(Long codigoMotvCcom) {
		this.codigoMotvCcom = codigoMotvCcom;
	}

	/**
	 * @return the codigoMotvAfst
	 */
	public Long getCodigoMotvAfst() {
		return codigoMotvAfst;
	}

	/**
	 * @param codigoMotvAfst the codigoMotvAfst to set
	 */
	public void setCodigoMotvAfst(Long codigoMotvAfst) {
		this.codigoMotvAfst = codigoMotvAfst;
	}

	/**
	 * @return the dataInicSitfun
	 */
	public LocalDate getDataInicSitfun() {
		return dataInicSitfun;
	}

	/**
	 * @param dataInicSitfun the dataInicSitfun to set
	 */
	public void setDataInicSitfun(LocalDate dataInicSitfun) {
		this.dataInicSitfun = dataInicSitfun;
	}

	/**
	 * @return the dataFimSitfun
	 */
	public LocalDate getDataFimSitfun() {
		return dataFimSitfun;
	}

	/**
	 * @param dataFimSitfun the dataFimSitfun to set
	 */
	public void setDataFimSitfun(LocalDate dataFimSitfun) {
		this.dataFimSitfun = dataFimSitfun;
	}

	/**
	 * @return the dataInicGozoFeri
	 */
	public LocalDate getDataInicGozoFeri() {
		return dataInicGozoFeri;
	}

	/**
	 * @param dataInicGozoFeri the dataInicGozoFeri to set
	 */
	public void setDataInicGozoFeri(LocalDate dataInicGozoFeri) {
		this.dataInicGozoFeri = dataInicGozoFeri;
	}

	/**
	 * @return the dataFimGozoFeri
	 */
	public LocalDate getDataFimGozoFeri() {
		return dataFimGozoFeri;
	}

	/**
	 * @param dataFimGozoFeri the dataFimGozoFeri to set
	 */
	public void setDataFimGozoFeri(LocalDate dataFimGozoFeri) {
		this.dataFimGozoFeri = dataFimGozoFeri;
	}

	/**
	 * @return the siglaInvtdInTrn
	 */
	public String getSiglaInvtdInTrn() {
		return siglaInvtdInTrn;
	}

	/**
	 * @param siglaInvtdInTrn the siglaInvtdInTrn to set
	 */
	public void setSiglaInvtdInTrn(String siglaInvtdInTrn) {
		this.siglaInvtdInTrn = siglaInvtdInTrn;
	}

	/**
	 * @return the codigoInvtdIntrn
	 */
	public String getCodigoInvtdIntrn() {
		return codigoInvtdIntrn;
	}

	/**
	 * @param codigoInvtdIntrn the codigoInvtdIntrn to set
	 */
	public void setCodigoInvtdIntrn(String codigoInvtdIntrn) {
		this.codigoInvtdIntrn = codigoInvtdIntrn;
	}

	/**
	 * @return the dataInicInvtdIntrn
	 */
	public LocalDate getDataInicInvtdIntrn() {
		return dataInicInvtdIntrn;
	}

	/**
	 * @param dataInicInvtdIntrn the dataInicInvtdIntrn to set
	 */
	public void setDataInicInvtdIntrn(LocalDate dataInicInvtdIntrn) {
		this.dataInicInvtdIntrn = dataInicInvtdIntrn;
	}

	/**
	 * @return the dataFimInvtdIntrn
	 */
	public LocalDate getDataFimInvtdIntrn() {
		return dataFimInvtdIntrn;
	}

	/**
	 * @param dataFimInvtdIntrn the dataFimInvtdIntrn to set
	 */
	public void setDataFimInvtdIntrn(LocalDate dataFimInvtdIntrn) {
		this.dataFimInvtdIntrn = dataFimInvtdIntrn;
	}

	/**
	 * @return the siglaOrgaInvtdIntrnExcp
	 */
	public String getSiglaOrgaInvtdIntrnExcp() {
		return siglaOrgaInvtdIntrnExcp;
	}

	/**
	 * @param siglaOrgaInvtdIntrnExcp the siglaOrgaInvtdIntrnExcp to set
	 */
	public void setSiglaOrgaInvtdIntrnExcp(String siglaOrgaInvtdIntrnExcp) {
		this.siglaOrgaInvtdIntrnExcp = siglaOrgaInvtdIntrnExcp;
	}

	/**
	 * @return the codigoInvtdIntrnExcp
	 */
	public String getCodigoInvtdIntrnExcp() {
		return codigoInvtdIntrnExcp;
	}

	/**
	 * @param codigoInvtdIntrnExcp the codigoInvtdIntrnExcp to set
	 */
	public void setCodigoInvtdIntrnExcp(String codigoInvtdIntrnExcp) {
		this.codigoInvtdIntrnExcp = codigoInvtdIntrnExcp;
	}

	/**
	 * @return the dataInicInvtdInternExcp
	 */
	public LocalDate getDataInicInvtdInternExcp() {
		return dataInicInvtdInternExcp;
	}

	/**
	 * @param dataInicInvtdInternExcp the dataInicInvtdInternExcp to set
	 */
	public void setDataInicInvtdInternExcp(LocalDate dataInicInvtdInternExcp) {
		this.dataInicInvtdInternExcp = dataInicInvtdInternExcp;
	}

	/**
	 * @return the dataFimInvtdIntrnExcp
	 */
	public LocalDate getDataFimInvtdIntrnExcp() {
		return dataFimInvtdIntrnExcp;
	}

	/**
	 * @param dataFimInvtdIntrnExcp the dataFimInvtdIntrnExcp to set
	 */
	public void setDataFimInvtdIntrnExcp(LocalDate dataFimInvtdIntrnExcp) {
		this.dataFimInvtdIntrnExcp = dataFimInvtdIntrnExcp;
	}

	/**
	 * @return the siglaOrgaSubi
	 */
	public String getSiglaOrgaSubi() {
		return siglaOrgaSubi;
	}

	/**
	 * @param siglaOrgaSubi the siglaOrgaSubi to set
	 */
	public void setSiglaOrgaSubi(String siglaOrgaSubi) {
		this.siglaOrgaSubi = siglaOrgaSubi;
	}

	/**
	 * @return the codigoInvtdSubi
	 */
	public String getCodigoInvtdSubi() {
		return codigoInvtdSubi;
	}

	/**
	 * @param codigoInvtdSubi the codigoInvtdSubi to set
	 */
	public void setCodigoInvtdSubi(String codigoInvtdSubi) {
		this.codigoInvtdSubi = codigoInvtdSubi;
	}

	/**
	 * @return the dataInicSubi
	 */
	public LocalDate getDataInicSubi() {
		return dataInicSubi;
	}

	/**
	 * @param dataInicSubi the dataInicSubi to set
	 */
	public void setDataInicSubi(LocalDate dataInicSubi) {
		this.dataInicSubi = dataInicSubi;
	}

	/**
	 * @return the dataFimSubi
	 */
	public LocalDate getDataFimSubi() {
		return dataFimSubi;
	}

	/**
	 * @param dataFimSubi the dataFimSubi to set
	 */
	public void setDataFimSubi(LocalDate dataFimSubi) {
		this.dataFimSubi = dataFimSubi;
	}

	/**
	 * @return the codigoEixo
	 */
	public Long getCodigoEixo() {
		return codigoEixo;
	}

	/**
	 * @param codigoEixo the codigoEixo to set
	 */
	public void setCodigoEixo(Long codigoEixo) {
		this.codigoEixo = codigoEixo;
	}

	/**
	 * @return the eixoDec
	 */
	public String getEixoDec() {
		return eixoDec;
	}

	/**
	 * @param eixoDec the eixoDec to set
	 */
	public void setEixoDec(String eixoDec) {
		this.eixoDec = eixoDec;
	}

	/**
	 * @return the codigoMacroProcesso
	 */
	public Long getCodigoMacroProcesso() {
		return codigoMacroProcesso;
	}

	/**
	 * @param codigoMacroProcesso the codigoMacroProcesso to set
	 */
	public void setCodigoMacroProcesso(Long codigoMacroProcesso) {
		this.codigoMacroProcesso = codigoMacroProcesso;
	}

	/**
	 * @return the macroProcesso
	 */
	public String getMacroProcesso() {
		return macroProcesso;
	}

	/**
	 * @param macroProcesso the macroProcesso to set
	 */
	public void setMacroProcesso(String macroProcesso) {
		this.macroProcesso = macroProcesso;
	}

	/**
	 * @return the codigoProcesso
	 */
	public Long getCodigoProcesso() {
		return codigoProcesso;
	}

	/**
	 * @param codigoProcesso the codigoProcesso to set
	 */
	public void setCodigoProcesso(Long codigoProcesso) {
		this.codigoProcesso = codigoProcesso;
	}

	/**
	 * @return the processo
	 */
	public String getProcesso() {
		return processo;
	}

	/**
	 * @param processo the processo to set
	 */
	public void setProcesso(String processo) {
		this.processo = processo;
	}

	/**
	 * @return the codigoFormacao
	 */
	public Long getCodigoFormacao() {
		return codigoFormacao;
	}

	/**
	 * @param codigoFormacao the codigoFormacao to set
	 */
	public void setCodigoFormacao(Long codigoFormacao) {
		this.codigoFormacao = codigoFormacao;
	}

	/**
	 * @return the formacao
	 */
	public String getFormacao() {
		return formacao;
	}

	/**
	 * @param formacao the formacao to set
	 */
	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

	/**
	 * @return the dataFimAfst
	 */
	public LocalDate getDataFimAfst() {
		return dataFimAfst;
	}

	/**
	 * @param dataFimAfst the dataFimAfst to set
	 */
	public void setDataFimAfst(LocalDate dataFimAfst) {
		this.dataFimAfst = dataFimAfst;
	}

	/**
	 * @return the idSap
	 */
	public String getIdSap() {
		return idSap;
	}

	/**
	 * @param idSap the idSap to set
	 */
	public void setIdSap(String idSap) {
		this.idSap = idSap;
	}
	
}
