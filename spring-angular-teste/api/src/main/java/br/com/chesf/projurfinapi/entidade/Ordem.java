package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por mapear o recurso Ordem.
 *
 */
@Entity
@Table(name = "TB_ORDEM", schema="PROJUR")
@SequenceGenerator(name = "SQ_ORDEM", sequenceName = "SQ_ORDEM", allocationSize = 1)
public class Ordem implements Serializable{

	private static final long serialVersionUID = 1766116694048070026L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ORDEM")
    @Column(name = "ID_ORDEM")
    private Long id;
	
	@Column(name = "CD_ORDEM", length = 12, nullable = false)
	private Long codigo;
	
	@Column(name = "DS_ORDEM", length = 50, nullable = false)
	private String descricao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
