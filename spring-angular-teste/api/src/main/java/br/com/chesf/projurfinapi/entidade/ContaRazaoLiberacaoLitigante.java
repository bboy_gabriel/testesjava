package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;

@Entity
@Table(name = "TB_CONTA_RAZAO_LIBERA_LITIG", schema  = "PROJUR")
@SequenceGenerator(name = "SQ_CONTA_RAZAO_LIBERA_LITG", sequenceName = "SQ_CONTA_RAZAO_LIBERA_LITG", allocationSize = 1)
public class ContaRazaoLiberacaoLitigante implements Serializable{

	private static final long serialVersionUID = 161239128561520553L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CONTA_RAZAO_LIBERA_LITG")
	@Column(name = "ID_CONTA_RAZAO_LIBERA_LITIG")
	private Long id;
	
	@Column(name = "CD_CONTA_DEBITO_LIBERA_LITIG", length = 10, nullable = false)
	private Long contaDebitoLiberacaoLitigante;
	
	@Column(name = "CD_CONTA_CREDITO_LIBERA_LITIG", length = 10, nullable = false)
	private Long contaCreditoLiberacaoLitigante;
	
	@Column(name = "TP_PROCESSO_LIBERA_LITIG ", length = 1, nullable = false)
	private String codigoTipoProcesso;
	
	@Transient
	private TipoProcessoContaRazaoEnum tipoProcessoContaRazaoEnum;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the contaDebitoLiberacaoLitigante
	 */
	public Long getContaDebitoLiberacaoLitigante() {
		return contaDebitoLiberacaoLitigante;
	}

	/**
	 * @param contaDebitoLiberacaoLitigante the contaDebitoLiberacaoLitigante to set
	 */
	public void setContaDebitoLiberacaoLitigante(Long contaDebitoLiberacaoLitigante) {
		this.contaDebitoLiberacaoLitigante = contaDebitoLiberacaoLitigante;
	}

	/**
	 * @return the contaCreditoLiberacaoLitigante
	 */
	public Long getContaCreditoLiberacaoLitigante() {
		return contaCreditoLiberacaoLitigante;
	}

	/**
	 * @param contaCreditoLiberacaoLitigante the contaCreditoLiberacaoLitigante to set
	 */
	public void setContaCreditoLiberacaoLitigante(Long contaCreditoLiberacaoLitigante) {
		this.contaCreditoLiberacaoLitigante = contaCreditoLiberacaoLitigante;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public String getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(String codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}

	/**
	 * @return the tipoProcessoContaRazaoEnum
	 */
	public TipoProcessoContaRazaoEnum getTipoProcessoContaRazaoEnum() {
		return tipoProcessoContaRazaoEnum;
	}

	/**
	 * @param tipoProcessoContaRazaoEnum the tipoProcessoContaRazaoEnum to set
	 */
	public void setTipoProcessoContaRazaoEnum(TipoProcessoContaRazaoEnum tipoProcessoContaRazaoEnum) {
		this.tipoProcessoContaRazaoEnum = tipoProcessoContaRazaoEnum;
	}
	
}
