package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.ElementoPepVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.ElementoPepServico;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por expor os serviços referentes ao recurso ElementoPep.
 *
 */
@RestController
@RequestMapping(value = "/api/elementosPep")
@Consumes("application/json")
@Produces("application/json")
public class ElementoPepControlador implements Serializable{

	private static final long serialVersionUID = 6398784262713662575L;

	@Autowired
	private ElementoPepServico elementoPepServico;
	
	/**
	 * 
	 * @return ResponseEntity
	 * 
	 * Metodo responsavel por consultar uma lista de recursos ElementosPep
	 * 
	 */
	@GetMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(elementoPepServico.consultarOrdenado(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<ElementoPepVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<ElementoPepVO>(elementoPepServico.consultarPorId(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/* crud retirado
	@PostMapping
	public ResponseEntity<ElementoPepVO> cadastrar(@RequestBody ElementoPepVO elementoVO) {
		try {
			return new ResponseEntity <ElementoPepVO>(elementoPepServico.cadastrar(elementoVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<ElementoPepVO> atualizar(@RequestBody ElementoPepVO elementoVO) {
		try {
			return new ResponseEntity<ElementoPepVO>(elementoPepServico.atualizar(elementoVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<ElementoPepVO> remover(@PathVariable Long id) {
		try {
			elementoPepServico.remover(id);
			return new ResponseEntity<ElementoPepVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	*/
	
	
}
