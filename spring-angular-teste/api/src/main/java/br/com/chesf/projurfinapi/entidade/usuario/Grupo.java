package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

public class Grupo implements Serializable{

	private static final long serialVersionUID = -6428149824030138414L;
	
	private Long id;
	private String descGrupo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescGrupo() {
		return descGrupo;
	}
	public void setDescGrupo(String descGrupo) {
		this.descGrupo = descGrupo;
	}

}
