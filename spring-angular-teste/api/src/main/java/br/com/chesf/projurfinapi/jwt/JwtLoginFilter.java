package br.com.chesf.projurfinapi.jwt;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.vo.UsuarioVO;

@Service
public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
	
	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;
	
	public JwtLoginFilter(String url) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(new JwtAuthenticationManager());
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");	
		
		return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(
				userName, password, Collections.emptyList()));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			FilterChain filterChain, Authentication auth) throws IOException, ServletException {

		User user = (User) auth.getPrincipal();
		UsuarioVO userVO = (UsuarioVO) user;
		tokenAuthenticationService.addAuthentication(response, userVO);
	}
}
