package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.ElementoPep;
import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.vo.ElementoPepVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ElementoPeprepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 *
 * Classe reponsavel pelos servicos referentes ao recurso ElemetoPep
 *
 */
@Service
public class ElementoPepServico implements Serializable{

	private static final long serialVersionUID = 4207808791543939266L;

	@Autowired
	private ElementoPeprepositorio elementoPEPRepositorio;
	
	@Autowired
	private InformacaoContabilServico informacaoContabilServico;
	
	Logger logger = LoggerFactory.getLogger(ElementoPepServico.class);
	
	private static final String ELEMENTO_PEP = "Elemento PEP";
	private static final String CODIGO = "Código "; 
	
	/**
	 * @return List<ElementoPepVO>
	 * 
	 * Metodo responsavel por consultar uma lista de ElementoPep, validar e montar uma respectiva lista de ElementoPepVO.
	 * 
	 */
	public List<ElementoPepVO> consultar(){
		try {
			List<ElementoPep> listaBase = elementoPEPRepositorio.findAll();
			validarListaElementoPepBase(listaBase);
			return montarListaElementoPepVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_ELEMENTO_PEP);
		}
	}
	
	/**
	 * @return List<ElementoPepVO>
	 * 
	 * Metodo responsavel por consultar uma lista de ElementoPep ordenada pelo codigo, validar e montar uma respectiva lista de ElementoPepVO.
	 * 
	 */
	public List<ElementoPepVO> consultarOrdenado() {
		try {
			List<ElementoPep> listaBase = elementoPEPRepositorio.findAll(new Sort(Sort.Direction.ASC, "codigo"));
			validarListaElementoPepBase(listaBase);
			return montarListaElementoPepVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_ELEMENTO_PEP);
		}
	}
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo responsavel por validar a lista consultada.
	 * 
	 */
	private void validarListaElementoPepBase(List<ElementoPep> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_ELEMENTO_PEP_NULO_OU_INVALIDO);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<ElementoPepVO>
	 * 
	 * Metodo Responsavel por montar uma lista de ElementoPepVo com base na lista de ElementoPep consultada.
	 * 
	 */
	private List<ElementoPepVO> montarListaElementoPepVO(List<ElementoPep> listaBase){
		List<ElementoPepVO> listaVO = new ArrayList<ElementoPepVO>();
		for (ElementoPep itemBase : listaBase) {
			ElementoPepVO vo = new ElementoPepVO();
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			listaVO.add(vo);
		}
		return listaVO;
	}
	
	public ElementoPepVO consultarPorId(Long id) {
		try {
			Validador.validarId(id);
			ElementoPep itemBase = elementoPEPRepositorio.findOne(id);
			validarEntidadeBase(itemBase);
			return montarElementoVO(itemBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_ELEMENTO_PEP);
		}
	}
	
	
	public ElementoPepVO cadastrar(ElementoPepVO elementoVO) {
		verificaExistenciaElementoPorCodigo(elementoVO);
		try {
			validarVO(elementoVO);
			ElementoPep elementoBase = elementoPEPRepositorio.save(montarElementoBase(elementoVO));
			validarEntidadeBase(elementoBase);			
			return montarElementoVO(elementoBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_ELEMENTO_PEP);
		}
	}
	
	public ElementoPepVO atualizar(@Valid ElementoPepVO elementoVO) {
		verificaExistenciaElementoPorCodigoIdNotIn(elementoVO);
		try {
			ElementoPep elementoBase = elementoPEPRepositorio.save(montarElementoBase(elementoVO));
			return montarElementoVO(elementoBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_ELEMENTO_PEP);
		}
	}
	
	public void remover(Long id) {
		Validador.validarId(id);
		List<InformacaoContabil> lstInfo = null;
		lstInfo = informacaoContabilServico.consultarInformacaoContabilPorElementoPEP(id); 
		if(lstInfo != null && !lstInfo.isEmpty()) {
			logger.info("Elemento PEP: " + lstInfo.get(0).getElementoPep().getDescricao() + " está sendo usado e não pode ser excluído!");
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO,ELEMENTO_PEP);
		}else {
			elementoPEPRepositorio.delete(id);				
		}
		
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código
	 * @param elementoPepVO
	 */
	private void verificaExistenciaElementoPorCodigo(ElementoPepVO elementoPepVO) {
		if(elementoPEPRepositorio.existsByCodigo(elementoPepVO.getCodigo())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, elementoPepVO.getCodigo().toString(), ELEMENTO_PEP);
		}
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código desconsiderando o próprio registro.
	 * @param elementoPepVO
	 */
	private void verificaExistenciaElementoPorCodigoIdNotIn(ElementoPepVO elementoPepVO) {
		if(elementoPEPRepositorio.existsByCodigoAndIdNotIn(elementoPepVO.getCodigo(), elementoPepVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, elementoPepVO.getCodigo().toString(), ELEMENTO_PEP);
		}
	}
	

	private ElementoPep montarElementoBase(ElementoPepVO elementoVO) {
		ElementoPep elementoBase = new ElementoPep();
		elementoBase.setId(elementoVO.getId());
		elementoBase.setCodigo(elementoVO.getCodigo());
		elementoBase.setDescricao(elementoVO.getDescricao());
		return elementoBase;
	}

	private ElementoPepVO montarElementoVO(ElementoPep itemBase) {
		ElementoPepVO elementoVO = new ElementoPepVO();
		elementoVO.setId(itemBase.getId());
		elementoVO.setCodigo(itemBase.getCodigo());
		elementoVO.setDescricao(itemBase.getDescricao());
		return elementoVO;
	}
	
	
	private void validarVO(ElementoPepVO elementoVO) {
		if(elementoVO == null) throw new NegocioException(MensagemEnum.ELEMENTO_PEP_NULO_OU_INVALIDO);
	}

	
	/**
	 * 
	 * @param entidadeBase
	 * 
	 * Metodo privado responsavel por validar o retorno base no serviço getById
	 * 
	 */
	private void validarEntidadeBase(ElementoPep entidadeBase) {
		if(entidadeBase == null) throw new NegocioException(MensagemEnum.ELEMENTO_PEP_NULO_OU_INVALIDO);
	}
	
}
