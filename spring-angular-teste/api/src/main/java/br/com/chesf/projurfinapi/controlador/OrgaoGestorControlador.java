package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.OrgaoGestorVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.OrgaoGestorServico;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer endpoints referentes ao recurso OrgaoGestor.
 *
 */
@RestController
@RequestMapping(value = "/api/orgaoGestor")
@Consumes("application/json")
@Produces("application/json")
public class OrgaoGestorControlador implements Serializable{

	private static final long serialVersionUID = 4136789584241149665L;

	@Autowired
	private OrgaoGestorServico orgaoGestorServico;
	
	/**
	 * 
	 * @return ResponseEntity
	 * 
	 * Endpoint responsavel por consultar em base e retornar uma lista do recurso OrgãoGestor
	 * 
	 */
	@GetMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(orgaoGestorServico.consultarOrdenado(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	

	@GetMapping(value = "/{id}")
	public ResponseEntity<OrgaoGestorVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<OrgaoGestorVO>(orgaoGestorServico.consultarPorIdVO(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/*
	@PostMapping
	public ResponseEntity<OrgaoGestorVO> cadastrar(@RequestBody OrgaoGestorVO orgaoGestorVO) {
		try {
			return new ResponseEntity <OrgaoGestorVO>(orgaoGestorServico.cadastrar(orgaoGestorVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<OrgaoGestorVO> atualizar(@RequestBody OrgaoGestorVO orgaoGestorVO) {
		try {
			return new ResponseEntity <OrgaoGestorVO>(orgaoGestorServico.atualizar(orgaoGestorVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<OrgaoGestorVO> remover(@PathVariable Long id) {
		try {
			orgaoGestorServico.remover(id);
			return new ResponseEntity<OrgaoGestorVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	*/
}
