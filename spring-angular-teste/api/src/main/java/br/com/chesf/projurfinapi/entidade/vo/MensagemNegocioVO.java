package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.util.ResourceMensagem;

public class MensagemNegocioVO implements Serializable {

	private static final long serialVersionUID = 4729280856974819729L;

	private String codigo;

	private String mensagem;
	
	private String campo;
	
	private List<MensagemNegocioVO> detalhes = new ArrayList<>();
	
	public MensagemNegocioVO(String codigo, String campo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
		this.campo = campo;
	}
	
	public MensagemNegocioVO(String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}

	public MensagemNegocioVO(MensagemEnum mensagemEnum, String... campos) {
		this.codigo = mensagemEnum.getStatus();
		this.mensagem = getMensagem(mensagemEnum, campos);
	}

	private String getMensagem(MensagemEnum mensagemEnum, String... campo) {
		if (campo.length > 0) {
			mensagem = ResourceMensagem.getMessage(mensagemEnum, campo);
		} else {
			mensagem = ResourceMensagem.getMessage(mensagemEnum);
		}
		return mensagem;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<MensagemNegocioVO> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<MensagemNegocioVO> detalhes) {
		this.detalhes = detalhes;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}
}
