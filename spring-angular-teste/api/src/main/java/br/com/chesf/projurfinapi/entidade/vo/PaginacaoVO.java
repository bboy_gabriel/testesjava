package br.com.chesf.projurfinapi.entidade.vo;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;


public class PaginacaoVO<T extends Object> extends PageImpl<T> {	
	 
	private static final long serialVersionUID = 1254445L;
	private String busca;
	private Map<String, Object> filtros;
	private PageRequest pageableLocal = new PageRequest(0, 10);
	private boolean buscarTodos = false;
	
	public PaginacaoVO(){
		super(Collections.emptyList());
	}
	
	public PaginacaoVO(PageRequest pageConfig, List<T> conteudo, long total){
		super(conteudo, pageConfig, total);
		this.pageableLocal = pageConfig;
	}
	
	public PaginacaoVO(PageRequest pageConfig, List<T> conteudo, long total, String busca){
		super(conteudo, pageConfig, total);
		this.busca = busca;
		this.pageableLocal = pageConfig;
	}
	
	public PaginacaoVO(PageRequest pageConfig, List<T> conteudo, long total, Map<String, Object> filtros){
		super(conteudo, pageConfig, total);
		this.filtros = filtros;
		this.pageableLocal = pageConfig;
	}
	
	public String getBusca() {
		return busca;
	}
	public void setBusca(String busca) {
		this.busca = busca;
	}

	public Map<String, Object> getFiltros() {
		return filtros;
	}

	public void setFiltros(Map<String, Object> filtros) {
		this.filtros = filtros;
	}

	public PageRequest getPageableLocal() {
		return isBuscarTodos()? null: pageableLocal;
	}

	public void setPageableLocal(PageRequest pageableLocal) {
		this.pageableLocal = pageableLocal;
	}

	public boolean isBuscarTodos() {
		return buscarTodos;
	}

	public void setBuscarTodos(boolean buscarTodos) {
		this.buscarTodos = buscarTodos;
	}
	
}
