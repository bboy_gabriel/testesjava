package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

/**
 * Representa um modulo do sistema
 * @author isfigueiredo
 *
 */
public class Modulo implements Serializable{

	private static final long serialVersionUID = -4940631656230666620L;
	
	private Long id;
	private String nomeModulo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomeModulo() {
		return nomeModulo;
	}
	public void setNomeModulo(String nomeModulo) {
		this.nomeModulo = nomeModulo;
	}

}
