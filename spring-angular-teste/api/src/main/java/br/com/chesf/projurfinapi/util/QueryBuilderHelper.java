package br.com.chesf.projurfinapi.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

/**
 * Classe que auxilia na construção de consultas HQL/JPA
 * @author Mauricio Alixandre (malixandre)
 */
public class QueryBuilderHelper {
	
	private Map<String,Root<?>> roots = new HashMap<>();
	private Map<String, Join<?,?>> joins = new HashMap<>();
	private EntityManager entityManager; 
	private CriteriaBuilder cb;
	private CriteriaQuery<?> query;
	private Class<?> tipoResultadoClass;
 
	private void reset() {
		this.joins = new HashMap<>();
		this.roots = new HashMap<>();
		this.query = cb.createQuery(tipoResultadoClass);
	}
	
	/**
	 * Cria uma instãncia de QueryBuilderHelper
	 *
	 * @param fromClasse nome da classe que se deseja fazer  from
	 * @param aliasRoot nome da chave para recuperar o root do atributo fromClasse
	 * @param tipoResultadoClass tipo de resutado esperado no retorno do select
	 * @param entityManager contexto de persistencia onde se tem o ORM
	 * @throws Exception se alguma atributo for null
	 */
	public QueryBuilderHelper(Class<?> fromClasse, String aliasRoot, Class<?> tipoResultadoClass, EntityManager entityManager) throws Exception {	
		if(fromClasse == null) throw new Exception("Atributo fromClasse não pode ser nulo");
		if(aliasRoot == null) throw new Exception("Atributo aliasRoot não pode ser nulo");
		if(tipoResultadoClass == null) throw new Exception("Atributo tipoResultadoClass não pode ser nulo");
		if(entityManager == null) throw new Exception("Atributo entityManager não pode ser nulo");
		this.entityManager = entityManager;
		this.cb = this.entityManager.getCriteriaBuilder();
		this.tipoResultadoClass = tipoResultadoClass;
		this.query = cb.createQuery(tipoResultadoClass);
		this.appendRoot(aliasRoot, fromClasse);
	}
	
	/**
	 * Acrescenta um uma classe Root<?> a consulta
	 *
	 * @param aliaRoot apelido do root
	 * @param rooEntity Classe java do root 
	 */
	public QueryBuilderHelper appendRoot(String aliaRoot, Class<?> rooEntity) {		
		Root<?> newRoot = this.query.from(rooEntity); 
		this.roots.put(aliaRoot, newRoot);
		return this;
	}
	
	/**
	 * Acrescenta um Join a consulta
	 *
	 * @param alisRoot informa a classe Root que fara join
	 * @param aliasJoin apelido do join que o identifca em futuro getJoin() metodos
	 * @param joinField campo que fara join com a classe definida no alisRoot
	 * @param joinType tipo de join (INNER, LEFTH RIGHT)
	 */
	public QueryBuilderHelper appendJoin(String alisRoot, String aliasJoin, String joinField, JoinType joinType ) {
		Join<?,?> newJoin = this.roots.get(alisRoot).join(joinField, joinType);
		this.joins.put(aliasJoin, newJoin);		
		return this;
		
	}	
 
	/**
	 * Acrescenta uma lista de Orders a consulta 
	 * @return CriteriaBuilder
	 */
	public CriteriaBuilder getCriteriaBuilder() {	
		return this.cb;		
	}
	
	/**
	 * Acrescenta uma lista de Orders a consulta 
	 * @return Root<?> 
	 */
	public Root<?> getRoot(String rootKey){		
		return this.roots.get(rootKey);
	}

	/**
	 * Acrescenta uma lista de Orders a consulta 
	 * @return Join<?,?>
	 */
	public Join<?,?> getJoin(String aliasjOIN){		
		return this.joins.get(aliasjOIN);
	}
	
	/**
	 * Adiciona clausulas where a consulta 
	 * @return QueryBuilderHelper
	 */
	public QueryBuilderHelper addWhere(Predicate ...predicates ) {
		this.query.where(predicates);
		return this;
	}
	
	/**
	 * Acrescenta uma lista de Orders a consulta 
	 * @return CriteriaQuery<?>
	 */
	public CriteriaQuery<?> getCriteriaQuery(){
		return this.query;
	}

	/**
	 * Retonro uma lista começando de uma posição inicial ao máximo resutado 
	 * definido pelo atributo tipoResultadoClass no construtor.
	 * @param startPosition posição inicial
	 * @param maxResult máximo resultados
	 * @throws Exception 
	 */
	public List<?> buid(Integer startPosition, Integer maxResult) throws Exception {	
		 List<?> resultado = null;
		 TypedQuery<?> tuplas = this.entityManager.createQuery(this.query)
				.setMaxResults(maxResult)
				.setFirstResult(startPosition);
		 resultado = tuplas.getResultList();
		 this.reset();
		 return resultado;
	}	
	
	/**
	 * Retonro uma lista de resultados baseado no retorno 
	 * definido pelo atributo tipoResultadoClass no construtor.
	 * @throws Exception 
	 */
	public List<?> buid() throws Exception{		
		return this.entityManager.createQuery(this.query).getResultList();
	}
	
	/**
	 * Define um ou mais atributos a serem selecionados na query 
	 * @return QueryBuilderHelper
	 */
	public QueryBuilderHelper appnedSelect(Selection<?>... selections) {
		this.query.multiselect(selections);
		return this;
	}

	/**
	 * Acrescenta uma lista de Orders a consulta 
	 * @return QueryBuilderHelper
	 */
	public QueryBuilderHelper appenddOrderBy(Order... orders){		
		this.query.orderBy(orders);		
		return this;
	}
	
	
	
}
