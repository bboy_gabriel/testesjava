package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class NumeroProcessoVO implements Serializable{

	private static final long serialVersionUID = -4672545078427020233L;

	@NotEmpty(message = "Numero do processo nulo inválido!")
	@Length(min = 2, message = "numero do processo menor que o valor minimo de 2 digitos!")
	private String numeroProcesso;

	/**
	 * @return the numeroProcesso
	 */
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

}
