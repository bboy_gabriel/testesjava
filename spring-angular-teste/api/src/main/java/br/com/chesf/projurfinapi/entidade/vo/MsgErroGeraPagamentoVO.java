package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

public class MsgErroGeraPagamentoVO implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -1364198858964165799L;

	private Long id;
	
	private String mensagens;
	
	private String dataInsercao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the mensagens
	 */
	public String getMensagens() {
		return mensagens;
	}

	/**
	 * @param mensagens the mensagens to set
	 */
	public void setMensagens(String mensagens) {
		this.mensagens = mensagens;
	}

	/**
	 * @return the dataInsercao
	 */
	public String getDataInsercao() {
		return dataInsercao;
	}

	/**
	 * @param dataInsercao the dataInsercao to set
	 */
	public void setDataInsercao(String dataInsercao) {
		this.dataInsercao = dataInsercao;
	}
	
}
