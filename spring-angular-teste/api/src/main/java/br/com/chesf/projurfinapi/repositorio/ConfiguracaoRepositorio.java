package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.Configuracao;

@Repository
public interface ConfiguracaoRepositorio extends JpaRepository<Configuracao, String>, QueryByExampleExecutor<Configuracao>{

	@Query("Select conf from Configuracao conf where conf.codPropriedade = :codigo")
	Configuracao findByCodPropriedade (@Param("codigo") String codigo); 
}
