package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.CentroDeCusto;
import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.vo.CentroDeCustoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.CentroDeCustoRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer os serviços referentes ao recurso CentroDeCusto.
 *
 */
@Service
public class CentroDeCustoServico implements Serializable{

	private static final long serialVersionUID = -1139762747524956604L;

	@Autowired
	private CentroDeCustoRepositorio centroDeCustoRepositorio;
	
	@Autowired
	private InformacaoContabilServico informacaoContabilServico;
	
	Logger logger = LoggerFactory.getLogger(CentroDeCustoServico.class);
	
	private static final String CENTRO_DE_CUSTO = "Centro de Custo";
	private static final String PREFIXO_DESCRICAO = "H";
	private static final String CODIGO = "Código "; 
	
	/**
	 * 
	 * @return
	 * 
	 * Metodo responsavel por consultar um lista de recursos CentroDeCusto, valida e retorna uma listaVO montada.
	 * 
	 */
	public List<CentroDeCustoVO> consultar(){
		try {
			List<CentroDeCusto> listaBase = centroDeCustoRepositorio.findAll();
			validarListaCentroDeCustoBase(listaBase);
			return montarListaCentroDeCustoVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_CENTRO_DE_CUSTO);
		}
	}
	
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo responsavel por validar a lista consultada de CentroDeCusto.
	 * 
	 */
	private void validarListaCentroDeCustoBase(List<CentroDeCusto> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_CENTRO_DE_CUSTO_NULA_OU_INVALIDA);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<CentroDeCustoVO>
	 * 
	 * Metodo responsavel por montar uma listaVO da lista consultada.
	 * 
	 */
	private List<CentroDeCustoVO> montarListaCentroDeCustoVO(List<CentroDeCusto> listaBase){
		List<CentroDeCustoVO> listaVO = new ArrayList<>();
		for (CentroDeCusto itemBase : listaBase) {
			CentroDeCustoVO vo = new CentroDeCustoVO();
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			listaVO.add(vo);
		}
		return listaVO;
	}
	
	public CentroDeCustoVO consultarPorId(Long id) {
		try {
			Validador.validarId(id);
			CentroDeCusto itemBase = centroDeCustoRepositorio.findOne(id);
			return montarCentroVO(itemBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_CENTRO_DE_CUSTO);
		}
	}
	
	
	public CentroDeCustoVO cadastrar(@Valid CentroDeCustoVO centroVO) {
		verificarExistenciaCentroPorCodigo(centroVO);		
		try {
			CentroDeCusto centroBase = centroDeCustoRepositorio.save(montarCentroBase(centroVO));
			return montarCentroVO(centroBase);
		} catch (Exception e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_CENTRO_DE_CUSTO);
		}
	}
	
	public CentroDeCustoVO atualizar(CentroDeCustoVO centroVO) {
		validarVO(centroVO);
		verificaExistenciaElementoPorCodigoIdNotIn(centroVO);
		try {
			CentroDeCusto centroBase = centroDeCustoRepositorio.save(montarCentroBase(centroVO));
			return montarCentroVO(centroBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_CENTRO_DE_CUSTO);
		}
	}
	
	private void verificarExistenciaCentroPorCodigo(CentroDeCustoVO centroVO) {
		validarVO(centroVO);
		if(centroDeCustoRepositorio.existsByCodigo(centroVO.getCodigo())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, centroVO.getCodigo().toString(), CENTRO_DE_CUSTO);
		}
	}
	
	private void verificaExistenciaElementoPorCodigoIdNotIn(CentroDeCustoVO centroVO) {
		validarVO(centroVO);
		if(centroDeCustoRepositorio.existsByCodigoAndIdNotIn(centroVO.getCodigo(), centroVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, centroVO.getCodigo().toString(), CENTRO_DE_CUSTO);
		}
	}
	
	public void remover(Long id) {
		List<InformacaoContabil> info = null;
		Validador.validarId(id);
		CentroDeCusto centro = new CentroDeCusto();
		centro.setId(id);
		//verifica se o centro de custo esta atrelado a alguma informacao contabil
		info = informacaoContabilServico.consultarInformacaoContabilPorCentroCusto(centro); 
		if(info != null && !info.isEmpty()) {
			logger.info("Centro de custo: " + info.get(0).getCentroDeCusto().getCodigo() + " está sendo usado e não pode ser excluído!");
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO, CENTRO_DE_CUSTO);
		}else {
			centroDeCustoRepositorio.delete(id);				
		}
	}
		
	private CentroDeCusto montarCentroBase(CentroDeCustoVO centroVO) {
		CentroDeCusto centro = new CentroDeCusto();
		centro.setId(centroVO.getId());
		centro.setCodigo(centroVO.getCodigo());
		centro.setDescricao(centroVO.getDescricao().toUpperCase());
		return centro;
		
	}

	private void validarVO(CentroDeCustoVO centroVO) {
		if(Validador.isNotNull(centroVO.getCodigo()) && !centroVO.getCodigo().isEmpty()) {
			if(!centroVO.getCodigo().substring(0,1).equalsIgnoreCase(PREFIXO_DESCRICAO)) {
				throw new NegocioException(MensagemEnum.CENTRO_CUSTO_DESCRICAO_INCORRETA);
			}
		}
	}

	private CentroDeCustoVO montarCentroVO(CentroDeCusto itemBase) {
		CentroDeCustoVO centroVO = new CentroDeCustoVO();
		centroVO.setId(itemBase.getId());
		centroVO.setCodigo(itemBase.getCodigo());
		centroVO.setDescricao(itemBase.getDescricao());
		return centroVO;
	}


	public List<CentroDeCustoVO> consultarOrdenado() {
		try {
			List<CentroDeCusto> listaBase = centroDeCustoRepositorio.findAll(new Sort(Sort.Direction.ASC, "descricao"));
			validarListaCentroDeCustoBase(listaBase);
			return montarListaCentroDeCustoVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_CENTRO_DE_CUSTO);
		}
	}
	
}
