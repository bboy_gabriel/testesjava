package br.com.chesf.projurfinapi.enums;

/**
 * 
 * Enum responsavel por mapear os possivel tipo de processo usados nas transações de provisões.
 * 
 * @author jvneto1
 *
 */
public enum TipoProcessoContaRazaoEnum {
	ADMINISTRATIVO(1, "Administrativo"),
	CIVIL(2, "Civil"),
	TRABALHISTA(3, "Trabalhista"),
	AMBIENTAL(4, "Ambiental"),
	FISCAL(5, "Fiscal"),
	FUNDIARIO(6, "Fundiário"),
	REGULATORIO(7, "Regulatório");
	
	private Integer id;
	private String descricao;
	
	private TipoProcessoContaRazaoEnum(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public static final TipoProcessoContaRazaoEnum getTipoProcessoContaRazaoEnumPorID(Integer id) {
		TipoProcessoContaRazaoEnum tipo = null;
		switch (id) {
		case 1:
			tipo = TipoProcessoContaRazaoEnum.ADMINISTRATIVO;
			break;
		case 2:
			tipo = TipoProcessoContaRazaoEnum.CIVIL;
			break;
		case 3:
			tipo = TipoProcessoContaRazaoEnum.TRABALHISTA;
			break;
		case 4:
			tipo = TipoProcessoContaRazaoEnum.AMBIENTAL;
			break;
		case 5:
			tipo = TipoProcessoContaRazaoEnum.FISCAL;
			break;
		case 6:
			tipo = TipoProcessoContaRazaoEnum.FUNDIARIO;
			break;
		case 7:
			tipo = TipoProcessoContaRazaoEnum.REGULATORIO;
			break;
		}
		return tipo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
