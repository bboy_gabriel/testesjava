package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.OrdemVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.OrdemServico;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por expor os serviços relacionados ao recuso Ordem
 *
 */
@RestController
@RequestMapping(value = "/api/ordens")
@Consumes("application/json")
@Produces("application/json")
public class OrdemControlador implements Serializable{

	private static final long serialVersionUID = -6184861427698392469L;

	@Autowired
	private OrdemServico ordemServico;
	
	/**
	 * 
	 * @return ResponseEntity
	 * 
	 * Metodo responsavel por consultar uma lista referente ao recurso Ordem.
	 * 
	 */
	@GetMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(ordemServico.consultarOrdenado(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<OrdemVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<OrdemVO>(ordemServico.consultarPorId(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/*
	@PostMapping
	public ResponseEntity<OrdemVO> cadastrar(@RequestBody OrdemVO ordemVO) {
		try {
			return new ResponseEntity <OrdemVO>(ordemServico.cadastrar(ordemVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<OrdemVO> atualizar(@RequestBody OrdemVO ordemVO) {
		try {
			return new ResponseEntity<OrdemVO>(ordemServico.atualizar(ordemVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<OrdemVO> remover(@PathVariable Long id) {
		try {
			ordemServico.remover(id);
			return new ResponseEntity<OrdemVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	*/
	
}
