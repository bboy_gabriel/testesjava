package br.com.chesf.projurfinapi.enums;

/**
 * Enumeração que representa os tipo de despesas de um processo.
 * @author isfigueiredo
 *
 */
public enum IndicadorDespesaEnum {
	P(1, "Processual"),
	A(2, "Administrativa");
	
	private Integer id;
	private String desc;
	
	private IndicadorDespesaEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
