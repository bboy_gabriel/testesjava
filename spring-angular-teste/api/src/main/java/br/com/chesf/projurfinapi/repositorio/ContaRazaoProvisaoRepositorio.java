package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.ContaRazaoProvisao;

/**
 * 
 * Interface responsavel por fornecer uma camada de persistencia a entidade ContaRazaoProvisao.
 * 
 * @author jvneto1
 *
 */
@Repository
public interface ContaRazaoProvisaoRepositorio extends JpaRepository<ContaRazaoProvisao, Long>, QueryByExampleExecutor<ContaRazaoProvisao>{

	boolean existsByContaDebitoProvisaoAndContaCreditoProvisao(Long contaDebitoProvisao, Long contaCreditoProvisao);
	
	List<ContaRazaoProvisao> findByCodigoTipoProcesso(String codigo);
}
