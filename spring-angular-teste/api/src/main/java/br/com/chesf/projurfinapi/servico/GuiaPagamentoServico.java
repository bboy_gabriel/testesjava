package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.GuiaPagamento;
import br.com.chesf.projurfinapi.entidade.vo.GuiaPagamentoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.GuiaPagamentoRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

@Service
public class GuiaPagamentoServico implements Serializable{

	private static final long serialVersionUID = 7370559471000956283L;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private GuiaPagamentoRepositorio guiaPagamentoRepositorio;
	
	Logger logger = LoggerFactory.getLogger(GuiaPagamentoServico.class);
	
	/**
	 * 
	 * Serviço responsável por consultar um recurso GuiaPagamento.
	 * 
	 * @param id
	 * @return GuiaPagamentoVO
	 * 
	 */
	public GuiaPagamentoVO consultarGuiaPorID(Long id) {
		try {
			return convertToVO(guiaPagamentoRepositorio.findOne(id));
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_GUIA_PAGAMENTO_POR_ID.getMensagem());
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_GUIA_PAGAMENTO_POR_ID);
		}
	}

	public GuiaPagamentoVO cadastrarGuiaPagamento(GuiaPagamentoVO guiaPagamentoVO) {
		validarGuiaPagamentoVO(guiaPagamentoVO);
		GuiaPagamento guiaBase = modelMapper.map(guiaPagamentoVO, GuiaPagamento.class);
		guiaBase = guiaPagamentoRepositorio.save(guiaBase);		
		guiaPagamentoVO.setId(guiaBase.getId());
		return guiaPagamentoVO;
	}
	
	/**
	 * 
	 * Serviço responsável por atualizar o recurso GuiaPagamento.
	 * 
	 * @param guiaPagamentoVO
	 * @return GuiaPagamentoVO
	 * 
	 */
	public GuiaPagamentoVO atualizarGuiaPagamento(GuiaPagamentoVO guiaPagamentoVO) {
		try {
			return convertToVO(guiaPagamentoRepositorio.save(convertToEntidadeBase(guiaPagamentoVO)));
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_ATUALIZAR_GUIA_PAGAMENTO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_GUIA_PAGAMENTO);
		}
	}

	/**
	 * Valida se a guia obedece todas as regras funcionais
	 * @param guiaPagamentoVO
	 */
	private void validarGuiaPagamentoVO(GuiaPagamentoVO guiaPagamentoVO) {
		//informou os 3
		if(Validador.isNotNull(guiaPagamentoVO.getCentroDeCustoVO()) && Validador.isNotNull(guiaPagamentoVO.getElementoPepVO())
				&& Validador.isNotNull(guiaPagamentoVO.getOrdemVO())) {
			logger.error("Elemento pep, ordem e centro de custo informados simultaneamente");
			throw new NegocioException(MensagemEnum.ERRO_CENTRO_ELEMENTO_ORDEM_INFORMADOS);
		}
		//Nao informou nenhum
		if(Validador.isNull(guiaPagamentoVO.getCentroDeCustoVO()) && Validador.isNull(guiaPagamentoVO.getElementoPepVO())
				&& Validador.isNull(guiaPagamentoVO.getOrdemVO())) {
			logger.error("Elemento pep, ordem e centro de custo não forma informados ");
			throw new NegocioException(MensagemEnum.ERRO_CENTRO_ELEMENTO_ORDEM_INFORMADOS);
		}
				
		//informou elemento pep e mais um outro
		if(Validador.isNotNull(guiaPagamentoVO.getElementoPepVO()) 
				&& ( Validador.isNotNull(guiaPagamentoVO.getCentroDeCustoVO()) || Validador.isNotNull(guiaPagamentoVO.getOrdemVO()))) {
			logger.error("Elemento PEP informado juntamente ou com centro de custo ou com ordem.");
			throw new NegocioException(MensagemEnum.ERRO_CENTRO_ELEMENTO_ORDEM_INFORMADOS);
		}
		
		//informou centro de custo e mais algum
		if(Validador.isNotNull(guiaPagamentoVO.getCentroDeCustoVO()) 
				&& ( Validador.isNotNull(guiaPagamentoVO.getElementoPepVO()) || Validador.isNotNull(guiaPagamentoVO.getOrdemVO()))) {
			logger.error("Elemento PEP informado juntamente ou com centro de custo ou com ordem.");
			throw new NegocioException(MensagemEnum.ERRO_CENTRO_ELEMENTO_ORDEM_INFORMADOS);
		}
		
	}

	/**
	 * 
	 * Serviço que retorna uma lista do recurso GuiaPagamento onde o documento SAP é inexistente.
	 * 
	 * Retorna todas as guias e pagamento em que o documento SAP não está preenchido
	 * @return List<GuiaPagamentoVO>
	 * 
	 */
	public List<GuiaPagamentoVO> consultarGuiaDocSAPNulo() {
		try {
			return convertToListVO(guiaPagamentoRepositorio.findByDocumentoSAPIsNull());
		} catch (Exception e) {
			logger.error("Erro ao consultar guias de pagamentos: ",e);
			throw new NegocioException(MensagemEnum.ERRO_GENERICO);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por excluir um recurso GuiaPagamento.
	 * 
	 * @param id
	 * 
	 */
	public void exluirGuiaPagamento(Long id) {
		GuiaPagamentoVO vo = consultarGuiaPorID(id);
		if(vo == null || vo.getDocumentoSAP() != null) throw new NegocioException(MensagemEnum.NAO_E_POSSIVEL_EXCLUIR_GUIA_PAGAMENTO_COM_DOC_SAP);
		try {
			guiaPagamentoRepositorio.delete(id);
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_EXLCUIR_GUIA_PAGAMENTO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_EXLCUIR_GUIA_PAGAMENTO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por converter uma entidade GuiaPagamento em VO!
	 * 
	 * @param guiaBase
	 * @return GuiaPagamentoVO
	 * 
	 */
	private GuiaPagamentoVO convertToVO(GuiaPagamento guiaBase) {
		try {
			GuiaPagamentoVO vo = modelMapper.map(guiaBase, GuiaPagamentoVO.class);
			return vo;
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_GUIA_PAGAMENTO_EM_VO.getMensagem());
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_GUIA_PAGAMENTO_EM_VO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por converter uma lista do recurso GuiaPagamento em uma lista VO.
	 * 
	 * @param listaBase
	 * @return List<GuiaPagamentoVO>
	 * 
	 */
	private List<GuiaPagamentoVO> convertToListVO(List<GuiaPagamento> listaBase){
		try {
			List<GuiaPagamentoVO> listaVO = new ArrayList<>();
			for (GuiaPagamento guia : listaBase) {
				GuiaPagamentoVO vo = modelMapper.map(guia, GuiaPagamentoVO.class);
				listaVO.add(vo);
			}
			return listaVO;
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_LISTA_GUIA_PAGAMENTO_EM_UMA_LISTA_VO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_LISTA_GUIA_PAGAMENTO_EM_UMA_LISTA_VO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por converer um GuiaPagamentoVO no recurso GuiaPagamento.
	 * 
	 * @param guiaPagamentoVO
	 * @return GuiaPagamento
	 * 
	 */
	private GuiaPagamento convertToEntidadeBase(GuiaPagamentoVO guiaPagamentoVO) {
		try {
			GuiaPagamento guiaBase = modelMapper.map(guiaPagamentoVO, GuiaPagamento.class);
			return guiaBase;
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_UM_VO_NA_ENTIDADE_GUIA_PAGAMENTO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_UM_VO_NA_ENTIDADE_GUIA_PAGAMENTO);
		}
	}

}
