package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.ContaRazaoLiberacaoLitigante;

@Repository
public interface ContaRazaoLiberacaoLitiganteRepositorio extends JpaRepository<ContaRazaoLiberacaoLitigante, Long>, QueryByExampleExecutor<ContaRazaoLiberacaoLitigante>{

}
