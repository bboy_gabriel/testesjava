package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;
import br.com.chesf.projurfinapi.entidade.GuiaPagamento;

@Repository
public interface GuiaPagamentoRepositorio extends JpaRepository<GuiaPagamento, Long>, QueryByExampleExecutor<GuiaPagamento>{
	
	public List<GuiaPagamento> findByDocumentoSAPIsNull() throws Exception;

}
