package br.com.chesf.projurfinapi.util;

import java.io.IOException;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import br.com.chesf.projurfinapi.enums.IndicadorSimNaoEnum;

public class IndicadorSimNaoStdDeserializer extends StdDeserializer<IndicadorSimNaoEnum> {
	
	 public IndicadorSimNaoStdDeserializer() { 
	        this(null); 
	 }
	 
	protected IndicadorSimNaoStdDeserializer(Class<?> vc) {
		super(vc);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public IndicadorSimNaoEnum deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException {
		String valor = p.getValueAsString();
		return Stream.of(IndicadorSimNaoEnum.values())
				.filter(en->en.name().equals(valor)).findFirst().orElseGet(null);
	}

}
