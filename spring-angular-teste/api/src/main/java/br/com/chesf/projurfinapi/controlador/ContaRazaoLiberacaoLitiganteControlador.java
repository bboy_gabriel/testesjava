package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.servico.ContaRazaoLiberacaoLitiganteServico;

/**
 * 
 * Classe responsavel por fornecer Endpoints referentes a manipulação do recurso ContaRazaoLiberacaoLitigante.
 * 
 * @author jvneto1
 *
 */
@RestController
@RequestMapping("/api/contaRazaoLiberacaoLitigante")
@Consumes("application/json")
@Produces("application/json")
public class ContaRazaoLiberacaoLitiganteControlador implements Serializable{

	private static final long serialVersionUID = 1L;

	@Autowired
	private ContaRazaoLiberacaoLitiganteServico contaRazaoLiberacaoLitiganteServico;
	
	/**
	 * 
	 * Endpoint responsavel por fornecer uma lista de recursos ContaRazaoLiberacaoLitigante.
	 * 
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping
	public ResponseEntity consultarContaRazaoLiberacaoLitigante() {
		return new ResponseEntity(contaRazaoLiberacaoLitiganteServico.consultarContaRazaoLiberacaoLitigante(), HttpStatus.OK);
	}
	
}
