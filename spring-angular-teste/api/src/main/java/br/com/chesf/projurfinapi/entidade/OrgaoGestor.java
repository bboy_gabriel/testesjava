package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.chesf.projurfinapi.enums.SituacaoOrgaoGestorEnum;

/**
 * 
 * @author jvneto1
 *
 * Classe responavel por mapear o recurso OrgaoGestor.
 *
 */
@Entity
@Table(name = "TB_ORGAO_GESTOR", schema="PROJUR")
@SequenceGenerator(name = "SQ_ORGAO_GESTOR", sequenceName = "SQ_ORGAO_GESTOR", allocationSize = 1)
public class OrgaoGestor implements Serializable{

	private static final long serialVersionUID = -7144414868786708221L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ORGAO_GESTOR")
    @Column(name = "ID_ORGAO_GESTOR")
    private Long id;
	
	@Column(name = "CD_ORGAO_GESTOR", length = 6, nullable = false)
	private Long codigo;
	
	@Column(name = "DS_ORGAO_GESTOR", length = 40, nullable = false)
	private String descricao;
	
	@Column(name = "IC_SITUACAO", length = 1, nullable = false)
    @Enumerated(EnumType.STRING)
    private SituacaoOrgaoGestorEnum situacaoOrgaoGestorEnum;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the situacaoOrgaoGestorEnum
	 */
	public SituacaoOrgaoGestorEnum getSituacaoOrgaoGestorEnum() {
		return situacaoOrgaoGestorEnum;
	}

	/**
	 * @param situacaoOrgaoGestorEnum the situacaoOrgaoGestorEnum to set
	 */
	public void setSituacaoOrgaoGestorEnum(SituacaoOrgaoGestorEnum situacaoOrgaoGestorEnum) {
		this.situacaoOrgaoGestorEnum = situacaoOrgaoGestorEnum;
	}
	
	public String toString(){
		return this.descricao;
	}
	
}
