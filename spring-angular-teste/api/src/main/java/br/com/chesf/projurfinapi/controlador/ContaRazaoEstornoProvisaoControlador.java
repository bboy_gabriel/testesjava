package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.ContaRazaoEstornoProvisaoServico;

/**
 * Classe que faz a interface com o módulo web para funcionalidade Estorno de uma provisão.
 * @author Italo Souto de Figueiredo - isfigueiredo
 *
 */
@RestController
@RequestMapping(value = "/api/contaRazaoEstornoProvisao")
@Consumes("application/json")
@Produces("application/json")
public class ContaRazaoEstornoProvisaoControlador implements Serializable{

	private static final long serialVersionUID = 5711874948647575465L;
	
	@Autowired
	private ContaRazaoEstornoProvisaoServico contaRazaoEstornoProvisaoServico;
	
	/**
	 * 
	 * Endpoint responsavel por consultar uma lista do ContaRazaoEstornoProvisao.
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping
	public ResponseEntity consultarContaRazaoEstornoProvisao() {
		return new ResponseEntity(contaRazaoEstornoProvisaoServico.consultarContaRazaoEstornoProvisao(), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint resonsável por consultar uma lista de recursos ContaRazaoEstornoProvisao pelo codigo do tipo do processo.
	 * 
	 * @param codigo
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/tipoProcesso/{codigo}")
	public ResponseEntity consultarLContaRazaoEstornoPorTipoProcesso(@PathVariable @Valid @NotEmpty String codigo) {
		try {
			return new ResponseEntity(contaRazaoEstornoProvisaoServico.consultarLContaRazaoPorTipoProcesso(codigo), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
