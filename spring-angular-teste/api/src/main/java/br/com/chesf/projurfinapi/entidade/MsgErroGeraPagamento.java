package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MENSG_ERRO", schema="PROJUR")
@SequenceGenerator(name = "SQ_MENSG_ERRO", sequenceName = "SQ_MENSG_ERRO", allocationSize = 1)
public class MsgErroGeraPagamento implements Serializable{

	private static final long serialVersionUID = 9023727249586211516L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_MENSG_ERRO")
    @Column(name = "ID_MENSG_ERRO")
    private Long id;
	
	@Column(name = "ID_SOLICT_PAGM", nullable = false)
	private Long idSolicitacao;
	
	@Column(name = "DS_MENSG_ERRO", nullable = false)
	private String mensagens;
	
	@Column(name = "DT_INSERCAO", nullable = false)
	private LocalDateTime dataInsercao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the mensagens
	 */
	public String getMensagens() {
		return mensagens;
	}

	/**
	 * @param mensagens the mensagens to set
	 */
	public void setMensagens(String mensagens) {
		this.mensagens = mensagens;
	}

	/**
	 * @return the idSolicitacao
	 */
	public Long getIdSolicitacao() {
		return idSolicitacao;
	}

	/**
	 * @param idSolicitacao the idSolicitacao to set
	 */
	public void setIdSolicitacao(Long idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}

	/**
	 * @return the dataInsercao
	 */
	public LocalDateTime getDataInsercao() {
		return dataInsercao;
	}

	/**
	 * @param dataInsercao the dataInsercao to set
	 */
	public void setDataInsercao(LocalDateTime dataInsercao) {
		this.dataInsercao = dataInsercao;
	}
	
}
