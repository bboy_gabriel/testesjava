package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.TlnomEmprgServico;

@RestController 
@RequestMapping("/api/tlnomEmprg")
public class TlnomEmprgControlador implements Serializable{

	@Autowired
	private TlnomEmprgServico emprgServico;
	
	@GetMapping("/valida/{matriculaSAP}")
	public ResponseEntity validarMatriculaSAP(@PathVariable @NotNull String matriculaSAP) {
		try {
			return new ResponseEntity(emprgServico.validarMatriculaSAP(matriculaSAP), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
