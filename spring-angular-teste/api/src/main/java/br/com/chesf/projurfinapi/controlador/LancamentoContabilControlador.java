package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.LancamentoContabilRelatorioVO;
import br.com.chesf.projurfinapi.entidade.vo.LancamentoContabilVO;
import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.servico.LancamentoContabilServico;

/**
 * 
 * Classe responsável por fornecer uma camada de endpoints para manipulação do recurso LancamentoContabil.
 * 
 * @author jvneto1
 *
 */
@RestController
@RequestMapping(value = "/api/lancamentoProvisao")
@Consumes("application/json")
@Produces("application/json")
public class LancamentoContabilControlador implements Serializable{

	private static final long serialVersionUID = -2468205109150101953L;
	
	@Autowired
	private LancamentoContabilServico lancamentoContabilServico;
	
	/**
	 * 
	 * Endpoint responsável por cadastrar um lançamento contábil referente a uma provisão.
	 * 
	 * @param lancamentoContabilVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/provisao")
	public ResponseEntity cadastrarLancamentoContabilProvisao(@RequestBody @Valid @NotNull LancamentoContabilVO lancamentoContabilVO) {
		return new ResponseEntity(lancamentoContabilServico.cadastrarLancamentoContabilProvisao(lancamentoContabilVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por cadastrar um lançamento contábil referente a um estorno.
	 * 
	 * @param lancamentoContabilVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/estorno")
	public ResponseEntity cadastrarLancamentoContabilEstorno(@RequestBody @Valid @NotNull LancamentoContabilVO lancamentoContabilVO) {
		return new ResponseEntity(lancamentoContabilServico.cadastrarLancamentoContabilEstorno(lancamentoContabilVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por consultar uma lista de lançamentos por um periodo determinado, para compor um relatorio.
	 * 
	 * @param dataLancamento
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/ciclo/{dataLancamentoMensal}")
	public ResponseEntity consultarLancamentosCiclo(@PathVariable @Valid @NotEmpty String dataLancamentoMensal) {
		return new ResponseEntity(lancamentoContabilServico.consultarLancamentosCiclo(dataLancamentoMensal), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por atualizar os recursos LancamentoContabil com a data da geração do arquivo CSV.
	 * 
	 * @param lancamentoContabilRelatorioVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/dataCSV")
	public ResponseEntity atualizarDataGeracaoArquivoCSV(@RequestBody @Valid @NotNull LancamentoContabilRelatorioVO lancamentoContabilRelatorioVO) {
		return new ResponseEntity(lancamentoContabilServico.atualizarDataGeracaoArquivoCSV(lancamentoContabilRelatorioVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por fornecer uma soma de todas as provisões referentes ao processo.
	 * 
	 * @param numeroProcessoVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/totalDisponivel")
	public ResponseEntity consultarValorDisponivelLancamentoProvisao(@RequestBody @Valid @NotNull NumeroProcessoVO numeroProcessoVO) {
		return new ResponseEntity(lancamentoContabilServico.consultarValorDisponivelLancamentoProvisao(numeroProcessoVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por consultar uma lista de recursos LancamentoContabil pelo número do processo.
	 * 
	 * @param numeroProcessoVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/processo")
	public ResponseEntity consultarLancamentoContabilPorProcesso(@RequestBody @Valid @NotNull NumeroProcessoVO numeroProcessoVO) {
		return new ResponseEntity(lancamentoContabilServico.consultarLancamentoContabilPorProcesso(numeroProcessoVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint resonsável por consultar um recurso LancamentoContabil por id!
	 * 
	 * @param id
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/{id}")
	public ResponseEntity consultarLancamentoContabilPorId(@PathVariable @Valid @NotEmpty Long id) {
		return new ResponseEntity(lancamentoContabilServico.consultarLancamentoContabilPorId(id), HttpStatus.OK);
	}
	
}
