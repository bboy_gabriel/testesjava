package br.com.chesf.projurfinapi.enums;

public enum TipoLancamentoContabilEnum {

	PROVISOES(1, "Provisões"),
	ESTORNOS_PROVISAO(2, "Est. Provisão"),
	LIBERACAO_LITIGANTE(3, "Lib. Litigante"),
	ESTORNO_LIBERACAO_LITIGANTE(4, "Est. Lib. Litigante");
	
	private Integer id;
	private String descricao;
	
	private TipoLancamentoContabilEnum(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public static final TipoLancamentoContabilEnum getTipoLancamentoContabilEnumPorID(Integer id) {
		TipoLancamentoContabilEnum tipo = null;
		switch (id) {
		case 1:
			tipo = TipoLancamentoContabilEnum.PROVISOES;
			break;
		case 2:
			tipo = TipoLancamentoContabilEnum.ESTORNOS_PROVISAO;
			break;
		case 3:
			tipo = TipoLancamentoContabilEnum.LIBERACAO_LITIGANTE;
			break;
		case 4:
			tipo = TipoLancamentoContabilEnum.ESTORNO_LIBERACAO_LITIGANTE;
			break;
		}
		return tipo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
