package br.com.chesf.projurfinapi.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.chesf.projurfinapi.jwt.CustomLogoutSuccessHandler;
import br.com.chesf.projurfinapi.jwt.JwtTokenAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class AuthorizationServerConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomLogoutSuccessHandler logOutSucessHandle;
	
	@Autowired
	private AuthenticacaoSuccessHandler authenticacaoSuccessHandler; 
	 
	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception {	 
		
		httpSecurity.authorizeRequests()
				 .antMatchers("/*", "/projur/index.html").permitAll()
			     .antMatchers("/api/**")
			     .authenticated()
			     .anyRequest().permitAll()
				.and()
				.exceptionHandling()
				.accessDeniedHandler(new CustomAccessDeniedHandler())
				.and()
//				 filtra outras requisições para verificar a presença do JWT no header
				.addFilterBefore(new JwtTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.logout().logoutSuccessHandler(logOutSucessHandle)
				.and()
				.formLogin().loginPage("/login")
				.successHandler(authenticacaoSuccessHandler)
				.and()
				.sessionManagement()				
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)				
				.and()				
				.logout().disable()				
				.csrf().disable();
				
		
	}
	
	 @Override
	    public void configure(WebSecurity web) throws Exception {
	        web.ignoring()
	        .antMatchers(HttpMethod.OPTIONS, "/**")
	        .antMatchers("/*.html")
	        .antMatchers("/app/**/*.{js,html}")
	        .antMatchers("/assets/**")
	        .antMatchers("/maps/**")
	        .antMatchers("/i18n/**")
	        .antMatchers("/scripts/**")
	        .antMatchers("/styles/**")
	        .antMatchers("/ws/**");
	    }
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
                registry.addMapping("/**").allowedMethods("PUT","POST", "DELETE", "OPTIONS", "GET")
                .allowedHeaders("*").allowedOrigins("*");
            }
        };
    }	
	
	private class CustomAccessDeniedHandler implements AccessDeniedHandler {		 
	    @Override
	    public void handle
	      (HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) 
	      throws IOException, ServletException {
	        response.sendRedirect("/");
	    }
	}
	 
}
