package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

public class ParteVO implements Serializable{

	private static final long serialVersionUID = 4011341142399265537L;

	private Long codigoParte;
	
	private Long nrCNPJ;
	
	private String nmRazaoSocial;
	
	private String nrCPF;
	
	private Long nrIdent;
	
	private String nrNit;
	
	private String nrPasep;
	
	private String cdMatricula;
	
	private String nmEmpresa;
	
	private String sgEstadoCTPS;
	
	private Long nrParteSijus;
	
	private Long tpParteSijus;
	
	private String idMigracaoSijus;
	
	private String nrCTPS;

	/**
	 * @return the codigoParte
	 */
	public Long getCodigoParte() {
		return codigoParte;
	}

	/**
	 * @param codigoParte the codigoParte to set
	 */
	public void setCodigoParte(Long codigoParte) {
		this.codigoParte = codigoParte;
	}

	/**
	 * @return the nrCNPJ
	 */
	public Long getNrCNPJ() {
		return nrCNPJ;
	}

	/**
	 * @param nrCNPJ the nrCNPJ to set
	 */
	public void setNrCNPJ(Long nrCNPJ) {
		this.nrCNPJ = nrCNPJ;
	}

	/**
	 * @return the nmRazaoSocial
	 */
	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	/**
	 * @param nmRazaoSocial the nmRazaoSocial to set
	 */
	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}

	/**
	 * @return the nrCPF
	 */
	public String getNrCPF() {
		return nrCPF;
	}

	/**
	 * @param nrCPF the nrCPF to set
	 */
	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	/**
	 * @return the nrIdent
	 */
	public Long getNrIdent() {
		return nrIdent;
	}

	/**
	 * @param nrIdent the nrIdent to set
	 */
	public void setNrIdent(Long nrIdent) {
		this.nrIdent = nrIdent;
	}

	/**
	 * @return the nrNit
	 */
	public String getNrNit() {
		return nrNit;
	}

	/**
	 * @param nrNit the nrNit to set
	 */
	public void setNrNit(String nrNit) {
		this.nrNit = nrNit;
	}

	/**
	 * @return the nrPasep
	 */
	public String getNrPasep() {
		return nrPasep;
	}

	/**
	 * @param nrPasep the nrPasep to set
	 */
	public void setNrPasep(String nrPasep) {
		this.nrPasep = nrPasep;
	}

	/**
	 * @return the cdMatricula
	 */
	public String getCdMatricula() {
		return cdMatricula;
	}

	/**
	 * @param cdMatricula the cdMatricula to set
	 */
	public void setCdMatricula(String cdMatricula) {
		this.cdMatricula = cdMatricula;
	}

	/**
	 * @return the nmEmpresa
	 */
	public String getNmEmpresa() {
		return nmEmpresa;
	}

	/**
	 * @param nmEmpresa the nmEmpresa to set
	 */
	public void setNmEmpresa(String nmEmpresa) {
		this.nmEmpresa = nmEmpresa;
	}

	/**
	 * @return the sgEstadoCTPS
	 */
	public String getSgEstadoCTPS() {
		return sgEstadoCTPS;
	}

	/**
	 * @param sgEstadoCTPS the sgEstadoCTPS to set
	 */
	public void setSgEstadoCTPS(String sgEstadoCTPS) {
		this.sgEstadoCTPS = sgEstadoCTPS;
	}

	/**
	 * @return the nrParteSijus
	 */
	public Long getNrParteSijus() {
		return nrParteSijus;
	}

	/**
	 * @param nrParteSijus the nrParteSijus to set
	 */
	public void setNrParteSijus(Long nrParteSijus) {
		this.nrParteSijus = nrParteSijus;
	}

	/**
	 * @return the tpParteSijus
	 */
	public Long getTpParteSijus() {
		return tpParteSijus;
	}

	/**
	 * @param tpParteSijus the tpParteSijus to set
	 */
	public void setTpParteSijus(Long tpParteSijus) {
		this.tpParteSijus = tpParteSijus;
	}

	/**
	 * @return the idMigracaoSijus
	 */
	public String getIdMigracaoSijus() {
		return idMigracaoSijus;
	}

	/**
	 * @param idMigracaoSijus the idMigracaoSijus to set
	 */
	public void setIdMigracaoSijus(String idMigracaoSijus) {
		this.idMigracaoSijus = idMigracaoSijus;
	}

	/**
	 * @return the nrCTPS
	 */
	public String getNrCTPS() {
		return nrCTPS;
	}

	/**
	 * @param nrCTPS the nrCTPS to set
	 */
	public void setNrCTPS(String nrCTPS) {
		this.nrCTPS = nrCTPS;
	}
	
}
