package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_LANC_CONTAB", schema = "PROJUR")
@SequenceGenerator(name = "SQ_LANC_CONTAB", sequenceName = "SQ_LANC_CONTAB", allocationSize = 1)
public class LancamentoContabil implements Serializable{

	private static final long serialVersionUID = 5242021935147408738L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_LANC_CONTAB")
	@Column(name = "ID_LANC_CONTAB", nullable = false)
	private Long id;
	
	@Column(name = "TP_LANC", length = 1, nullable = false)
	private String tipoLancamento;
	
	@Column(name = "NR_PROC", length = 9, nullable = false)
	private Long numeroProcesso;
	
	@Column(name = "DT_LANC", nullable = false)
	private LocalDate dataLancamento;
	
	@Column(name = "VL_PROVISAO", nullable = false)
	private BigDecimal valor;
	
	@Column(name = "NR_CONTA_RAZAO_CREDITO", length = 10, nullable = false)
	private Long contaCredito;
	
	@Column(name = "NR_CONTA_RAZAO_DEBITO", length = 10, nullable = false)
	private Long contaDebito;
	
	@OneToOne(targetEntity = CentroDeCusto.class)
	@JoinColumn(name = "ID_CENTR_CUST", referencedColumnName = "ID_CENTR_CUST")
	private CentroDeCusto centroDeCusto;
	
	@OneToOne(targetEntity = ElementoPep.class)
	@JoinColumn(name = "CD_ELEMENTO_PEP", referencedColumnName = "CD_ELEMENTO_PEP")
	private ElementoPep elementoPep;
	
	@OneToOne(targetEntity = Ordem.class)
	@JoinColumn(name = "ID_ORDEM", referencedColumnName = "ID_ORDEM")
	private Ordem ordem;
	
	@Column(name = "DS_HIST", length = 30, nullable = false)
	private String historico;
	
	@Column(name = "DT_GERC_CSV")
	private LocalDate dataGeracaoCSV;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the tipoLancamento
	 */
	public String getTipoLancamento() {
		return tipoLancamento;
	}

	/**
	 * @param tipoLancamento the tipoLancamento to set
	 */
	public void setTipoLancamento(String tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	/**
	 * @return the numeroProcesso
	 */
	public Long getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(Long numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	/**
	 * @return the dataLancamento
	 */
	public LocalDate getDataLancamento() {
		return dataLancamento;
	}

	/**
	 * @param dataLancamento the dataLancamento to set
	 */
	public void setDataLancamento(LocalDate dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the contaCredito
	 */
	public Long getContaCredito() {
		return contaCredito;
	}

	/**
	 * @param contaCredito the contaCredito to set
	 */
	public void setContaCredito(Long contaCredito) {
		this.contaCredito = contaCredito;
	}

	/**
	 * @return the contaDebito
	 */
	public Long getContaDebito() {
		return contaDebito;
	}

	/**
	 * @param contaDebito the contaDebito to set
	 */
	public void setContaDebito(Long contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * @return the centroDeCusto
	 */
	public CentroDeCusto getCentroDeCusto() {
		return centroDeCusto;
	}

	/**
	 * @param centroDeCusto the centroDeCusto to set
	 */
	public void setCentroDeCusto(CentroDeCusto centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	/**
	 * @return the elementoPep
	 */
	public ElementoPep getElementoPep() {
		return elementoPep;
	}

	/**
	 * @param elementoPep the elementoPep to set
	 */
	public void setElementoPep(ElementoPep elementoPep) {
		this.elementoPep = elementoPep;
	}

	/**
	 * @return the ordem
	 */
	public Ordem getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem the ordem to set
	 */
	public void setOrdem(Ordem ordem) {
		this.ordem = ordem;
	}

	/**
	 * @return the historico
	 */
	public String getHistorico() {
		return historico;
	}

	/**
	 * @param historico the historico to set
	 */
	public void setHistorico(String historico) {
		this.historico = historico;
	}

	/**
	 * @return the dataGeracaoCSV
	 */
	public LocalDate getDataGeracaoCSV() {
		return dataGeracaoCSV;
	}

	/**
	 * @param dataGeracaoCSV the dataGeracaoCSV to set
	 */
	public void setDataGeracaoCSV(LocalDate dataGeracaoCSV) {
		this.dataGeracaoCSV = dataGeracaoCSV;
	}
	
}
