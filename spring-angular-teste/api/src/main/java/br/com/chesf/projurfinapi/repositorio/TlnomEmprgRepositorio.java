package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.TlnomEmprg;

@Repository
public interface TlnomEmprgRepositorio extends JpaRepository<TlnomEmprg, Long>, QueryByExampleExecutor<TlnomEmprg> {

	TlnomEmprg findByIdSap(String matriculaSAP);
}
