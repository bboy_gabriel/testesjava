package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;

/**
 * 
 * Classe responsavel por mapear a tabela TB_CONTA_RAZAO_PROVISAO.
 * 
 * @author jvneto1
 *
 */
@Entity
@Table(name = "TB_CONTA_RAZAO_PROVISAO", schema = "PROJUR")
@SequenceGenerator(name = "SQ_CONTA_RAZAO_PROVISAO", sequenceName = "SQ_CONTA_RAZAO_PROVISAO", allocationSize = 1)
public class ContaRazaoProvisao implements Serializable{

	private static final long serialVersionUID = 5739290991973445388L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CONTA_RAZAO_PROVISAO")
	@Column(name = "ID_CONTA_RAZAO_PROVISAO")
	private Long id;
	
	@Column(name = "CD_CONTA_DEBITO_PROVISAO", length = 10, nullable = false)
	private Long contaDebitoProvisao;
	
	@Column(name = "CD_CONTA_CREDITO_PROVISAO", length = 10, nullable = false)
	private Long contaCreditoProvisao;
	
	@Column(name = "TP_PROCESSO_PROVISAO", length = 1, nullable = false)
	private String codigoTipoProcesso;
	
	@Transient
	private TipoProcessoContaRazaoEnum tipoProcessoContaRazaoEnum;

	/**
	 * @return the contaDebitoProvisao
	 */
	public Long getContaDebitoProvisao() {
		return contaDebitoProvisao;
	}

	/**
	 * @param contaDebitoProvisao the contaDebitoProvisao to set
	 */
	public void setContaDebitoProvisao(Long contaDebitoProvisao) {
		this.contaDebitoProvisao = contaDebitoProvisao;
	}

	/**
	 * @return the contaCreditoProvisao
	 */
	public Long getContaCreditoProvisao() {
		return contaCreditoProvisao;
	}

	/**
	 * @param contaCreditoProvisao the contaCreditoProvisao to set
	 */
	public void setContaCreditoProvisao(Long contaCreditoProvisao) {
		this.contaCreditoProvisao = contaCreditoProvisao;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public String getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(String codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}

	/**
	 * @return the tipoProcessoContaRazaoEnum
	 */
	public TipoProcessoContaRazaoEnum getTipoProcessoContaRazaoEnum() {
		return tipoProcessoContaRazaoEnum;
	}

	/**
	 * @param tipoProcessoContaRazaoEnum the tipoProcessoContaRazaoEnum to set
	 */
	public void setTipoProcessoContaRazaoEnum(TipoProcessoContaRazaoEnum tipoProcessoContaRazaoEnum) {
		this.tipoProcessoContaRazaoEnum = tipoProcessoContaRazaoEnum;
	}

}
