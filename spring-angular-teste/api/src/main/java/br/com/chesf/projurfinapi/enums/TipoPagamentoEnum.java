package br.com.chesf.projurfinapi.enums;

/**
 * Enum que mapeia os tipo de pagamento para um tipo de despesa.
 * @author isfigueiredo
 *
 */
public enum TipoPagamentoEnum implements ITipoPagamento{
	
	ADM(1, "Administratitvo"),
	PROC(2, "Processual");
	
	private Integer codigo;
	private String desc;
	
	private TipoPagamentoEnum(Integer codigo, String desc) {
		this.codigo = codigo;
		this.desc = desc;			
	}
	
	/**
	 * Retorna o Enum usando o código
	 * @param codigo
	 * @return
	 */
	public TipoPagamentoEnum getByID(Integer codigo) {
		TipoPagamentoEnum tipoEncontrado = null;
		switch (codigo) {
		case 1:
			tipoEncontrado = TipoPagamentoEnum.ADM;
			break;
		case 2:
			tipoEncontrado = TipoPagamentoEnum.PROC;
		}
		return tipoEncontrado;
	}

}
