package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

public class EmpregadoMatriculaValidoVO implements Serializable{

	private static final long serialVersionUID = -608587679048040206L;

	private String nomeEmpregado;
	
	private String areaLotacao;
	
	private boolean statusMatricula;

	/**
	 * @return the nomeEmpregado
	 */
	public String getNomeEmpregado() {
		return nomeEmpregado;
	}

	/**
	 * @param nomeEmpregado the nomeEmpregado to set
	 */
	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}

	/**
	 * @return the areaLotacao
	 */
	public String getAreaLotacao() {
		return areaLotacao;
	}

	/**
	 * @param areaLotacao the areaLotacao to set
	 */
	public void setAreaLotacao(String areaLotacao) {
		this.areaLotacao = areaLotacao;
	}

	/**
	 * @return the statusMatricula
	 */
	public boolean isStatusMatricula() {
		return statusMatricula;
	}

	/**
	 * @param statusMatricula the statusMatricula to set
	 */
	public void setStatusMatricula(boolean statusMatricula) {
		this.statusMatricula = statusMatricula;
	}
	
}
