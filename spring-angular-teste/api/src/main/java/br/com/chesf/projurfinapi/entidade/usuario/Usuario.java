package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.chesf.projurfinapi.enums.StatusUsuarioEnum;

/**
 * Classe que representa um usuário do sistema. No caso um funcionário da Chesf.
 * @author isfigueiredo
 *
 */
@Entity
@Table(name = "TB_USUARIO", schema = "PROJUR")
public class Usuario implements Serializable{

	private static final long serialVersionUID = 6234823572645119089L;
	
	@Id
    @Column(name = "CD_MATR", nullable = false, length = 6)
	private String matricula;
	
	@OneToOne(targetEntity = Perfil.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
	@JoinColumn(name = "CD_PERFIL", referencedColumnName = "CD_PERFIL", nullable = false)
	private Perfil perfil;
	
	
	@Column(name = "IC_SITUACAO", length = 1, nullable = false)
    @Enumerated(EnumType.STRING)
	private StatusUsuarioEnum status;
	
	@Column(name = "NM_LOGIN")
	private String nomeUsuario;
	
//	@Column(name = "DT_ULTM_ACES")
//	private Date dataUltimoAcesso;
//	
//	@Column(name = "DT_MANUT")
//	private Date dataManutencao;
//
//	@Column(name = "CD_MATR",insertable = false, updatable = false, nullable = true)
//	private Usuario usuarioManutencao;
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public StatusUsuarioEnum getStatus() {
		return status;
	}
	public void setStatus(StatusUsuarioEnum status) {
		this.status = status;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
//	public Date getDataUltimoAcesso() {
//		return dataUltimoAcesso;
//	}
//	public void setDataUltimoAcesso(Date dataUltimoAcesso) {
//		this.dataUltimoAcesso = dataUltimoAcesso;
//	}
//	public Date getDataManutencao() {
//		return dataManutencao;
//	}
//	public void setDataManutencao(Date dataManutencao) {
//		this.dataManutencao = dataManutencao;
//	}
//	public Usuario getUsuarioManutencao() {
//		return usuarioManutencao;
//	}
//	public void setUsuarioManutencao(Usuario usuarioManutencao) {
//		this.usuarioManutencao = usuarioManutencao;
//	}	

}
