package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import br.com.chesf.projurfinapi.entidade.ProcessoParte;

/**
 * 
 * Interface responsável por fornecer uma camada de persistencia ao recurso ProcessoParte.
 * 
 * @author jvneto1
 *
 */
public interface ProcessoParteRepositorio extends JpaRepository<ProcessoParte, Long>, QueryByExampleExecutor<ProcessoParte>{

	ProcessoParte findByCdParte(Long cdParte);
	
}
