package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import br.com.chesf.projurfinapi.entidade.TipoDeDespesa;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer uma camada de persistencia ao recurso TipoDeDespesa.
 *
 */
public interface TipoDeDespesaRepositorio extends JpaRepository<TipoDeDespesa, Long>, QueryByExampleExecutor<TipoDeDespesa>{

	TipoDeDespesa findByCodigo(String codigo);
	
	Boolean existsByCodigoAndIdNotIn(String codigo,Long ... ids);
}
