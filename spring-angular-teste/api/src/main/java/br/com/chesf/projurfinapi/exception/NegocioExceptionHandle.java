package br.com.chesf.projurfinapi.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import br.com.chesf.projurfinapi.entidade.vo.MensagemNegocioVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;

@ControllerAdvice
public class NegocioExceptionHandle{	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NegocioExceptionHandle.class);
	
	private static final String ERRO_GENERICO = "Campos inválidos";
	
	@ExceptionHandler(value = { NegocioException.class })
	public ResponseEntity<Object> handleConflict(NegocioException negocioException, WebRequest request) {
		return handleExceptionInternal(negocioException, negocioException.getMensagemNegocioVO(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	@ExceptionHandler(value = { MethodArgumentNotValidException.class})
	public ResponseEntity<Object> handleConflict(MethodArgumentNotValidException ex, WebRequest request) {
		MensagemNegocioVO mensagemPricipal = new MensagemNegocioVO(HttpStatus.PRECONDITION_REQUIRED.toString(), ERRO_GENERICO);
		List<MensagemNegocioVO> pilhaErros = ex.getBindingResult().getAllErrors().stream().map(error-> {
			FieldError campoError = (FieldError)error;
			return new MensagemNegocioVO(null, campoError.getField(), error.getDefaultMessage());
		}).collect(Collectors.toList());
		mensagemPricipal.setDetalhes(pilhaErros);		
		return handleExceptionInternal(ex, mensagemPricipal, new HttpHeaders(), HttpStatus.PRECONDITION_REQUIRED, request);
	}
	
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) { 		
		return handleExceptionInternal(ex, new MensagemNegocioVO(MensagemEnum.ERRO_GENERICO), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	 
	public ResponseEntity<Object> handleExceptionInternal(
			Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex.getMessage(), WebRequest.SCOPE_REQUEST);
		}
		LOGGER.info("Tratando a exceção",ex);
		return new ResponseEntity<>(body, headers, status);
	}

	
}
