package br.com.chesf.projurfinapi.jwt;

import java.nio.file.attribute.UserPrincipal;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {
	@Value("${app.jwt.secret}")
	private String jwtSecret;
	
	@Value("${app.jwt.expirationTime}")
	private int jwtExpirationTime;
	
	public String generateToken(Authentication authentication) {
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		
		Calendar newExpirationTime = Calendar.getInstance();
		newExpirationTime.set(Calendar.MILLISECOND, jwtExpirationTime);
		
		return Jwts.builder()
				.setSubject(userPrincipal.getName())
				.setExpiration(newExpirationTime.getTime())
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}
}
