package br.com.chesf.projurfinapi.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.vo.UsuarioVO;
import br.com.chesf.projurfinapi.jwt.TokenAuthenticationService;

@Service
public class AuthenticacaoSuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		User user = (User) authentication.getPrincipal();
		UsuarioVO userVO = (UsuarioVO) user;
		tokenAuthenticationService.addAuthentication(response, userVO);
	}

}
