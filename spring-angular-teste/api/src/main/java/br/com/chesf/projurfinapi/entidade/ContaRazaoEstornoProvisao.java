package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CONTA_RAZAO_ESTORNO", schema = "PROJUR")
@SequenceGenerator(name = "SQ_CONTA_RAZAO_ESTORNO", sequenceName = "SQ_CONTA_RAZAO_ESTORNO", allocationSize = 1)
public class ContaRazaoEstornoProvisao implements Serializable{
	
	private static final long serialVersionUID = -3246430326229775446L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CONTA_RAZAO_ESTORNO")
	@Column(name = "ID_CONTA_RAZAO_ESTORNO")
	private Long id;
	
	@Column(name = "CD_CONTA_CREDITO_ESTORNO", nullable = false, length = 19)
	private Long contaCreditoEstorno;
	
	@Column(name = "CD_CONTA_DEBITO_ESTORNO", nullable = false, length = 19)
	private Long contaDebitoEstorno;
	
	@Column(name = "TP_PROCESSO_ESTORNO_PROVISAO", nullable = false, length = 1)
	private String codigoTipoProcesso;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the contaCreditoEstorno
	 */
	public Long getContaCreditoEstorno() {
		return contaCreditoEstorno;
	}

	/**
	 * @param contaCreditoEstorno the contaCreditoEstorno to set
	 */
	public void setContaCreditoEstorno(Long contaCreditoEstorno) {
		this.contaCreditoEstorno = contaCreditoEstorno;
	}

	/**
	 * @return the contaDebitoEstorno
	 */
	public Long getContaDebitoEstorno() {
		return contaDebitoEstorno;
	}

	/**
	 * @param contaDebitoEstorno the contaDebitoEstorno to set
	 */
	public void setContaDebitoEstorno(Long contaDebitoEstorno) {
		this.contaDebitoEstorno = contaDebitoEstorno;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public String getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(String codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}
	
	

}
