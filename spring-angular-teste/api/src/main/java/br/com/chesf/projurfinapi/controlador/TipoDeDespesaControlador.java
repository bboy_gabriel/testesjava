package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.TipoDeDespesaVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.TipoDeDespesaServico;

/**
 * 
 * @author jvneto1
 * 
 * Controlador responsavel por  fornecer Endpoints referentes ao recurso TipoDeDespesa. 
 *
 */
@RestController
@RequestMapping("/api/tipoDeDespesas")
@Consumes("application/json")
@Produces("application/json")
public class TipoDeDespesaControlador implements Serializable{

	private static final long serialVersionUID = 7240828498944199058L;

	@Autowired
	private TipoDeDespesaServico tipoDeDespesaService;
	
	/**
	 * 
	 * @return ResponseEntity
	 * 
	 * Endpoint responsável por consultar uma lista de recursos TipoDeDespesa.
	 * 
	 */
	@GetMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(tipoDeDespesaService.consultarOrdenada(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity<>(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<TipoDeDespesaVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<TipoDeDespesaVO>(tipoDeDespesaService.consultarPorId(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/* crud retirado
	@PostMapping
	public ResponseEntity<TipoDeDespesaVO> cadastrar(@RequestBody TipoDeDespesaVO tipoDeDespesaVO) {
		try {
			return new ResponseEntity <TipoDeDespesaVO>(tipoDeDespesaService.cadastrar(tipoDeDespesaVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<TipoDeDespesaVO> atualizar(@RequestBody TipoDeDespesaVO tipoDeDespesaVO) {
		try {
			return new ResponseEntity <TipoDeDespesaVO>(tipoDeDespesaService.atualizar(tipoDeDespesaVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<TipoDeDespesaVO> remover(@PathVariable Long id) {
		try {
			tipoDeDespesaService.remover(id);
			return new ResponseEntity<TipoDeDespesaVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	*/
}
