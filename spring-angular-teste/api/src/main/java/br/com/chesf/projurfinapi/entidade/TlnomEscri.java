package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TLNOM_ESCRI", schema = "TLNOM")
@SequenceGenerator(name = "SQ_TLNOM_ESCRI", sequenceName = "SQ_TLNOM_ESCRI", allocationSize = 1)
public class TlnomEscri implements Serializable{
	
	private static final long serialVersionUID = -761448156860834807L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TLNOM_ESCRI")
	@Column(name = "PK_ESCRI")
	private Long id;
	
	@Column(name = "DS_ESCRI", length = 15)
	private String descricaoEscri;
	
	@Column(name = "SG_ORGA_AQSC", length = 6)
	private String siglaOrgaAqsc;
	
	@Column(name = "NR_CGC", length = 15)
	private Long numeroCgc;
	
	@Column(name = "VL_ALQT_ICMS", length = 7)
	private Long valorAlqtIcms;
	
	@Column(name = "DS_ORGA_AQSC", length = 100)
	private String descricaoOrgaAqsc;
	
	@Column(name = "DS_ORGA_SUBO", length = 100)
	private String descricaoOrgaSubo;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the descricaoEscri
	 */
	public String getDescricaoEscri() {
		return descricaoEscri;
	}

	/**
	 * @param descricaoEscri the descricaoEscri to set
	 */
	public void setDescricaoEscri(String descricaoEscri) {
		this.descricaoEscri = descricaoEscri;
	}

	/**
	 * @return the siglaOrgaAqsc
	 */
	public String getSiglaOrgaAqsc() {
		return siglaOrgaAqsc;
	}

	/**
	 * @param siglaOrgaAqsc the siglaOrgaAqsc to set
	 */
	public void setSiglaOrgaAqsc(String siglaOrgaAqsc) {
		this.siglaOrgaAqsc = siglaOrgaAqsc;
	}

	/**
	 * @return the numeroCgc
	 */
	public Long getNumeroCgc() {
		return numeroCgc;
	}

	/**
	 * @param numeroCgc the numeroCgc to set
	 */
	public void setNumeroCgc(Long numeroCgc) {
		this.numeroCgc = numeroCgc;
	}

	/**
	 * @return the valorAlqtIcms
	 */
	public Long getValorAlqtIcms() {
		return valorAlqtIcms;
	}

	/**
	 * @param valorAlqtIcms the valorAlqtIcms to set
	 */
	public void setValorAlqtIcms(Long valorAlqtIcms) {
		this.valorAlqtIcms = valorAlqtIcms;
	}

	/**
	 * @return the descricaoOrgaAqsc
	 */
	public String getDescricaoOrgaAqsc() {
		return descricaoOrgaAqsc;
	}

	/**
	 * @param descricaoOrgaAqsc the descricaoOrgaAqsc to set
	 */
	public void setDescricaoOrgaAqsc(String descricaoOrgaAqsc) {
		this.descricaoOrgaAqsc = descricaoOrgaAqsc;
	}

	/**
	 * @return the descricaoOrgaSubo
	 */
	public String getDescricaoOrgaSubo() {
		return descricaoOrgaSubo;
	}

	/**
	 * @param descricaoOrgaSubo the descricaoOrgaSubo to set
	 */
	public void setDescricaoOrgaSubo(String descricaoOrgaSubo) {
		this.descricaoOrgaSubo = descricaoOrgaSubo;
	}

}
