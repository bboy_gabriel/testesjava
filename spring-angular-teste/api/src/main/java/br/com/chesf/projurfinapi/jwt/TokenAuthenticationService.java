package br.com.chesf.projurfinapi.jwt;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.vo.UsuarioVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.ConfiguracaoServico;
import br.com.chesf.projurfinapi.servico.SapServico;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {
	static final String jwtSecret = "83D2BBD72B8F3486506C18F7ED32A0C7DAFF67A7646664841BA35C97BA7D22C10030626DB451B1118533054A49A2029118F34C8AE164DCE8A9B4F372F0396134";
	static final int jwtExpirationTime = 6000;
	static final String AUTHENTICATION_HEADER = "Authorization";
	
	@Autowired
	private ConfiguracaoServico configuracaoServico;
	
	private Logger logger = LoggerFactory.getLogger(SapServico.class);
	private static int tempoExpiracao = 30;
	
	@PostConstruct
	void init(){
		tempoExpiracao = recuperarTempoDeExpiracao();
	}
	
	public void addAuthentication(HttpServletResponse response, UsuarioVO usuario) throws IOException {
		
		  Claims claims = Jwts.claims().setSubject(usuario.getUsername());
	        claims.put("username", usuario.getUsername());
	        claims.put("matricula", usuario.getMatricula());
	        Calendar date =  Calendar.getInstance();
	       	date.add(Calendar.MINUTE, recuperarTempoDeExpiracao());
	        claims.put("exp", date.getTime()); 

	        String tokenSession = Jwts.builder()
	                .setClaims(claims)
	                .signWith(SignatureAlgorithm.HS512, jwtSecret)
	                .setExpiration(date.getTime())
	                .compact();
		
		response.addHeader(AUTHENTICATION_HEADER, "Bearer " + tokenSession);

	}
	
	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(AUTHENTICATION_HEADER);
		String tokenFinal = token != null? token.replace("Bearer ", ""):Strings.EMPTY;
		if (!tokenFinal.isEmpty() && !tokenFinal.contains("null")) {			
			try{		
					
				String user = Jwts.parser()
						.setSigningKey(jwtSecret)
						.parseClaimsJws(tokenFinal)
						.getBody()
						.getSubject();
				
				Date expirationTime = Jwts.parser()
						.setSigningKey(jwtSecret).
						parseClaimsJws(tokenFinal)
						.getBody().getExpiration();
				
				if (user != null) {
					return new UsernamePasswordAuthenticationToken(user, null, Collections.EMPTY_LIST);
				}
			}catch (ExpiredJwtException e) {
				throw new NegocioException(MensagemEnum.ERRO_SESSAO_EXPIRADA);
			}catch(Exception e)	{	
				throw new NegocioException(MensagemEnum.ERRO_SESSAO_EXPIRADA);
			}
		}
		
		return null;
	}
	
	public UsuarioVO getUserByToken(String token) {
		try {
            Claims body = Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
        	UsuarioVO user = new UsuarioVO((String) body.get("username"), "pwd", Arrays.asList(new GrantedAuthority() {			
    			@Override
    			public String getAuthority() {
    				return "USER";
    			}
    		}));                        
        	user.setMatricula((String) body.get("matricula"));
        	user.setExpiration(((Integer) body.get("exp")).toString());
            return user;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
	}
	
	private int recuperarTempoDeExpiracao() {
		int tempoExpiracao = 30;
		try {
			String tempo = configuracaoServico.consultarTempoExpiracao();
			tempoExpiracao = new Integer(tempo).intValue();
		} catch (NumberFormatException e) {
			logger.error("Erro ao recuperar o tempo de expiração do site. Será considerado um tempo de 30 min.");
		}
		
		return tempoExpiracao;
	}
}
