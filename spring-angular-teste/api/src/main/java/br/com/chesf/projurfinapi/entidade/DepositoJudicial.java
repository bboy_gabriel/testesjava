package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.chesf.projurfinapi.enums.TipoDepositoJudicialEnum;

/**
 * Representa a tabela TB_DEPOS_JUDC um deposíto judicial.
 * @author isfigueiredo
 *
 */
@Entity
@Table(name = "TB_DEPOS_JUDC", schema="PROJUR")
@SequenceGenerator(name = "SQ_DEPOS_JUDC", sequenceName = "SQ_DEPOS_JUDC", allocationSize = 1)
public class DepositoJudicial implements Serializable{

	private static final long serialVersionUID = 5028407308054536185L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_DEPOS_JUDC")
    @Column(name = "ID_DEPOS_JUDC")
	private Long id;
	
	@Column(name = "NR_PROC", length = 30, nullable = false)
	private String processo;
	
	@Column(name = "TP_DEPOS_JUDC", length = 1, nullable = false)
	private Long codigoDepositoJudicial;
	
	@Transient
	private TipoDepositoJudicialEnum tipoDepositoJudicialEnum;
	
	@Column(name = "DT_DEPOS", nullable = false)
	private LocalDate dataDeposito;
	
	@Column(name = "VL_DEPOS", nullable = false)
	private BigDecimal valorDeposito;
	
	@Column(name = "CD_TRANSACAO", length = 255)
	private String transacaoBancaria;
	
	@Column(name = "VL_LITIGANTE")
	private BigDecimal valorLitigante;
	
	@Column(name = "VL_CHESF")
	private BigDecimal valorChesf;
	
	@Column(name = "VL_CORRECAO_LITIGANTE")
	private BigDecimal valorCorrecaoLitigante;
	
	@Column(name = "VL_CORRECAO_CHESF")
	private BigDecimal valorCorrecaoChesf;
	
	@Column(name="CD_MATR", length = 6, nullable = false)
	private String usuario;
	
	@Column(name = "ID_DOCM_SAP", length= 10, nullable = true)
	private String documentoSap;
	
	public DepositoJudicial() {
		
	}
	
	public DepositoJudicial(Long id, String processo, TipoDepositoJudicialEnum tipoDepositoJudicialEnum, LocalDate dataDeposito,
			BigDecimal valorDeposito, BigDecimal valorLitigante, BigDecimal valorChesf,  String usuario) {
		
		this.id = id;
		this.processo = processo;
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
		this.dataDeposito = dataDeposito;
		this.valorDeposito = valorDeposito;
		this.valorChesf = valorChesf;
		this.valorLitigante = valorLitigante;
		this.usuario = usuario;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the processo
	 */
	public String getProcesso() {
		return processo;
	}

	/**
	 * @param processo the processo to set
	 */
	public void setProcesso(String processo) {
		this.processo = processo;
	}

	/**
	 * @return the tipoDepositoJudicialEnum
	 */
	public TipoDepositoJudicialEnum getTipoDepositoJudicialEnum() {
		return tipoDepositoJudicialEnum;
	}

	/**
	 * @param tipoDepositoJudicialEnum the tipoDepositoJudicialEnum to set
	 */
	public void setTipoDepositoJudicialEnum(TipoDepositoJudicialEnum tipoDepositoJudicialEnum) {
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
	}

	/**
	 * @return the dataDeposito
	 */
	public LocalDate getDataDeposito() {
		return dataDeposito;
	}

	/**
	 * @param dataDeposito the dataDeposito to set
	 */
	public void setDataDeposito(LocalDate dataDeposito) {
		this.dataDeposito = dataDeposito;
	}

	/**
	 * @return the valorDeposito
	 */
	public BigDecimal getValorDeposito() {
		return valorDeposito;
	}

	/**
	 * @param valorDeposito the valorDeposito to set
	 */
	public void setValorDeposito(BigDecimal valorDeposito) {
		this.valorDeposito = valorDeposito;
	}

	/**
	 * @return the transacaoBancaria
	 */
	public String getTransacaoBancaria() {
		return transacaoBancaria;
	}

	/**
	 * @param transacaoBancaria the transacaoBancaria to set
	 */
	public void setTransacaoBancaria(String transacaoBancaria) {
		this.transacaoBancaria = transacaoBancaria;
	}

	/**
	 * @return the valorLitigante
	 */
	public BigDecimal getValorLitigante() {
		return valorLitigante;
	}

	/**
	 * @param valorLitigante the valorLitigante to set
	 */
	public void setValorLitigante(BigDecimal valorLitigante) {
		this.valorLitigante = valorLitigante;
	}

	/**
	 * @return the valorChesf
	 */
	public BigDecimal getValorChesf() {
		return valorChesf;
	}

	/**
	 * @param valorChesf the valorChesf to set
	 */
	public void setValorChesf(BigDecimal valorChesf) {
		this.valorChesf = valorChesf;
	}

	/**
	 * @return the valorCorrecaoLitigante
	 */
	public BigDecimal getValorCorrecaoLitigante() {
		return valorCorrecaoLitigante;
	}

	/**
	 * @param valorCorrecaoLitigante the valorCorrecaoLitigante to set
	 */
	public void setValorCorrecaoLitigante(BigDecimal valorCorrecaoLitigante) {
		this.valorCorrecaoLitigante = valorCorrecaoLitigante;
	}

	/**
	 * @return the valorCorrecaoChesf
	 */
	public BigDecimal getValorCorrecaoChesf() {
		return valorCorrecaoChesf;
	}

	/**
	 * @param valorCorrecaoChesf the valorCorrecaoChesf to set
	 */
	public void setValorCorrecaoChesf(BigDecimal valorCorrecaoChesf) {
		this.valorCorrecaoChesf = valorCorrecaoChesf;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 */
	public String getDocumentoSap() {
		return documentoSap;
	}

	/**
	 * @param documentoSap the documentoSap to set
	 */
	public void setDocumentoSap(String documentoSap) {
		this.documentoSap = documentoSap;
	}
	
	/**
	 * @return the codigoDepositoJudicial
	 */
	public Long getCodigoDepositoJudicial() {
		return codigoDepositoJudicial;
	}

	/**
	 * @param codigoDepositoJudicial the codigoDepositoJudicial to set
	 */
	public void setCodigoDepositoJudicial(Long codigoDepositoJudicial) {
		this.codigoDepositoJudicial = codigoDepositoJudicial;
	}

}
