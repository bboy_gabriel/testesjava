package br.com.chesf.projurfinapi.repositorio;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.CentroDeCusto;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer uma camada de persistencia sobre o recurso CentroDeCusto.
 *
 */
@Repository
public interface CentroDeCustoRepositorio extends JpaRepository<CentroDeCusto, Long>, QueryByExampleExecutor<CentroDeCusto>{
	
	@SuppressWarnings("unchecked")
	@Override
	public CentroDeCusto save(CentroDeCusto entity) throws ConstraintViolationException;
	
	public Boolean existsByCodigo(String codigo);

	public Boolean existsByCodigoAndIdNotIn(String codigo, Long ... ids);
}
