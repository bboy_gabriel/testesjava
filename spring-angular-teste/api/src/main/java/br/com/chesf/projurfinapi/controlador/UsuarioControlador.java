package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.jwt.TokenAuthenticationService;
import br.com.chesf.projurfinapi.servico.UsuarioServico;

/**
 * Endpoint responsável por receber as requisições referente à usuários
 * @author isfigueiredo
 *
 */
@RestController
@RequestMapping("usuarios")
public class UsuarioControlador implements Serializable{

	private static final long serialVersionUID = -1306839829910396619L;
	
	@Autowired
	private UsuarioServico usuarioService;
	
	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;	
	
	@PostMapping
	public @ResponseBody ResponseEntity getUsersLogado(@RequestBody String token) {
		return new ResponseEntity(tokenAuthenticationService.getUserByToken(token), HttpStatus.OK);		
	}
	
	@GetMapping
	public @ResponseBody ResponseEntity getUsersLogadoTeste() {
		return new ResponseEntity("XXX", HttpStatus.OK);		
	}
	
	@GetMapping("/getPrivado")
	public @ResponseBody ResponseEntity getPrivado() {
		return new ResponseEntity("XXX", HttpStatus.OK);		
	}
	
}
