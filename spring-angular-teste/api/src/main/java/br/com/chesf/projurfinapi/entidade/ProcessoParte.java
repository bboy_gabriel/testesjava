package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PROCESSO_PARTE", schema = "PROJUR")
@SequenceGenerator(name = "SQ_PROCESSO_PARTE", sequenceName = "SQ_PROCESSO_PARTE", allocationSize = 1)
public class ProcessoParte implements Serializable{

	private static final long serialVersionUID = -8178922813467561159L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PROCESSO_PARTE")
    @Column(name = "ID_PROCESSO_PARTE")
    private Long id;
	
	@Column(name = "CD_PROCESSO", nullable = false)
	private Long cdProcesso;
	
	@Column(name = "CD_PARTE", nullable = false)
	private Long cdParte;
	
	@Column(name = "TP_PARTE", length = 3, nullable = true)
	private String tpParte; 

	@Column(name = "ID_PARTE", length = 3, nullable = true)
	private String idParte;
	
	@Column(name = "ID_LITISCONSORTE", length = 1, nullable = true)
	private String idLitisconsorte;
	
	@Column(name = "CD_SITUACAO", nullable = true)
	private Long cdSituacao;
	
	@Column(name = "IC_PARTE_PRINC", nullable = false)
	private String icPartePrinc;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cdProcesso
	 */
	public Long getCdProcesso() {
		return cdProcesso;
	}

	/**
	 * @param cdProcesso the cdProcesso to set
	 */
	public void setCdProcesso(Long cdProcesso) {
		this.cdProcesso = cdProcesso;
	}

	/**
	 * @return the cdParte
	 */
	public Long getCdParte() {
		return cdParte;
	}

	/**
	 * @param cdParte the cdParte to set
	 */
	public void setCdParte(Long cdParte) {
		this.cdParte = cdParte;
	}

	/**
	 * @return the tpParte
	 */
	public String getTpParte() {
		return tpParte;
	}

	/**
	 * @param tpParte the tpParte to set
	 */
	public void setTpParte(String tpParte) {
		this.tpParte = tpParte;
	}

	/**
	 * @return the idParte
	 */
	public String getIdParte() {
		return idParte;
	}

	/**
	 * @param idParte the idParte to set
	 */
	public void setIdParte(String idParte) {
		this.idParte = idParte;
	}

	/**
	 * @return the idLitisconsorte
	 */
	public String getIdLitisconsorte() {
		return idLitisconsorte;
	}

	/**
	 * @param idLitisconsorte the idLitisconsorte to set
	 */
	public void setIdLitisconsorte(String idLitisconsorte) {
		this.idLitisconsorte = idLitisconsorte;
	}

	/**
	 * @return the cdSituacao
	 */
	public Long getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * @param cdSituacao the cdSituacao to set
	 */
	public void setCdSituacao(Long cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * @return the icPartePrinc
	 */
	public String getIcPartePrinc() {
		return icPartePrinc;
	}

	/**
	 * @param icPartePrinc the icPartePrinc to set
	 */
	public void setIcPartePrinc(String icPartePrinc) {
		this.icPartePrinc = icPartePrinc;
	}
	
}
