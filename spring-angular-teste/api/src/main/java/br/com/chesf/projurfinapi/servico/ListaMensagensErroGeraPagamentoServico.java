package br.com.chesf.projurfinapi.servico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.chesf.projurfinapi.entidade.MsgErroGeraPagamento;
import br.com.chesf.projurfinapi.repositorio.ListaMensagensErroGeraPagamentoRepositorio;

@Service
@Transactional
public class ListaMensagensErroGeraPagamentoServico {
	
	@Autowired
	private ListaMensagensErroGeraPagamentoRepositorio listaMensagensErroGeraPagamentoRepositorio;
	
	
	public List<MsgErroGeraPagamento> consultarMsgErroGeraPagamentoPorIDSolicitacao(Long idSolicitacao){
		return listaMensagensErroGeraPagamentoRepositorio.findByIdSolicitacaoPagamento(idSolicitacao);
	}

}
