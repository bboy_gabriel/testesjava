package br.com.chesf.projurfinapi.config;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;

import br.com.chesf.projurfinapi.servico.ConfiguracaoServico;

@Configuration
public class ScheduledConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 76108720693930518L;
	
	@Autowired
	private ConfiguracaoServico configuracaoServico;
	
	Logger logger = LoggerFactory.getLogger(ScheduledConfig.class);	
	
	@Bean(name = "jobConfirmaPagamento")
	@Primary
	public String jobConfirmaPagamento(){
		logger.info("Consultando o tempo de execução para o rotina CONFIRMA-PAGAMENTO.");
		return configuracaoServico.consultarTempoExecucaoConfirmaPagamento();
	}
	
	@Bean(name = "jobAdiantamento")
	@Lazy
	public String jobAdiantamento(){
		logger.info("Consultando o tempo de execução para o rotina CONFIRMA-ADIANTAMENTO.");
		return configuracaoServico.consultarTempoExecucaoConfirmaAdiantamento();
	}

}
