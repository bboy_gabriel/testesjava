package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import br.com.chesf.projurfinapi.entidade.Parte;

/**
 * 
 * Interface responsável por fornecer uma camada de persistencia ao recurso Parte.
 * 
 * @author jvneto1
 *
 */
public interface ParteRepositorio extends JpaRepository<Parte, Long>, QueryByExampleExecutor<Parte>{

	Parte findByNrCNPJ(Long CNPJ);
	
	@Query(value = "SELECT * FROM PROJUR.TB_PARTE pt JOIN PROJUR.TB_PROCESSO_PARTE ppt ON ppt.CD_PARTE = pt.CD_PARTE JOIN PROJUR.TB_PROCESSO p ON p.CD_PROCESSO = :codProc AND ppt.CD_PROCESSO = p.CD_PROCESSO WHERE pt.TP_PARTE_SIJUS = 1", nativeQuery = true)
	List<Parte> consultarAutoresProcesso(@Param("codProc") Long codProc) throws Exception;
	
	@Query(value = "SELECT * FROM PROJUR.TB_PARTE pt JOIN PROJUR.TB_PROCESSO_PARTE ppt ON ppt.CD_PARTE = pt.CD_PARTE JOIN PROJUR.TB_PROCESSO p ON p.CD_PROCESSO = :codProc AND ppt.CD_PROCESSO = p.CD_PROCESSO WHERE pt.TP_PARTE_SIJUS = 2", nativeQuery = true)
	List<Parte> consultarReusProcesso(@Param("codProc") Long codProc) throws Exception;
	
}
