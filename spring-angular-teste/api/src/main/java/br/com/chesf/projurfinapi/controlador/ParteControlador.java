package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.servico.ParteServico;

@RestController 
@RequestMapping("/api/parte")
@Consumes("application/json")
@Produces("application/json")
public class ParteControlador implements Serializable{

	private static final long serialVersionUID = 5565819535410332244L;
	
	@Autowired
	private ParteServico parteServico;
	
	/**
	 * 
	 * Endpoint responsável por consultar uma lista de autores referentes ao processo.
	 * 
	 * @param codigoProcesso
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/autores/{codigoProcesso}")
	public ResponseEntity consultarAutoresPorProcesso(@PathVariable @Valid @NotNull Long codigoProcesso) {
		return new ResponseEntity(parteServico.consultarListaAutoresPorProcesso(codigoProcesso), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por consultar uma lista de reus referentes ao processo.
	 * 
	 * @param codigoProcesso
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/reus/{codigoProcesso}")
	public ResponseEntity consultarReusPorProcesso(@PathVariable @Valid @NotNull Long codigoProcesso) {
		return new ResponseEntity(parteServico.consultarListaReusPorProcesso(codigoProcesso), HttpStatus.OK);
	}

}
