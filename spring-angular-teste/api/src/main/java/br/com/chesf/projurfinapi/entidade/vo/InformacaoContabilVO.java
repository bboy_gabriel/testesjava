package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class InformacaoContabilVO implements Serializable{

	private static final long serialVersionUID = 223308461405121296L;
	
    private Long id;
	
    private CentroDeCustoVO centroDeCusto;
	
	private ElementoPepVO pep;
	
	private OrdemVO ordem;
	
	private BigDecimal valor;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the centroDeCusto
	 */
	public CentroDeCustoVO getCentroDeCusto() {
		return centroDeCusto;
	}

	/**
	 * @param centroDeCusto the centroDeCusto to set
	 */
	public void setCentroDeCusto(CentroDeCustoVO centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	/**
	 * @return the pep
	 */
	public ElementoPepVO getPep() {
		return pep;
	}

	/**
	 * @param pep the pep to set
	 */
	public void setPep(ElementoPepVO pep) {
		this.pep = pep;
	}

	/**
	 * @return the ordem
	 */
	public OrdemVO getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem the ordem to set
	 */
	public void setOrdem(OrdemVO ordem) {
		this.ordem = ordem;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}
