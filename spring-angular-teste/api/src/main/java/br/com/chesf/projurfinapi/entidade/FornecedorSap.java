package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o recurso FornecedorSap.
 *
 */
@Entity
@Table(name = "TB_FORN_SAP", schema="PROJUR")
@SequenceGenerator(name = "SQ_FORN_SAP", sequenceName = "SQ_FORN_SAP", allocationSize = 1)
public class FornecedorSap implements Serializable{

	private static final long serialVersionUID = 2325865836966058723L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_FORN_SAP")
    @Column(name = "ID_FORN_SAP")
    private Long id;
	
	@Column(name = "CD_FORN_SAP", length = 8, nullable = false)
	private Long codigo;
	
	@Column(name = "DS_FORN_SAP", length = 50, nullable = false)
	private String descricao;
	
	@Column(name = "NR_CNPJ", length = 14, nullable = false)
	private String cnpj;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj the cnpj to set
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
}
