//package br.com.chesf.projurfinapi.entidade;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Lob;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "TB_ARQUIVO", schema="PROJUR")
//@SequenceGenerator(name = "SQ_ARQUIVO", sequenceName = "SQ_ARQUIVO", allocationSize = 1)
//public class Arquivo implements Serializable{
//
//	private static final long serialVersionUID = -6897902167213994206L;
//
//	@Id
//	@Column(name = "ID_ARQUIVO", length = 19)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ARQUIVO")
//	private Long id;
//
//	@Column(name = "AR_NOME", length = 500)
//	private String nome;
//	
//	@Column(name = "AR_TIPO", length = 30)
//	private String tipo;
//	
//	@Column(name = "AR_FILE")	
//	@Lob
//	private byte[] file;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getNome() {
//		return nome;
//	}
//
//	public void setNome(String nome) {
//		this.nome = nome;
//	}
//
//	public String getTipo() {
//		return tipo;
//	}
//
//	public void setTipo(String tipo) {
//		this.tipo = tipo;
//	}
//
//	public byte[] getFile() {
//		return file;
//	}
//
//	public void setFile(byte[] file) {
//		this.file = file;
//	}
//	
//}
