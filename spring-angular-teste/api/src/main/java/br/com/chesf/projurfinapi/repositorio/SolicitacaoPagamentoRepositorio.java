package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.FornecedorSap;
import br.com.chesf.projurfinapi.entidade.OrgaoGestor;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.TipoDeDespesa;

@Repository 
public interface SolicitacaoPagamentoRepositorio extends JpaRepository<SolicitacaoPagamento, Long>, QueryByExampleExecutor<SolicitacaoPagamento>, PagingAndSortingRepository<SolicitacaoPagamento, Long>{
	
//	@Query("Select new br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento(pag.id, pag.desc, pag.matricula) from SolicitacaoPagamento pag where pag.matricula = :matricula")
	@Query("Select pag from SolicitacaoPagamento pag where pag.usuario = :matricula")
	List<SolicitacaoPagamento> findByMatricula(@Param("matricula") String matricula);
	
//	@Query("Select pag from SolicitacaoPagamento pag where pag.codigoStatus in (:listaStatus)")
//	List<SolicitacaoPagamento> findByStatusPagamento(@Param("listaStatus") List<String> listaStatus);	
	List<SolicitacaoPagamento> findByCodigoStatusIn(List<String> listaStatus);
	
	List<SolicitacaoPagamento> findByOrgaoGestor(OrgaoGestor orgaoGestor) throws Exception;
	
	List<SolicitacaoPagamento> findByTipoDeDespesa(TipoDeDespesa tipoDeDespesa) throws Exception;
	
	List<SolicitacaoPagamento> findByFornecedorSap(FornecedorSap fornecedorSap) throws Exception;

	Page findByProcesso(Pageable pageable) throws Exception;


}
	