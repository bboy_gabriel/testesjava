package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.ContaRazaoEstornoProvisao;
import br.com.chesf.projurfinapi.entidade.vo.ContaRazaoEstornoProvisaoVO;
import br.com.chesf.projurfinapi.entidade.vo.ContaRazaoProvisaoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ContaRazaoEstornoProvisaoRepositorio;

/**
 * Classe responsável por aplicar todas as regras de negócios referente ao estorno de uma provisão
 * @author Italo
 *
 */
@Service
public class ContaRazaoEstornoProvisaoServico implements Serializable{
	
	@Autowired
	private ContaRazaoEstornoProvisaoRepositorio contaRazaoEstornoProvisaoRepositorio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/**
	 * 
	 * Serviço responsavel por consultar e retornar uma lista do recurso ContaRazaoEstornoProvisão.
	 * 
	 * @return List<ContaRazaoEstornoProvisaoVO> listaVO
	 */
	public List<ContaRazaoEstornoProvisaoVO> consultarContaRazaoEstornoProvisao(){
		try {
			return converToListVO(contaRazaoEstornoProvisaoRepositorio.findAll());
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_CONTA_RAZAO_ESTORNO_PROVISAO);
		}
	}
	
	/**
	 * 
	 * Metodo responsável por fornecer um serviço onde é possível consultar uma lista de ContaRazaoEstornoProvisao pelo codigo do tipo de processo.
	 * 
	 * @param codigo
	 * @return List<ContaRazaoProvisaoVO>
	 * 
	 */
	public List<ContaRazaoEstornoProvisaoVO> consultarLContaRazaoPorTipoProcesso(String codigo){
		try {
			return converToListVO(contaRazaoEstornoProvisaoRepositorio.findByCodigoTipoProcesso(codigo));
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.NAO_FOI_POSSIVEL_CONSULTAR_LISTA_CONTA_POR_TIPO_PROCESSO);
		}
	}
	
	/**
	 * 
	 * Metodo privado que converte uma lista da entidade ContaRazaoEstornoProvisao para uma lista tipada com ContaRazaoEstornoProvisaoVO.
	 * 
	 * @param List<ContaRazaoEstornoProvisao> listaBase
	 * @return List<ContaRazaoEstornoProvisaoVO> listaVO
	 */
	private List<ContaRazaoEstornoProvisaoVO> converToListVO(List<ContaRazaoEstornoProvisao> listaBase){
		List<ContaRazaoEstornoProvisaoVO> listaVO = new ArrayList<>();
		for (ContaRazaoEstornoProvisao contaBase : listaBase) {
			ContaRazaoEstornoProvisaoVO vo = modelMapper.map(contaBase, ContaRazaoEstornoProvisaoVO.class);
			vo.setTipoProcessoProvisaoEnum(TipoProcessoContaRazaoEnum.getTipoProcessoContaRazaoEnumPorID(Integer.parseInt(contaBase.getCodigoTipoProcesso())));
			listaVO.add(vo);
		}
		return listaVO;
	}

}
