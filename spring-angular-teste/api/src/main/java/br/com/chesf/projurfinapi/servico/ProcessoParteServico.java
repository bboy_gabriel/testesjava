package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.ProcessoParte;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ProcessoParteRepositorio;

/**
 * 
 * Classer responsável por fornecer uma camada de serviço parao recurso ProcessoParte.
 * 
 * @author jvneto1
 *
 */
@Service
public class ProcessoParteServico implements Serializable{

	private static final long serialVersionUID = -1834052196697583778L;

	@Autowired
	private ProcessoParteRepositorio processoParteRepositorio;
	
	/**
	 * 
	 * Serviço responsável por fornecer uma consultar do recurso ProcessoParte pelo codigoParte.
	 * 
	 * @param cdParte
	 * @return ProcessoParte
	 * 
	 */
	public ProcessoParte consultarPorCdParte(Long cdParte) {
		try {
			return processoParteRepositorio.findByCdParte(cdParte);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.NAO_FOI_POSSIVEL_CONSULTAR_PROCESSO_PARTE_POR_CD_PARTE);
		}
	}
	
}
