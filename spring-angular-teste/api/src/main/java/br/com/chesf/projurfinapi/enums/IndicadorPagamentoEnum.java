package br.com.chesf.projurfinapi.enums;

public enum IndicadorPagamentoEnum {
	P("PAGTO"),
	G("GARANTIA");
	
	private String desc;
	
	private IndicadorPagamentoEnum(String desc){
		this.desc = desc;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
