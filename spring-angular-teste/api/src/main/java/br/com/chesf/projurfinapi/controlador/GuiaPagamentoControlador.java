package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.GuiaPagamentoVO;
import br.com.chesf.projurfinapi.servico.GuiaPagamentoServico;

@RestController
@RequestMapping(value = "/api/guiasPagamento")
@Consumes("application/json")
@Produces("application/json")
public class GuiaPagamentoControlador implements Serializable{

	private static final long serialVersionUID = 4594576546284885942L;
	
	@Autowired
	private GuiaPagamentoServico guiaPagamentoServico;
	
	/**
	 * 
	 * Endpoint responsavel por consultar uma guia de pagamento usando o id.
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping("/{id}")
	public ResponseEntity consultarPorID(Long id) {
		return new ResponseEntity(guiaPagamentoServico.consultarGuiaPorID(id), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por cadastrar um recurso GuiaPagamento.
	 * 
	 * @param guiaPagamentoVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping
	public ResponseEntity cadastrarGuiaPagamento(@RequestBody GuiaPagamentoVO guiaPagamentoVO) {
		return new ResponseEntity(guiaPagamentoServico.cadastrarGuiaPagamento(guiaPagamentoVO), HttpStatus.OK);		
	}
	
	/**
	 * 
	 * Endpoint responsável por atualizar o recurso GuiaPagamento.
	 * 
	 * @param guiaPagamentoVO
	 * @return ResponseEntity
	 * 
	 */
	@PutMapping
	public ResponseEntity atualizarGuiaPagamento(@RequestBody @NotNull @Valid GuiaPagamentoVO guiaPagamentoVO) {
		return new ResponseEntity(guiaPagamentoServico.atualizarGuiaPagamento(guiaPagamentoVO), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsável por consultar uma lisga do recurso GuiaPagamento cujo o documento SAP é inexistente.
	 * 
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/docSAPNulo")
	public ResponseEntity consultar() {
		return new ResponseEntity(guiaPagamentoServico.consultarGuiaDocSAPNulo(), HttpStatus.OK);
	}	
	
	/**
	 * 
	 * Endpoint responsável por exluir um recurso GuiaPagamento.
	 * 
	 * @param id
	 * @return ResponseEntity
	 * 
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity excluirGuiaPagamento(@PathVariable @NotNull Long id) {
		guiaPagamentoServico.exluirGuiaPagamento(id);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	

}
