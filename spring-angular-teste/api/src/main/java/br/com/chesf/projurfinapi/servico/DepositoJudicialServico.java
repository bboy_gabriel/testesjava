package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.DepositoJudicial;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.vo.ConsultaDepositoJudicialVO;
import br.com.chesf.projurfinapi.entidade.vo.DepositoJudicialVO;
import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.entidade.vo.ProcessoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoDepositoJudicialEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.DepositoJudicialRepositorio;
import br.com.chesf.projurfinapi.sap.ConfirmaPagamentoResponseDT;

@Service
public class DepositoJudicialServico implements Serializable{

	private static final long serialVersionUID = -6566429651822832971L;
	
	@Autowired
	private DepositoJudicialRepositorio depositoJudicialRepositorio;	
	
	@Autowired
	private ProcessoServico processoServico;
	
	@Autowired
	private LancamentoContabilServico lancamentoContabilServico;
	
	@Autowired
	private ModelMapper modelMapper;
	
	Logger logger = LoggerFactory.getLogger(DepositoJudicialServico.class);
	
	/**
	 * Método que salva na base de dados um deposito judicial. Tabela PROJUR.TB_DEPOS_JUDC
	 * @param solicitacaoPagamento
	 * @param response
	 * @return
	 */
	public DepositoJudicial gerarDepositoJudicialSAP(SolicitacaoPagamento solicitacaoPagamento, ConfirmaPagamentoResponseDT response) {
		try {
			return depositoJudicialRepositorio.save(montarDepositoJudicialSolicitacaoPagamento(solicitacaoPagamento, response));
		} catch (NegocioException e) {
			logger.error("Erro ao gerar deposito judicial: ", e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_DEPOSITO_JUDICIAL_SAP);
		}
	}
	
	private DepositoJudicial montarDepositoJudicialSolicitacaoPagamento(SolicitacaoPagamento solicitacaoPagamento, ConfirmaPagamentoResponseDT response) {
		DepositoJudicial depositoJudicial = new DepositoJudicial();
		depositoJudicial.setProcesso(solicitacaoPagamento.getProcesso());
		depositoJudicial.setCodigoDepositoJudicial(solicitacaoPagamento.getCodigoDepositoJudicial());
		depositoJudicial.setDataDeposito(LocalDate.now());
		depositoJudicial.setValorDeposito(solicitacaoPagamento.getValor());
		if(response.getITDOCSOUT().getItem().get(0).getAUTENTICABANCO() != null && !response.getITDOCSOUT().getItem().get(0).getAUTENTICABANCO().isEmpty()) {
			depositoJudicial.setTransacaoBancaria(response.getITDOCSOUT().getItem().get(0).getAUTENTICABANCO());
		}
		depositoJudicial.setUsuario(solicitacaoPagamento.getUsuario());
		depositoJudicial.setDocumentoSap(solicitacaoPagamento.getDocumentoSap());
		return depositoJudicial;
	}
	
	/**
	 * 
	 * Metodo responsavel por consultar informações sobre os depositos judiciais vinculados ao processo.
	 * 
	 * @param numeroProcesso
	 * @return ConsultaDepositoJudicialVO
	 * 
	 */
	public ConsultaDepositoJudicialVO consultarInformacoesDepositoJudicial(String numeroProcesso) {
		NumeroProcessoVO numeroProcessoVO = new NumeroProcessoVO();
		numeroProcessoVO.setNumeroProcesso(numeroProcesso);
		processoServico.consultarPorNumero(numeroProcessoVO);
		try {
			List<DepositoJudicial> listaBase = depositoJudicialRepositorio.findByProcesso(null, numeroProcesso);
			return montarConsultaDepositoJudicialVO(listaBase, numeroProcesso);
		} catch (NegocioException e) {
			logger.error("Erro ao consultar lista base pelo numero do processo: " + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_DEPOSITO_JUDICIAL_SAP);
		}
	}
	
	/**
	 * 
	 * Metodo responsavel por montar o vo consultaDepositoJudicialVO.
	 * 
	 * @param listaBase
	 * @return ConsultaDepositoJudicialVO
	 * 
	 */
	private ConsultaDepositoJudicialVO montarConsultaDepositoJudicialVO(List<DepositoJudicial> listaBase, String numeroProcesso) {
		if(listaBase == null || listaBase.isEmpty()) {
			logger.error("lista depositoJudicial consultada em base, nula ou vaiza!");
			throw new NegocioException(MensagemEnum.LISTA_BASE_DEPOSITO_JUDICIAL_NULA_OU_VAZIA);
		} 
		ConsultaDepositoJudicialVO vo = new ConsultaDepositoJudicialVO();
		for (DepositoJudicial depositoJudicial : listaBase) {
			vo.somarTotalDepositado(depositoJudicial.getValorDeposito());
			vo.somarTotalPagoLitigante(depositoJudicial.getValorLitigante());
			vo.somarTotalPagoChesf(depositoJudicial.getValorChesf());
		}
		vo.setTotalProvisionado(lancamentoContabilServico.consultarTotalProvisionado(numeroProcesso));
		vo.montarSaldoDepositado();
		return vo;
	}
	
	/**
	 * 
	 * Serviço responsavel por receber o numero do processo, validar
	 * e consultar na base todos os depositos vinculados ao processo.
	 * 
	 * @param numeroProcesso
	 * @return List<DepositoJudicialVO>
	 * 
	 */
	public List<DepositoJudicialVO> consultarTotalDepositadoPorProcesso(String numeroProcesso){
		NumeroProcessoVO numeroProcessoVO = new NumeroProcessoVO();
		numeroProcessoVO.setNumeroProcesso(numeroProcesso);
		processoServico.consultarPorNumero(numeroProcessoVO);
		try {
			Sort ordenador = new Sort(Sort.Direction.DESC, "dataDeposito");
			List<DepositoJudicial> listaBase = depositoJudicialRepositorio.findByProcesso(ordenador, numeroProcesso);
			return convertListaBaseToListaVO(listaBase);
		} catch (NegocioException e) {
			logger.error("Erro ao consultar lista base pelo numero do processo: " + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_TOTAL_DEPOSITADO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsavel por converter uma entidade DepositoJudicial em DepositoJudicialVO.
	 * 
	 * @param listaBase
	 * @return List<DepositoJudicialVO>
	 * 
	 */
	private List<DepositoJudicialVO> convertListaBaseToListaVO(List<DepositoJudicial> listaBase){
		if(listaBase == null || listaBase.isEmpty()) {
			logger.error("lista depositoJudicial consultada em base, nula ou vaiza!");
			throw new NegocioException(MensagemEnum.LISTA_BASE_DEPOSITO_JUDICIAL_NULA_OU_VAZIA);
		} 
		
		List<DepositoJudicialVO> listaVO = new ArrayList<>();
		
		for (DepositoJudicial depositoJudicial : listaBase) {
			DepositoJudicialVO vo = modelMapper.map(depositoJudicial, DepositoJudicialVO.class);
			vo.setTipoDepositoJudicialEnum(TipoDepositoJudicialEnum.getTipoDepositoJudicialEnumByID(vo.getCodigoDepositoJudicial().intValue()));
			listaVO.add(vo);
		}
		
		return listaVO;
	}
		
	
	
}
