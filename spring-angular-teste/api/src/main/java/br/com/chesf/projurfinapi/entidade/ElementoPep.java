package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por mapear o recurso ElementoPep.
 *
 */
@Entity
@Table(name = "TB_ELEMENTO_PEP", schema="PROJUR")
@SequenceGenerator(name = "SQ_ELEMENTO_PEP", sequenceName = "SQ_ELEMENTO_PEP", allocationSize = 1)
public class ElementoPep implements Serializable{

	private static final long serialVersionUID = -3963238596784899735L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ELEMENTO_PEP")
    @Column(name = "ID_ELEMENTO_PEP")
    private Long id;
	
	@Column(name = "CD_ELEMENTO_PEP", length = 24, nullable = false)
	private String codigo;
	
	@Column(name = "DS_ELEMENTO_PEP", length = 50, nullable = false)
	private String descricao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
