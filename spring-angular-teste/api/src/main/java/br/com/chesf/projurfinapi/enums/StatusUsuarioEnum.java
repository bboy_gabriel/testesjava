package br.com.chesf.projurfinapi.enums;

/**
 * Enum para o status do usuario.
 * A - Ativo
 * I - Inativo
 * @author isfigueiredo
 *
 */
public enum StatusUsuarioEnum {
	A("Ativo"),
	I("Inativo");
	
	private String status;
	
	private StatusUsuarioEnum(String status){
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static StatusUsuarioEnum getStatusUsuarioEnumPorStatus(String status){
		StatusUsuarioEnum retorno = null;
		switch (status) {
		case "Ativo":
			retorno = StatusUsuarioEnum.A;
			break;
		case "Inativo":
			retorno = StatusUsuarioEnum.I;
		}
		return retorno;
	}
}
