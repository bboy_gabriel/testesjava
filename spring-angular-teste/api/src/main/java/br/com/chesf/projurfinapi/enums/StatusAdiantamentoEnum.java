package br.com.chesf.projurfinapi.enums;

public enum StatusAdiantamentoEnum {
	
	AGUARDANDO_APROVACAO(1, "Aguardando Aprovação"),
	APROVADO(2, "Aprovado"),
	COMPENSADO(3, "Compensado"),
	ESTORNADO(4, "Estornado"),
	CONTAS_EFETUADAS(5, "Prestação de Contas Efetuadas"),
	CONCILIADO(6, "Conciliado");
	
	private Integer id;
	private String desc;
	
	private StatusAdiantamentoEnum(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static final StatusAdiantamentoEnum getStatusAdiantamentoEnumPorDesc(String descricao) {
		StatusAdiantamentoEnum status = null;
		if(StatusAdiantamentoEnum.AGUARDANDO_APROVACAO.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.AGUARDANDO_APROVACAO;
		}else if (StatusAdiantamentoEnum.APROVADO.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.APROVADO;
		}else if (StatusAdiantamentoEnum.COMPENSADO.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.COMPENSADO;
		}else if (StatusAdiantamentoEnum.CONCILIADO.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.CONCILIADO;
		}else if(StatusAdiantamentoEnum.CONTAS_EFETUADAS.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.CONTAS_EFETUADAS;
		}else if(StatusAdiantamentoEnum.ESTORNADO.desc.equalsIgnoreCase(descricao)) {
			status = StatusAdiantamentoEnum.ESTORNADO;
		}
		
		return status;

	}
	
}
