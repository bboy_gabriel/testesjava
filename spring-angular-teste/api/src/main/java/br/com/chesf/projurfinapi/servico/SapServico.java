package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;
import br.com.chesf.projurfinapi.entidade.DepositoJudicial;
import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.MsgErroGeraPagamento;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.vo.DomicilioBancarioVO;
import br.com.chesf.projurfinapi.enums.IndicadorPagamentoEnum;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.StatusPagamentoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.sap.ConfirmaPagamentoRequestDT;
import br.com.chesf.projurfinapi.sap.ConfirmaPagamentoRequestDT.ITDOCSIN;
import br.com.chesf.projurfinapi.sap.ConfirmaPagamentoResponseDT;
import br.com.chesf.projurfinapi.sap.GeraPagamentoRequestDT;
import br.com.chesf.projurfinapi.sap.GeraPagamentoRequestDT.ClassContab;
import br.com.chesf.projurfinapi.sap.GeraPagamentoResponseDT;
import br.com.chesf.projurfinapi.sap.ObjectFactory;
import br.com.chesf.projurfinapi.sap.ObterDomicilioRequestDT;
import br.com.chesf.projurfinapi.sap.ObterDomicilioRespondeDT;
import br.com.chesf.projurfinapi.sap.ObterDomicilioRespondeDT.ITDOMICILIOBANCARIO.Item;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer serviços integrados com os Webservices SAP. 
 *
 */
@Service
public class SapServico implements Serializable {

	private static final long serialVersionUID = 4873756084299936332L;

	@Autowired
	private Jaxb2Marshaller jaxb2Marshaller;
	
	@Autowired
	private HttpComponentsMessageSender httpComponentsMessageSender;
	
	@Autowired
	private DepositoJudicialServico depositoJudicialServico;
	
	@Autowired
	private ConfiguracaoServico configuracaoServico;
	
	Logger logger = LoggerFactory.getLogger(SapServico.class);
	
	private final String QUEBRA_LINHA = System.getProperty("line.separator");
	
	private String SIGLA_CHESF = "CHSF";
	private String LOCAL = "0001";
	private static final String SIJUS = "SIJUS ";


	/**
	 * 
	 * @param domicilioBancario
	 * @return List<DomicilioBancarioVO>
	 * 
	 * Metodo reponsável por buscar uma listaDomicilioBancario e retornar uma listaDomicilioBancarioVO.
	 * 
	 */
	public List<DomicilioBancarioVO> obterDomicilioBancario(String domicilioBancario) {
		try {
			final WebServiceTemplate webT = createWebServiceTempalte(configuracaoServico.consultarLinkObterDomicilio());
			ObjectFactory factory = new ObjectFactory();
			ObterDomicilioRequestDT obterDomicilioRequest = new ObterDomicilioRequestDT();
			obterDomicilioRequest.setIFORNECEDOR(domicilioBancario);
			logger.info("Recuperando os domicilios bancarios.");
			logger.info("IFornecedor: " + domicilioBancario);
			JAXBElement<ObterDomicilioRequestDT> requestXml = factory.createObterDomicilioRequestMT(obterDomicilioRequest);
			
			ObterDomicilioRespondeDT response = null;
			JAXBElement<ObterDomicilioRespondeDT> responseXml = (JAXBElement<ObterDomicilioRespondeDT>) webT.marshalSendAndReceive(requestXml);
			response = responseXml.getValue();
			
			validarResponseObterDomicilio(response);
			return montarListaDomicilioVO(response);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Erro ao consultar o domicilio bancario: ").append(e.getMessage());
			logger.error(sb.toString(), e);
			throw new NegocioException(MensagemEnum.ERRO_AO_OBTER_DOMICILIO_BANCARIO_SAP);
		}
	}
	
	public SolicitacaoPagamento geraPagamento(SolicitacaoPagamento solicitacaoPagamento) {
		try {
			final WebServiceTemplate webT = createWebServiceTempalte(configuracaoServico.consultarLinkGeraPagamento());
			ObjectFactory factory = new ObjectFactory();
			GeraPagamentoRequestDT geraPagamentoRequest = montarGeraPagamentoRequest(solicitacaoPagamento);
			JAXBElement<GeraPagamentoRequestDT> requestXml = factory.createGeraPagamentoRequestMT(geraPagamentoRequest);
			
			GeraPagamentoResponseDT response = enviarRequisicaoGeraPagamentoSAP(webT, requestXml);
			LocalDateTime dataRecebimentoSAP = LocalDateTime.now();
			
			if(response.getDOCSAP() != null && !response.getDOCSAP().isEmpty()) {
				solicitacaoPagamento.setDocumentoSap(response.getDOCSAP());
				solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.AGUARDANDO_APROVACAO);
				if(solicitacaoPagamento.getListaMensagensErroGeraPagamento() != null && !solicitacaoPagamento.getListaMensagensErroGeraPagamento().isEmpty()) {
					MsgErroGeraPagamento msg = new MsgErroGeraPagamento();
					msg.setMensagens("DOCSAP gerado com sucesso! " + response.getDOCSAP());
					msg.setDataInsercao(dataRecebimentoSAP);
					solicitacaoPagamento.getListaMensagensErroGeraPagamento().add(msg);
				}else {
					List<MsgErroGeraPagamento> listaMsg = new ArrayList<>();
					MsgErroGeraPagamento msg = new MsgErroGeraPagamento();
					msg.setMensagens("DOCSAP gerado com sucesso! " + response.getDOCSAP());
					msg.setDataInsercao(dataRecebimentoSAP);
					listaMsg.add(msg);
					solicitacaoPagamento.setListaMensagensErroGeraPagamento(listaMsg);
				}
			}else {
				solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.PENDENTE);
				if(solicitacaoPagamento.getListaMensagensErroGeraPagamento() != null && !solicitacaoPagamento.getListaMensagensErroGeraPagamento().isEmpty()) {
					solicitacaoPagamento.getListaMensagensErroGeraPagamento().addAll(montarMensagemErroGeraPagamento(response, dataRecebimentoSAP));
				}else {
					solicitacaoPagamento.setListaMensagensErroGeraPagamento(montarMensagemErroGeraPagamento(response, dataRecebimentoSAP));
				}
			}
			return solicitacaoPagamento;
		} catch (Exception e) {
			logger.error("Não foi possível criar a solicitação de pagamento: " , e);
			throw new NegocioException(MensagemEnum.ERRO_AO_GERAR_SOLICITACAO_PAGAMENTO_NO_SAP);
		}
	}
	
	private GeraPagamentoResponseDT enviarRequisicaoGeraPagamentoSAP(final WebServiceTemplate webT, JAXBElement<GeraPagamentoRequestDT> requestXml){
		GeraPagamentoResponseDT response = null;
		JAXBElement<GeraPagamentoResponseDT> responseXml= null;
		try {
			responseXml = (JAXBElement<GeraPagamentoResponseDT>) webT.marshalSendAndReceive(requestXml);
			response = responseXml.getValue();
			StringBuilder sb = new StringBuilder();
			sb.append("Resposta do SAP do servico de gerar pagamento: ").append(QUEBRA_LINHA)
			.append("Documento SAP: ").append(response.getDOCSAP()).append(QUEBRA_LINHA)
			.append("EMSGERRO: ").append(response.getEMSGERRO()).append(QUEBRA_LINHA);
			for (br.com.chesf.projurfinapi.sap.GeraPagamentoResponseDT.MSGERRO.Item mensagem : response.getMSGERRO().getItem()) {
				sb.append("Mensagem de erro: ").append(mensagem.getMSG()).append(QUEBRA_LINHA);
			}			
			logger.info(sb.toString());			
		} catch (Exception e) {
			logger.error("Erro ao executar chamada ao SAP. Serviço de gerar pagamento: ", e);
			throw new NegocioException(MensagemEnum.ERRO_AO_GERAR_SOLICITACAO_PAGAMENTO_NO_SAP);
		}
		return response;
	}
	
	public SolicitacaoPagamento confirmaPagamento(SolicitacaoPagamento solicitacaoPagamento) {
		try {
			final WebServiceTemplate webT = createWebServiceTempalte(configuracaoServico.consultarLinkConfirmarPagamento());
			ObjectFactory factory = new ObjectFactory();
			ConfirmaPagamentoRequestDT confirmaPagamento = montarConfirmaPagamentoRequestDT(solicitacaoPagamento);
			JAXBElement<ConfirmaPagamentoRequestDT> requestXml = factory.createConfirmaPagamentoRequestMT(confirmaPagamento);
			
			ConfirmaPagamentoResponseDT response = enviaRequisicaoConfirmaPagamentoSAP(webT, requestXml);	
			if(response.getITDOCSOUT() != null && response.getITDOCSOUT().getItem() != null && !response.getITDOCSOUT().getItem().isEmpty()) {
				br.com.chesf.projurfinapi.sap.ConfirmaPagamentoResponseDT.ITDOCSOUT.Item itemResposta = response.getITDOCSOUT().getItem().get(0); 
				
				if(itemResposta.getSITUACAO().equalsIgnoreCase("compensado")
						&& itemResposta.getPGTOFINALIZADO().equalsIgnoreCase("s")) {
					if(itemResposta.getPGTOFINALIZADO().equalsIgnoreCase("S") && solicitacaoPagamento.getTipoDeDespesa().getIndicadorPagamentoEnum().compareTo(IndicadorPagamentoEnum.G) == 0) {
						logger.info("Gerando deposito judicial!");
						DepositoJudicial dep = depositoJudicialServico.gerarDepositoJudicialSAP(solicitacaoPagamento, response);
						logger.info("Deposito judicial gerado com sucesso. ID: " + dep.getId());
					}
				}
				
				if(!solicitacaoPagamento.getStatusPagamentoEnum().getDesc().equalsIgnoreCase(itemResposta.getSITUACAO())) {
					montarNovoStatusSolicitacaoConfirmaPagamento(solicitacaoPagamento, response);				
				}
			}
			
			
			return solicitacaoPagamento;
		} catch (Exception e) {
			logger.error("Erro ao confirmar pagamento: ",e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONFIRMA_PAGAMENTO_SAP);
		}
	}
	
	private ConfirmaPagamentoResponseDT enviaRequisicaoConfirmaPagamentoSAP(final WebServiceTemplate webT, JAXBElement<ConfirmaPagamentoRequestDT> requestXml) throws Exception{
		ConfirmaPagamentoResponseDT response = null;
		JAXBElement<ConfirmaPagamentoResponseDT> responseXml = (JAXBElement<ConfirmaPagamentoResponseDT>) webT.marshalSendAndReceive(requestXml);
		response = responseXml.getValue();
		br.com.chesf.projurfinapi.sap.ConfirmaPagamentoResponseDT.ITDOCSOUT.Item item = response.getITDOCSOUT().getItem().get(0);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Resposta ao SAP do servico confirma pagamento: ").append(QUEBRA_LINHA)
		.append("AutenticaBanco: ").append(item.getAUTENTICABANCO()).append(QUEBRA_LINHA)
		.append("Compensado: ").append(item.getCOMPENSADO()).append(QUEBRA_LINHA)
		.append("DOCSAP: ").append(item.getDOCSAP()).append(QUEBRA_LINHA)
		.append("FORMAPGTO: ").append(item.getFORMAPGTO()).append(QUEBRA_LINHA)
		.append("PGTOFINALIZADO: ").append(item.getPGTOFINALIZADO()).append(QUEBRA_LINHA)
		.append("SITUACAO: ").append(item.getSITUACAO()).append(QUEBRA_LINHA)
		.append("STATUSOPERACAO: ").append(item.getSTATUSOPERACAO()).append(QUEBRA_LINHA);
		
		logger.info(sb.toString());
		return response;
	}
	
	private void montarNovoStatusSolicitacaoConfirmaPagamento(SolicitacaoPagamento solicitacaoPagamento, ConfirmaPagamentoResponseDT response) {
		String novoStatus = response.getITDOCSOUT().getItem().get(0).getSITUACAO(); 
		if(novoStatus.equalsIgnoreCase(StatusPagamentoEnum.AGUARDANDO_APROVACAO.getDesc())){
			solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.AGUARDANDO_APROVACAO);
			solicitacaoPagamento.setCodigoStatus(StatusPagamentoEnum.AGUARDANDO_APROVACAO.getId().toString());
		}
		if(novoStatus.equalsIgnoreCase(StatusPagamentoEnum.APROVADO.getDesc())){
			solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.APROVADO);
			solicitacaoPagamento.setCodigoStatus(StatusPagamentoEnum.APROVADO.getId().toString());
		}
		if(novoStatus.equalsIgnoreCase(StatusPagamentoEnum.COMPENSADO.getDesc())){
			solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.COMPENSADO);
			solicitacaoPagamento.setCodigoStatus(StatusPagamentoEnum.COMPENSADO.getId().toString());
		}
		if(novoStatus.equalsIgnoreCase(StatusPagamentoEnum.ESTORNADO.getDesc())){
			solicitacaoPagamento.setStatusPagamentoEnum(StatusPagamentoEnum.ESTORNADO);
			solicitacaoPagamento.setCodigoStatus(StatusPagamentoEnum.ESTORNADO.getId().toString());
		}
	}
	
	private ConfirmaPagamentoRequestDT montarConfirmaPagamentoRequestDT(SolicitacaoPagamento solicitacaoPagamento) {
		ConfirmaPagamentoRequestDT confirmaPagamentoRequestDT = new ConfirmaPagamentoRequestDT();
		ITDOCSIN itdcsin = new ITDOCSIN(); 
		br.com.chesf.projurfinapi.sap.ConfirmaPagamentoRequestDT.ITDOCSIN.Item item = new br.com.chesf.projurfinapi.sap.ConfirmaPagamentoRequestDT.ITDOCSIN.Item();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		item.setDATADOCUMENTO(solicitacaoPagamento.getDataEmissao().format(formatter));
		item.setDOCSAP(solicitacaoPagamento.getDocumentoSap());
		item.setIDTHEMIS(solicitacaoPagamento.getId().toString());
		itdcsin.getItem().add(item);
		confirmaPagamentoRequestDT.setEMPRESA(SIGLA_CHESF);
		confirmaPagamentoRequestDT.setITDOCSIN(itdcsin);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Dados enviados ao SAP do servico de Confirma Pagamento: ")
		.append("Empresa: ").append(confirmaPagamentoRequestDT.getEMPRESA()).append(QUEBRA_LINHA)
		.append("Documento SAP: ").append(item.getDOCSAP()).append(QUEBRA_LINHA)
		.append("Data do documento: ").append(item.getDATADOCUMENTO()).append(QUEBRA_LINHA)
		.append("IdThemis: ").append(item.getIDTHEMIS()).append(QUEBRA_LINHA);
		
		logger.info(sb.toString());
		return confirmaPagamentoRequestDT;
	}
	
	private List<MsgErroGeraPagamento> montarMensagemErroGeraPagamento(GeraPagamentoResponseDT response, LocalDateTime dataRecimentoSAP) {
		List<MsgErroGeraPagamento> listaErros = new ArrayList<>();
		for (br.com.chesf.projurfinapi.sap.GeraPagamentoResponseDT.MSGERRO.Item item : response.getMSGERRO().getItem()) {
			MsgErroGeraPagamento erro = new MsgErroGeraPagamento();
			erro.setMensagens(item.getMSG());
			erro.setDataInsercao(dataRecimentoSAP);
			listaErros.add(erro);
		}
		return listaErros;
	}
	
	private GeraPagamentoRequestDT montarGeraPagamentoRequest(SolicitacaoPagamento solicitacaoPagamento) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		DateTimeFormatter idThemis = DateTimeFormatter.ofPattern("yyyyMMdd");
		
		GeraPagamentoRequestDT geraPagamentoRequest = new GeraPagamentoRequestDT();
		geraPagamentoRequest.setMontante(solicitacaoPagamento.getValor().toString());
		geraPagamentoRequest.setTipoDespesa(solicitacaoPagamento.getTipoDeDespesa().getCodigo()); 
		geraPagamentoRequest.setFornecedor(solicitacaoPagamento.getFornecedorSap().getCodigo().toString());
		geraPagamentoRequest.setDataDocumento(solicitacaoPagamento.getDataEmissao().format(formatter)); // dd.mm.aaaa 
		geraPagamentoRequest.setDataBase(solicitacaoPagamento.getDataVencimento().format(formatter)); // dd.mm.aaaa
		geraPagamentoRequest.setOrgaoGestor(solicitacaoPagamento.getOrgaoGestor().getDescricao()); 
		geraPagamentoRequest.setEmpresa(SIGLA_CHESF);
		geraPagamentoRequest.setLocalNegocios(LOCAL);
		geraPagamentoRequest.setReferenciaID(SIJUS + solicitacaoPagamento.getProcesso());
		if(Validador.isNotNull(solicitacaoPagamento.getCodigoBarra())){
			geraPagamentoRequest.setCodBarras(solicitacaoPagamento.getCodigoBarra());			
		}
		if(Validador.isNotNull(solicitacaoPagamento.getCodigoBarra())){
			geraPagamentoRequest.setCodBarras(solicitacaoPagamento.getCodigoBarra());
		}
		geraPagamentoRequest.setTipoPgtoThemis(LocalDate.now().format(idThemis));//TODO depois verificar qual valor
		geraPagamentoRequest.setClassContab(montarClassContabGeraPagamento(solicitacaoPagamento.getInformacoesContabeis()));
		geraPagamentoRequest.setFormaPgto(solicitacaoPagamento.getFormaPagamentoEnum().toString());
		if(solicitacaoPagamento.getDomicilioBancario() != null) {
			geraPagamentoRequest.setDomicilioBancario(String.valueOf(solicitacaoPagamento.getDomicilioBancario())); 
		}else {
			geraPagamentoRequest.setDomicilioBancario("0001"); 
		} 
		
		StringBuilder sb = new StringBuilder("Dados enviado ao SAP Gera Pagamento: ");
		sb.append("Montante: ").append(geraPagamentoRequest.getMontante()).append(QUEBRA_LINHA);
		sb.append("Tipo de despesa: ").append(geraPagamentoRequest.getTipoDespesa()).append(QUEBRA_LINHA);
		sb.append("Fornecedor SAP: ").append(geraPagamentoRequest.getFornecedor()).append(QUEBRA_LINHA);
		sb.append("Data Base: ").append(geraPagamentoRequest.getDataBase()).append(QUEBRA_LINHA);
		sb.append("Data Documento: ").append(geraPagamentoRequest.getDataDocumento()).append(QUEBRA_LINHA);
		sb.append("Orgao Gestor: ").append(geraPagamentoRequest.getOrgaoGestor()).append(QUEBRA_LINHA);
		sb.append("ID Themis: ").append(geraPagamentoRequest.getReferenciaID()).append(QUEBRA_LINHA);
		sb.append("Local: ").append(geraPagamentoRequest.getLocalNegocios()).append(QUEBRA_LINHA);
		
		if(Validador.isNotNull(geraPagamentoRequest.getCodBarras())){
			sb.append("Código  de barras: ").append(geraPagamentoRequest.getCodBarras()).append(QUEBRA_LINHA);			
		}
		
		sb.append("Empresa: ").append(geraPagamentoRequest.getEmpresa()).append(QUEBRA_LINHA);
		sb.append("Forma de Pagamento: ").append(geraPagamentoRequest.getFormaPgto()).append(QUEBRA_LINHA);
		sb.append("Tipo pagamento Themis: ").append(geraPagamentoRequest.getTipoPgtoThemis()).append(QUEBRA_LINHA);
		sb.append("Domicilio Bancario: ").append(geraPagamentoRequest.getDomicilioBancario()).append(QUEBRA_LINHA);
		
		if(geraPagamentoRequest.getClassContab().getItem() != null){
			for (br.com.chesf.projurfinapi.sap.GeraPagamentoRequestDT.ClassContab.Item item : geraPagamentoRequest.getClassContab().getItem()) {
				sb.append("Informacao contabil: ").append(QUEBRA_LINHA);
				if(Validador.isNotNull(item.getCENTROCUSTO())){
					sb.append("Centro de custo: ").append(item.getCENTROCUSTO()).append(QUEBRA_LINHA);					
				}
				if(Validador.isNotNull(item.getORDEM())){
					sb.append("Ordem: ").append(item.getORDEM()).append(QUEBRA_LINHA);					
				}
				if(Validador.isNotNull(item.getPEP())){
					sb.append("Elemento PEP: ").append(item.getPEP()).append(QUEBRA_LINHA);
				}
				sb.append("Valor: ").append(item.getVALCLASSIFICA()).append(QUEBRA_LINHA);				
			}
		}
		
		logger.info(sb.toString());
		return geraPagamentoRequest;
	}
	
	private ClassContab montarClassContabGeraPagamento(List<InformacaoContabil> listaInformacaoContabil) {
		ClassContab classContab = new ClassContab();
		if(listaInformacaoContabil != null) {
		for (InformacaoContabil informacaoContabil : listaInformacaoContabil) {
			br.com.chesf.projurfinapi.sap.GeraPagamentoRequestDT.ClassContab.Item item = new br.com.chesf.projurfinapi.sap.GeraPagamentoRequestDT.ClassContab.Item();
			if(informacaoContabil.getCentroDeCusto() != null && informacaoContabil.getCentroDeCusto().getDescricao() != null && !informacaoContabil.getCentroDeCusto().getDescricao().isEmpty()) {
				item.setCENTROCUSTO(informacaoContabil.getCentroDeCusto().getCodigo()); //Verificar em qual campo de Centro de Custo vamos colocar.
			}
			if(informacaoContabil.getOrdem() != null && informacaoContabil.getOrdem().getDescricao() != null && !informacaoContabil.getOrdem().getDescricao().isEmpty()) {
				item.setORDEM(informacaoContabil.getOrdem().getDescricao()); //Verificar em qual campo de Ordem vamos colocar.
			}
			if(informacaoContabil.getElementoPep() != null && informacaoContabil.getElementoPep().getCodigo() != null && informacaoContabil.getElementoPep().getCodigo() != null) {
				item.setPEP(informacaoContabil.getElementoPep().getCodigo().toString()); //Verificar em qual campo ElementoPEP vamos colocar.
			}
			item.setVALCLASSIFICA(informacaoContabil.getValor().toString());
			classContab.getItem().add(item);
		}
		}
		return classContab;
	}
	
	/**
	 * 
	 * @param response
	 * 
	 * Metodo responsável por validar a resposta do webservice obterDomicilioBancario SAP.
	 * 
	 */
	private void validarResponseObterDomicilio(ObterDomicilioRespondeDT response) {
		if(response == null || response.getITDOMICILIOBANCARIO() == null 
				|| response.getITDOMICILIOBANCARIO().getItem() == null 
				|| response.getITDOMICILIOBANCARIO().getItem().isEmpty()) {
			logger.error("Nenhum domicilio bancario retornado pelo SAP!");
			throw new NegocioException(MensagemEnum.LISTA_RESPONSE_OBTER_DOMICILIO_BANCARIO_NULA_OU_INVALIDA);
			
		}
	}
	
	/**
	 * 
	 * @param response
	 * @return List<DomicilioBancarioVO>
	 * 
	 * Metodo responsavel por receber o response ObterDomicilioBancario e montar uma listaDomicilioBancarioVO.
	 * 
	 */
	private List<DomicilioBancarioVO> montarListaDomicilioVO(ObterDomicilioRespondeDT response){
		List<DomicilioBancarioVO> listaDomicilioBancarioVO = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		sb.append("Resposta do SAP do servico ObterDomicilio: ").append(QUEBRA_LINHA);
		for (Item item : response.getITDOMICILIOBANCARIO().getItem()) {
			DomicilioBancarioVO vo = new DomicilioBancarioVO();
			vo.setBancoAgencia(item.getBANCOAGENCIA());
			vo.setBancoConta(item.getCONTABANCARIA());
			vo.setBancoNome(item.getNOMEINTITUI());
			vo.setBancoDomicilio(item.getDOMICILIOBANCARIO());

			sb.append("Banco Agencia: ").append(item.getBANCOAGENCIA()).append(QUEBRA_LINHA)
			.append("Conta: ").append(item.getCONTABANCARIA()).append(QUEBRA_LINHA)
			.append("Nome: ").append(item.getNOMEINTITUI()).append(QUEBRA_LINHA)
			.append("Banco Domicilio: ").append(item.getDOMICILIOBANCARIO()).append(QUEBRA_LINHA);
			logger.info(sb.toString());
			
			listaDomicilioBancarioVO.add(vo);
		}
		return listaDomicilioBancarioVO;
	}
	
	/**
	 * 
	 * @param url
	 * @return WebServiceTemplate
	 * 
	 * Metodo responsavel por criar e setar as credenciais no WebServiceTemplate. 
	 * 
	 */
	public WebServiceTemplate createWebServiceTempalte(String url) {
		WebServiceTemplate webT = new WebServiceTemplate();
		webT.setMarshaller(jaxb2Marshaller);
		webT.setUnmarshaller(jaxb2Marshaller);
		webT.setMessageSender(httpComponentsMessageSender);
		webT.setDefaultUri(url);
		return  webT;
	}

}
