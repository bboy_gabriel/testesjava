package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.CentroDeCusto;
import br.com.chesf.projurfinapi.entidade.ElementoPep;
import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.Ordem;

/**
 * Classe responsável por acessa a base de dados para fornecer as informações referente a entidade informação contábil
 * @author isfigueiredo
 *
 */
@Repository
public interface InformacaoContabilRepositorio extends JpaRepository<InformacaoContabil, Long>, QueryByExampleExecutor<InformacaoContabil>{
	
//	@Query("SELECT info FROM InformacaoContabil info where info.centroDeCusto.id = :idCentroCusto")
//	public List<InformacaoContabil> findByCentroDeCusto(@Param("idCentroCusto") Long idCentroCusto) throws Exception;
	public List<InformacaoContabil> findByCentroDeCusto(CentroDeCusto centroCusto) throws Exception;
	
	public List<InformacaoContabil> findByElementoPep(ElementoPep elementoPep) throws Exception;
	
	public List<InformacaoContabil> findByOrdem(Ordem ordem) throws Exception;

}
