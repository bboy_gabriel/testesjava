package br.com.chesf.projurfinapi.exception;

import br.com.chesf.projurfinapi.entidade.vo.MensagemNegocioVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;

public class NegocioException extends RuntimeException {

	private static final long serialVersionUID = 7878379467637872061L;

	private MensagemNegocioVO mensagemNegocioVO;

	public NegocioException(MensagemEnum mensagemEnum, String... campos) {
		this.mensagemNegocioVO = new MensagemNegocioVO(mensagemEnum, campos);
	}

	public MensagemNegocioVO getMensagemNegocioVO() {
		return mensagemNegocioVO;
	}

	@Override
	public String getMessage() {
		return mensagemNegocioVO.getMensagem();
	}
}
