package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.chesf.projurfinapi.entidade.DomicilioBancario;
import br.com.chesf.projurfinapi.enums.TipoDepositoJudicialEnum;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class DepositoJudicialVO implements Serializable{


	private static final long serialVersionUID = -1146963495557112253L;
	
	private Long id;
	
	@NotNull(message = "Processo nulo ou inválido!")
	private String processo;
	
	private TipoDepositoJudicialEnum tipoDepositoJudicialEnum;
	
	@NotNull(message = "Codigo do tipo de deposito nulo ou inválido!")
	private Long codigoDepositoJudicial;
	
	@NotEmpty(message = "Data de deposito nula ou inválida!")
	private String dataDeposito;
	
	@NotNull(message = "Domicilio Bancario nulo ou inválido!")
	private String domicilioBancario;
	
	@NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valorDeposito;
	
	private String transacaoBancaria;
	
	
	private BigDecimal valorLitigante;
	
	@NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valorChesf;
	
	
	private BigDecimal valorCorrecaoLitigante;
	
	@NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valorCorrecaoChesf;
	
	@NotEmpty(message = "Usuário nulo ou inválido!")
	private String usuario;
	
	private String documentoSap;
	
	public DepositoJudicialVO() {
		
	}
	
	public DepositoJudicialVO(Long id, String processo, TipoDepositoJudicialEnum tipoDepositoJudicialEnum, String dataDeposito,
			String domicilioBancario, BigDecimal valorDeposito, BigDecimal valorLitigante, BigDecimal valorChesf,  String usuario) {
		
		this.id = id;
		this.processo = processo;
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
		this.dataDeposito = dataDeposito;
		this.domicilioBancario = domicilioBancario;
		this.valorDeposito = valorDeposito;
		this.valorChesf = valorChesf;
		this.valorLitigante = valorLitigante;
		this.usuario = usuario;
	}
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProcesso() {
		return processo;
	}
	public void setProcesso(String processo) {
		this.processo = processo;
	}
	public TipoDepositoJudicialEnum getTipoDepositoJudicialEnum() {
		return tipoDepositoJudicialEnum;
	}
	public void setTipoDepositoJudicialEnum(TipoDepositoJudicialEnum tipoDepositoJudicialEnum) {
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
	}
	public String getDataDeposito() {
		return dataDeposito;
	}
	public void setDataDeposito(String dataDeposito) {
		this.dataDeposito = dataDeposito;
	}
	public String getDomicilioBancario() {
		return domicilioBancario;
	}
	public void setDomicilioBancario(String domicilioBancario) {
		this.domicilioBancario = domicilioBancario;
	}
	public BigDecimal getValorDeposito() {
		return valorDeposito;
	}
	public void setValorDeposito(BigDecimal valorDeposito) {
		this.valorDeposito = valorDeposito;
	}
	public String getTransacaoBancaria() {
		return transacaoBancaria;
	}
	public void setTransacaoBancaria(String transacaoBancaria) {
		this.transacaoBancaria = transacaoBancaria;
	}
	public BigDecimal getValorLitigante() {
		return valorLitigante;
	}
	public void setValorLitigante(BigDecimal valorLitigante) {
		this.valorLitigante = valorLitigante;
	}
	public BigDecimal getValorChesf() {
		return valorChesf;
	}
	public void setValorChesf(BigDecimal valorChesf) {
		this.valorChesf = valorChesf;
	}
	public BigDecimal getValorCorrecaoLitigante() {
		return valorCorrecaoLitigante;
	}
	public void setValorCorrecaoLitigante(BigDecimal valorCorrecaoLitigante) {
		this.valorCorrecaoLitigante = valorCorrecaoLitigante;
	}
	public BigDecimal getValorCorrecaoChesf() {
		return valorCorrecaoChesf;
	}
	public void setValorCorrecaoChesf(BigDecimal valorCorrecaoChesf) {
		this.valorCorrecaoChesf = valorCorrecaoChesf;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Long getCodigoDepositoJudicial() {
		return codigoDepositoJudicial;
	}
	public void setCodigoDepositoJudicial(Long codigoDepositoJudicial) {
		this.codigoDepositoJudicial = codigoDepositoJudicial;
	}
	public String getDocumentoSap() {
		return documentoSap;
	}
	public void setDocumentoSap(String documentoSap) {
		this.documentoSap = documentoSap;
	}
	
}
