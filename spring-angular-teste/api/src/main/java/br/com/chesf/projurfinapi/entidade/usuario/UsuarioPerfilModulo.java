package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;
import java.util.Date;

/**
 * Classe que atrela um usuário a um perfil e modulo.
 * @author isfigueiredo
 *
 */
public class UsuarioPerfilModulo implements Serializable {

	private static final long serialVersionUID = -7799608938279623903L;
	
	private String matricula;
	private Perfil perfil;
	private Modulo modulo;
	private Date dataManutencao;
	private String codUsuarioRespManutecao;
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Modulo getModulo() {
		return modulo;
	}
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	public Date getDataManutencao() {
		return dataManutencao;
	}
	public void setDataManutencao(Date dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	public String getCodUsuarioRespManutecao() {
		return codUsuarioRespManutecao;
	}
	public void setCodUsuarioRespManutecao(String codUsuarioRespManutecao) {
		this.codUsuarioRespManutecao = codUsuarioRespManutecao;
	}

}
