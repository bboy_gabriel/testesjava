package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

/**
 * Faz a junção de uma funcionalidade a um perfil
 * @author isfigueiredo
 *
 */
public class PerfilFuncionalidade implements Serializable{

	private static final long serialVersionUID = 1668064186994618407L;
	
	private Perfil perfil;
	private Funcionalidade funcionalidade;
	
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Funcionalidade getFuncionalidade() {
		return funcionalidade;
	}
	public void setFuncionalidade(Funcionalidade funcionalidade) {
		this.funcionalidade = funcionalidade;
	}

	
}
