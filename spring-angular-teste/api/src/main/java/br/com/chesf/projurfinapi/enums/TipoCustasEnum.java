package br.com.chesf.projurfinapi.enums;

/**
 * Enum para mapear os tipos de custas:
 * Custas iniciais, 
 * Custas recursais, 
 * Custas Complementares 
 * Custas Finais
 * @author isfigueiredo
 *
 */
public enum TipoCustasEnum {
	CUSTAS_INICIAIS(1, "Custas iniciais"),
	CUSTAS_RECURSAIS(2, "Custas recursais"),
	CUSTAS_COMPLEMENTARES(3, "Custas Complementares"),
	CUSTAS_FINAIS(4, "Custas Finais");
	
	private Integer id;
	private String desc;
	
	private TipoCustasEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}
	
	/**
	 * Recupera o tipoCustas pelo id
	 * @param id
	 * @return
	 */
	public static final TipoCustasEnum getTipoCustasEnumByID(Integer id) {
		TipoCustasEnum tipoEscolhido = null;
		switch (id) {
		case 1:
			tipoEscolhido = TipoCustasEnum.CUSTAS_INICIAIS;
			break;
		case 2:
			tipoEscolhido = TipoCustasEnum.CUSTAS_RECURSAIS;
			break;
		case 3:
			tipoEscolhido = TipoCustasEnum.CUSTAS_COMPLEMENTARES;
			break;
		case 4:
			tipoEscolhido = TipoCustasEnum.CUSTAS_FINAIS;
			break;
		}
		return tipoEscolhido;
	}

}
