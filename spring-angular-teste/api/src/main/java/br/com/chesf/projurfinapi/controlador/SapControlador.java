package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.SapServico;

@RestController
@RequestMapping(value = "/api/sap")
@Consumes("application/json")
@Produces("application/json")
public class SapControlador implements Serializable{

	private static final long serialVersionUID = 8179539165596746479L;

	@Autowired
	private SapServico sapServico;
	
	@GetMapping(value = "/obterDomicilioBancario/{codigoFornecedorSAP}")
	public ResponseEntity consultarDomicilioBancario(@PathVariable String codigoFornecedorSAP) {
		try {
			return new ResponseEntity(sapServico.obterDomicilioBancario(codigoFornecedorSAP), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
