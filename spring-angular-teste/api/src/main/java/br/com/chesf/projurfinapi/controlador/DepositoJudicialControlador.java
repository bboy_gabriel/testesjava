package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.DepositoJudicialVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.DepositoJudicialServico;

@RestController
@RequestMapping("/api/depositosJudiciais")
public class DepositoJudicialControlador implements Serializable{

	
	private static final long serialVersionUID = 8397704028419831583L;
	
	@Autowired
	private DepositoJudicialServico depositoJudicialServico;
	
	/**
	 * 
	 * Endpoint responavel por consultar as informações referentes aos depositos judiciais vinculados ao processo.
	 * 
	 * @param numeroProcesso
	 * @return
	 * 
	 */
	@GetMapping("/informacao/{numeroProcesso}")
	public ResponseEntity conultarDepositoJudicial(@PathVariable @NotEmpty String numeroProcesso) {
		try {
			return new ResponseEntity(depositoJudicialServico.consultarInformacoesDepositoJudicial(numeroProcesso), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * Endpoint responsavel por consultar a lista de depositos judiciais relacionados ao processo.
	 * 
	 * @param num_processo
	 * @return
	 * 
	 */
	@GetMapping("/totalDepositado/{numeroProcesso}")
	public ResponseEntity<List<DepositoJudicialVO>> consultarTotalDepositadoPorProcesso(@PathVariable @NotEmpty String numeroProcesso){ 
		try {
			return new ResponseEntity(depositoJudicialServico.consultarTotalDepositadoPorProcesso(numeroProcesso), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
