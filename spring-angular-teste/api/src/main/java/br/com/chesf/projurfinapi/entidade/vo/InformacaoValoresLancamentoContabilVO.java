package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Classe responsável por fornecer informações sobre os valores relacionados a entidade LancamentoContabil.
 * 
 * @author jvneto1
 *
 */
public class InformacaoValoresLancamentoContabilVO implements Serializable{
	private static final long serialVersionUID = 3309850180920613074L;

	private BigDecimal valorTotalProvisao;
	
	/**
	 * 
	 * Metodo resonsável por somar o valor totalProvisao + um novo valor.
	 * 
	 * @param valor
	 * 
	 */
	public void somarTotalProvisao(BigDecimal valor) {
		valorTotalProvisao = valorTotalProvisao.add(valor);
	}

	/**
	 * @return the valorTotalProvisao
	 */
	public BigDecimal getValorTotalProvisao() {
		return valorTotalProvisao;
	}

	/**
	 * @param valorTotalProvisao the valorTotalProvisao to set
	 */
	public void setValorTotalProvisao(BigDecimal valorTotalProvisao) {
		this.valorTotalProvisao = valorTotalProvisao;
	}
	
}
