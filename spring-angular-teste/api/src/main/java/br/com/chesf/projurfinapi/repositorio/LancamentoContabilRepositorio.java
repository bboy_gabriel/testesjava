package br.com.chesf.projurfinapi.repositorio;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.LancamentoContabil;

/**
 * 
 * Interface responsável por prover uma camada de persistencia ao recurso LancamentoContabil.
 * 
 * @author jvneto1
 *
 */
@Repository
public interface LancamentoContabilRepositorio extends JpaRepository<LancamentoContabil, Long>, QueryByExampleExecutor<LancamentoContabil>{

	List<LancamentoContabil> findByTipoLancamentoAndNumeroProcesso(String tipoLancamento, Long numeroProcesso);
	
	List<LancamentoContabil> findByNumeroProcesso(Long numeroProcesso);
	
	@Query("select la from LancamentoContabil la where la.dataLancamento between :dtInicial and :dtFinal order by la.dataLancamento asc")
	List<LancamentoContabil> consultarLancamentoIntervalo(@Param("dtInicial") LocalDate dtInicial, @Param("dtFinal") LocalDate dtFinal);
	
}
