package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.ElementoPep;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer uma camada de persistencia ao recurso ElementoPep
 *
 */
@Repository
public interface ElementoPeprepositorio extends JpaRepository<ElementoPep, Long>, QueryByExampleExecutor<ElementoPep>{
	
	
	public Boolean existsByCodigo(String codigo);
	
	
	public Boolean existsByCodigoAndIdNotIn(String codigo, Long ... ids);

}
