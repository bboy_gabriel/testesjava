package br.com.chesf.projurfinapi.enums;

/**
 * Enum criado para mapear os tipo de pagamento para o caso específico do tipo de despesa OU12.
 * @author isfigueiredo
 *
 */
public enum TipoPagamentoOU12Enum {
	SERVIDAO(1, "Servidão Administrativa"),
	DESAPROPIACAO(2, "Desapropriação Amigável"),
	CESSAO(3, "Cessão de Posse"),
	SOMENTE(4, "Somente Benfeitorias"),
	DESPESAS(5, "Despesas com Cartório");
	
	private Integer codigo;
	private String desc;
	
	private TipoPagamentoOU12Enum(Integer codigo, String desc) {
		this.codigo = codigo;
		this.desc = desc;			
	}
	
	/**
	 * Retorna o enum usando o código
	 * @param codigo
	 * @return
	 */
	public static final TipoPagamentoOU12Enum getTipoPagamentoOU12ByID(Integer codigo) {
		TipoPagamentoOU12Enum tipoEncontrado = null;
		switch (codigo) {
		case 1:
			tipoEncontrado = TipoPagamentoOU12Enum.SERVIDAO;
			break;
		case 2:
			tipoEncontrado = TipoPagamentoOU12Enum.DESAPROPIACAO;
			break;
		case 3:
			tipoEncontrado = TipoPagamentoOU12Enum.CESSAO;
			break;
		case 4:
			tipoEncontrado = TipoPagamentoOU12Enum.SOMENTE;
			break;
		case 5:
			tipoEncontrado = TipoPagamentoOU12Enum.DESPESAS;
		}
		return tipoEncontrado;
	}

}
