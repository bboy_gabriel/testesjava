package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por mapear o VO da entidade CentroDeCusto. 
 *
 */
public class CentroDeCustoVO implements Serializable{

	private static final long serialVersionUID = 1800572514187660911L;

    private Long id;
	
    @NotNull(message = "Codigo centro de custo nulo ou inválido!")
	private String codigo;
	
    @NotEmpty(message = "codigo descricao centro de custo nulo ou inválido!")
	private String descricao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
