package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.chesf.projurfinapi.enums.IndicadorPagamentoEnum;
import br.com.chesf.projurfinapi.enums.IndicadorSimNaoEnum;
import br.com.chesf.projurfinapi.util.IndicadorSimNaoStdDeserializer;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por exibir os dados referentes a entidade TipoDeDespesa.
 * Com o objetivo de não expor a entidade. 
 *
 */
public class TipoDeDespesaVO implements Serializable{

	private static final long serialVersionUID = -6946152706942445677L;
	
    private Long id;
	
    @NotNull(message = "Codigo nulo ou inválido!")
	private String codigo;
	
    @NotEmpty(message = "Descrição nulo ou inválido!")
	private String descricao;
	
    @NotNull(message = "Indicador de Pagamento nulo ou inválido!")
    private IndicadorPagamentoEnum indicadorPagamentoEnum;
    
    @JsonDeserialize(using = IndicadorSimNaoStdDeserializer.class)
    private IndicadorSimNaoEnum indicadorProcesso;

    @JsonDeserialize(using = IndicadorSimNaoStdDeserializer.class)
	private IndicadorSimNaoEnum indicadorInfoContabeis;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the indicadorPagamentoEnum
	 */
	public IndicadorPagamentoEnum getIndicadorPagamentoEnum() {
		return indicadorPagamentoEnum;
	}

	/**
	 * @param indicadorPagamentoEnum the indicadorPagamentoEnum to set
	 */
	public void setIndicadorPagamentoEnum(IndicadorPagamentoEnum indicadorPagamentoEnum) {
		this.indicadorPagamentoEnum = indicadorPagamentoEnum;
	}

	public IndicadorSimNaoEnum getIndicadorProcesso() {
		return indicadorProcesso;
	}

	public void setIndicadorProcesso(IndicadorSimNaoEnum indicadorProesso) {
		this.indicadorProcesso = indicadorProesso;
	}

	public IndicadorSimNaoEnum getIndicadorInfoContabeis() {
		return indicadorInfoContabeis;
	}

	public void setIndicadorInfoContabeis(IndicadorSimNaoEnum indicadorInfoContabeis) {
		this.indicadorInfoContabeis = indicadorInfoContabeis;
	}
    
}
