package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Classe de transporte de dados que representa uma guia de pagamento
 * @author isfigueiredo
 *
 */
public class GuiaPagamentoVO implements Serializable{

	private static final long serialVersionUID = 2757262671232125323L;

	private Long id;
	
	@NotEmpty(message = "Matrícula do usuário não pode ser vazia")
	private String usuario;
	
	@NotEmpty(message = "Nome do usuário não pode ser vazia")
	private String nomeUsuario;
	
	private ProcessoVO processoVO;
	
	private OrgaoGestorVO orgaoGestorVO;
	
	private CentroDeCustoVO centroDeCustoVO;
	
	private ElementoPepVO elementoPepVO;
	
	private OrdemVO ordemVO;
	
	private GuiaDepositoJudicialVO guiaDepositoJudicialVO;
	
	private CustasVO custasVO;
	
	private String documentoSAP;
	
	@NotEmpty(message = "Data da solicitação não pode ser vazia")
	private String dataSolicitacao;

	@NotEmpty(message = "Prazo não pode ser vazia")
	private String prazo;

	private ParteVO autor;	

	private ParteVO reu;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the nomeUsuario
	 */
	public String getNomeUsuario() {
		return nomeUsuario;
	}

	/**
	 * @param nomeUsuario the nomeUsuario to set
	 */
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	/**
	 * @return the processoVO
	 */
	public ProcessoVO getProcessoVO() {
		return processoVO;
	}

	/**
	 * @param processoVO the processoVO to set
	 */
	public void setProcessoVO(ProcessoVO processoVO) {
		this.processoVO = processoVO;
	}

	/**
	 * @return the orgaoGestorVO
	 */
	public OrgaoGestorVO getOrgaoGestorVO() {
		return orgaoGestorVO;
	}

	/**
	 * @param orgaoGestorVO the orgaoGestorVO to set
	 */
	public void setOrgaoGestorVO(OrgaoGestorVO orgaoGestorVO) {
		this.orgaoGestorVO = orgaoGestorVO;
	}

	/**
	 * @return the centroDeCustoVO
	 */
	public CentroDeCustoVO getCentroDeCustoVO() {
		return centroDeCustoVO;
	}

	/**
	 * @param centroDeCustoVO the centroDeCustoVO to set
	 */
	public void setCentroDeCustoVO(CentroDeCustoVO centroDeCustoVO) {
		this.centroDeCustoVO = centroDeCustoVO;
	}

	/**
	 * @return the elementoPepVO
	 */
	public ElementoPepVO getElementoPepVO() {
		return elementoPepVO;
	}

	/**
	 * @param elementoPepVO the elementoPepVO to set
	 */
	public void setElementoPepVO(ElementoPepVO elementoPepVO) {
		this.elementoPepVO = elementoPepVO;
	}

	/**
	 * @return the ordemVO
	 */
	public OrdemVO getOrdemVO() {
		return ordemVO;
	}

	/**
	 * @param ordemVO the ordemVO to set
	 */
	public void setOrdemVO(OrdemVO ordemVO) {
		this.ordemVO = ordemVO;
	}

	/**
	 * @return the guiaDepositoJudicialVO
	 */
	public GuiaDepositoJudicialVO getGuiaDepositoJudicialVO() {
		return guiaDepositoJudicialVO;
	}

	/**
	 * @param guiaDepositoJudicialVO the guiaDepositoJudicialVO to set
	 */
	public void setGuiaDepositoJudicialVO(GuiaDepositoJudicialVO guiaDepositoJudicialVO) {
		this.guiaDepositoJudicialVO = guiaDepositoJudicialVO;
	}

	/**
	 * @return the custasVO
	 */
	public CustasVO getCustasVO() {
		return custasVO;
	}

	/**
	 * @param custasVO the custasVO to set
	 */
	public void setCustasVO(CustasVO custasVO) {
		this.custasVO = custasVO;
	}

	/**
	 * @return the documentoSAP
	 */
	public String getDocumentoSAP() {
		return documentoSAP;
	}

	/**
	 * @param documentoSAP the documentoSAP to set
	 */
	public void setDocumentoSAP(String documentoSAP) {
		this.documentoSAP = documentoSAP;
	}

	/**
	 * @return the dataSolicitacao
	 */
	public String getDataSolicitacao() {
		return dataSolicitacao;
	}

	/**
	 * @param dataSolicitacao the dataSolicitacao to set
	 */
	public void setDataSolicitacao(String dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	/**
	 * @return the prazo
	 */
	public String getPrazo() {
		return prazo;
	}

	/**
	 * @param prazo the prazo to set
	 */
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}

	/**
	 * @return the autor
	 */
	public ParteVO getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(ParteVO autor) {
		this.autor = autor;
	}

	/**
	 * @return the reu
	 */
	public ParteVO getReu() {
		return reu;
	}

	/**
	 * @param reu the reu to set
	 */
	public void setReu(ParteVO reu) {
		this.reu = reu;
	}
	
}
