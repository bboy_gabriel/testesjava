package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import br.com.chesf.projurfinapi.entidade.usuario.Usuario;

public interface UsuarioRepositorio extends JpaRepository<Usuario, String>, QueryByExampleExecutor<Usuario>{

	@Query("Select user from Usuario user where UPPER(user.nomeUsuario) = UPPER(:nomeLogin)")
	Usuario findByNomeLogin(@Param("nomeLogin") String nomeLogin);
	
	Usuario findByMatricula(String matricula);
}
