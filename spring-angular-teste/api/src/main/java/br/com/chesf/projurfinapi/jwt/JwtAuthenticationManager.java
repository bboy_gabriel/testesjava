package br.com.chesf.projurfinapi.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import br.com.chesf.projurfinapi.config.ApplicationContextProvider;
import br.com.chesf.projurfinapi.entidade.vo.UsuarioVO;
import br.com.chesf.projurfinapi.servico.UsuarioServico;

@Component
public class JwtAuthenticationManager implements AuthenticationManager {
	
	@Autowired
	private UsuarioServico userService;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String credentials = (String) auth.getCredentials();
		UsuarioVO user = getUserService().autenticarUsuario(auth.getName(), credentials);
		
		if (user.getMatricula() != null) {
			UsernamePasswordAuthenticationToken userAutenicado =new UsernamePasswordAuthenticationToken(user, credentials);
			userAutenicado.setDetails(user);			
			return userAutenicado;
		}
		
		throw new AccessDeniedException("Usuario ou senha não encontrados");
	}
	
	protected UsuarioServico getUserService() {
		if (userService == null) {
			userService = ApplicationContextProvider.getContext().getBean(UsuarioServico.class);
		}		
		return userService;
	}
}
