package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.usuario.ChesfAdAuthentication;
import br.com.chesf.projurfinapi.entidade.usuario.Usuario;
import br.com.chesf.projurfinapi.entidade.vo.UsuarioVO;
import br.com.chesf.projurfinapi.enums.StatusUsuarioEnum;
import br.com.chesf.projurfinapi.repositorio.UsuarioRepositorio;

@Service
public class UsuarioServico implements Serializable{

	private static final long serialVersionUID = 3249378933020651895L;
	
	private static final String DOMAIN = "redechesf.local";
	
	@Autowired
	private ChesfAdAuthentication chesfAdAuthentication;
	
	@Autowired
	private UsuarioRepositorio usuarioRepositorio;
	
	public UsuarioVO autenticarUsuario(String username, String password) {
		UsuarioVO user = new UsuarioVO(username, password, Arrays.asList(new GrantedAuthority() {			
			@Override
			public String getAuthority() {
				return "USER";
			}
		}));
		Usuario usuario = usuarioRepositorio.findByNomeLogin(username);
		
		if(usuario == null) {
			throw new AccessDeniedException("Usuário não cadastrado.");
		}
		
		if(StatusUsuarioEnum.A.equals(usuario.getStatus())
				&& ChesfAdAuthentication.authenticateUser(username,
						password, DOMAIN)){			
				user.setMatricula(usuario.getMatricula());
				user.setUsername(user.getUsername());				
			return user;
		}else {
			throw new AccessDeniedException("Login ou senha inválidos.");
		}
	}
	
	public Usuario consultarUsuarioByMatricula(String matricula) {
		
		return usuarioRepositorio.findByMatricula(matricula);
	}

}
