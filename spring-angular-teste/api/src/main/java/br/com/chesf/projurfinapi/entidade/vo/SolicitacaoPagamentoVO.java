package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.chesf.projurfinapi.enums.FormaPagamentoEnum;
import br.com.chesf.projurfinapi.enums.TipoDepositoJudicialEnum;

public class SolicitacaoPagamentoVO implements Serializable{

	private static final long serialVersionUID = -8782162333772867061L;

    private Long id;
	
    @NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valor;
	
    @NotNull(message = "Tipo de despesa nulo ou inválido!")
	private TipoDeDespesaVO tipoDeDespesa;
	
    private TipoDepositoJudicialEnum tipoDepositoJudicialEnum;
	
    @NotNull(message = "Fornecedor Sap nulo ou inválido!")
	private FornecedorSapVO fornecedorSap;
	
    @NotEmpty(message = "Data da emissão nula ou inválida!")
	private String dataEmissao;
	
    @NotEmpty(message = "Data vencimento nula ou inválida!")
	private String dataVencimento;
	
    @NotNull(message = "Orgão gestor nulo ou inválido!")
	private OrgaoGestorVO orgaoGestor;
	
	private String processo;
	
	private List<InformacaoContabilVO> informacoesContabeis;

	@NotNull(message = "Forma de pagamento nulo ou inválido!")
    private FormaPagamentoEnum formaPagamentoEnum;	
	
    private String formaPagamentoEnumDesc;
	
    @NotNull(message = "Domicilio Bancario nulo ou inválido!")
	private DomicilioBancarioVO domicilioBancario;
	
	private String codigoBarra;
	
	private String statusPagamentoEnum; 
	
	@NotEmpty(message = "Usuário nulo ou inválido!")
	private String usuario;
	
	private String documentoSap;

	private List<MsgErroGeraPagamentoVO> listaMensagensErroGeraPagamento;
	
	private Long codigoStatus;
	
	private Long codigoDepositoJudicial;
	
	private String dataInsercao;
	
	private String loginSolicitacao;
	
	//private Arquivo arquivoPagamento;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the tipoDeDespesa
	 */
	public TipoDeDespesaVO getTipoDeDespesa() {
		return tipoDeDespesa;
	}

	/**
	 * @param tipoDeDespesa the tipoDeDespesa to set
	 */
	public void setTipoDeDespesa(TipoDeDespesaVO tipoDeDespesa) {
		this.tipoDeDespesa = tipoDeDespesa;
	}

	/**
	 * @return the tipoDepositoJudicialEnum
	 */
	public TipoDepositoJudicialEnum getTipoDepositoJudicialEnum() {
		if(this.codigoDepositoJudicial != null) {
			return TipoDepositoJudicialEnum.getTipoDepositoJudicialEnumByID(this.codigoDepositoJudicial.intValue());			
		}
		else {
			return null;
		}
	}
	/**
	 * @param tipoDepositoJudicialEnum the tipoDepositoJudicialEnum to set
	 */
	public void setTipoDepositoJudicialEnum(TipoDepositoJudicialEnum tipoDepositoJudicialEnum) {
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
	}

	/**
	 * @return the fornecedorSap
	 */
	public FornecedorSapVO getFornecedorSap() {
		return fornecedorSap;
	}

	/**
	 * @param fornecedorSap the fornecedorSap to set
	 */
	public void setFornecedorSap(FornecedorSapVO fornecedorSap) {
		this.fornecedorSap = fornecedorSap;
	}

	/**
	 * @return the dataEmissao
	 */
	public String getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * @param dataEmissao the dataEmissao to set
	 */
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the dataVencimento
	 */
	public String getDataVencimento() {
		return dataVencimento;
	}

	/**
	 * @param dataVencimento the dataVencimento to set
	 */
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the orgaoGestor
	 */
	public OrgaoGestorVO getOrgaoGestor() {
		return orgaoGestor;
	}

	/**
	 * @param orgaoGestor the orgaoGestor to set
	 */
	public void setOrgaoGestor(OrgaoGestorVO orgaoGestor) {
		this.orgaoGestor = orgaoGestor;
	}

	/**
	 * @return the processo
	 */
	public String getProcesso() {
		return processo;
	}

	/**
	 * @param processo the processo to set
	 */
	public void setProcesso(String processo) {
		this.processo = processo;
	}

	/**
	 * @return the informacoesContabeis
	 */
    public List<InformacaoContabilVO> getInformacoesContabeis() {
		return informacoesContabeis;
	}

	public void setInformacoesContabeis(List<InformacaoContabilVO> informacoesContabeis) {
		this.informacoesContabeis = informacoesContabeis;
	}
	
	/**
	 * @return the formaPagamentoEnum
	 */
	public FormaPagamentoEnum getFormaPagamentoEnum() {
		return formaPagamentoEnum;
	}

	/**
	 * @param formaPagamentoEnum the formaPagamentoEnum to set
	 */
	public void setFormaPagamentoEnum(FormaPagamentoEnum formaPagamentoEnum) {
		this.formaPagamentoEnum = formaPagamentoEnum;
	}

	/**
	 * @return the domicilioBancario
	 */
	public DomicilioBancarioVO getDomicilioBancario() {
		return domicilioBancario;
	}

	/**
	 * @param domicilioBancario the domicilioBancario to set
	 */
	public void setDomicilioBancario(DomicilioBancarioVO domicilioBancario) {
		this.domicilioBancario = domicilioBancario;
	}

	/**
	 * @return the codigoBarra
	 */
	public String getCodigoBarra() {
		return codigoBarra;
	}

	/**
	 * @param codigoBarra the codigoBarra to set
	 */
	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	/**
	 * @return the statusPagamentoEnum
	 */
	public String getStatusPagamentoEnum() {
		return statusPagamentoEnum;
	}

	/**
	 * @param statusPagamentoEnum the statusPagamentoEnum to set
	 */
	public void setStatusPagamentoEnum(String statusPagamentoEnum) {
		this.statusPagamentoEnum = statusPagamentoEnum;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the documentoSap
	 */
	public String getDocumentoSap() {
		return documentoSap;
	}

	/**
	 * @param documentoSap the documentoSap to set
	 */
	public void setDocumentoSap(String documentoSap) {
		this.documentoSap = documentoSap;
	}

	/**
	 * @return the listaMensagensErroGeraPagamento
	 */
	public List<MsgErroGeraPagamentoVO> getListaMensagensErroGeraPagamento() {
		return listaMensagensErroGeraPagamento;
	}

	/**
	 * @param listaMensagensErroGeraPagamento the listaMensagensErroGeraPagamento to set
	 */
	public void setListaMensagensErroGeraPagamento(List<MsgErroGeraPagamentoVO> listaMensagensErroGeraPagamento) {
		this.listaMensagensErroGeraPagamento = listaMensagensErroGeraPagamento;
	}

	/**
	 * @return the codigoStatus
	 */
	public Long getCodigoStatus() {
		return codigoStatus;
	}

	/**
	 * @param codigoStatus the codigoStatus to set
	 */
	public void setCodigoStatus(Long codigoStatus) {
		this.codigoStatus = codigoStatus;
	}
	
	/**
	 * @return the codigoDepositoJudicial
	 */
	public Long getCodigoDepositoJudicial() {
		return codigoDepositoJudicial;
	}

	/**
	 * @param codigoDepositoJudicial the codigoDepositoJudicial to set
	 */
	public void setCodigoDepositoJudicial(Long codigoDepositoJudicial) {
		this.codigoDepositoJudicial = codigoDepositoJudicial;
	}

	/**
	 * @return the dataInsercao
	 */
	public String getDataInsercao() {
		return dataInsercao;
	}

	/**
	 * @param dataInsercao the dataInsercao to set
	 */
	public void setDataInsercao(String dataInsercao) {
		this.dataInsercao = dataInsercao;
	}

	/**
	 * @return the loginSolicitacao
	 */
	public String getLoginSolicitacao() {
		return loginSolicitacao;
	}

	/**
	 * @param loginSolicitacao the loginSolicitacao to set
	 */
	public void setLoginSolicitacao(String loginSolicitacao) {
		this.loginSolicitacao = loginSolicitacao;
	}

	public String getFormaPagamentoEnumDesc() {
		return formaPagamentoEnumDesc;
	}

	public void setFormaPagamentoEnumDesc(String formaPagamentoEnumDesc) {
		this.formaPagamentoEnumDesc = formaPagamentoEnumDesc;
	}

//	public Arquivo getArquivoPagamento() {
//		return arquivoPagamento;
//	}
//
//	public void setArquivoPagamento(Arquivo arquivoPagamento) {
//		this.arquivoPagamento = arquivoPagamento;
//	}
	
}
