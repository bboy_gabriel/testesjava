package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.FornecedorSap;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.vo.FornecedorSapVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.FornecedorSapRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer servicos referente ao recurso FornecedorSap.
 *
 */
@Service
public class FornecedorSapServico implements Serializable{

	private static final long serialVersionUID = 4561121377487944833L;
	
	@Autowired
	private FornecedorSapRepositorio fornecedorSapRepositorio;
	
	@Autowired
	private SolicitacaoPagamentoServico solicitacaoPagamentoServico;
	
	Logger logger = LoggerFactory.getLogger(FornecedorSap.class);
	
	private static final String FORNECEDOR_SAP = "Fornecedor SAP";
	
	private static final String CNPJ = "CNPJ ";
	private static final String CODIGO = "Código ";

	
	/**
	 * 
	 * @return List<FornecedorSapVO>
	 * 
	 * Metodo responsavel por consultar a lista de recursos FornecedorSap, validar e montar uma lista de FornecedorSapVO
	 * 
	 */
	public List<FornecedorSapVO> consultar(){
		try {
			List<FornecedorSap> listaBase = fornecedorSapRepositorio.findAll();
			validarListaFornecedorSapBase(listaBase);
			return montarListaFornecedorSapVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_FORNECEDOR_SAP);
		}
	}
	
	/**
	 * 
	 * @return List<FornecedorSapVO>
	 * 
	 * Metodo responsavel por consultar a lista de recursos FornecedorSap ordenada pelo codigo, validar e montar uma lista de FornecedorSapVO
	 * 
	 */
	public List<FornecedorSapVO> consultarOrdenado(){
		try {
			List<FornecedorSap> listaBase = fornecedorSapRepositorio.findAll(new Sort(Sort.Direction.ASC, "codigo"));
			validarListaFornecedorSapBase(listaBase);
			return montarListaFornecedorSapVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_FORNECEDOR_SAP);
		}
	}
	
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo responsavel por validar a lista de recursos FornecedorSap
	 * 
	 */
	private void validarListaFornecedorSapBase(List<FornecedorSap> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_FORNECEDOR_SAP_NULA_OU_INVALIDA);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<FornecedorSapVO>
	 * 
	 * Metodo responsavel por montar a lista de FornecedorSapVO 
	 * 
	 */
	private List<FornecedorSapVO> montarListaFornecedorSapVO(List<FornecedorSap> listaBase){
		List<FornecedorSapVO> listaVO = new ArrayList<FornecedorSapVO>();
		for (FornecedorSap itemBase : listaBase) {
			FornecedorSapVO vo = new FornecedorSapVO();
			vo.setCnpj(itemBase.getCnpj());
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			listaVO.add(vo);
		}
		return listaVO;
	}

	public FornecedorSap consultarPorId(Long id) {
		try {
			Validador.validarId(id);
			FornecedorSap itemBase =  fornecedorSapRepositorio.findOne(id);
			validarEntidadeBase(itemBase);
			return itemBase;			
		} catch(NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_FORNECEDOR_SAP);
		}
	}
	
	public FornecedorSapVO consultarPorIdVO(Long id) {
		try {
			Validador.validarId(id);
			FornecedorSap itemBase =  consultarPorId(id);
			return montarFornecedorVO(itemBase);			
		} catch(NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_FORNECEDOR_SAP);
		}
	}
	
	
	public FornecedorSapVO cadastrar(FornecedorSapVO fornecedorVO) {
		verificarExistenciaPorCodigoOuCnpj(fornecedorVO);
		validarVO(fornecedorVO);
		try {
			FornecedorSap fornecedorBase = fornecedorSapRepositorio.save(montarFornecedorBase(fornecedorVO));
			validarEntidadeBase(fornecedorBase);			
			return montarFornecedorVO(fornecedorBase);
		} catch (Exception e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_FORNECEDOR_SAP);
		}
	}
	
	public FornecedorSapVO atualizar(FornecedorSapVO fornecedorVO) {
		verificaExistenciaPorCodigoOuCnpjAndIdNotIn(fornecedorVO);
		try {
			validarVO(fornecedorVO);
			FornecedorSap fornecedorBase = fornecedorSapRepositorio.save(montarFornecedorBase(fornecedorVO));
			validarEntidadeBase(fornecedorBase);			
			return montarFornecedorVO(fornecedorBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_FORNECEDOR_SAP);
		}
	}
	
	public void remover(Long id) {
		List<SolicitacaoPagamento> lstSolic = null;

		FornecedorSap fornecedorSap = new FornecedorSap();
		fornecedorSap.setId(id);
		lstSolic = solicitacaoPagamentoServico.consultarSolicitacaoPorFornecedorSAP(fornecedorSap); 
		if(lstSolic != null && !lstSolic.isEmpty()) {
			logger.info("Fornecedor SAP: " + lstSolic.get(0).getFornecedorSap().getDescricao() + " está sendo usado e não pode ser excluído!");
			
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO, FORNECEDOR_SAP);
		}else {
			fornecedorSapRepositorio.delete(id);				
		}
	}

	
	private FornecedorSap montarFornecedorBase(FornecedorSapVO fornecedorVO) {
		FornecedorSap fornecedorBase = new FornecedorSap();
		fornecedorBase.setId(fornecedorVO.getId());
		fornecedorBase.setCodigo(fornecedorVO.getCodigo());
		fornecedorBase.setDescricao(fornecedorVO.getDescricao());
		fornecedorBase.setCnpj(fornecedorVO.getCnpj());
		return fornecedorBase;
	}
	

	private FornecedorSapVO montarFornecedorVO(FornecedorSap fornecedorBase) {
		FornecedorSapVO fornecedorVO = new FornecedorSapVO();
		fornecedorVO.setId(fornecedorBase.getId());
		fornecedorVO.setCodigo(fornecedorBase.getCodigo());
		fornecedorVO.setDescricao(fornecedorBase.getDescricao());
		fornecedorVO.setCnpj(fornecedorBase.getCnpj());
		return fornecedorVO;
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código
	 * @param fornecedorSapVO
	 */
	private void verificarExistenciaPorCodigoOuCnpj(FornecedorSapVO fornecedorSapVO) {
		if(fornecedorSapRepositorio.existsByCodigo(fornecedorSapVO.getCodigo())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, fornecedorSapVO.getCodigo().toString(), FORNECEDOR_SAP);
		}
		
		if(fornecedorSapRepositorio.existsByCnpj(fornecedorSapVO.getCnpj())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CNPJ, fornecedorSapVO.getCnpj(), FORNECEDOR_SAP);
		}
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código desconsiderando o próprio registro.
	 * @param fornecedorSapVO
	 */
	private void verificaExistenciaPorCodigoOuCnpjAndIdNotIn(FornecedorSapVO fornecedorSapVO) {
		if(fornecedorSapRepositorio.existsByCodigoAndIdNotIn(fornecedorSapVO.getCodigo(),fornecedorSapVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO,fornecedorSapVO.getCodigo().toString(), FORNECEDOR_SAP);
		}
		
		if(fornecedorSapRepositorio.existsByCnpjAndIdNotIn(fornecedorSapVO.getCnpj(), fornecedorSapVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CNPJ, fornecedorSapVO.getCnpj(), FORNECEDOR_SAP);
		}
	}
	
	private void validarVO(FornecedorSapVO fornecedorVO) {
		if(fornecedorVO == null) throw new NegocioException(MensagemEnum.FORNECEDOR_SAP_NULO_OU_INVALIDO);
		if(fornecedorVO.getCnpj() == null || fornecedorVO.getCnpj().isEmpty() || !validarCNPJ(fornecedorVO.getCnpj())) throw new NegocioException(MensagemEnum.CNPJ_NULO_OU_INVALIDO);
	}
	
	private boolean validarCNPJ(String CNPJ) {
		// considera-se erro CNPJ's formados por uma sequencia de numeros iguais
	    if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111") ||
	        CNPJ.equals("22222222222222") || CNPJ.equals("33333333333333") ||
	        CNPJ.equals("44444444444444") || CNPJ.equals("55555555555555") ||
	        CNPJ.equals("66666666666666") || CNPJ.equals("77777777777777") ||
	        CNPJ.equals("88888888888888") || CNPJ.equals("99999999999999") ||
	       (CNPJ.length() != 14))
	       return(false);
	 
	    char dig13, dig14;
	    int sm, i, r, num, peso;
	 
	// "try" - protege o código para eventuais erros de conversao de tipo (int)
	    try {
	// Calculo do 1o. Digito Verificador
	      sm = 0;
	      peso = 2;
	      for (i=11; i>=0; i--) {
	// converte o i-ésimo caractere do CNPJ em um número:
	// por exemplo, transforma o caractere '0' no inteiro 0
	// (48 eh a posição de '0' na tabela ASCII)
	        num = (int)(CNPJ.charAt(i) - 48);
	        sm = sm + (num * peso);
	        peso = peso + 1;
	        if (peso == 10)
	           peso = 2;
	      }
	 
	      r = sm % 11;
	      if ((r == 0) || (r == 1))
	         dig13 = '0';
	      else dig13 = (char)((11-r) + 48);
	 
	// Calculo do 2o. Digito Verificador
	      sm = 0;
	      peso = 2;
	      for (i=12; i>=0; i--) {
	        num = (int)(CNPJ.charAt(i)- 48);
	        sm = sm + (num * peso);
	        peso = peso + 1;
	        if (peso == 10)
	           peso = 2;
	      }
	 
	      r = sm % 11;
	      if ((r == 0) || (r == 1))
	         dig14 = '0';
	      else dig14 = (char)((11-r) + 48);
	 
	// Verifica se os dígitos calculados conferem com os dígitos informados.
	      if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13)))
	         return(true);
	      else return(false);
	    } catch (NegocioException e) {
	        throw new NegocioException(MensagemEnum.CNPJ_NULO_OU_INVALIDO);
	    }
	  }

	
	/**
	 * 
	 * @param entidadeBase
	 * 
	 * Metodo privado responsavel por validar o retorno base no serviço getById
	 * 
	 */
	private void validarEntidadeBase(FornecedorSap entidadeBase) {
		if(entidadeBase == null) throw new NegocioException(MensagemEnum.FORNECEDOR_SAP_NULO_OU_INVALIDO);
	}
	
}
