package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o VO referente ao recurso Ordem.
 *
 */
public class OrdemVO implements Serializable{

	private static final long serialVersionUID = 3103249164206834154L;

    private Long id;
	
    @NotNull(message = "Codigo Ordem nulo ou inválido!")
	private Long codigo;
	
    @NotEmpty(message = "Descrição Ordem nulo ou inválido!")
	private String descricao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
