package br.com.chesf.projurfinapi.enums;

public enum FormaPagamentoEnum {
	
	C("CHEQUE"),
	I("CONCILIACAO"),
	B("CODIGO DE BARRAS"),
	R("CREDITO EM CONTA"),
	M("MONITORAÇÃO CONTAS");
	
	private String desc;
	
	private FormaPagamentoEnum(String desc){
		this.desc = desc;
	}
	
	public String getDesc() {
		return desc;
	}
	
}
