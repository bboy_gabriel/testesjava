package br.com.chesf.projurfinapi.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import br.com.chesf.projurfinapi.exception.NegocioException;

@Component
public class JwtTokenAuthenticationFilter extends GenericFilterBean {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		Authentication authentication = null;
		try{
			authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest) request);
			if(authentication != null)
				SecurityContextHolder.getContext().setAuthentication(authentication);
		}catch (NegocioException e) {		
			((HttpServletResponse)response).sendError(HttpServletResponse.SC_UNAUTHORIZED);	
			response.getWriter().print("");
			response.flushBuffer();
		}		
		chain.doFilter(request, response);
	}
	
}
