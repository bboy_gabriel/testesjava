package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.MsgErroGeraPagamento;

@Repository
public interface ListaMensagensErroGeraPagamentoRepositorio extends JpaRepository<MsgErroGeraPagamento, Long>, QueryByExampleExecutor<MsgErroGeraPagamento>{
	
	@Query("Select lstErro from MsgErroGeraPagamento lstErro where lstErro.idSolicitacao = :idSolicitacao")
	List<MsgErroGeraPagamento> findByIdSolicitacaoPagamento(@Param("idSolicitacao") Long idSolicitacao);
}
