package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;


public class DomicilioBancario implements Serializable{

	private static final long serialVersionUID = 7515238400660091208L;

    private Long id;
	
	private String bancoAgencia;
	
	private String bancoConta; 
	
	private String bancoNome;
	
	private int bancoDomicilio;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the bancoAgencia
	 */
	public String getBancoAgencia() {
		return bancoAgencia;
	}

	/**
	 * @param bancoAgencia the bancoAgencia to set
	 */
	public void setBancoAgencia(String bancoAgencia) {
		this.bancoAgencia = bancoAgencia;
	}

	/**
	 * @return the bancoConta
	 */
	public String getBancoConta() {
		return bancoConta;
	}

	/**
	 * @param bancoConta the bancoConta to set
	 */
	public void setBancoConta(String bancoConta) {
		this.bancoConta = bancoConta;
	}

	/**
	 * @return the bancoNome
	 */
	public String getBancoNome() {
		return bancoNome;
	}

	/**
	 * @param bancoNome the bancoNome to set
	 */
	public void setBancoNome(String bancoNome) {
		this.bancoNome = bancoNome;
	}

	/**
	 * @return the bancoDomicilio
	 */
	public int getBancoDomicilio() {
		return bancoDomicilio;
	}

	/**
	 * @param bancoDomicilio the bancoDomicilio to set
	 */
	public void setBancoDomicilio(int bancoDomicilio) {
		this.bancoDomicilio = bancoDomicilio;
	}
	
}
