package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.Parte;
import br.com.chesf.projurfinapi.entidade.vo.ParteVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ParteRepositorio;

/**
 * 
 * Classe responsável por fornecer uma camada de serviços ao recurso Parte.
 * 
 * @author jvneto1
 *
 */
@Service
public class ParteServico implements Serializable{

	private static final long serialVersionUID = -1103831599181650310L;
	
	@Autowired
	private ParteRepositorio parteRepositorio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	Logger logger = LoggerFactory.getLogger(LancamentoContabilServico.class);
	
	/**
	 * 
	 * Serviço responssável por consultar um recruso Parte pelo numero do CNPJ.
	 * 
	 * @param nrCNPJ
	 * @return Parte
	 * 
	 */
	public Parte consultarPartePorNrCNPJ(Long nrCNPJ) {
		try {
			return parteRepositorio.findByNrCNPJ(nrCNPJ);
		} catch (NegocioException e) {
			logger.error(MensagemEnum.NAO_FOI_POSSIVEL_CONSULTAR_LISTA_CONTA_POR_TIPO_PROCESSO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.NAO_FOI_POSSIVEL_CONSULTAR_PARTE_POR_NR_CNPJ);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por consultar uma lista do recurso Parte referente aos autores do processo.
	 * 
	 * @param codProcesso
	 * @return List<ParteVO>
	 * 
	 */
	public List<ParteVO> consultarListaAutoresPorProcesso(Long codigoProcesso){
		try {
			return convertListToVo(parteRepositorio.consultarAutoresProcesso(codigoProcesso));
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_AUTORES_POR_PROCESSO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_AUTORES_POR_PROCESSO);
		}
	}
	
	/**
	 * 
	 * Serviço reponsável por consultar uma lista do recurso Parte referente aos reus do processo.
	 * 
	 * @param condigoProcesso
	 * @return List<ParteVO>
	 * 
	 */
	public List<ParteVO> consultarListaReusPorProcesso(Long condigoProcesso){
		try {
			return convertListToVo(parteRepositorio.consultarReusProcesso(condigoProcesso));
		} catch (Exception e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_REU_POR_PROCESSO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_REU_POR_PROCESSO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por converter uma lista do recurso Parte em uma lista VO.
	 * 
	 * @param listaBase
	 * @return List<ParteVO>
	 * 
	 */
	private List<ParteVO> convertListToVo(List<Parte> listaBase){
		try {
			List<ParteVO> listaVO = new ArrayList<>();
			for (Parte parte : listaBase) {
				ParteVO vo = modelMapper.map(parte, ParteVO.class);
				listaVO.add(vo);
			}
			return listaVO;
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_LISTA_PARTE_PARA_LISTA_VO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_LISTA_PARTE_PARA_LISTA_VO);
		}
	}

}
