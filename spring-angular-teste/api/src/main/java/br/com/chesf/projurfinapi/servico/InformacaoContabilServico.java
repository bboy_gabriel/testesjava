package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.CentroDeCusto;
import br.com.chesf.projurfinapi.entidade.ElementoPep;
import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.Ordem;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.InformacaoContabilRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

@Service
public class InformacaoContabilServico implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1591957762039012928L;
	
	@Autowired
	private InformacaoContabilRepositorio informacaoContabilRepositorio;
	
	Logger logger = LoggerFactory.getLogger(InformacaoContabilServico.class);
	
	public List<InformacaoContabil> consultarInformacaoContabilPorCentroCusto(CentroDeCusto centroCusto) throws NegocioException {
		List<InformacaoContabil> lstInfoContabeis;
		
		try {
			lstInfoContabeis = informacaoContabilRepositorio.findByCentroDeCusto(centroCusto);
		} catch (Exception e) {
			logger.error("Não foi possível consultar a informação contábil: ", e);
			throw new NegocioException(MensagemEnum.CENTRO_DE_CUSTO_NULO_OU_INVALIDO);
		}		
		
		return lstInfoContabeis;
	}
	
	public List<InformacaoContabil> consultarInformacaoContabilPorElementoPEP(Long idElementoPEP) throws NegocioException {
		Validador.validarId(idElementoPEP);
		List<InformacaoContabil> lstInfoContabeis;
		
		try {
			ElementoPep elementoPep = new ElementoPep();
			elementoPep.setId(idElementoPEP);
			lstInfoContabeis = informacaoContabilRepositorio.findByElementoPep(elementoPep);
		} catch (Exception e) {
			logger.error("Não foi possível consultar a informação contábil: ", e);
			throw new NegocioException(MensagemEnum.ELEMENTO_PEP_NULO_OU_INVALIDO);
		}		
		
		return lstInfoContabeis;
	}

	
	public List<InformacaoContabil> consultarInformacaoContabilPorOrdem(Long idOrdem) throws NegocioException {
		Validador.validarId(idOrdem);
		List<InformacaoContabil> lstInfoContabeis;
		
		try {
			Ordem ordem = new Ordem();
			ordem.setId(idOrdem);
			lstInfoContabeis = informacaoContabilRepositorio.findByOrdem(ordem);
		} catch (Exception e) {
			logger.error("Não foi possível consultar a informação contábil: ", e);
			throw new NegocioException(MensagemEnum.ORDEM_NULA_OU_INVALIDA);
		}		
		
		return lstInfoContabeis;
	}
}
