package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TLNOM_ARELO", schema = "TLNOM")
@SequenceGenerator(name = "SQ_TLNOM_ARELO", sequenceName = "SQ_TLNOM_ARELO", allocationSize = 1)
public class TlnomArelo implements Serializable{

	private static final long serialVersionUID = -3312597893172275913L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TLNOM_ARELO")
	@Column(name = "PK_ARELO")
	private Long id;
	
	@Column(name = "DS_ARELO", length = 50)
	private String descricaoArelo;
	
	@Column(name = "SG_ARELO", length = 6)
	private String siglaArelo;
	
	@Column(name = "CD_SISG", length = 1)
	private String codigoSisg;
	
	@Column(name = "FK_EMPS", length = 4)
	private Long empresa;
	
	@Column(name = "CD_CLAS_ORGA", length = 1)
	private String codigoClasOrga;
	
	@Column(name = "NR_SEQN_SI", length = 5)
	private Long numeroSeqnSi;
	
	@Column(name = "NR_SEQN_PM", length = 5)
	private Long numeroSeqnPm;
	
	@Column(name = "NR_SEQN_RM", length = 5)
	private Long numeroSeqnRm;
	
	@Column(name = "NR_SEQN_RA", length = 5)
	private Long numeroSeqnRa;
	
	@Column(name = "NR_SEQN_PD", length = 5)
	private Long numeroSeqnPd;
	
	@Column(name = "NR_SEQN_COME", length = 5)
	private Long numeroSeqnCome;
	
	@Column(name = "DS_TLNOM_ARELO", length = 25)
	private String decricaoTlnomArelo;
	
	@Column(name =  "NR_SEQN_MENG", length = 5) 
	private Long numeroSeqnMeng;
	
	@Column(name = "NR_SEQN_PTQT", length = 5)
	private Long numeroSeqnPtqt;
	
	@Column(name = "NR_SEQN_SMI", length = 5)
	private Long numeroSeqnSmi;
	
	@Column(name = "CD_ARELO_NOVA", length = 6)
	private String codigoAreloNova;
	
	@Column(name = "DS_SISEX", length = 30)
	private String descricaoSisex;
	
	@Column(name = "NR_MATR_USUR", length = 6)
	private String numeroMatriculaUsuario;
	
	@Column(name = "SG_GERN", length = 6)
	private String siglaGern;
	
	@Column(name = "ID_SITC", length = 1)
	private String idSitc;
	
	@Column(name = "VL_REMN_HORA_MEDI", length = 7)
	private Long valorRemnHoraMedi;
	
	@Column(name = "SG_DIRT", length = 6)
	private String siglaDirt;
	
	@Column(name = "SG_SUPR", length = 6)
	private String siglaSupr;
	
	@Column(name = "SG_DEPT", length = 6)
	private String siglaDept;
	
	@Column(name = "SG_DIVS", length = 6)
	private String siglaDivs;
	
	@Column(name = "ID_SAP", length = 4)
	private String idSap;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the descricaoArelo
	 */
	public String getDescricaoArelo() {
		return descricaoArelo;
	}

	/**
	 * @param descricaoArelo the descricaoArelo to set
	 */
	public void setDescricaoArelo(String descricaoArelo) {
		this.descricaoArelo = descricaoArelo;
	}

	/**
	 * @return the siglaArelo
	 */
	public String getSiglaArelo() {
		return siglaArelo;
	}

	/**
	 * @param siglaArelo the siglaArelo to set
	 */
	public void setSiglaArelo(String siglaArelo) {
		this.siglaArelo = siglaArelo;
	}

	/**
	 * @return the codigoSisg
	 */
	public String getCodigoSisg() {
		return codigoSisg;
	}

	/**
	 * @param codigoSisg the codigoSisg to set
	 */
	public void setCodigoSisg(String codigoSisg) {
		this.codigoSisg = codigoSisg;
	}

	/**
	 * @return the empresa
	 */
	public Long getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa the empresa to set
	 */
	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}

	/**
	 * @return the codigoClasOrga
	 */
	public String getCodigoClasOrga() {
		return codigoClasOrga;
	}

	/**
	 * @param codigoClasOrga the codigoClasOrga to set
	 */
	public void setCodigoClasOrga(String codigoClasOrga) {
		this.codigoClasOrga = codigoClasOrga;
	}

	/**
	 * @return the numeroSeqnSi
	 */
	public Long getNumeroSeqnSi() {
		return numeroSeqnSi;
	}

	/**
	 * @param numeroSeqnSi the numeroSeqnSi to set
	 */
	public void setNumeroSeqnSi(Long numeroSeqnSi) {
		this.numeroSeqnSi = numeroSeqnSi;
	}

	/**
	 * @return the numeroSeqnPm
	 */
	public Long getNumeroSeqnPm() {
		return numeroSeqnPm;
	}

	/**
	 * @param numeroSeqnPm the numeroSeqnPm to set
	 */
	public void setNumeroSeqnPm(Long numeroSeqnPm) {
		this.numeroSeqnPm = numeroSeqnPm;
	}

	/**
	 * @return the numeroSeqnRm
	 */
	public Long getNumeroSeqnRm() {
		return numeroSeqnRm;
	}

	/**
	 * @param numeroSeqnRm the numeroSeqnRm to set
	 */
	public void setNumeroSeqnRm(Long numeroSeqnRm) {
		this.numeroSeqnRm = numeroSeqnRm;
	}

	/**
	 * @return the numeroSeqnRa
	 */
	public Long getNumeroSeqnRa() {
		return numeroSeqnRa;
	}

	/**
	 * @param numeroSeqnRa the numeroSeqnRa to set
	 */
	public void setNumeroSeqnRa(Long numeroSeqnRa) {
		this.numeroSeqnRa = numeroSeqnRa;
	}

	/**
	 * @return the numeroSeqnPd
	 */
	public Long getNumeroSeqnPd() {
		return numeroSeqnPd;
	}

	/**
	 * @param numeroSeqnPd the numeroSeqnPd to set
	 */
	public void setNumeroSeqnPd(Long numeroSeqnPd) {
		this.numeroSeqnPd = numeroSeqnPd;
	}

	/**
	 * @return the numeroSeqnCome
	 */
	public Long getNumeroSeqnCome() {
		return numeroSeqnCome;
	}

	/**
	 * @param numeroSeqnCome the numeroSeqnCome to set
	 */
	public void setNumeroSeqnCome(Long numeroSeqnCome) {
		this.numeroSeqnCome = numeroSeqnCome;
	}

	/**
	 * @return the decricaoTlnomArelo
	 */
	public String getDecricaoTlnomArelo() {
		return decricaoTlnomArelo;
	}

	/**
	 * @param decricaoTlnomArelo the decricaoTlnomArelo to set
	 */
	public void setDecricaoTlnomArelo(String decricaoTlnomArelo) {
		this.decricaoTlnomArelo = decricaoTlnomArelo;
	}

	/**
	 * @return the numeroSeqnMeng
	 */
	public Long getNumeroSeqnMeng() {
		return numeroSeqnMeng;
	}

	/**
	 * @param numeroSeqnMeng the numeroSeqnMeng to set
	 */
	public void setNumeroSeqnMeng(Long numeroSeqnMeng) {
		this.numeroSeqnMeng = numeroSeqnMeng;
	}

	/**
	 * @return the numeroSeqnPtqt
	 */
	public Long getNumeroSeqnPtqt() {
		return numeroSeqnPtqt;
	}

	/**
	 * @param numeroSeqnPtqt the numeroSeqnPtqt to set
	 */
	public void setNumeroSeqnPtqt(Long numeroSeqnPtqt) {
		this.numeroSeqnPtqt = numeroSeqnPtqt;
	}

	/**
	 * @return the numeroSeqnSmi
	 */
	public Long getNumeroSeqnSmi() {
		return numeroSeqnSmi;
	}

	/**
	 * @param numeroSeqnSmi the numeroSeqnSmi to set
	 */
	public void setNumeroSeqnSmi(Long numeroSeqnSmi) {
		this.numeroSeqnSmi = numeroSeqnSmi;
	}

	/**
	 * @return the codigoAreloNova
	 */
	public String getCodigoAreloNova() {
		return codigoAreloNova;
	}

	/**
	 * @param codigoAreloNova the codigoAreloNova to set
	 */
	public void setCodigoAreloNova(String codigoAreloNova) {
		this.codigoAreloNova = codigoAreloNova;
	}

	/**
	 * @return the descricaoSisex
	 */
	public String getDescricaoSisex() {
		return descricaoSisex;
	}

	/**
	 * @param descricaoSisex the descricaoSisex to set
	 */
	public void setDescricaoSisex(String descricaoSisex) {
		this.descricaoSisex = descricaoSisex;
	}

	/**
	 * @return the numeroMatriculaUsuario
	 */
	public String getNumeroMatriculaUsuario() {
		return numeroMatriculaUsuario;
	}

	/**
	 * @param numeroMatriculaUsuario the numeroMatriculaUsuario to set
	 */
	public void setNumeroMatriculaUsuario(String numeroMatriculaUsuario) {
		this.numeroMatriculaUsuario = numeroMatriculaUsuario;
	}

	/**
	 * @return the siglaGern
	 */
	public String getSiglaGern() {
		return siglaGern;
	}

	/**
	 * @param siglaGern the siglaGern to set
	 */
	public void setSiglaGern(String siglaGern) {
		this.siglaGern = siglaGern;
	}

	/**
	 * @return the idSitc
	 */
	public String getIdSitc() {
		return idSitc;
	}

	/**
	 * @param idSitc the idSitc to set
	 */
	public void setIdSitc(String idSitc) {
		this.idSitc = idSitc;
	}

	/**
	 * @return the valorRemnHoraMedi
	 */
	public Long getValorRemnHoraMedi() {
		return valorRemnHoraMedi;
	}

	/**
	 * @param valorRemnHoraMedi the valorRemnHoraMedi to set
	 */
	public void setValorRemnHoraMedi(Long valorRemnHoraMedi) {
		this.valorRemnHoraMedi = valorRemnHoraMedi;
	}

	/**
	 * @return the siglaDirt
	 */
	public String getSiglaDirt() {
		return siglaDirt;
	}

	/**
	 * @param siglaDirt the siglaDirt to set
	 */
	public void setSiglaDirt(String siglaDirt) {
		this.siglaDirt = siglaDirt;
	}

	/**
	 * @return the siglaSupr
	 */
	public String getSiglaSupr() {
		return siglaSupr;
	}

	/**
	 * @param siglaSupr the siglaSupr to set
	 */
	public void setSiglaSupr(String siglaSupr) {
		this.siglaSupr = siglaSupr;
	}

	/**
	 * @return the siglaDept
	 */
	public String getSiglaDept() {
		return siglaDept;
	}

	/**
	 * @param siglaDept the siglaDept to set
	 */
	public void setSiglaDept(String siglaDept) {
		this.siglaDept = siglaDept;
	}

	/**
	 * @return the siglaDivs
	 */
	public String getSiglaDivs() {
		return siglaDivs;
	}

	/**
	 * @param siglaDivs the siglaDivs to set
	 */
	public void setSiglaDivs(String siglaDivs) {
		this.siglaDivs = siglaDivs;
	}

	/**
	 * @return the idSap
	 */
	public String getIdSap() {
		return idSap;
	}

	/**
	 * @param idSap the idSap to set
	 */
	public void setIdSap(String idSap) {
		this.idSap = idSap;
	}
	
}
