package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa um perfil que o usuário pode assumir.
 * @author isfigueiredo
 *
 */
@Entity
@Table(name = "TB_PERFIL", schema = "PROJUR")
@SequenceGenerator(name = "SQ_PERFIL", sequenceName = "SQ_PERFIL", allocationSize = 1)
public class Perfil implements Serializable{

	private static final long serialVersionUID = 5347910116285791742L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PERFIL")
    @Column(name = "CD_PERFIL")
	private Long id;
	@Column(name = "DS_PERFIL")
	private String descPerfil;
	//TODO tipo perfil pode virar um enum. Temos que ver quais os possiveis valores
	@Column(name = "TP_PERFIL")
	private String tipoPerfil;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescPerfil() {
		return descPerfil;
	}
	public void setDescPerfil(String descPerfil) {
		this.descPerfil = descPerfil;
	}
	public String getTipoPerfil() {
		return tipoPerfil;
	}
	public void setTipoPerfil(String tipoPerfil) {
		this.tipoPerfil = tipoPerfil;
	}

}
