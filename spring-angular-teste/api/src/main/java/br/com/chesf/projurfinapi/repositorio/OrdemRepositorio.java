package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.Ordem;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer uma camada de persistencia ao recurso Ordem.
 *
 */
@Repository
public interface OrdemRepositorio extends JpaRepository<Ordem, Long>, QueryByExampleExecutor<Ordem>{
	
	
	public Boolean existsByCodigo(Long codigo);
	
	
	public Boolean existsByCodigoAndIdNotIn(Long codigo, Long ... ids);
}
