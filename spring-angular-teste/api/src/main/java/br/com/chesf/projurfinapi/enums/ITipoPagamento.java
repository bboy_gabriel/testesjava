package br.com.chesf.projurfinapi.enums;

public interface ITipoPagamento {
	
	ITipoPagamento getByID(Integer codigo);

}
