package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.ProcessoServico;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer Endpoints referentes ao recurso Processo.
 *
 */
@RestController
@RequestMapping(value = "/api/processos")
@Consumes("application/json")
@Produces("application/json")
public class ProcessoControlador implements Serializable{

	private static final long serialVersionUID = -1727000317291523593L;

	@Autowired
	private ProcessoServico processoServico;
	
	/**
	 * 
	 * @param numeroProcesso
	 * @return ResponseEntity
	 * 
	 * Endpoint responsavel por consultar o recurso Processo pelo numero do processo.
	 * 
	 */
	@PostMapping
	public ResponseEntity consultarPorNumero(@RequestBody @NotNull @Valid NumeroProcessoVO numeroProcesso) {
			return new ResponseEntity(processoServico.consultarPorNumero(numeroProcesso), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Endpoint responsavel por validar o processo para a inclusão de provisão.
	 * 
	 * @param numeroProcessoVO
	 * @return ResponseEntity
	 * 
	 */
	@PostMapping("/registroProvisao/valida")
	public ResponseEntity validarProcessoRegistroProvisao(@RequestBody @NotNull @Valid NumeroProcessoVO numeroProcessoVO) {
		return new ResponseEntity(processoServico.validarProcessoRegistroProvisao(numeroProcessoVO), HttpStatus.OK);
	}
	
	
}
