package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.Configuracao;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ConfiguracaoRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

@Service
public class ConfiguracaoServico implements Serializable {

	private static final long serialVersionUID = -4398873440108687378L;

	@Autowired
	private ConfiguracaoRepositorio configuracaoRepositorio;

	private final String USER_NAME_SAP = "client.sap.user.name";
	private final String USER_PASSWORD_SAP = "client.sap.user.password";
	private final String NOME_LINK_GERA_PAGAMENTO = "link.servico.gera.pagamento";
	private final String NOME_LINK_CONFIRMA_PAGAMENTO = "link.servico.confirma.pagamento";
	private final String NOME_LINK_OBTER_DOMICILIO = "link.servico.obter.domicilio";
	private final String NOME_LINK_GERA_ADIANTAMENTO = "link.servico.gera.adiantamento";	
	private final String NOME_LINK_CONFIRMA_ADIANTAMENTO = "link.servico.confirma.adiantamento";
	private final String TEMPO_EXECUCAO_CONFIRMA_PAGAMENTO = "tempo.execucao.confirma.pagamento";
	private final String TEMPO_EXECUCAO_CONFIRMA_ADIANTAMENTO = "tempo.execucao.confirma.adiantamento";
	private final String TEMPO_EXPIRACAO = "tempo.expiracao";
	
	//Ler prpriedades de email da tabela de configuração
	private final String EMAIL_REMETENTE_JURIDICO_CHESF = "email.remetente.juridico.chesf";	
	private final String EMAIL_PAGAMENTO_SAP_ASSUNTO = "chesf.email.paga.sap.assunto";
	private final String EMAIL_PAGAMENTO_SAP_CORPO = "chesf.email.paga.sap.corpo";
	private final String EMAIL_CHESF_JURIDICO = "chesf.email.destinatario";
	private final String EMAIL_HOST = "chesf.email.host";
	private final String EMAIL_SERVER_USERNAME = "chesf.email.server.username";
	private final String EMAIL_SERVER_PASSWORD = "chesf.email.server.password";
	private final String EMAIL_CHESF_DOMINEO = "chesf.email.domineo";
	private final String EMAIL_CHESF_PORTA = "chesf.email.porta";
	
	
	private Configuracao linkGeraPagamento;
	private Configuracao linkConfirmaPagamento;
	private Configuracao linkObterDomicilio;	
	private Configuracao linkGeraAdiantamento;
	private Configuracao linkConfirmaAdiantamento;
	private Configuracao tempoExcucaoConfirmaPagamento;
	private Configuracao tempoExcucaoConfirmaAdiantamento;
	private Configuracao tempoExpiracao;
	 
	
	Logger logger = LoggerFactory.getLogger(ConfiguracaoRepositorio.class);
	
	
	@PostConstruct
	void init(){
		linkGeraPagamento = configuracaoRepositorio.findByCodPropriedade(NOME_LINK_GERA_PAGAMENTO);
		linkConfirmaPagamento = configuracaoRepositorio.findByCodPropriedade(NOME_LINK_CONFIRMA_PAGAMENTO);
		linkObterDomicilio = configuracaoRepositorio.findByCodPropriedade(NOME_LINK_OBTER_DOMICILIO);
		linkGeraAdiantamento = configuracaoRepositorio.findByCodPropriedade(NOME_LINK_GERA_ADIANTAMENTO);
		linkConfirmaAdiantamento = configuracaoRepositorio.findByCodPropriedade(NOME_LINK_CONFIRMA_ADIANTAMENTO);
		tempoExcucaoConfirmaPagamento = configuracaoRepositorio.findByCodPropriedade(TEMPO_EXECUCAO_CONFIRMA_PAGAMENTO);
		tempoExcucaoConfirmaAdiantamento = configuracaoRepositorio.findByCodPropriedade(TEMPO_EXECUCAO_CONFIRMA_ADIANTAMENTO);
		tempoExpiracao = configuracaoRepositorio.findByCodPropriedade(TEMPO_EXPIRACAO);
	}

	public Configuracao consultarConfiguracao(String codConfiguracao) {
		return configuracaoRepositorio.findByCodPropriedade(codConfiguracao);
	}

	public String consultarUserNameSAP() {
		Configuracao configuracao = this.consultarConfiguracao(USER_NAME_SAP);
		if (Validador.isNull(configuracao) || Validador.isNull(configuracao.getCodPropriedade())) {
			throw new NegocioException(MensagemEnum.USUARIO_SAP_NA0_ENCONTRADO);
		}
		return configuracao.getValorPropriedade();
	}

	public String consultarPasswordSAP() {		
		Configuracao configuracao = this.consultarConfiguracao(USER_PASSWORD_SAP);
		if (Validador.isNull(configuracao) || Validador.isNull(configuracao.getCodPropriedade())) {
			throw new NegocioException(MensagemEnum.SENHA_SAP_NA0_ENCONTRADO);
		}
		return configuracao.getValorPropriedade();
	}
	
	public String consultarLinkObterDomicilio(){
		logger.info("Buscando o link do servico SAP Obter Domicilio bancario.");
		validaConfiguracao(linkObterDomicilio);
		logger.info("Link Obter Domicilio bancario: " + linkObterDomicilio.getValorPropriedade());
		return linkObterDomicilio.getValorPropriedade();
	}
	
	public String consultarLinkGeraPagamento(){
		logger.info("Buscando o link do servico SAP Gera Pagamento.");
		validaConfiguracao(linkGeraPagamento);
		logger.info("Link Gera Pagamento: " + linkGeraPagamento.getValorPropriedade());
		return linkGeraPagamento.getValorPropriedade();
	}
	
	public String consultarLinkConfirmarPagamento(){
		logger.info("Buscando o link do servico SAP Confirma Pagamento.");
		validaConfiguracao(linkConfirmaPagamento);
		logger.info("Link Confirma Pagamento: " + linkConfirmaPagamento.getValorPropriedade());
		return linkConfirmaPagamento.getValorPropriedade();
	}
	
	public String consultarLinkGeraAdiantamento() {
		logger.info("Buscando o link do servico SAP Gera Adiantamento.");
		validaConfiguracao(linkGeraAdiantamento);
		logger.info("Link Gera Adiantamento: " + linkGeraAdiantamento.getValorPropriedade());
		return linkGeraAdiantamento.getValorPropriedade();
	}
	
	public String consultarLinkConfirmaAdiantamento() {
		logger.info("Buscando o link do servico SAP confirma adiantamento.");
		validaConfiguracao(linkConfirmaAdiantamento);
		logger.info("Link confirma adiantamento: " + linkConfirmaAdiantamento.getValorPropriedade());
		return linkConfirmaAdiantamento.getValorPropriedade();
	}
	
	public String consultarTempoExecucaoConfirmaPagamento() {
		logger.info("Buscando o tempo de execução para reenvio automático para serviço CONFIRMA-PAGAMENTO.");
		validaConfiguracao(tempoExcucaoConfirmaPagamento);
		logger.info("Tempo de execução em milisegundos: " + tempoExcucaoConfirmaPagamento.getValorPropriedade());
		return tempoExcucaoConfirmaPagamento.getValorPropriedade();
	}
	
	public String consultarTempoExecucaoConfirmaAdiantamento() {
		logger.info("Buscando o tempo de execução para reenvio automático para serviço CONFIRMA-ADIANTAMENTO.");
		validaConfiguracao(tempoExcucaoConfirmaAdiantamento);
		logger.info("Tempo de execução em milisegundos: " + tempoExcucaoConfirmaAdiantamento.getValorPropriedade());
		return tempoExcucaoConfirmaAdiantamento.getValorPropriedade();
	}
	
	public String consultarTempoExpiracao() {
		logger.info("Buscando o tempo de expiração da sessão do site.");
		validaConfiguracao(tempoExpiracao);
		logger.info("Tempo de execução em minutos: " + tempoExpiracao.getValorPropriedade());
		return tempoExpiracao.getValorPropriedade();
	}
	
	private void validaConfiguracao(Configuracao config){
		if(Validador.isNull(config)){
			logger.error("Configuracao nula ou vazia.");
			throw new NegocioException(MensagemEnum.ERRO_CONFIGURACAO_NAO_ENCONTRADA);
		}
		if(Validador.isNull(config.getValorPropriedade())){
			logger.error("Valor da Configuracao nula ou vazia.");
			throw new NegocioException(MensagemEnum.ERRO_CONFIGURACAO_NAO_ENCONTRADA);
		}
	}
	
	public Configuracao getDestinatarioChesf() {
		return configuracaoRepositorio.findByCodPropriedade(EMAIL_REMETENTE_JURIDICO_CHESF);
	}
	
}
