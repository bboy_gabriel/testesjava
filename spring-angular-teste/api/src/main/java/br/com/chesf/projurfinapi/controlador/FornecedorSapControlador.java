package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.FornecedorSapVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.FornecedorSapServico;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer Endpoints referentes ao recurso FornecedorSap.
 *
 */
@RestController
@RequestMapping(value = "/api/fornecedoresSap")
@Consumes("application/json")
@Produces("application/json")
public class FornecedorSapControlador implements Serializable{

	private static final long serialVersionUID = -8910902379741425604L;
	
	@Autowired
	private FornecedorSapServico fornecedorSapServico;
	
	/**
	 * 
	 * @return ResponseEntity
	 * 
	 * Endpoint responsavel por consultar uma lista de recursos FornecedorSap.
	 * 
	 */
	@GetMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(fornecedorSapServico.consultarOrdenado(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<FornecedorSapVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<FornecedorSapVO>(fornecedorSapServico.consultarPorIdVO(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping
	public ResponseEntity<FornecedorSapVO> cadastrar(@RequestBody FornecedorSapVO ordemVO) {
		try {
			return new ResponseEntity <FornecedorSapVO>(fornecedorSapServico.cadastrar(ordemVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<FornecedorSapVO> atualizar(@RequestBody FornecedorSapVO ordemVO) {
		try {
			return new ResponseEntity <FornecedorSapVO>(fornecedorSapServico.atualizar(ordemVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/*
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<FornecedorSapVO> remover(@PathVariable Long id) {
		try {
			fornecedorSapServico.remover(id);
			return new ResponseEntity<FornecedorSapVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	} */

}
