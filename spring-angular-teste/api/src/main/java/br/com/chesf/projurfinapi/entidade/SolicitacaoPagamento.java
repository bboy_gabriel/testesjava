package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.chesf.projurfinapi.enums.FormaPagamentoEnum;
import br.com.chesf.projurfinapi.enums.StatusPagamentoEnum;
import br.com.chesf.projurfinapi.enums.TipoDepositoJudicialEnum;

@Entity
@Table(name = "TB_SOLICT_PAGM", schema="PROJUR")
@SequenceGenerator(name = "SQ_SOLICT_PAGM", sequenceName = "SQ_SOLICT_PAGM", allocationSize = 1)
public class SolicitacaoPagamento implements Serializable{

	private static final long serialVersionUID = 2898423179597403480L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_SOLICT_PAGM")
    @Column(name = "ID_SOLICT_PAGM")
    private Long id;
	
	@Column(name = "VL_SOLICT_PAGM", length = 9, nullable = false)
	private BigDecimal valor;
	
	@OneToOne(targetEntity = TipoDeDespesa.class)
	@JoinColumn(name = "ID_TIPO_DESP", referencedColumnName = "ID_TIPO_DESP", nullable = false)
	private TipoDeDespesa tipoDeDespesa;
	
	@Column(name = "TP_DEPOS_JUDC", length = 1, nullable = true)
	private Long codigoDepositoJudicial;
	
	@Transient
    private TipoDepositoJudicialEnum tipoDepositoJudicialEnum;
	
	@OneToOne(targetEntity = FornecedorSap.class)
	@JoinColumn(name = "ID_FORN_SAP", referencedColumnName = "ID_FORN_SAP", nullable = false)
	private FornecedorSap fornecedorSap;
	
	@Column(name = "DT_EMISSAO", nullable = false)
	private LocalDate dataEmissao;
	
	@Column(name = "DT_VCTO", nullable = false)
	private LocalDate dataVencimento;	
	
	@OneToOne(targetEntity = OrgaoGestor.class)
	@JoinColumn(name = "ID_ORGAO_GESTOR", referencedColumnName = "ID_ORGAO_GESTOR", nullable = false)
	private OrgaoGestor orgaoGestor;
	
	@Column(name = "NR_PROC", length = 30, nullable = false)
	private String processo;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "ID_SOLICT_PAGM")
	private List<InformacaoContabil> informacoesContabeis;
	
	@Column(name = "TP_FORMA_PAGM", length = 1, nullable = false)
    @Enumerated(EnumType.STRING)
    private FormaPagamentoEnum formaPagamentoEnum;
	
	@Column(name = "ID_DOMICILIO_BANCARIO", length = 4, nullable = false)
	private String domicilioBancario;
	
	@Column(name = "CD_BARRA", length = 48, nullable = true)
	private String codigoBarra;
	
	@Column(name = "IC_SOLICT_PAGM", length = 1, nullable = false)
    private String codigoStatus;
	
	@Column(name = "ID_DOCM_SAP", length= 10, nullable = true)
	private String documentoSap;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "ID_SOLICT_PAGM")
	@OrderBy(value = "DT_INSERCAO DESC")
	private List<MsgErroGeraPagamento> listaMensagensErroGeraPagamento;
	
	@Transient
	private StatusPagamentoEnum statusPagamentoEnum;
	
//	@ManyToOne(targetEntity = Usuario.class, fetch = FetchType.LAZY)
//	@JoinColumn(name = "CD_MATR", referencedColumnName= "CD_MATR", nullable = false)
//	private Usuario usuario;
	@Column(name="CD_MATR", length = 6, nullable = false)
	private String usuario;	
	
	@Column(name = "DT_INSERCAO")
	private LocalDateTime dataInsercao;
	
//	@OneToOne(targetEntity = Arquivo.class)
//	@JoinColumn(name = "ID_ARQUIVO", referencedColumnName = "ID_ARQUIVO", nullable = true)
//	private Arquivo arquivoPagamento;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the tipoDeDespesa
	 */
	public TipoDeDespesa getTipoDeDespesa() {
		return tipoDeDespesa;
	}

	/**
	 * @param tipoDeDespesa the tipoDeDespesa to set
	 */
	public void setTipoDeDespesa(TipoDeDespesa tipoDeDespesa) {
		this.tipoDeDespesa = tipoDeDespesa;
	}

	/**
	 * @return the tipoDepositoJudicialEnum
	 */
	public TipoDepositoJudicialEnum getTipoDepositoJudicialEnum() {
		return tipoDepositoJudicialEnum;
	}

	/**
	 * @param tipoDepositoJudicialEnum the tipoDepositoJudicialEnum to set
	 */
	public void setTipoDepositoJudicialEnum(TipoDepositoJudicialEnum tipoDepositoJudicialEnum) {
		this.tipoDepositoJudicialEnum = tipoDepositoJudicialEnum;
	}

	/**
	 * @return the fornecedorSap
	 */
	public FornecedorSap getFornecedorSap() {
		return fornecedorSap;
	}

	/**
	 * @param fornecedorSap the fornecedorSap to set
	 */
	public void setFornecedorSap(FornecedorSap fornecedorSap) {
		this.fornecedorSap = fornecedorSap;
	}

	/**
	 * @return the dataEmissao
	 */
	public LocalDate getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * @param dataEmissao the dataEmissao to set
	 */
	public void setDataEmissao(LocalDate dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the dataVencimento
	 */
	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	/**
	 * @param dataVencimento the dataVencimento to set
	 */
	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the orgaoGestor
	 */
	public OrgaoGestor getOrgaoGestor() {
		return orgaoGestor;
	}

	/**
	 * @param orgaoGestor the orgaoGestor to set
	 */
	public void setOrgaoGestor(OrgaoGestor orgaoGestor) {
		this.orgaoGestor = orgaoGestor;
	}

	/**
	 * @return the processo
	 */
	public String getProcesso() {
		return processo;
	}

	/**
	 * @param processo the processo to set
	 */
	public void setProcesso(String processo) {
		this.processo = processo;
	}

	/**
	 * @return the informacoesContabeis
	 */
	public List<InformacaoContabil> getInformacoesContabeis() {
		return informacoesContabeis;
	}

	/**
	 * @param informacoesContabeis the informacoesContabeis to set
	 */
	public void setInformacoesContabeis(List<InformacaoContabil> informacoesContabeis) {
		this.informacoesContabeis = informacoesContabeis;
	}

	/**
	 * @return the formaPagamentoEnum
	 */
	public FormaPagamentoEnum getFormaPagamentoEnum() {
		return formaPagamentoEnum;
	}

	/**
	 * @param formaPagamentoEnum the formaPagamentoEnum to set
	 */
	public void setFormaPagamentoEnum(FormaPagamentoEnum formaPagamentoEnum) {
		this.formaPagamentoEnum = formaPagamentoEnum;
	}

	/**
	 * @return the domicilioBancario
	 */
	public String getDomicilioBancario() {
		return domicilioBancario;
	}

	/**
	 * @param domicilioBancario the domicilioBancario to set
	 */
	public void setDomicilioBancario(String domicilioBancario) {
		this.domicilioBancario = domicilioBancario;
	}

	/**
	 * @return the codigoBarra
	 */
	public String getCodigoBarra() {
		return codigoBarra;
	}

	/**
	 * @param codigoBarra the codigoBarra to set
	 */
	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	/**
	 * @return the codigoStatus
	 */
	public String getCodigoStatus() {
		return codigoStatus;
	}

	/**
	 * @param codigoStatus the codigoStatus to set
	 */
	public void setCodigoStatus(String codigoStatus) {
		this.codigoStatus = codigoStatus;
	}

	/**
	 * @return the documentoSap
	 */
	public String getDocumentoSap() {
		return documentoSap;
	}

	/**
	 * @param documentoSap the documentoSap to set
	 */
	public void setDocumentoSap(String documentoSap) {
		this.documentoSap = documentoSap;
	}

	/**
	 * @return the listaMensagensErroGeraPagamento
	 */
	public List<MsgErroGeraPagamento> getListaMensagensErroGeraPagamento() {
		return listaMensagensErroGeraPagamento;
	}

	/**
	 * @param listaMensagensErroGeraPagamento the listaMensagensErroGeraPagamento to set
	 */
	public void setListaMensagensErroGeraPagamento(List<MsgErroGeraPagamento> listaMensagensErroGeraPagamento) {
		this.listaMensagensErroGeraPagamento = listaMensagensErroGeraPagamento;
	}

	/**
	 * @return the statusPagamentoEnum
	 */
	public StatusPagamentoEnum getStatusPagamentoEnum() {
		return statusPagamentoEnum;
	}

	/**
	 * @param statusPagamentoEnum the statusPagamentoEnum to set
	 */
	public void setStatusPagamentoEnum(StatusPagamentoEnum statusPagamentoEnum) {
		this.statusPagamentoEnum = statusPagamentoEnum;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the dataInsercao
	 */
	public LocalDateTime getDataInsercao() {
		return dataInsercao;
	}

	/**
	 * @param dataInsercao the dataInsercao to set
	 */
	public void setDataInsercao(LocalDateTime dataInsercao) {
		this.dataInsercao = dataInsercao;
	}

	/**
	 * @return the codigoDepositoJudicial
	 */
	public Long getCodigoDepositoJudicial() {
		return codigoDepositoJudicial;
	}

	/**
	 * @param codigoDepositoJudicial the codigoDepositoJudicial to set
	 */
	public void setCodigoDepositoJudicial(Long codigoDepositoJudicial) {
		this.codigoDepositoJudicial = codigoDepositoJudicial;
	}
	
//	
//	/**
//	 * @return the arquivoPagamento
//	 */
//	public Arquivo getArquivoPagamento() {
//		return arquivoPagamento;
//	}
//
//	/**
//	 * @param arquivoPagamento
//	 */
//	public void setArquivoPagamento(Arquivo arquivoPagamento) {
//		this.arquivoPagamento = arquivoPagamento;
//	}
	 
}

