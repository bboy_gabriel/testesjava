package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.ContaRazaoProvisaoVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.ContaRazaoProvisaoServico;

/**
 * 
 * Classe responsavel por expor endpoints para consumo e manipulação do recurso ContaRazaoProvisão.
 * 
 * @author jvneto1
 *
 */
@RestController
@RequestMapping("/api/contaRazaoProvisao")
@Consumes("application/json")
@Produces("application/json")
public class ContaRazaoProvisaoControlador implements Serializable{

	private static final long serialVersionUID = 5157846961737156001L;
	
	@Autowired
	private ContaRazaoProvisaoServico contaRazaoProvisaoServico;

	/**
	 * 
	 * Endpoint responsavel por listar todas as razões provisões cadastradas no sistema.
	 * 
	 * @return
	 * 
	 */
	@GetMapping
	public ResponseEntity consultarContaRazaoProvisao() {
		try {
			return new ResponseEntity(contaRazaoProvisaoServico.consultarContaRazaoProvisao(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * Endpoint resonsável por consultar uma lista de recursos ContaRazaoProvisao pelo codigo do tipo do processo.
	 * 
	 * @param codigo
	 * @return ResponseEntity
	 * 
	 */
	@GetMapping("/tipoProcesso/{codigo}")
	public ResponseEntity consultarLContaRazaoPorTipoProcesso(@PathVariable @Valid @NotEmpty String codigo) {
		try {
			return new ResponseEntity(contaRazaoProvisaoServico.consultarLContaRazaoPorTipoProcesso(codigo), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
//	/**
//	 * 
//	 * Endpoint responsavel por inserir um recurso ContaRazaoProvisão
//	 * 
//	 * @return
//	 * 
//	 */
//	@PostMapping
//	public ResponseEntity cadastrarContaRazaoProvisao(@Valid @RequestBody ContaRazaoProvisaoVO contaVO) {
//		return new ResponseEntity(contaRazaoProvisaoServico.cadastrarContaRazaoProvisao(contaVO), HttpStatus.OK);
//	}
	
}
