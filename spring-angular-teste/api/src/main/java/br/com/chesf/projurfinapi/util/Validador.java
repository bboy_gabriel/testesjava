package br.com.chesf.projurfinapi.util;

import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;

public class Validador {
	
	public static void validarId(Long id) {
		if(id == null) throw new NegocioException(MensagemEnum.ID_NULO_OU_INVALIDO);
	}
	
    /**
     * Verifica se um objeto é nulo. Caso o objeto seja nulo é retornado TRUE
     * caso contrário é retornado FALSE
     *
     * @param obj
     * @return boolean
     */
    public static Boolean isNull(Object obj) {
        return obj == null;
    }
    
    /**
     * Verifica se um objeto é nulo. Caso o objeto seja nulo é retornado TRUE
     * caso contrário é retornado FALSE
     *
     * @param obj
     * @return boolean
     */
    public static Boolean isNotNull(Object obj) {
        return obj != null;
    }
    
}
