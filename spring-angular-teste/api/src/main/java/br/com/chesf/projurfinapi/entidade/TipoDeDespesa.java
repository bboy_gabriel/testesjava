package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.chesf.projurfinapi.enums.IndicadorPagamentoEnum;
import br.com.chesf.projurfinapi.enums.IndicadorSimNaoEnum;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o recurso TipoDeDespesa.
 *
 */
@Entity
@Table(name = "TB_TIPO_DESP", schema="PROJUR")
@SequenceGenerator(name = "SQ_TIPO_DESP", sequenceName = "SQ_TIPO_DESP", allocationSize = 1)
public class TipoDeDespesa implements Serializable{

	private static final long serialVersionUID = -8484065459977249992L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TIPO_DESP")
    @Column(name = "ID_TIPO_DESP")
    private Long id;
	
	@Column(name = "CD_TIPO_DESP", length = 4, nullable = false, unique = true)
	private String codigo;
	
	@Column(name = "DS_TIPO_DESP", length = 50, nullable = false)
	private String descricao;
	
	@Column(name = "TP_PAGM", length = 1, nullable = false)
    @Enumerated(EnumType.STRING)
    private IndicadorPagamentoEnum indicadorPagamentoEnum;

	@Column(name = "IND_PROC", length = 1, nullable = false)
	@Enumerated(EnumType.STRING)
	private IndicadorSimNaoEnum indicadorProcesso;

	@Column(name = "IND_INFO_CONT", length = 1, nullable = false)
	@Enumerated(EnumType.STRING)
	private IndicadorSimNaoEnum indicadorInfoContabeis;
	
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the indicadorPagamentoEnum
	 */
	public IndicadorPagamentoEnum getIndicadorPagamentoEnum() {
		return indicadorPagamentoEnum;
	}

	/**
	 * @param indicadorPagamentoEnum the indicadorPagamentoEnum to set
	 */
	public void setIndicadorPagamentoEnum(IndicadorPagamentoEnum indicadorPagamentoEnum) {
		this.indicadorPagamentoEnum = indicadorPagamentoEnum;
	}

	public IndicadorSimNaoEnum getIndicadorProcesso() {
		return indicadorProcesso;
	}

	public void setIndicadorProcesso(IndicadorSimNaoEnum indicadorProesso) {
		this.indicadorProcesso = indicadorProesso;
	}

	public IndicadorSimNaoEnum getIndicadorInfoContabeis() {
		return indicadorInfoContabeis;
	}

	public void setIndicadorInfoContabeis(IndicadorSimNaoEnum indicadorInfoContabeis) {
		this.indicadorInfoContabeis = indicadorInfoContabeis;
	}
	
}
