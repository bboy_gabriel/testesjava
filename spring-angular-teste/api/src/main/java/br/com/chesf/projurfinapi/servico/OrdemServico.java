package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.InformacaoContabil;
import br.com.chesf.projurfinapi.entidade.Ordem;
import br.com.chesf.projurfinapi.entidade.vo.OrdemVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.OrdemRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer os serviços referentes ao recurso Ordem.
 *
 */
@Service
public class OrdemServico implements Serializable{

	private static final long serialVersionUID = -4268464748362017119L;

	@Autowired
	private OrdemRepositorio ordemRepositorio;
	
	@Autowired
	private InformacaoContabilServico informacaoContabilServico;
	
	Logger logger = LoggerFactory.getLogger(OrdemServico.class);
	
	private static final String ORDEM = "Ordem";
	private static final String CODIGO = "Código ";

	
	/**
	 * 
	 * @return List<OrdemVO>
	 * 
	 * Metodo responsavel por consultar uma lista do recurso Ordem, validar a lista e montar a sua respectiva listaVO.
	 * 
	 */
	public List<OrdemVO> consultar(){
		try {
			List<Ordem> listaBase = ordemRepositorio.findAll();
			validarListaOrdemBase(listaBase);
			return montarListaOrdemVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_ORDEM);
		}
	}
	
	/**
	 * 
	 * @return List<OrdemVO>
	 * 
	 * Metodo responsavel por consultar uma lista do recurso Ordem ordernada pelo codigo, validar a lista e montar a sua respectiva listaVO.
	 * 
	 */
	public List<OrdemVO> consultarOrdenado(){
		try {
			List<Ordem> listaBase = ordemRepositorio.findAll(new Sort(Sort.Direction.ASC, "codigo"));
			validarListaOrdemBase(listaBase);
			return montarListaOrdemVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LISTA_ORDEM);
		}
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código
	 * @param ordemVO
	 */
	private void verificaExistenciaElementoPorCodigo(OrdemVO ordemVO) {
		if(ordemRepositorio.existsByCodigo(ordemVO.getCodigo())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, ordemVO.getCodigo().toString(), ORDEM);
		}
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código desconsiderando o próprio registro.
	 * @param ordemVO
	 */
	private void verificaExistenciaElementoPorCodigoIdNotIn(OrdemVO ordemVO) {
		if(ordemRepositorio.existsByCodigoAndIdNotIn(ordemVO.getCodigo(), ordemVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, ordemVO.getCodigo().toString(), ORDEM);
		}
	}
	
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo responsavel por validar a lista consultada do recurso Ordem.
	 * 
	 */
	private void validarListaOrdemBase(List<Ordem> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_ORDEM_NULA_OU_INVALIDA);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<OrdemVO>
	 * 
	 * Metodo responsavel por montar a lista VO do recurso Ordem, com base na lista consultada.
	 * 
	 */
	private List<OrdemVO> montarListaOrdemVO(List<Ordem> listaBase){
		List<OrdemVO> listaVO = new ArrayList<>();
		for (Ordem itemBase : listaBase) {
			OrdemVO vo = new OrdemVO();
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			listaVO.add(vo);
		}
		return listaVO;
	}
	
	public OrdemVO consultarPorId(Long id) {
		try {
			Validador.validarId(id);
			Ordem itemBase = ordemRepositorio.findOne(id);
			validarEntidadeBase(itemBase);
			return montarOrdemVO(itemBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_ORDEM);
		}
	}
	
	public OrdemVO cadastrar(OrdemVO ordemVO) {
		verificaExistenciaElementoPorCodigo(ordemVO);
		try {
			validarVO(ordemVO);
			Ordem ordemBase = ordemRepositorio.save(montarOrdemBase(ordemVO));
			validarEntidadeBase(ordemBase);			
			return montarOrdemVO(ordemBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_ORDEM);
		}
	}
	
	public OrdemVO atualizar(OrdemVO ordemVO) {
		verificaExistenciaElementoPorCodigoIdNotIn(ordemVO);
		try {
			validarVO(ordemVO);
			Ordem ordemBase = ordemRepositorio.save(montarOrdemBase(ordemVO));
			validarEntidadeBase(ordemBase);			
			return montarOrdemVO(ordemBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_ORDEM);
		}
	}
	
	public void remover(Long id) {

		Validador.validarId(id);
		List<InformacaoContabil> lstInfo = null;	
		
		lstInfo = informacaoContabilServico.consultarInformacaoContabilPorOrdem(id); 
		if(lstInfo != null && !lstInfo.isEmpty()) {
			logger.info("Ordem: " + lstInfo.get(0).getElementoPep().getDescricao() + " está sendo usada e não pode ser excluída!");
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO, ORDEM);
		}else {
			ordemRepositorio.delete(id);				
		}
		
	}
	
	
	private Ordem montarOrdemBase(OrdemVO ordemVO) {
		Ordem ordemBase = new Ordem();
		ordemBase.setId(ordemVO.getId());
		ordemBase.setCodigo(ordemVO.getCodigo());
		ordemBase.setDescricao(ordemVO.getDescricao());
		return ordemBase;
	}
	

	private OrdemVO montarOrdemVO(Ordem ordemBase) {
		OrdemVO ordemVO = new OrdemVO();
		ordemVO.setId(ordemBase.getId());
		ordemVO.setCodigo(ordemBase.getCodigo());
		ordemVO.setDescricao(ordemBase.getDescricao());
		return ordemVO;
	}
	
	
	private void validarVO(OrdemVO ordemVO) {
		if(ordemVO == null) throw new NegocioException(MensagemEnum.ORDEM_NULA_OU_INVALIDA);
	}

	
	/**
	 * 
	 * @param entidadeBase
	 * 
	 * Metodo privado responsavel por validar o retorno base no serviço getById
	 * 
	 */
	private void validarEntidadeBase(Ordem entidadeBase) {
		if(entidadeBase == null) throw new NegocioException(MensagemEnum.ORDEM_NULA_OU_INVALIDA);
	}
}
