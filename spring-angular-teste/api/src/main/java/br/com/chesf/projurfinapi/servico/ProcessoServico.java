package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.Processo;
import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.entidade.vo.ProcessoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ProcessoRepositorio;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer serviços referentes ao recurso Processo.
 *
 */
@Service
public class ProcessoServico implements Serializable{

	private static final long serialVersionUID = -5378222207901024664L;

	@Autowired
	private ProcessoRepositorio processoRepositorio;
	
	@Autowired
	private ParteServico parteServico;
	
	@Autowired
	private ProcessoParteServico processoParteServico;
	
	/**
	 * 
	 * @param numeroProcesso
	 * @return ProcessoVO
	 * 
	 * Metodo responsavel por consultar um recurso Processo pelo numero do processo, validar e montar o ProcessoVO.
	 * 
	 */
	public ProcessoVO consultarPorNumero(NumeroProcessoVO numeroProcessoVO) {
		validarNumeroProcesso(numeroProcessoVO);
		Processo processoBase = null;
		try {
			Long digitoVerificador = new Long(numeroProcessoVO.getNumeroProcesso().substring(numeroProcessoVO.getNumeroProcesso().length()-1));
			Long numeroProcessoSIJUS = new Long(numeroProcessoVO.getNumeroProcesso().substring(0,numeroProcessoVO.getNumeroProcesso().length()-1));			
			processoBase =  processoRepositorio.findByNumeroProcessoSijus(numeroProcessoSIJUS, digitoVerificador);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_PROCESSO_POR_NUMERO);
		}
		validarProcessoBase(processoBase);
		validarSituacaoProcesso(processoBase);
		return montarProcessoVO(processoBase);
	}
	
	/**
	 * 
	 * Serviço responsável por validar se o processo está apto para receber uma inclusão provisão.
	 * 
	 * @param numeroProcessoVO
	 * @return ProcessoVO
	 * 
	 */
	public ProcessoVO validarProcessoRegistroProvisao(NumeroProcessoVO numeroProcessoVO) {
		ProcessoVO processoVO = consultarPorNumero(numeroProcessoVO);
		validarProcessoInclusaoDeProvisao(processoVO);
		return processoVO;
	}
	
	/**
	 * 
	 * Metodo responsável por validar se o processo está ou não encerrado.
	 * 
	 * @param processoBase
	 * 
	 */
	private void validarSituacaoProcesso(Processo processoBase) {
		if(processoBase.getIdStatus() == null || processoBase.getIdStatus().isEmpty()) throw new NegocioException(MensagemEnum.PROBLEMA_NA_VERIFICACAO_DA_SITUACAO_PROCESSO);
		if(!processoBase.getIdStatus().equalsIgnoreCase("A")) throw new NegocioException(MensagemEnum.PROCESSO_ENCERRADO);
	}
	
	/**
	 * 
	 * Metodo responsável por validar se o processo é ou não apto para receber uma provisão.
	 * 
	 * @param processoVO
	 * 
	 */
	private void validarProcessoInclusaoDeProvisao(ProcessoVO processoVO) {
		if(processoVO.getCodigoTipoProcesso() == null) throw new NegocioException(MensagemEnum.CODIGO_TIPO_PROCESSO_NULO_OU_INVALIDO);
		if(processoVO.getIdRisco() == null || processoVO.getIdRisco().isEmpty()) throw new NegocioException(MensagemEnum.PROBLEMA_NA_VERIFICACAO_ID_RISCO_PARA_VALIDAR_INCLUSAO_DE_PROVISAO);
		if(!processoVO.getIdRisco().equalsIgnoreCase("1")) throw new NegocioException(MensagemEnum.PROCESSO_NAO_PERMITE_INCLUIR_PROVISAO);
		if(processoVO.getIdRisco().equalsIgnoreCase("1") && !processoVO.getCodigoTipoProcesso().equals(6L)) {
			List<Processo> listaProcessoChesfRe = processoRepositorio.consultarProcessoChesfRe(processoVO.getCodigoProcesso());
			if(!listaProcessoChesfRe.isEmpty()) {
				processoVO.setMensagemOpcional("Processo com provisão opcional");
			}
		}
	}
	
	/**
	 * 
	 * @param numeroProcesso
	 * 
	 * Metodo responsavel por validar o numero do processo.
	 * 
	 */
	private void validarNumeroProcesso(NumeroProcessoVO numeroProcesso) {
		if(numeroProcesso == null || numeroProcesso.getNumeroProcesso() == null 
				|| numeroProcesso.getNumeroProcesso().isEmpty()){
			throw new NegocioException(MensagemEnum.NUMERO_DO_PROCESSO_NULO_OU_INVALIDO);
		}
		if(numeroProcesso.getNumeroProcesso().length() < 2) throw new NegocioException(MensagemEnum.PROCESSO_DIGITO_ABAIXO_DO_MINIMO);
	}
	
	/**
	 * 
	 * @param processoBase
	 * 
	 * Metodo responsavel por validar o recurso Processo consultado.
	 * 
	 */
	private void validarProcessoBase(Processo processoBase) {
		if(processoBase == null || processoBase.getCodigoProcesso() == null 
				|| processoBase.getNumeroProcesso() == null 
				|| processoBase.getNumeroProcesso().isEmpty()) throw new NegocioException(MensagemEnum.PROCESSO_BASE_NULO_OU_INVALIDO); 
	}
	
	/**
	 * 
	 * @param processoBase
	 * @return ProcessoVO
	 * 
	 * Metodo responsavel por montar o ProcessoVO com base no recurso Processo.
	 * 
	 */
	private ProcessoVO montarProcessoVO(Processo processoBase) {
		try {
			ProcessoVO vo = new ProcessoVO();
			vo.setCodigoProcesso(processoBase.getCodigoProcesso());
			vo.setNumeroProcesso(processoBase.getNumeroProcesso());
			vo.setCodigoTipoProcesso(processoBase.getCodigoTipoProcesso());
			vo.setCodigoTipoAcao(processoBase.getCodigoTipoAcao());
			vo.setValorPedido(processoBase.getValorPedido());
			vo.setValorEstimado(processoBase.getValorEstimado());
			vo.setCodigoOrgaoJulgador(processoBase.getCodigoOrgaoJulgador());
			vo.setCodigoMunicipio(processoBase.getCodigoMunicipio());
			vo.setDescricaoVara(processoBase.getDescricaoVara());
			vo.setDescricaoAcao(processoBase.getDescricaoAcao());
			vo.setIdRisco(processoBase.getIdRisco());
			vo.setCodigoMatrizAdvogado(processoBase.getCodigoMatrizAdvogado());
			vo.setIdPrincipal(processoBase.getIdPrincipal());
			vo.setCodigoOrgaoReq(processoBase.getCodigoOrgaoReq());
			vo.setIdStatus(processoBase.getIdStatus());
			vo.setSiglaEstado(processoBase.getSiglaEstado());
			vo.setCodigoEscritorioTerceiro(processoBase.getCodigoEscritorioTerceiro());
			vo.setNumeroProcessoSijus(processoBase.getNumeroProcessoSijus());
			vo.setNumeroDigVerfSijus(processoBase.getNumeroDigVerfSijus());
			if(processoBase.getDataAutuacao() != null) vo.setDataAutuacao(processoBase.getDataAutuacao().toString());
			vo.setIdRelevante(processoBase.getIdRelevante());
			vo.setCodigoEscritorioChesf(processoBase.getCodigoEscritorioChesf());
			vo.setDescricaoOrgaoJulgAdm(processoBase.getDescricaoOrgaoJulgAdm());
			vo.setDataAnoPrcs(processoBase.getDataAnoPrcs());
			vo.setIdComplexidade(processoBase.getIdComplexidade());
			vo.setDescricaoCriterioValorRisco(processoBase.getDescricaoCriterioValorRisco());
			if(processoBase.getDataCliente() != null) vo.setDataCliente(processoBase.getDataCliente().toString());
			vo.setIdSituacaoCliente(processoBase.getIdSituacaoCliente());
			vo.setIdMigracaoSijus(processoBase.getIdMigracaoSijus());
			vo.setIdFiscal(processoBase.getIdFiscal());
			vo.setIdSolEncerramento(processoBase.getIdSolEncerramento());
			if(processoBase.getDataAjuizamento() != null) vo.setDataAjuizamento(processoBase.getDataAjuizamento().toString());
			vo.setValorSentenca(processoBase.getValorSentenca());
			vo.setNomeApelidoParte(processoBase.getNomeApelidoParte());
			vo.setIdLocad(processoBase.getIdLocad());
			vo.setTipEncr(processoBase.getTipEncr());
			vo.setTipoProcesso(TipoProcessoContaRazaoEnum.getTipoProcessoContaRazaoEnumPorID(vo.getCodigoTipoProcesso().intValue()));
			return vo;
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_MONTAR_PROCESSO_VO);
		}
	}
	
}
