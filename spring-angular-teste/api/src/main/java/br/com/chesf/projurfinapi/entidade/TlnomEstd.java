package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TLNOM_ESTD", schema = "TLNOM")
@SequenceGenerator(name = "SQ_TLNOM_ESTD", sequenceName = "SQ_TLNOM_ESTD", allocationSize = 1)
public class TlnomEstd implements Serializable{

	private static final long serialVersionUID = -4382482102083731798L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TLNOM_ESTD")
	@Column(name = "PK_ESTD")
	private Long id;
	
	@Column(name = "SG_ESTD", length = 2)
	private String siglaEstd;
	
	@Column(name = "NM_ESTD", length = 20)
	private String nomeEstd;
	
	@Column(name = "NM_CAPT", length = 20)
	private String nomeCapt;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the siglaEstd
	 */
	public String getSiglaEstd() {
		return siglaEstd;
	}

	/**
	 * @param siglaEstd the siglaEstd to set
	 */
	public void setSiglaEstd(String siglaEstd) {
		this.siglaEstd = siglaEstd;
	}

	/**
	 * @return the nomeEstd
	 */
	public String getNomeEstd() {
		return nomeEstd;
	}

	/**
	 * @param nomeEstd the nomeEstd to set
	 */
	public void setNomeEstd(String nomeEstd) {
		this.nomeEstd = nomeEstd;
	}

	/**
	 * @return the nomeCapt
	 */
	public String getNomeCapt() {
		return nomeCapt;
	}

	/**
	 * @param nomeCapt the nomeCapt to set
	 */
	public void setNomeCapt(String nomeCapt) {
		this.nomeCapt = nomeCapt;
	}
	
}
