package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CONFIG", schema="PROJUR")
public class Configuracao implements Serializable{

	private static final long serialVersionUID = -4347010501551047675L;
	
	@Id
	@Column(name = "CD_PROPRIEDADE", nullable = false, length = 40)
	private String codPropriedade;
	
	@Column(name = "VL_PROPRIEDADE", length = 240)
	private String valorPropriedade;
	
	/**
	 * @return the codPropriedade
	 */
	public String getCodPropriedade() {
		return codPropriedade;
	}
	/**
	 * @param codPropriedade the codPropriedade to set
	 */
	public void setCodPropriedade(String codPropriedade) {
		this.codPropriedade = codPropriedade;
	}
	/**
	 * @return the valorPropriedade
	 */
	public String getValorPropriedade() {
		return valorPropriedade;
	}
	/**
	 * @param valorPropriedade the valorPropriedade to set
	 */
	public void setValorPropriedade(String valorPropriedade) {
		this.valorPropriedade = valorPropriedade;
	}
	
}
