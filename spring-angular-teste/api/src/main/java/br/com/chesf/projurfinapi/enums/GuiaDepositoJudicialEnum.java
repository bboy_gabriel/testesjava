package br.com.chesf.projurfinapi.enums;

/**
 * Enum criado para mapear os tipos de deposítos judiciais de uma guia
 * @author isfigueiredo
 *
 */
public enum GuiaDepositoJudicialEnum {
	RO_DEPOSITO(1, "Depósito Recursal RO"),
	RR_DEPOSITO(2, "Depósito Recursal – RR"),
	AI_DEPOSITO(3, "Depósito Recursal – AI"),
	PREVIO(4, "Depósito Prévio"),
	COMPLEMENTAR(5, "Depósito Complementar"),
	PAGAMENTO(6, "Pagamento"),
	PERICIAIS(7, "Honorários Periciais"),
	ADVOGATICIOS(8, "Honorários Advocatícios");
	
	private Integer id;
	private String desc;
	
	private GuiaDepositoJudicialEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}

}
