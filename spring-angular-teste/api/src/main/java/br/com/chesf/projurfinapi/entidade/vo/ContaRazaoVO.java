package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;

public class ContaRazaoVO implements Serializable{

	private static final long serialVersionUID = -4345699210772167405L;

	private Long id;

	@NotNull(message = "Conta debito nula ou inválida!")
	private Long contaDebito;
	
	@NotNull(message = "Conta credito nula ou inválida!")
	private Long contaCredito;
	
	@NotEmpty(message ="Codigo tipo de processo nula ou inválida!")
	private String codigoTipoProcesso;
	
	private TipoProcessoContaRazaoEnum tipoProcessoProvisaoEnum;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the contaDebito
	 */
	public Long getContaDebito() {
		return contaDebito;
	}

	/**
	 * @param contaDebito the contaDebito to set
	 */
	public void setContaDebito(Long contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * @return the contaCredito
	 */
	public Long getContaCredito() {
		return contaCredito;
	}

	/**
	 * @param contaCredito the contaCredito to set
	 */
	public void setContaCredito(Long contaCredito) {
		this.contaCredito = contaCredito;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public String getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(String codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}

	/**
	 * @return the tipoProcessoProvisaoEnum
	 */
	public TipoProcessoContaRazaoEnum getTipoProcessoProvisaoEnum() {
		return tipoProcessoProvisaoEnum;
	}

	/**
	 * @param tipoProcessoProvisaoEnum the tipoProcessoProvisaoEnum to set
	 */
	public void setTipoProcessoProvisaoEnum(TipoProcessoContaRazaoEnum tipoProcessoProvisaoEnum) {
		this.tipoProcessoProvisaoEnum = tipoProcessoProvisaoEnum;
	}
	
}
