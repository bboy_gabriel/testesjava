package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.OrgaoGestor;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.vo.OrgaoGestorVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.OrgaoGestorRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por fornecer os serviços referentes ao recurso OrgaoGestor
 *
 */
@Service
public class OrgaoGestorServico implements Serializable{

	private static final long serialVersionUID = 6787475213533320728L;

	@Autowired
	private OrgaoGestorRepositorio orgaoGestorRepositorio;
	
	@Autowired
	private SolicitacaoPagamentoServico solicitacaoPagamentoServico;
	
	Logger logger = LoggerFactory.getLogger(OrgaoGestorServico.class);
	
	private static final String ORGAO_GESTOR = "Orgão Gestor";
	private static final String CODIGO = "Código ";

	
	/**
	 * 
	 * @return List<OrgaoGestorVO>
	 * 
	 * Metodo responsavel por consultar em base todos os recursos OrgaoGestor, valida e monta uma lista OrgaogestorVO
	 * 
	 */
	public List<OrgaoGestorVO> consultar(){
		try {
			List<OrgaoGestor> listaBase = orgaoGestorRepositorio.findAll();
			validarListaOrgaoGestorBase(listaBase);
			return montarListaOrgaoGestorVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_ORGAO_GESTOR);
		}
	}
	
	/**
	 * 
	 * @return List<OrgaoGestorVO>
	 * 
	 * Metodo responsavel por consultar em base todos os recursos OrgaoGestor ordenados pelo codigo, valida e monta uma lista OrgaogestorVO
	 * 
	 */
	public List<OrgaoGestorVO> consultarOrdenado(){
		try {
			List<OrgaoGestor> listaBase = orgaoGestorRepositorio.findAll(new Sort(Sort.Direction.ASC, "codigo"));
			validarListaOrgaoGestorBase(listaBase);
			return montarListaOrgaoGestorVO(listaBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_CONSULTAR_LISTA_ORGAO_GESTOR);
		}
	}
	
	/**
	 * 
	 * @param listaBase
	 * 
	 * Metodo privado responsavel por validar uma lista OrgaoGestor
	 * 
	 */
	private void validarListaOrgaoGestorBase(List<OrgaoGestor> listaBase) {
		if(listaBase == null || listaBase.isEmpty()) throw new NegocioException(MensagemEnum.LISTA_BASE_ORGAO_GESTOR_NULA_OU_INVALIDA);
	}
	
	/**
	 * 
	 * @param listaBase
	 * @return List<OrgaoGestorVO>
	 * 
	 * Metodo privado responsavel por montar uma lista OrgaoGestorVO.
	 * 
	 */
	private List<OrgaoGestorVO> montarListaOrgaoGestorVO(List<OrgaoGestor> listaBase){
		List<OrgaoGestorVO> listaVO = new ArrayList<>();
		for (OrgaoGestor itemBase : listaBase) {
			OrgaoGestorVO vo = new OrgaoGestorVO();
			vo.setCodigo(itemBase.getCodigo());
			vo.setDescricao(itemBase.getDescricao());
			vo.setId(itemBase.getId());
			vo.setSituacaoOrgaoGestorEnum(itemBase.getSituacaoOrgaoGestorEnum());
			listaVO.add(vo);
		}
		return listaVO;
	}

	public OrgaoGestor consultarPorId(Long id) {
		try {
		Validador.validarId(id);
		OrgaoGestor itemBase = orgaoGestorRepositorio.findOne(id);
		validarEntidadeBase(itemBase);
		return itemBase;
		} catch(NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_ORGAO_GESTOR);
		}
	}
	
	public OrgaoGestorVO consultarPorIdVO(Long id) {
		try {
		Validador.validarId(id);
		OrgaoGestor itemBase = consultarPorId(id);
		return montarOrgaoGestorVO(itemBase);
		} catch(NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_ORGAO_GESTOR);
		}
	}
	
	
	
	public OrgaoGestorVO cadastrar(OrgaoGestorVO orgaoGestorVO) {
		verificaExistenciaElementoPorCodigo(orgaoGestorVO);
		try {
			validarVO(orgaoGestorVO);
			OrgaoGestor orgaoGestorBase = orgaoGestorRepositorio.save(montarOrgaoGestorBase(orgaoGestorVO));
			validarEntidadeBase(orgaoGestorBase);			
			return montarOrgaoGestorVO(orgaoGestorBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_ORGAO_GESTOR);
		}
	}
	
	public OrgaoGestorVO atualizar(OrgaoGestorVO orgaoGestorVO) {
		verificaExistenciaElementoPorCodigoIdNotIn(orgaoGestorVO);
		try {
			validarVO(orgaoGestorVO);
			OrgaoGestor orgaoGestorBase = orgaoGestorRepositorio.save(montarOrgaoGestorBase(orgaoGestorVO));
			validarEntidadeBase(orgaoGestorBase);			
			return montarOrgaoGestorVO(orgaoGestorBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_ORGAO_GESTOR);
		}
	}
	
	
	public void remover(Long id) {
		Validador.validarId(id);
		List<SolicitacaoPagamento> lstSolic = null;

		OrgaoGestor orgaoGestor = new OrgaoGestor();
		orgaoGestor.setId(id);
		lstSolic = solicitacaoPagamentoServico.consultarSolicitacaoPorOrgaoGestor(orgaoGestor); 
		if(lstSolic != null && !lstSolic.isEmpty()) {
			logger.info("Orgão gestor: " + lstSolic.get(0).getOrgaoGestor().getDescricao() + " está sendo usado e não pode ser excluído!");
			throw new NegocioException(MensagemEnum.ENTIDADE_ATRELADA_SOLICITACAO, ORGAO_GESTOR);
		}else {
			orgaoGestorRepositorio.delete(id);				
		}		
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código
	 * @param orgaoGestorVO
	 */
	private void verificaExistenciaElementoPorCodigo(OrgaoGestorVO orgaoGestorVO) {
		if(orgaoGestorRepositorio.existsByCodigo(orgaoGestorVO.getCodigo())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, orgaoGestorVO.getCodigo().toString(), ORGAO_GESTOR);
		}
	}
	
	/**
	 * Verifica na base de dados se existe um registro com o mesmo código desconsiderando o próprio registro.
	 * @param orgaoGestorVO
	 */
	private void verificaExistenciaElementoPorCodigoIdNotIn(OrgaoGestorVO orgaoGestorVO) {
		if(orgaoGestorRepositorio.existsByCodigoAndIdNotIn(orgaoGestorVO.getCodigo(), orgaoGestorVO.getId())) {
			throw new NegocioException(MensagemEnum.ENTIDADE_CODIGO_REPETIDO, CODIGO, orgaoGestorVO.getCodigo().toString(), ORGAO_GESTOR);
		}
	}
	
	
	private OrgaoGestor montarOrgaoGestorBase(OrgaoGestorVO orgaoGestorVO) {
		OrgaoGestor orgaoGestorBase = new OrgaoGestor();
		orgaoGestorBase.setId(orgaoGestorVO.getId());
		orgaoGestorBase.setCodigo(orgaoGestorVO.getCodigo());
		orgaoGestorBase.setDescricao(orgaoGestorVO.getDescricao());
		orgaoGestorBase.setSituacaoOrgaoGestorEnum(orgaoGestorVO.getSituacaoOrgaoGestorEnum());
		return orgaoGestorBase;
	}
	

	private OrgaoGestorVO montarOrgaoGestorVO(OrgaoGestor orgaoGestorBase) {
		OrgaoGestorVO orgaoGestorVO = new OrgaoGestorVO();
		orgaoGestorVO.setId(orgaoGestorBase.getId());
		orgaoGestorVO.setCodigo(orgaoGestorBase.getCodigo());
		orgaoGestorVO.setDescricao(orgaoGestorBase.getDescricao());
		orgaoGestorVO.setSituacaoOrgaoGestorEnum(orgaoGestorBase.getSituacaoOrgaoGestorEnum());
		return orgaoGestorVO;
	}
	
	
	private void validarVO(OrgaoGestorVO orgaoGestorVO) {
		if(orgaoGestorVO == null) throw new NegocioException(MensagemEnum.ORGAO_GESTOR_NULO_OU_INVALIDO);
	}

	
	/**
	 * 
	 * @param entidadeBase
	 * 
	 * Metodo privado responsavel por validar o retorno base no serviço getById
	 * 
	 */
	private void validarEntidadeBase(OrgaoGestor entidadeBase) {
		if(entidadeBase == null) throw new NegocioException(MensagemEnum.ORGAO_GESTOR_NULO_OU_INVALIDO);
	}
	
	
}
