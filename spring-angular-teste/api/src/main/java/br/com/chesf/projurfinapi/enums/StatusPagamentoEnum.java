package br.com.chesf.projurfinapi.enums;

import java.util.Arrays;
import java.util.Optional;

public enum StatusPagamentoEnum {
	AGUARDANDO_APROVACAO(1, "Aguardando Aprovação"),
	APROVADO(2, "Aprovado"),
	COMPENSADO(3, "Compensado"),
	ESTORNADO(4, "Estornado"),
	PENDENTE(9, "Pendente"),
	PRESTACAO_DE_CONTAS_EFETUADA(5,"prestação de contas efetuada"),
	CONCILIADO(6,"conciliado");
	
	
	private Integer id;
	private String desc;
	
	private StatusPagamentoEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static StatusPagamentoEnum getStatusPagamentoEnumPorID(Long  id){
		StatusPagamentoEnum retorno = null;
		switch (id.intValue()) {
		case 1:
			retorno = StatusPagamentoEnum.AGUARDANDO_APROVACAO;
			break;
		case 2:
			retorno = StatusPagamentoEnum.APROVADO;
			break;
		case 3:
			retorno =  StatusPagamentoEnum.COMPENSADO;
			break;
		case 4:
			retorno = StatusPagamentoEnum.ESTORNADO;
			break;
		case 9:
			retorno = StatusPagamentoEnum.PENDENTE;
			break;
		case 5:
			retorno = StatusPagamentoEnum.PRESTACAO_DE_CONTAS_EFETUADA;
			break;
		case 6:
			retorno = StatusPagamentoEnum.CONCILIADO;
			break;
		}
	return retorno;
	}
	
	public static StatusPagamentoEnum fromDescricao(String descricao) {
		Optional<StatusPagamentoEnum> filter = Arrays.asList(StatusPagamentoEnum.values()).stream().filter(en->en.getDesc().equalsIgnoreCase(descricao)).findFirst();
		return filter.orElseGet(null);
	}

	public static StatusPagamentoEnum fromCodigo(String codigo) {
		Optional<StatusPagamentoEnum> filter = Arrays.asList(StatusPagamentoEnum.values()).stream().filter(en->en.getId().toString().equalsIgnoreCase(codigo)).findFirst();
		return filter.orElseGet(null);
	}
	
}
