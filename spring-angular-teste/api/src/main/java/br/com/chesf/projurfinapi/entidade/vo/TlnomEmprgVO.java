package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

public class TlnomEmprgVO implements Serializable{

	private static final long serialVersionUID = -1237605972534719720L;
	
	private String nomeFuncionario;
	
	private String areaFuncionario;

	/**
	 * @return the nomeFuncionario
	 */
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	/**
	 * @param nomeFuncionario the nomeFuncionario to set
	 */
	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	/**
	 * @return the areaFuncionario
	 */
	public String getAreaFuncionario() {
		return areaFuncionario;
	}

	/**
	 * @param areaFuncionario the areaFuncionario to set
	 */
	public void setAreaFuncionario(String areaFuncionario) {
		this.areaFuncionario = areaFuncionario;
	}

}
