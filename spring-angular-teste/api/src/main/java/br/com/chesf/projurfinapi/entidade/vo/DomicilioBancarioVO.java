package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class DomicilioBancarioVO implements Serializable{

	private static final long serialVersionUID = 4570574508507428131L;

    private Long id;
	
	private String bancoAgencia;
	
	private String bancoConta; 
	
	private String bancoNome;
    
    @NotEmpty(message = "Domicilio Bancario nulo ou inválido!")
    private String bancoDomicilio;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the bancoAgencia
	 */
	public String getBancoAgencia() {
		return bancoAgencia;
	}

	/**
	 * @param bancoAgencia the bancoAgencia to set
	 */
	public void setBancoAgencia(String bancoAgencia) {
		this.bancoAgencia = bancoAgencia;
	}

	/**
	 * @return the bancoConta
	 */
	public String getBancoConta() {
		return bancoConta;
	}

	/**
	 * @param bancoConta the bancoConta to set
	 */
	public void setBancoConta(String bancoConta) {
		this.bancoConta = bancoConta;
	}

	/**
	 * @return the bancoNome
	 */
	public String getBancoNome() {
		return bancoNome;
	}

	/**
	 * @param bancoNome the bancoNome to set
	 */
	public void setBancoNome(String bancoNome) {
		this.bancoNome = bancoNome;
	}

	/**
	 * @return the bancoDomicilio
	 */
	public String getBancoDomicilio() {
		return bancoDomicilio;
	}

	/**
	 * @param bancoDomicilio the bancoDomicilio to set
	 */
	public void setBancoDomicilio(String bancoDomicilio) {
		this.bancoDomicilio = bancoDomicilio;
	}
	
	
}
