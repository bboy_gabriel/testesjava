package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;

public class ProcessoVO implements Serializable{

	private static final long serialVersionUID = -6689943217742268948L;
	
	private Long codigoProcesso;
	
	private String numeroProcesso;
	
	private Long codigoTipoProcesso;
	
	private Long codigoTipoAcao;
	
	private BigDecimal valorPedido;
	
	private BigDecimal valorEstimado;
	
	private Long codigoOrgaoJulgador;
	
	private Long codigoMunicipio;
	
	private String descricaoVara;
	
	private String descricaoAcao;
	
	private String idRisco;
	
	private String codigoMatrizAdvogado;
	
	private String idPrincipal;
	
	private String codigoOrgaoReq;
	
	private String idStatus;
	
	private String siglaEstado;
	
	private Long codigoEscritorioTerceiro;
	
	private Long numeroProcessoSijus;
	
	private Long numeroDigVerfSijus;
	
	private String dataAutuacao;
	
	private String idRelevante;
	
	private String codigoEscritorioChesf;
	
	private String descricaoOrgaoJulgAdm;
	
	private Integer dataAnoPrcs;
	
	private Long idComplexidade;
	
	private String descricaoCriterioValorRisco;
	
	private String dataCliente;
	
	private String idSituacaoCliente;
	
	private String idMigracaoSijus;
	
	private String idFiscal;
	
	private String idSolEncerramento;
	
	private String dataAjuizamento;
	
	private BigDecimal valorSentenca;
	
	private String nomeApelidoParte;
	
	private Long idLocad;
	
	private String tipEncr;

	private TipoProcessoContaRazaoEnum tipoProcesso;
	
	private String mensagemOpcional;
	
	/**
	 * @return the codigoProcesso
	 */
	public Long getCodigoProcesso() {
		return codigoProcesso;
	}

	/**
	 * @param codigoProcesso the codigoProcesso to set
	 */
	public void setCodigoProcesso(Long codigoProcesso) {
		this.codigoProcesso = codigoProcesso;
	}

	/**
	 * @return the numeroProcesso
	 */
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public Long getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(Long codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}

	/**
	 * @return the codigoTipoAcao
	 */
	public Long getCodigoTipoAcao() {
		return codigoTipoAcao;
	}

	/**
	 * @param codigoTipoAcao the codigoTipoAcao to set
	 */
	public void setCodigoTipoAcao(Long codigoTipoAcao) {
		this.codigoTipoAcao = codigoTipoAcao;
	}

	/**
	 * @return the valorPedido
	 */
	public BigDecimal getValorPedido() {
		return valorPedido;
	}

	/**
	 * @param valorPedido the valorPedido to set
	 */
	public void setValorPedido(BigDecimal valorPedido) {
		this.valorPedido = valorPedido;
	}

	/**
	 * @return the valorEstimado
	 */
	public BigDecimal getValorEstimado() {
		return valorEstimado;
	}

	/**
	 * @param valorEstimado the valorEstimado to set
	 */
	public void setValorEstimado(BigDecimal valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	/**
	 * @return the codigoOrgaoJulgador
	 */
	public Long getCodigoOrgaoJulgador() {
		return codigoOrgaoJulgador;
	}

	/**
	 * @param codigoOrgaoJulgador the codigoOrgaoJulgador to set
	 */
	public void setCodigoOrgaoJulgador(Long codigoOrgaoJulgador) {
		this.codigoOrgaoJulgador = codigoOrgaoJulgador;
	}

	/**
	 * @return the codigoMunicipio
	 */
	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}

	/**
	 * @param codigoMunicipio the codigoMunicipio to set
	 */
	public void setCodigoMunicipio(Long codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	/**
	 * @return the descricaoVara
	 */
	public String getDescricaoVara() {
		return descricaoVara;
	}

	/**
	 * @param descricaoVara the descricaoVara to set
	 */
	public void setDescricaoVara(String descricaoVara) {
		this.descricaoVara = descricaoVara;
	}

	/**
	 * @return the descricaoAcao
	 */
	public String getDescricaoAcao() {
		return descricaoAcao;
	}

	/**
	 * @param descricaoAcao the descricaoAcao to set
	 */
	public void setDescricaoAcao(String descricaoAcao) {
		this.descricaoAcao = descricaoAcao;
	}

	/**
	 * @return the idRisco
	 */
	public String getIdRisco() {
		return idRisco;
	}

	/**
	 * @param idRisco the idRisco to set
	 */
	public void setIdRisco(String idRisco) {
		this.idRisco = idRisco;
	}

	/**
	 * @return the codigoMatrizAdvogado
	 */
	public String getCodigoMatrizAdvogado() {
		return codigoMatrizAdvogado;
	}

	/**
	 * @param codigoMatrizAdvogado the codigoMatrizAdvogado to set
	 */
	public void setCodigoMatrizAdvogado(String codigoMatrizAdvogado) {
		this.codigoMatrizAdvogado = codigoMatrizAdvogado;
	}

	/**
	 * @return the idPrincipal
	 */
	public String getIdPrincipal() {
		return idPrincipal;
	}

	/**
	 * @param idPrincipal the idPrincipal to set
	 */
	public void setIdPrincipal(String idPrincipal) {
		this.idPrincipal = idPrincipal;
	}

	/**
	 * @return the codigoOrgaoReq
	 */
	public String getCodigoOrgaoReq() {
		return codigoOrgaoReq;
	}

	/**
	 * @param codigoOrgaoReq the codigoOrgaoReq to set
	 */
	public void setCodigoOrgaoReq(String codigoOrgaoReq) {
		this.codigoOrgaoReq = codigoOrgaoReq;
	}

	/**
	 * @return the idStatus
	 */
	public String getIdStatus() {
		return idStatus;
	}

	/**
	 * @param idStatus the idStatus to set
	 */
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	/**
	 * @return the siglaEstado
	 */
	public String getSiglaEstado() {
		return siglaEstado;
	}

	/**
	 * @param siglaEstado the siglaEstado to set
	 */
	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	/**
	 * @return the codigoEscritorioTerceiro
	 */
	public Long getCodigoEscritorioTerceiro() {
		return codigoEscritorioTerceiro;
	}

	/**
	 * @param codigoEscritorioTerceiro the codigoEscritorioTerceiro to set
	 */
	public void setCodigoEscritorioTerceiro(Long codigoEscritorioTerceiro) {
		this.codigoEscritorioTerceiro = codigoEscritorioTerceiro;
	}

	/**
	 * @return the numeroProcessoSijus
	 */
	public Long getNumeroProcessoSijus() {
		return numeroProcessoSijus;
	}

	/**
	 * @param numeroProcessoSijus the numeroProcessoSijus to set
	 */
	public void setNumeroProcessoSijus(Long numeroProcessoSijus) {
		this.numeroProcessoSijus = numeroProcessoSijus;
	}

	/**
	 * @return the numeroDigVerfSijus
	 */
	public Long getNumeroDigVerfSijus() {
		return numeroDigVerfSijus;
	}

	/**
	 * @param numeroDigVerfSijus the numeroDigVerfSijus to set
	 */
	public void setNumeroDigVerfSijus(Long numeroDigVerfSijus) {
		this.numeroDigVerfSijus = numeroDigVerfSijus;
	}

	/**
	 * @return the dataAutuacao
	 */
	public String getDataAutuacao() {
		return dataAutuacao;
	}

	/**
	 * @param dataAutuacao the dataAutuacao to set
	 */
	public void setDataAutuacao(String dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	/**
	 * @return the idRelevante
	 */
	public String getIdRelevante() {
		return idRelevante;
	}

	/**
	 * @param idRelevante the idRelevante to set
	 */
	public void setIdRelevante(String idRelevante) {
		this.idRelevante = idRelevante;
	}

	/**
	 * @return the codigoEscritorioChesf
	 */
	public String getCodigoEscritorioChesf() {
		return codigoEscritorioChesf;
	}

	/**
	 * @param codigoEscritorioChesf the codigoEscritorioChesf to set
	 */
	public void setCodigoEscritorioChesf(String codigoEscritorioChesf) {
		this.codigoEscritorioChesf = codigoEscritorioChesf;
	}

	/**
	 * @return the descricaoOrgaoJulgAdm
	 */
	public String getDescricaoOrgaoJulgAdm() {
		return descricaoOrgaoJulgAdm;
	}

	/**
	 * @param descricaoOrgaoJulgAdm the descricaoOrgaoJulgAdm to set
	 */
	public void setDescricaoOrgaoJulgAdm(String descricaoOrgaoJulgAdm) {
		this.descricaoOrgaoJulgAdm = descricaoOrgaoJulgAdm;
	}

	/**
	 * @return the dataAnoPrcs
	 */
	public Integer getDataAnoPrcs() {
		return dataAnoPrcs;
	}

	/**
	 * @param dataAnoPrcs the dataAnoPrcs to set
	 */
	public void setDataAnoPrcs(Integer dataAnoPrcs) {
		this.dataAnoPrcs = dataAnoPrcs;
	}

	/**
	 * @return the idComplexidade
	 */
	public Long getIdComplexidade() {
		return idComplexidade;
	}

	/**
	 * @param idComplexidade the idComplexidade to set
	 */
	public void setIdComplexidade(Long idComplexidade) {
		this.idComplexidade = idComplexidade;
	}

	/**
	 * @return the descricaoCriterioValorRisco
	 */
	public String getDescricaoCriterioValorRisco() {
		return descricaoCriterioValorRisco;
	}

	/**
	 * @param descricaoCriterioValorRisco the descricaoCriterioValorRisco to set
	 */
	public void setDescricaoCriterioValorRisco(String descricaoCriterioValorRisco) {
		this.descricaoCriterioValorRisco = descricaoCriterioValorRisco;
	}

	/**
	 * @return the dataCliente
	 */
	public String getDataCliente() {
		return dataCliente;
	}

	/**
	 * @param dataCliente the dataCliente to set
	 */
	public void setDataCliente(String dataCliente) {
		this.dataCliente = dataCliente;
	}

	/**
	 * @return the idSituacaoCliente
	 */
	public String getIdSituacaoCliente() {
		return idSituacaoCliente;
	}

	/**
	 * @param idSituacaoCliente the idSituacaoCliente to set
	 */
	public void setIdSituacaoCliente(String idSituacaoCliente) {
		this.idSituacaoCliente = idSituacaoCliente;
	}

	/**
	 * @return the idMigracaoSijus
	 */
	public String getIdMigracaoSijus() {
		return idMigracaoSijus;
	}

	/**
	 * @param idMigracaoSijus the idMigracaoSijus to set
	 */
	public void setIdMigracaoSijus(String idMigracaoSijus) {
		this.idMigracaoSijus = idMigracaoSijus;
	}

	/**
	 * @return the idFiscal
	 */
	public String getIdFiscal() {
		return idFiscal;
	}

	/**
	 * @param idFiscal the idFiscal to set
	 */
	public void setIdFiscal(String idFiscal) {
		this.idFiscal = idFiscal;
	}

	/**
	 * @return the idSolEncerramento
	 */
	public String getIdSolEncerramento() {
		return idSolEncerramento;
	}

	/**
	 * @param idSolEncerramento the idSolEncerramento to set
	 */
	public void setIdSolEncerramento(String idSolEncerramento) {
		this.idSolEncerramento = idSolEncerramento;
	}

	/**
	 * @return the dataAjuizamento
	 */
	public String getDataAjuizamento() {
		return dataAjuizamento;
	}

	/**
	 * @param dataAjuizamento the dataAjuizamento to set
	 */
	public void setDataAjuizamento(String dataAjuizamento) {
		this.dataAjuizamento = dataAjuizamento;
	}

	/**
	 * @return the valorSentenca
	 */
	public BigDecimal getValorSentenca() {
		return valorSentenca;
	}

	/**
	 * @param valorSentenca the valorSentenca to set
	 */
	public void setValorSentenca(BigDecimal valorSentenca) {
		this.valorSentenca = valorSentenca;
	}

	/**
	 * @return the nomePelidoParte
	 */
	public String getNomeApelidoParte() {
		return nomeApelidoParte;
	}

	/**
	 * @param nomePelidoParte the nomePelidoParte to set
	 */
	public void setNomeApelidoParte(String nomePelidoParte) {
		this.nomeApelidoParte = nomePelidoParte;
	}

	/**
	 * @return the idLocad
	 */
	public Long getIdLocad() {
		return idLocad;
	}

	/**
	 * @param idLocad the idLocad to set
	 */
	public void setIdLocad(Long idLocad) {
		this.idLocad = idLocad;
	}

	/**
	 * @return the tipEncr
	 */
	public String getTipEncr() {
		return tipEncr;
	}

	/**
	 * @param tipEncr the tipEncr to set
	 */
	public void setTipEncr(String tipEncr) {
		this.tipEncr = tipEncr;
	}

	/**
	 * @return the tipoProcesso
	 */
	public TipoProcessoContaRazaoEnum getTipoProcesso() {
		return tipoProcesso;
	}

	/**
	 * @param tipoProcesso the tipoProcesso to set
	 */
	public void setTipoProcesso(TipoProcessoContaRazaoEnum tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	/**
	 * @return the mensagemOpcional
	 */
	public String getMensagemOpcional() {
		return mensagemOpcional;
	}

	/**
	 * @param mensagemOpcional the mensagemOpcional to set
	 */
	public void setMensagemOpcional(String mensagemOpcional) {
		this.mensagemOpcional = mensagemOpcional;
	}

}
