package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_INFORMACAO_CONTAB", schema="PROJUR")
@SequenceGenerator(name = "SQ_INFORMACAO_CONTAB", sequenceName = "SQ_INFORMACAO_CONTAB", allocationSize = 1)
public class InformacaoContabil implements Serializable{

	private static final long serialVersionUID = -2546909843421528955L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_INFORMACAO_CONTAB")
    @Column(name = "ID_INFORMACAO_CONTAB")
    private Long id;
	
	@OneToOne(targetEntity = CentroDeCusto.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CENTR_CUST", referencedColumnName = "ID_CENTR_CUST", nullable = true)
	private CentroDeCusto centroDeCusto;
	
	@OneToOne(targetEntity = ElementoPep.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ELEMENTO_PEP", referencedColumnName = "ID_ELEMENTO_PEP", nullable = true)
	private ElementoPep elementoPep;
	
	@OneToOne(targetEntity = Ordem.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ORDEM", referencedColumnName = "ID_ORDEM", nullable = true)
	private Ordem ordem;
	
	@Column(name = "VL_INFORMACAO_CONTAB", nullable = false)
	private BigDecimal valor;
	
	
//	public InformacaoContabil(Long id, CentroDeCusto centroDeCusto, ElementoPep elementoPep, Ordem ordem,
//			BigDecimal valor) {
//		//super();
//		this.id = id;
//		this.centroDeCusto = centroDeCusto;
//		this.elementoPep = elementoPep;
//		this.ordem = ordem;
//		this.valor = valor;
//	}
//	
//	public InformacaoContabil(Long id) {
//		//super();
//		this.id = id;
//	}



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the centroDeCusto
	 */
	public CentroDeCusto getCentroDeCusto() {
		return centroDeCusto;
	}

	/**
	 * @param centroDeCusto the centroDeCusto to set
	 */
	public void setCentroDeCusto(CentroDeCusto centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	/**
	 * @return the elementoPep
	 */
	public ElementoPep getElementoPep() {
		return elementoPep;
	}

	/**
	 * @param elementoPep the elementoPep to set
	 */
	public void setElementoPep(ElementoPep elementoPep) {
		this.elementoPep = elementoPep;
	}

	/**
	 * @return the ordem
	 */
	public Ordem getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem the ordem to set
	 */
	public void setOrdem(Ordem ordem) {
		this.ordem = ordem;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
}
