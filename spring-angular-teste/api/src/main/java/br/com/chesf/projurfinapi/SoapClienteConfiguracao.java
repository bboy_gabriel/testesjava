package br.com.chesf.projurfinapi;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import br.com.chesf.projurfinapi.servico.ConfiguracaoServico;

@Configuration
public class SoapClienteConfiguracao {
	

	@Autowired
	private ConfiguracaoServico configuracaoServico;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("br.com.chesf.projurfinapi.sap");
		return marshaller;
	}

	@Bean
	public HttpComponentsMessageSender httpComponentsMessageSender() {
		HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
		httpComponentsMessageSender.setCredentials(usernamePasswordCredentials());
		return httpComponentsMessageSender;
	}

	@Bean
	public UsernamePasswordCredentials usernamePasswordCredentials() {
		//return new UsernamePasswordCredentials(configuracaoServico.consultarUserNameSAP(), configuracaoServico.consultarPasswordSAP());
		return new UsernamePasswordCredentials("APCHSFPROJUR", "Bc71#PRO");
	}
}
