package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.TlnomEmprg;
import br.com.chesf.projurfinapi.entidade.vo.EmpregadoMatriculaValidoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.TlnomEmprgRepositorio;

@Service
public class TlnomEmprgServico implements Serializable{

	private static final long serialVersionUID = 1289493457676338610L;

	@Autowired
	private TlnomEmprgRepositorio emprgRepositorio;
	
	public EmpregadoMatriculaValidoVO validarMatriculaSAP(String matriculaSAP) {
		try {
			TlnomEmprg empregadoBase = emprgRepositorio.findByIdSap(matriculaSAP);
			return montarEmpregadoMatriculaValidoVO(empregadoBase);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_VALIDAR_MATRICULA_SAP);
		}
	}
	
	private EmpregadoMatriculaValidoVO montarEmpregadoMatriculaValidoVO(TlnomEmprg empregadoBase) {
		EmpregadoMatriculaValidoVO empregadoVO = new EmpregadoMatriculaValidoVO();
		if(empregadoBase != null && empregadoBase.getNomeEmprgCompl() != null && !empregadoBase.getNomeEmprgCompl().isEmpty() && 
				empregadoBase.getArelo() != null && empregadoBase.getArelo().getSiglaArelo() != null && !empregadoBase.getArelo().getSiglaArelo().isEmpty()) {
			empregadoVO.setNomeEmpregado(empregadoBase.getNomeEmprgCompl());
			empregadoVO.setAreaLotacao(empregadoBase.getArelo().getSiglaArelo());
			empregadoVO.setStatusMatricula(true);
		}else {
			empregadoVO.setStatusMatricula(false);
		}
		return empregadoVO;
	}
	
}
