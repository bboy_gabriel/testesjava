package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.CentroDeCustoVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.CentroDeCustoServico;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por exportar os serviços referentes ao recurso CentroDeCusto.
 *
 */
@RestController
@RequestMapping(value = "/api/centrosDeCusto")
@Consumes("application/json")
@Produces("application/json")
public class CentroDeCustoControlador implements Serializable{

	private static final long serialVersionUID = -7118494254664818566L;

	@Autowired
	private CentroDeCustoServico centroDeCustoServico;
	
	/**
	 * 
	 * @return
	 * 
	 * Metodo responsavel por consultar uma lista de recursos CentroDeCusto
	 * 
	 */
	@RequestMapping
	public ResponseEntity consultar() {
		try {
			return new ResponseEntity(centroDeCustoServico.consultarOrdenado(), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<CentroDeCustoVO> consultarPorId(@PathVariable Long id) {
		try {
			return new ResponseEntity<CentroDeCustoVO>(centroDeCustoServico.consultarPorId(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/*
	@PostMapping
	public ResponseEntity<CentroDeCustoVO> cadastrar(@RequestBody @Valid CentroDeCustoVO centroVO) {
		try {
			return new ResponseEntity <CentroDeCustoVO>(centroDeCustoServico.cadastrar(centroVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity<CentroDeCustoVO> atualizar(@RequestBody CentroDeCustoVO centroVO) {
		try {
			return new ResponseEntity<CentroDeCustoVO>(centroDeCustoServico.atualizar(centroVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<CentroDeCustoVO> remover(@PathVariable Long id) {
		try {
			centroDeCustoServico.remover(id);
			return new ResponseEntity<CentroDeCustoVO>(HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	*/
}
