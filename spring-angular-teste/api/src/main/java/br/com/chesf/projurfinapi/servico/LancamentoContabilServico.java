package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.LancamentoContabil;
import br.com.chesf.projurfinapi.entidade.LancamentoContabilRelatorioVO;
import br.com.chesf.projurfinapi.entidade.vo.InformacaoValoresLancamentoContabilVO;
import br.com.chesf.projurfinapi.entidade.vo.LancamentoContabilVO;
import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoLancamentoContabilEnum;
import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.LancamentoContabilRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

@Service
public class LancamentoContabilServico implements Serializable{

	private static final long serialVersionUID = 2939584793511350022L;

	@Autowired
	private LancamentoContabilRepositorio lancamentoContabilrepositorio;
	
	@Autowired
	private ProcessoServico processoServico;
	
	@Autowired
	private ModelMapper modelMapper;
	
	Logger logger = LoggerFactory.getLogger(LancamentoContabilServico.class);
	
	DateTimeFormatter FormatoDataLancamentoContabilVO = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	/**
	 * 
	 * Serviço responsável por cadastrar um recurso LancamentoContabil.
	 * 
	 * @param lancamentoVO
	 * @return LancamentoContabilVO
	 * 
	 */
	public LancamentoContabilVO cadastrarLancamentoContabilProvisao(LancamentoContabilVO lancamentoVO) {
		LancamentoContabil lancamentoBase = convertVoToLancamentoContabil(lancamentoVO);
		validarDataLancamentoContabil(lancamentoBase.getDataLancamento());
		validarValorLancamentoContabilProvisao(lancamentoBase.getValor());
		try {
			lancamentoBase.setTipoLancamento(TipoLancamentoContabilEnum.PROVISOES.getId().toString());
			return convertToLancamentoContabilVO(lancamentoContabilrepositorio.save(lancamentoBase));
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_GERAR_UM_LANCAMENTO_CONTABIL.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_GERAR_UM_LANCAMENTO_CONTABIL);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por cadastrar um regitro de lançamento contábil referente a um estorno.
	 * 
	 * @param lancamentoContabilVO
	 * @return LancamentoContabilVO
	 * 
	 */
	public LancamentoContabilVO cadastrarLancamentoContabilEstorno(LancamentoContabilVO lancamentoContabilVO) {
		LancamentoContabil lancamento = convertVoToLancamentoContabil(lancamentoContabilVO);
		validarDataLancamentoContabil(lancamento.getDataLancamento());
		validarValorLancamentoContabilEstorno(lancamento.getValor(), lancamento.getNumeroProcesso());
		try {
			lancamento.setTipoLancamento(TipoLancamentoContabilEnum.ESTORNOS_PROVISAO.getId().toString());
			return convertToLancamentoContabilVO(lancamentoContabilrepositorio.save(lancamento));
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CADASTRAR_LANCAMENTO_CONTABIL_ESTORNO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_LANCAMENTO_CONTABIL_ESTORNO);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por fornecer uma soma de todas as provisões registradas para o processo.
	 * 
	 * @param numeroProcessoVO
	 * @return numeroProcessoVO.getNumeroProcesso()
	 * 
	 */
	public InformacaoValoresLancamentoContabilVO consultarValorDisponivelLancamentoProvisao(NumeroProcessoVO numeroProcessoVO) {
		BigDecimal totalProvisao = consultarTotalLancamentoProvisaoPorProcesso(Long.parseLong(numeroProcessoVO.getNumeroProcesso()));
		BigDecimal totalEstorno = consultarTotalLancamentoEstornoPorProcesso(Long.parseLong(numeroProcessoVO.getNumeroProcesso()));
		BigDecimal valorDisponivel = totalProvisao.subtract(totalEstorno);
		InformacaoValoresLancamentoContabilVO vo = new InformacaoValoresLancamentoContabilVO();
		vo.setValorTotalProvisao(valorDisponivel);
		return vo;
	}
	
	/**
	 * 
	 * Serviço que retorna o valor total provisionado referente ao processo.
	 * 
	 * @param numeroProcesso
	 * @return BigDecimal totalProvisionado
	 * 
	 */
	public BigDecimal consultarTotalProvisionado(String numeroProcesso) {
		BigDecimal totalProvisao = consultarTotalLancamentoProvisaoPorProcesso(Long.parseLong(numeroProcesso));
		BigDecimal totalEstorno = consultarTotalLancamentoEstornoPorProcesso(Long.parseLong(numeroProcesso));
		return totalProvisao.subtract(totalEstorno);
	}
	
	/**
	 * 
	 * Serviço responsável por listar todos os recursos LancamentoContabil referentes ao processo!
	 * 
	 * @param numeroProcessoVO
	 * @return List<LancamentoContabil>
	 * 
	 */
	public List<LancamentoContabilVO> consultarLancamentoContabilPorProcesso(NumeroProcessoVO numeroProcessoVO){
		processoServico.consultarPorNumero(numeroProcessoVO);
		try {
			return convertToListLancamentoContabilVO(lancamentoContabilrepositorio.findByNumeroProcesso(Long.parseLong(numeroProcessoVO.getNumeroProcesso())));
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LANCAMENTO_CONTABIL_POR_PROCESSO);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por consultar um recurso LançamentoContabil por id.
	 * 
	 * @param id
	 * @return LancamentoContabil
	 * 
	 */
	public LancamentoContabilVO consultarLancamentoContabilPorId(Long id) {
		Validador.validarId(id);
		try {
			return convertToLancamentoContabilVO(lancamentoContabilrepositorio.findOne(id));
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LANCAMENTO_CONTABIL_POR_ID);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por retornar uma lista de lancamentoContabil dentro de um intervalo mensal com o objetivo de compor um relatório.
	 * 
	 * @param dataLancamentoMensal
	 * @return List<LancamentoContabilVO>
	 * 
	 */
	public List<LancamentoContabilVO> consultarLancamentosCiclo(String dataLancamentoMensal){
		try {
			YearMonth anoMes = YearMonth.parse(dataLancamentoMensal);
			LocalDate dtInicial = anoMes.atDay(1);
			LocalDate dtFinal = anoMes.atEndOfMonth();
			return convertToListLancamentoContabilVO(lancamentoContabilrepositorio.consultarLancamentoIntervalo(dtInicial, dtFinal));
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_LANCAMENTO_CONTABIL_MES_RELATORIO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_LANCAMENTO_CONTABIL_MES_RELATORIO);
		}
	}
	
	/**
	 * 
	 * Serviço responsável por atualizar todas os recursos LancamentoContabil com a data da geração do arquivo CSV.
	 * 
	 * @param lancamentoContabilRelatorioVO
	 * @return List<LancamentoContabilVO>
	 * 
	 */
	public List<LancamentoContabilVO> atualizarDataGeracaoArquivoCSV(LancamentoContabilRelatorioVO lancamentoContabilRelatorioVO){
		try {
			List<LancamentoContabil> listaBase = lancamentoContabilrepositorio.findAll(lancamentoContabilRelatorioVO.getListaIds());
			for (LancamentoContabil lancamentoContabil : listaBase) {
				lancamentoContabil.setDataGeracaoCSV(LocalDate.parse(lancamentoContabilRelatorioVO.getDataGeracao()));
			}
			return convertToListLancamentoContabilVO(lancamentoContabilrepositorio.save(listaBase));
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_ATUALIZAR_DATA_GERACAO_ARQUIVO_CSV.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_DATA_GERACAO_ARQUIVO_CSV);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por validar o valor solicitado para estorno, 
	 * não sendo possível estornar um valor maior que o total de todas as provisões cadastradas para o processo.
	 * 
	 * @param valor
	 * @param numeroProcesso
	 * 
	 */
	private void validarValorLancamentoContabilEstorno(BigDecimal valor, Long numeroProcesso) {
		BigDecimal totalProvisao = consultarTotalLancamentoProvisaoPorProcesso(numeroProcesso);
		BigDecimal totalEstorno = consultarTotalLancamentoEstornoPorProcesso(numeroProcesso);
		if(totalProvisao.compareTo(totalEstorno) == -1) {
			logger.error(MensagemEnum.VALOR_ESTORNO_MENOR_QUE_VALOR_PROVISAO.getMensagem());
			throw new NegocioException(MensagemEnum.VALOR_ESTORNO_MENOR_QUE_VALOR_PROVISAO);
		} 
	}
	
	/**
	 * 
	 * Metodo privado responsável por consultar as provisões cadastradas para o processo e somar os seus valores.
	 * 
	 * @param numeroProcesso
	 * @return BigDecimal - valor total das provisões cadastradas.
	 * 
	 */
	private BigDecimal consultarTotalLancamentoProvisaoPorProcesso(Long numeroProcesso) {
		try {
			List<LancamentoContabil> listaProvisoes = lancamentoContabilrepositorio.findByTipoLancamentoAndNumeroProcesso(TipoLancamentoContabilEnum.PROVISOES.getId().toString(), numeroProcesso);
			BigDecimal totalProvisao = BigDecimal.ZERO;
			
			for (LancamentoContabil provisao : listaProvisoes) {
				totalProvisao = totalProvisao.add(provisao.getValor());
			}
			
			return totalProvisao;
			
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_TOTAL_LANCAMENTO_PROVISAO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_TOTAL_LANCAMENTO_PROVISAO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por consultar todos os estornos cadastrados para o processo e retornar a soma do seus valores.
	 * 
	 * @param numeroProcesso
	 * @return BigDecimal - valor total dos estornos cadastrados para o processo.
	 * 
	 */
	private BigDecimal consultarTotalLancamentoEstornoPorProcesso(Long numeroProcesso) {
		try {
			List<LancamentoContabil> listaEstornos = lancamentoContabilrepositorio.findByTipoLancamentoAndNumeroProcesso(TipoLancamentoContabilEnum.ESTORNOS_PROVISAO.getId().toString(), numeroProcesso);
			BigDecimal totalEstorno = BigDecimal.ZERO;
			
			for (LancamentoContabil estorno : listaEstornos) {
				totalEstorno = totalEstorno.add(estorno.getValor());
			}
			
			return totalEstorno;
			
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONSULTAR_TOTAL_LANCAMENTO_ESTORNO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_TOTAL_LANCAMENTO_ESTORNO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por validar a regra de negocio referente a data do lançamento,
	 * verificando se a mesma pertence ao mês corrente e não é maior que a data atual.
	 * 
	 * @param dataLancamento
	 * 
	 */
	private void validarDataLancamentoContabil(LocalDate dataLancamento) {
		try {
			LocalDate hoje = LocalDate.now();
			if(dataLancamento.isAfter(hoje)) {
				logger.error(MensagemEnum.DATA_LANCAMENTO_MAIOR_DATA_ATUAL.getMensagem());
				throw new NegocioException(MensagemEnum.DATA_LANCAMENTO_MAIOR_DATA_ATUAL);
			} 
			if(dataLancamento.getMonthValue() < hoje.getMonthValue()) {
				logger.error(MensagemEnum.DATA_LANCAMENTO_MENOR_MES_ATUAL.getMensagem());
				throw new NegocioException(MensagemEnum.DATA_LANCAMENTO_MENOR_MES_ATUAL);
			} 
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_VALIDAR_DATA_LANCAMENTO_CONTABIL.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_VALIDAR_DATA_LANCAMENTO_CONTABIL);
		}
	}

	/**
	 * 
	 * Metodo private responsável por validar se o valor do lançamento contábil referente a provisão é maior que zero!
	 * 
	 * @param valorLancamento
	 * 
	 */
	private void validarValorLancamentoContabilProvisao(BigDecimal valorLancamento) {
		BigDecimal valorZerado = BigDecimal.ZERO;
		if(valorLancamento.equals(valorZerado)) {
			logger.error(MensagemEnum.VALOR_LANCAMENTO_CONTABIL_INVALIDO.getMensagem());
			throw new NegocioException(MensagemEnum.VALOR_LANCAMENTO_CONTABIL_INVALIDO);
		} 
	}
	
	/**
	 * 
	 * Metodo privado responsável por converter uma entidade LancamentoContabilVO em uma entidade LancamentoContabil.
	 * 
	 * @param lancamentoVO
	 * @return LancamentoContabil
	 * 
	 */
	private LancamentoContabil convertVoToLancamentoContabil(LancamentoContabilVO lancamentoVO) {
		try {
			LancamentoContabil lancamentoBase = modelMapper.map(lancamentoVO, LancamentoContabil.class);
			lancamentoBase.setDataLancamento(LocalDate.parse(lancamentoVO.getDataLancamento()));
			return lancamentoBase;
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_LANCAMENTO_CONTABIL_VO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_LANCAMENTO_CONTABIL_VO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por converter uma entidade LancamentoContabil em uma entidade LancamentoContabilVO
	 * 
	 * @param lancamentoBase
	 * @return LancamentoContabilVO
	 * 
	 */
	private LancamentoContabilVO convertToLancamentoContabilVO(LancamentoContabil lancamentoBase) {
		try {
			LancamentoContabilVO vo = modelMapper.map(lancamentoBase, LancamentoContabilVO.class);
			vo.setDataLancamento(lancamentoBase.getDataLancamento().format(FormatoDataLancamentoContabilVO));
			if(lancamentoBase.getDataGeracaoCSV() != null) {
				vo.setDataGeracaoCSV(lancamentoBase.getDataGeracaoCSV().format(FormatoDataLancamentoContabilVO));
			}
			vo.setTipoLancamentoContabilDescricao(TipoLancamentoContabilEnum.getTipoLancamentoContabilEnumPorID(Integer.parseInt(vo.getTipoLancamento())).getDescricao());
			return vo;
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_VO_EM_LANCAMENTO_CONTABIL.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_VO_EM_LANCAMENTO_CONTABIL);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsável por fornecer um conversor de uma lista LancamentoContabil em LancamentoContabilVO.
	 * 
	 * @param listaBase
	 * @return List<LancamentoContabilVO>
	 * 
	 */
	private List<LancamentoContabilVO> convertToListLancamentoContabilVO(List<LancamentoContabil> listaBase){
		try {
			List<LancamentoContabilVO> listaVO = new ArrayList<>();
			for (LancamentoContabil lancamento : listaBase) {
				LancamentoContabilVO vo = new LancamentoContabilVO();
				vo = modelMapper.map(lancamento, LancamentoContabilVO.class);
				vo.setDataLancamento(lancamento.getDataLancamento().format(FormatoDataLancamentoContabilVO));
				if(lancamento.getDataGeracaoCSV() != null) {
					vo.setDataGeracaoCSV(lancamento.getDataGeracaoCSV().format(FormatoDataLancamentoContabilVO));
				}
				vo.setTipoLancamentoContabilDescricao(TipoLancamentoContabilEnum.getTipoLancamentoContabilEnumPorID(Integer.parseInt(vo.getTipoLancamento())).getDescricao());
				listaVO.add(vo);
			}
			return listaVO;
		} catch (NegocioException e) {
			logger.error(MensagemEnum.ERRO_AO_CONVERTER_LISTA_LANCAMENTO_CONTABIL_EM_VO.getMensagem() + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONVERTER_LISTA_LANCAMENTO_CONTABIL_EM_VO);
		}
	}
	
}
