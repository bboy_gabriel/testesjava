package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.chesf.projurfinapi.entidade.vo.LancamentoContabilVO;

/**
 * 
 * Classe responsável por encapsular uma lista de lancamentoContabilVO para atualização da dataGeracao do arquivo CSV.
 * 
 * @author jvneto1
 *
 */
public class LancamentoContabilRelatorioVO implements Serializable{

	@NotNull(message = "Lista de ids nula ou inválida!")
	private List<Long> listaIds;
	
	@NotEmpty(message = "Data da geração do arquivo CSV nula ou inválida!")
	private String dataGeracao;

	/**
	 * @return the listaIds
	 */
	public List<Long> getListaIds() {
		return listaIds;
	}

	/**
	 * @param listaIds the listaIds to set
	 */
	public void setListaIds(List<Long> listaIds) {
		this.listaIds = listaIds;
	}

	/**
	 * @return the dataGeracao
	 */
	public String getDataGeracao() {
		return dataGeracao;
	}

	/**
	 * @param dataGeracao the dataGeracao to set
	 */
	public void setDataGeracao(String dataGeracao) {
		this.dataGeracao = dataGeracao;
	}
	
}
