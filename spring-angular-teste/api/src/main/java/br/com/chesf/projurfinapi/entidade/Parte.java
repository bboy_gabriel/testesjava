package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_PARTE", schema = "PROJUR")
@SequenceGenerator(name = "SQ_PARTE", sequenceName = "SQ_PARTE", allocationSize = 1)
public class Parte implements Serializable{

	private static final long serialVersionUID = 6504286218773250644L;
	
	@Id
	@Column(name = "CD_PARTE", nullable = false)
	private Long codigoParte;
	
	@Column(name = "NR_CNPJ", length = 14)
	private Long nrCNPJ;
	
	@Column(name = "NM_RAZAO_SOCIAL", length = 100)
	private String nmRazaoSocial;
	
	@Column(name = "NR_CPF", length = 14)
	private String nrCPF;
	
	@Column(name = "NR_IDENT", length = 10)
	private Long nrIdent;
	
	@Column(name = "NR_NIT", length = 10)
	private String nrNit;
	
	@Column(name = "NR_PASEP", length = 11)
	private String nrPasep;
	
	@Column(name = "CD_MATRICULA", length = 6)
	private String cdMatricula;
	
	@Column(name = "NM_EMPRESA", length = 100)
	private String nmEmpresa;
	
	@Column(name = "SG_ESTADO_CTPS", length = 2)
	private String sgEstadoCTPS;
	
	@Column(name = "NR_PARTE_SIJUS", length = 14)
	private Long nrParteSijus;
	
	@Column(name = "TP_PARTE_SIJUS", length = 1)
	private Long tpParteSijus;
	
	@Column(name = "ID_MIGRACAO_SIJUS", length = 1)
	private String idMigracaoSijus;
	
	@Column(name = "NR_CTPS", length = 14)
	private String nrCTPS;

	/**
	 * @return the codigoParte
	 */
	public Long getCodigoParte() {
		return codigoParte;
	}

	/**
	 * @param codigoParte the codigoParte to set
	 */
	public void setCodigoParte(Long codigoParte) {
		this.codigoParte = codigoParte;
	}

	/**
	 * @return the nrCNPJ
	 */
	public Long getNrCNPJ() {
		return nrCNPJ;
	}

	/**
	 * @param nrCNPJ the nrCNPJ to set
	 */
	public void setNrCNPJ(Long nrCNPJ) {
		this.nrCNPJ = nrCNPJ;
	}

	/**
	 * @return the nmRazaoSocial
	 */
	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}

	/**
	 * @param nmRazaoSocial the nmRazaoSocial to set
	 */
	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}

	/**
	 * @return the nrCPF
	 */
	public String getNrCPF() {
		return nrCPF;
	}

	/**
	 * @param nrCPF the nrCPF to set
	 */
	public void setNrCPF(String nrCPF) {
		this.nrCPF = nrCPF;
	}

	/**
	 * @return the nrIdent
	 */
	public Long getNrIdent() {
		return nrIdent;
	}

	/**
	 * @param nrIdent the nrIdent to set
	 */
	public void setNrIdent(Long nrIdent) {
		this.nrIdent = nrIdent;
	}

	/**
	 * @return the nrNit
	 */
	public String getNrNit() {
		return nrNit;
	}

	/**
	 * @param nrNit the nrNit to set
	 */
	public void setNrNit(String nrNit) {
		this.nrNit = nrNit;
	}

	/**
	 * @return the nrPasep
	 */
	public String getNrPasep() {
		return nrPasep;
	}

	/**
	 * @param nrPasep the nrPasep to set
	 */
	public void setNrPasep(String nrPasep) {
		this.nrPasep = nrPasep;
	}

	/**
	 * @return the cdMatricula
	 */
	public String getCdMatricula() {
		return cdMatricula;
	}

	/**
	 * @param cdMatricula the cdMatricula to set
	 */
	public void setCdMatricula(String cdMatricula) {
		this.cdMatricula = cdMatricula;
	}

	/**
	 * @return the nmEmpresa
	 */
	public String getNmEmpresa() {
		return nmEmpresa;
	}

	/**
	 * @param nmEmpresa the nmEmpresa to set
	 */
	public void setNmEmpresa(String nmEmpresa) {
		this.nmEmpresa = nmEmpresa;
	}

	/**
	 * @return the sgEstadoCTPS
	 */
	public String getSgEstadoCTPS() {
		return sgEstadoCTPS;
	}

	/**
	 * @param sgEstadoCTPS the sgEstadoCTPS to set
	 */
	public void setSgEstadoCTPS(String sgEstadoCTPS) {
		this.sgEstadoCTPS = sgEstadoCTPS;
	}

	/**
	 * @return the nrParteSijus
	 */
	public Long getNrParteSijus() {
		return nrParteSijus;
	}

	/**
	 * @param nrParteSijus the nrParteSijus to set
	 */
	public void setNrParteSijus(Long nrParteSijus) {
		this.nrParteSijus = nrParteSijus;
	}

	/**
	 * @return the tpParteSijus
	 */
	public Long getTpParteSijus() {
		return tpParteSijus;
	}

	/**
	 * @param tpParteSijus the tpParteSijus to set
	 */
	public void setTpParteSijus(Long tpParteSijus) {
		this.tpParteSijus = tpParteSijus;
	}

	/**
	 * @return the idMigracaoSijus
	 */
	public String getIdMigracaoSijus() {
		return idMigracaoSijus;
	}

	/**
	 * @param idMigracaoSijus the idMigracaoSijus to set
	 */
	public void setIdMigracaoSijus(String idMigracaoSijus) {
		this.idMigracaoSijus = idMigracaoSijus;
	}

	/**
	 * @return the nrCTPS
	 */
	public String getNrCTPS() {
		return nrCTPS;
	}

	/**
	 * @param nrCTPS the nrCTPS to set
	 */
	public void setNrCTPS(String nrCTPS) {
		this.nrCTPS = nrCTPS;
	}
	
}
