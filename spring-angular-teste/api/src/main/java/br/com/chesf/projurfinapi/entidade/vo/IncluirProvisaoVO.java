package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;

public class IncluirProvisaoVO implements Serializable{
	
	private static final long serialVersionUID = 4525285874462706661L;

	@NotEmpty(message = "Número do processo nulo ou inválido!")
	private String numeroProcesso;
	
	@NotNull(message = "Tipo do processo nulo ou inválido!")
	private TipoProcessoContaRazaoEnum tipoProcesso;
	
	@NotNull(message = "Data nula inválida!")
	private String data;
	
	@NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valor;
	
	@NotNull(message = "Conta razão nula ou inválida!")
	private ContaRazaoProvisaoVO contaRazaoProvisao;
	
	@NotEmpty(message = "Histórico nulo ou inválido@")
	private String historico;
	
	private InformacaoContabilVO informacaoContabil;

	/**
	 * @return the numeroProcesso
	 */
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	/**
	 * @return the tipoProcesso
	 */
	public TipoProcessoContaRazaoEnum getTipoProcesso() {
		return tipoProcesso;
	}

	/**
	 * @param tipoProcesso the tipoProcesso to set
	 */
	public void setTipoProcesso(TipoProcessoContaRazaoEnum tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the contaRazaoProvisao
	 */
	public ContaRazaoProvisaoVO getContaRazaoProvisao() {
		return contaRazaoProvisao;
	}

	/**
	 * @param contaRazaoProvisao the contaRazaoProvisao to set
	 */
	public void setContaRazaoProvisao(ContaRazaoProvisaoVO contaRazaoProvisao) {
		this.contaRazaoProvisao = contaRazaoProvisao;
	}

	/**
	 * @return the historico
	 */
	public String getHistorico() {
		return historico;
	}

	/**
	 * @param historico the historico to set
	 */
	public void setHistorico(String historico) {
		this.historico = historico;
	}

	/**
	 * @return the informacaoContabil
	 */
	public InformacaoContabilVO getInformacaoContabil() {
		return informacaoContabil;
	}

	/**
	 * @param informacaoContabil the informacaoContabil to set
	 */
	public void setInformacaoContabil(InformacaoContabilVO informacaoContabil) {
		this.informacaoContabil = informacaoContabil;
	}
	
}
