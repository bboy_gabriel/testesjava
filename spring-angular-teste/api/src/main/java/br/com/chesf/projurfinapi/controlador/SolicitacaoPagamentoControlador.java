package br.com.chesf.projurfinapi.controlador;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.chesf.projurfinapi.entidade.vo.PaginacaoVO;
import br.com.chesf.projurfinapi.entidade.vo.SolicitacaoPagamentoVO;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.servico.SolicitacaoPagamentoServico;

@RestController 
@RequestMapping("/api/solicitacoesPagamentos")
public class SolicitacaoPagamentoControlador implements Serializable{

	private static final long serialVersionUID = 8984853304857056245L;
	
	@Autowired
	private SolicitacaoPagamentoServico solicitacaoPagamentoServico;
	
	/**
	 * 
	 * @param id
	 * @return ResponseEntity
	 * 
	 * EndPoint responsável por consultar uma solicitacao usando o id.
	 * 
	 */
	@GetMapping("/{id}")
	public ResponseEntity consultarSolicitacaoPorID(@PathVariable @NotNull Long id) {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.consultarPorID(id), HttpStatus.OK);			
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	

	/**
	 * 
	 * @param matricula
	 * @return ResponseEntity
	 * 
	 * EndPoint responsável por consultar uma solicitacao usando a matricula do usuário.
	 * 
	 */
	@CrossOrigin
	@GetMapping
	public ResponseEntity consultarSolicitacaoPorStatus() {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.consultarSolicPorUsuario(), HttpStatus.OK);			
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param adiantamentoVO
	 * @return ResponseEntity
	 * 
	 * EndPoint responsável salvar na base do projur um adiantamento.
	 * 
	 */
	@PostMapping
	public ResponseEntity cadastrarSolicitacaoPagamento(@RequestBody SolicitacaoPagamentoVO solicitacaoPagamentoVO) {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.cadastrarSolicitacaoPagamento(solicitacaoPagamentoVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return ResponseEntity
	 * 
	 * Endpoint responsavel por reenviar a solicitação de pagamento para geração do documento SAP.
	 * 
	 */
	@PutMapping(value = "/reenviar/{id}")
	public ResponseEntity reenviarSolicitacaoPagamento(@PathVariable @NotNull Long id) {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.reenviarSolicitacaoPagamento(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Endpoint responsável por consultar o status da solicitação de pagamento no SAP.
	 * Caso tenho alterado o status no SAP. Atualizamos nossa base de dados.
	 * @param id
	 * @return
	 */
	@PutMapping(value = "/consultarSolicitacaoSAP/{id}")
	public ResponseEntity consultarSituacaoSolicitacaoPagamento(@PathVariable @NotNull Long id) {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.consultarSituacaoSolicitacaoPagamento(id), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping
	public ResponseEntity atualizarSolicitacaoPagamento(@RequestBody SolicitacaoPagamentoVO solicitacaoPendenteVO) {
		try {
			return new ResponseEntity(solicitacaoPagamentoServico.atualizarSolicitacaoPagamentoPendente(solicitacaoPendenteVO), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Endpoint responsável por consultar Pagamentos.
	 * @param PaginacaoVO<?> paginaBusca
	 * @return  ResponseEntity<PaginacaoVO<SolicitacaoPagamentoVO>>
	 * 
	 */
	@PostMapping(value = "/consulta")
	public ResponseEntity<PaginacaoVO<SolicitacaoPagamentoVO>> consultar(@RequestBody PaginacaoVO<?> paginaBusca) {
		try {
			return new ResponseEntity<>(solicitacaoPagamentoServico.consultar(paginaBusca), HttpStatus.OK);
		} catch (NegocioException e) {
			return new ResponseEntity(e.getMensagemNegocioVO(), HttpStatus.BAD_REQUEST);
		}			
	}


}
