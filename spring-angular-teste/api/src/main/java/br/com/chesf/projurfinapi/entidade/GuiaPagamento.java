package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_GUIA_PAGM", schema="PROJUR")
@SequenceGenerator(name = "SQ_GUIA_PAGM", sequenceName = "SQ_GUIA_PAGM", allocationSize = 1)
public class GuiaPagamento implements Serializable{
	
	private static final long serialVersionUID = 3958698255904683121L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_GUIA_PAGM")
    @Column(name = "ID_GUIA_PAGM")
	private Long id;
	
	@Column(name="CD_MATR", length = 6, nullable = false)
	private String usuario;	
	
	@Column(name = "ID_DOCM_SAP", length = 10, nullable = true)
	private String documentoSAP;
	
	@Column(name = "DT_GUIA", nullable = false)
	private LocalDateTime dataGuia;
	
	@OneToOne(targetEntity = CentroDeCusto.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CENTR_CUST", referencedColumnName = "ID_CENTR_CUST", nullable = true)
	private CentroDeCusto centroDeCusto;
	
	@OneToOne(targetEntity = ElementoPep.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ELEMENTO_PEP", referencedColumnName = "ID_ELEMENTO_PEP", nullable = true)
	private ElementoPep elementoPep;
	
	@OneToOne(targetEntity = Ordem.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ORDEM", referencedColumnName = "ID_ORDEM", nullable = true)
	private Ordem ordem;
	
	@OneToOne(targetEntity = OrgaoGestor.class)
	@JoinColumn(name = "ID_ORGAO_GESTOR", referencedColumnName = "ID_ORGAO_GESTOR", nullable = false)
	private OrgaoGestor orgaoGestor;
	
	@Column(name = "DT_PRAZO")
	private LocalDate prazo;
	
	@OneToOne(targetEntity = Parte.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "CD_PARTE_AUTOR", referencedColumnName = "CD_PARTE", nullable = false)
	private Parte autor;
	
	@OneToOne(targetEntity = Parte.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "CD_PARTE_REU", referencedColumnName = "CD_PARTE", nullable = false)
	private Parte reu;


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the documentoSAP
	 */
	public String getDocumentoSAP() {
		return documentoSAP;
	}

	/**
	 * @param documentoSAP the documentoSAP to set
	 */
	public void setDocumentoSAP(String documentoSAP) {
		this.documentoSAP = documentoSAP;
	}

	/**
	 * @return the dataGuia
	 */
	public LocalDateTime getDataGuia() {
		return dataGuia;
	}

	/**
	 * @param dataGuia the dataGuia to set
	 */
	public void setDataGuia(LocalDateTime dataGuia) {
		this.dataGuia = dataGuia;
	}

	/**
	 * @return the centroDeCusto
	 */
	public CentroDeCusto getCentroDeCusto() {
		return centroDeCusto;
	}

	/**
	 * @param centroDeCusto the centroDeCusto to set
	 */
	public void setCentroDeCusto(CentroDeCusto centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	/**
	 * @return the elementoPep
	 */
	public ElementoPep getElementoPep() {
		return elementoPep;
	}

	/**
	 * @param elementoPep the elementoPep to set
	 */
	public void setElementoPep(ElementoPep elementoPep) {
		this.elementoPep = elementoPep;
	}

	/**
	 * @return the ordem
	 */
	public Ordem getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem the ordem to set
	 */
	public void setOrdem(Ordem ordem) {
		this.ordem = ordem;
	}

	/**
	 * @return the orgaoGestor
	 */
	public OrgaoGestor getOrgaoGestor() {
		return orgaoGestor;
	}

	/**
	 * @param orgaoGestor the orgaoGestor to set
	 */
	public void setOrgaoGestor(OrgaoGestor orgaoGestor) {
		this.orgaoGestor = orgaoGestor;
	}

	/**
	 * @return the prazo
	 */
	public LocalDate getPrazo() {
		return prazo;
	}

	/**
	 * @param prazo the prazo to set
	 */
	public void setPrazo(LocalDate prazo) {
		this.prazo = prazo;
	}

	/**
	 * @return the autor
	 */
	public Parte getAutor() {
		return autor;
	}

	/**
	 * @param autor the autor to set
	 */
	public void setAutor(Parte autor) {
		this.autor = autor;
	}

	/**
	 * @return the reu
	 */
	public Parte getReu() {
		return reu;
	}

	/**
	 * @param reu the reu to set
	 */
	public void setReu(Parte reu) {
		this.reu = reu;
	}

}
