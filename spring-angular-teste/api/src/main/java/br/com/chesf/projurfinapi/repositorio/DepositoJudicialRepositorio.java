package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import br.com.chesf.projurfinapi.entidade.DepositoJudicial;

/**
 * 
 * @author jvneto1
 * 
 * Classe responsavel por persistir o recurso DepositoJudicial.
 *
 */
public interface DepositoJudicialRepositorio extends JpaRepository<DepositoJudicial, Long>, QueryByExampleExecutor<DepositoJudicial>{
	
	List<DepositoJudicial> findByProcesso(Sort sort, String numeroProcesso);
	
}
