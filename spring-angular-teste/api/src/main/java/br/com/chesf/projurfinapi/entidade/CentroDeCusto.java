package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o recurso CentroDeCusto
 *
 */
@Entity
@Table(name = "TB_CENTR_CUST", schema="PROJUR", uniqueConstraints = {@UniqueConstraint(columnNames={"CD_CENTR_CUST"})})
@SequenceGenerator(name = "SQ_CENTR_CUST", sequenceName = "SQ_CENTR_CUST", allocationSize = 1)
public class CentroDeCusto implements Serializable{

	private static final long serialVersionUID = -6897902167213994206L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CENTR_CUST")
    @Column(name = "ID_CENTR_CUST")
    private Long id;
	
	
	@Column(name = "CD_CENTR_CUST", length = 10, nullable = false)
	private String codigo;
	
	@Column(name = "DS_CENTR_CUST", length = 50, nullable = false)
	private String descricao;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
