package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConsultaDepositoJudicialVO implements Serializable{

	private static final long serialVersionUID = 2484722687805178587L;

	private BigDecimal totalDepositado;
	
	private BigDecimal totalPagoLitigante;
	
	private BigDecimal saldoDepositado;
	
	private BigDecimal totalProvisionado;
	
	private BigDecimal totalPagoChesf;

	public ConsultaDepositoJudicialVO() {
		super();
		this.totalDepositado = BigDecimal.ZERO;
		this.totalPagoLitigante = BigDecimal.ZERO;
		this.saldoDepositado = BigDecimal.ZERO;
		this.totalProvisionado = BigDecimal.ZERO;
		this.totalPagoChesf = BigDecimal.ZERO;
	}
	
	public void somarTotalDepositado(BigDecimal valorNovo) {
		this.totalDepositado = this.totalDepositado.add(valorNovo);
	}
	
	public void somarTotalPagoLitigante(BigDecimal valorNovo) {
		if(valorNovo == null) {
			this.totalPagoLitigante =  this.totalPagoLitigante.add(BigDecimal.ZERO);
		}else {
			this.totalPagoLitigante =  this.totalPagoLitigante.add(valorNovo);			
		}
	}
	
	public void somarTotalPagoChesf(BigDecimal valorNovo) {
		if(valorNovo == null) {
			this.totalPagoChesf =  this.totalPagoChesf.add(BigDecimal.ZERO);
		}else {
			this.totalPagoChesf =  this.totalPagoChesf.add(valorNovo);			
		}
	}
	
	public void montarSaldoDepositado() {
		this.saldoDepositado = this.totalDepositado.subtract(this.totalPagoLitigante.add(this.totalPagoChesf));
	}

	/**
	 * @return the totalDepositado
	 */
	public BigDecimal getTotalDepositado() {
		return totalDepositado;
	}

	/**
	 * @param totalDepositado the totalDepositado to set
	 */
	public void setTotalDepositado(BigDecimal totalDepositado) {
		this.totalDepositado = totalDepositado;
	}

	/**
	 * @return the totalPagoLitigante
	 */
	public BigDecimal getTotalPagoLitigante() {
		return totalPagoLitigante;
	}

	/**
	 * @param totalPagoLitigante the totalPagoLitigante to set
	 */
	public void setTotalPagoLitigante(BigDecimal totalPagoLitigante) {
		this.totalPagoLitigante = totalPagoLitigante;
	}

	/**
	 * @return the saldoDepositado
	 */
	public BigDecimal getSaldoDepositado() {
		return saldoDepositado;
	}

	/**
	 * @param saldoDepositado the saldoDepositado to set
	 */
	public void setSaldoDepositado(BigDecimal saldoDepositado) {
		this.saldoDepositado = saldoDepositado;
	}

	/**
	 * @return the totalProvisionado
	 */
	public BigDecimal getTotalProvisionado() {
		return totalProvisionado;
	}

	/**
	 * @param totalProvisionado the totalProvisionado to set
	 */
	public void setTotalProvisionado(BigDecimal totalProvisionado) {
		this.totalProvisionado = totalProvisionado;
	}

	/**
	 * @return the totalPagoChesf
	 */
	public BigDecimal getTotalPagoChesf() {
		return totalPagoChesf;
	}

	/**
	 * @param totalPagoChesf the totalPagoChesf to set
	 */
	public void setTotalPagoChesf(BigDecimal totalPagoChesf) {
		this.totalPagoChesf = totalPagoChesf;
	}
	
}
