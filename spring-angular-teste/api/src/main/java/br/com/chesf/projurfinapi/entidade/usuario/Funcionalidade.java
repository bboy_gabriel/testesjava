package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

/**
 * 
 * @author isfigueiredo
 *
 */
public class Funcionalidade implements Serializable{

	private static final long serialVersionUID = -4836488528724053232L;
	
	private Long id;
	private String descFuncionalidade;
	private String urlFuncionalidade;
	private Modulo modulo;
	private Grupo grupo;
	private Long numOrden;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescFuncionalidade() {
		return descFuncionalidade;
	}
	public void setDescFuncionalidade(String descFuncionalidade) {
		this.descFuncionalidade = descFuncionalidade;
	}
	public String getUrlFuncionalidade() {
		return urlFuncionalidade;
	}
	public void setUrlFuncionalidade(String urlFuncionalidade) {
		this.urlFuncionalidade = urlFuncionalidade;
	}
	public Modulo getModulo() {
		return modulo;
	}
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Long getNumOrden() {
		return numOrden;
	}
	public void setNumOrden(Long numOrden) {
		this.numOrden = numOrden;
	}

}
