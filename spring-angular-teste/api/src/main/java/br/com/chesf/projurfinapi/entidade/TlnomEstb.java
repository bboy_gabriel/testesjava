package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TLNOM_ESTB", schema = "TLNOM")
@SequenceGenerator(name = "SQ_TLNOM_ESTB", sequenceName = "SQ_TLNOM_ESTB", allocationSize = 1)
public class TlnomEstb implements Serializable{

	private static final long serialVersionUID = -7025693690882700087L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TLNOM_ESTB")
	@Column(name = "PK_ESTB")
	private Long id;
	
	@Column(name = "CD_ESTB", length = 4)
	private String codigoEstb;
	
	@Column(name = "DS_ESTB", length = 50)
	private String descricaoEstb;
	
	@Column(name = "NR_CGC", length = 15)
	private Long numeroCgc;
	
	@Column(name = "DS_ENDR", length = 50)
	private String descricaoEndr;
	
	@Column(name = "NM_CIDD", length = 20)
	private String nomeCidd;
	
	@Column(name = "NR_CEP", length = 8)
	private String numeroCep;
	
	@Column(name = "SG_ESTD", length = 2)
	private String siglaEstd;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigoEstb
	 */
	public String getCodigoEstb() {
		return codigoEstb;
	}

	/**
	 * @param codigoEstb the codigoEstb to set
	 */
	public void setCodigoEstb(String codigoEstb) {
		this.codigoEstb = codigoEstb;
	}

	/**
	 * @return the descricaoEstb
	 */
	public String getDescricaoEstb() {
		return descricaoEstb;
	}

	/**
	 * @param descricaoEstb the descricaoEstb to set
	 */
	public void setDescricaoEstb(String descricaoEstb) {
		this.descricaoEstb = descricaoEstb;
	}

	/**
	 * @return the numeroCgc
	 */
	public Long getNumeroCgc() {
		return numeroCgc;
	}

	/**
	 * @param numeroCgc the numeroCgc to set
	 */
	public void setNumeroCgc(Long numeroCgc) {
		this.numeroCgc = numeroCgc;
	}

	/**
	 * @return the descricaoEndr
	 */
	public String getDescricaoEndr() {
		return descricaoEndr;
	}

	/**
	 * @param descricaoEndr the descricaoEndr to set
	 */
	public void setDescricaoEndr(String descricaoEndr) {
		this.descricaoEndr = descricaoEndr;
	}

	/**
	 * @return the nomeCidd
	 */
	public String getNomeCidd() {
		return nomeCidd;
	}

	/**
	 * @param nomeCidd the nomeCidd to set
	 */
	public void setNomeCidd(String nomeCidd) {
		this.nomeCidd = nomeCidd;
	}

	/**
	 * @return the numeroCep
	 */
	public String getNumeroCep() {
		return numeroCep;
	}

	/**
	 * @param numeroCep the numeroCep to set
	 */
	public void setNumeroCep(String numeroCep) {
		this.numeroCep = numeroCep;
	}

	/**
	 * @return the siglaEstd
	 */
	public String getSiglaEstd() {
		return siglaEstd;
	}

	/**
	 * @param siglaEstd the siglaEstd to set
	 */
	public void setSiglaEstd(String siglaEstd) {
		this.siglaEstd = siglaEstd;
	}
	
}
