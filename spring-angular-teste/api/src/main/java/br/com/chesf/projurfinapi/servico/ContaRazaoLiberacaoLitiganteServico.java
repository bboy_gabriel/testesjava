package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.ContaRazaoLiberacaoLitigante;
import br.com.chesf.projurfinapi.entidade.vo.ContaRazaoLiberacaoLitiganteVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ContaRazaoLiberacaoLitiganteRepositorio;

/**
 * 
 * Classe responsavel por fornecer serviços referentes ao recurso ContaRazaoLiberacaoLitigante.
 * 
 * @author jvneto1
 *
 */
@Service
public class ContaRazaoLiberacaoLitiganteServico implements Serializable{

	private static final long serialVersionUID = -5926451771524011962L;

	@Autowired
	private ContaRazaoLiberacaoLitiganteRepositorio contaRazaoLiberacaoLitiganteRepositorio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/**
	 * 
	 * Serviço responsavel por consultar e retornar uma lista referente ao recurso ContaRazaoLiberacaoLitigante.
	 * 
	 * @return List<ContaRazaoLiberacaoLitiganteVO>
	 * 
	 */
	public List<ContaRazaoLiberacaoLitiganteVO> consultarContaRazaoLiberacaoLitigante(){
		try {
			return converToListaVO(contaRazaoLiberacaoLitiganteRepositorio.findAll());
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_CONTA_RAZAO_LIBERACAO_LITIGANTE);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsavel por converter uma lista ContaRazaoLiberacaoLitigante para uma lista ContaRazaoLiberacaoLitiganteVO.
	 * 
	 * @param listaBase
	 * @return List<ContaRazaoLiberacaoLitiganteVO>
	 * 
	 */
	private List<ContaRazaoLiberacaoLitiganteVO> converToListaVO(List<ContaRazaoLiberacaoLitigante> listaBase){
		List<ContaRazaoLiberacaoLitiganteVO> listaVO = new ArrayList<>();
		for (ContaRazaoLiberacaoLitigante conta : listaBase) {
			ContaRazaoLiberacaoLitiganteVO vo = new ContaRazaoLiberacaoLitiganteVO();
			vo = modelMapper.map(conta, ContaRazaoLiberacaoLitiganteVO.class);
			vo.setTipoProcessoProvisaoEnum(TipoProcessoContaRazaoEnum.getTipoProcessoContaRazaoEnumPorID(Integer.parseInt(conta.getCodigoTipoProcesso())));
			listaVO.add(vo);
		}
		return listaVO;
	}

}
