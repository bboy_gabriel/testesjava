package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class LancamentoContabilVO implements Serializable{

	private static final long serialVersionUID = 8735888305492639576L;

	private Long id;
	
	private String tipoLancamento;
	
	private String tipoLancamentoContabilDescricao;
	
	@NotNull(message = "Número do processo nulo ou inválido!")
	private Long numeroProcesso;
	
	@NotEmpty(message = "Data do lançamento nula ou inválida!")
	private String dataLancamento;

	@NotNull(message = "Valor nulo ou inválido!")
	private BigDecimal valor;
	
	@NotNull(message = "Conta de crédito nula ou inválida!")
	private Long contaCredito;
	
	@NotNull(message = "Conta de débito nula ou inválida!")
	private Long contaDebito;
	
	@NotNull(message = "Centro de custo nulo ou inválido!")
	private CentroDeCustoVO centroDeCusto;
	
	private ElementoPepVO elementoPep;
	
	private OrdemVO ordem;
	
	@NotEmpty(message = "Historico nulo ou inválido!")
	private String historico;
	
	private String dataGeracaoCSV;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the tipoLancamento
	 */
	public String getTipoLancamento() {
		return tipoLancamento;
	}

	/**
	 * @param tipoLancamento the tipoLancamento to set
	 */
	public void setTipoLancamento(String tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	/**
	 * @return the tipoLancamentoContabilDescricao
	 */
	public String getTipoLancamentoContabilDescricao() {
		return tipoLancamentoContabilDescricao;
	}

	/**
	 * @param tipoLancamentoContabilDescricao the tipoLancamentoContabilDescricao to set
	 */
	public void setTipoLancamentoContabilDescricao(String tipoLancamentoContabilDescricao) {
		this.tipoLancamentoContabilDescricao = tipoLancamentoContabilDescricao;
	}

	/**
	 * @return the numeroProcesso
	 */
	public Long getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(Long numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	/**
	 * @return the dataLancamento
	 */
	public String getDataLancamento() {
		return dataLancamento;
	}

	/**
	 * @param dataLancamento the dataLancamento to set
	 */
	public void setDataLancamento(String dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the contaCredito
	 */
	public Long getContaCredito() {
		return contaCredito;
	}

	/**
	 * @param contaCredito the contaCredito to set
	 */
	public void setContaCredito(Long contaCredito) {
		this.contaCredito = contaCredito;
	}

	/**
	 * @return the contaDebito
	 */
	public Long getContaDebito() {
		return contaDebito;
	}

	/**
	 * @param contaDebito the contaDebito to set
	 */
	public void setContaDebito(Long contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * @return the centroDeCusto
	 */
	public CentroDeCustoVO getCentroDeCusto() {
		return centroDeCusto;
	}

	/**
	 * @param centroDeCusto the centroDeCusto to set
	 */
	public void setCentroDeCusto(CentroDeCustoVO centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	/**
	 * @return the elementoPep
	 */
	public ElementoPepVO getElementoPep() {
		return elementoPep;
	}

	/**
	 * @param elementoPep the elementoPep to set
	 */
	public void setElementoPep(ElementoPepVO elementoPep) {
		this.elementoPep = elementoPep;
	}

	/**
	 * @return the ordem
	 */
	public OrdemVO getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem the ordem to set
	 */
	public void setOrdem(OrdemVO ordem) {
		this.ordem = ordem;
	}

	/**
	 * @return the historico
	 */
	public String getHistorico() {
		return historico;
	}

	/**
	 * @param historico the historico to set
	 */
	public void setHistorico(String historico) {
		this.historico = historico;
	}

	/**
	 * @return the dataGeracaoCSV
	 */
	public String getDataGeracaoCSV() {
		return dataGeracaoCSV;
	}

	/**
	 * @param dataGeracaoCSV the dataGeracaoCSV to set
	 */
	public void setDataGeracaoCSV(String dataGeracaoCSV) {
		this.dataGeracaoCSV = dataGeracaoCSV;
	}
	
}
