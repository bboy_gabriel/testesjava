package br.com.chesf.projurfinapi.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.FornecedorSap;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer uma camada de persistencia ao recurso FornecedorSap.
 *
 */
@Repository
public interface FornecedorSapRepositorio extends JpaRepository<FornecedorSap, Long>, QueryByExampleExecutor<FornecedorSap>{
	
	public Boolean existsByCodigo(Long codigo);
	
	public Boolean existsByCnpj(String cnpj);
			
	public Boolean existsByCodigoAndIdNotIn(Long codigo,Long ... ids);
	
	public Boolean existsByCnpjAndIdNotIn(String cnpj, Long ... ids);

}
