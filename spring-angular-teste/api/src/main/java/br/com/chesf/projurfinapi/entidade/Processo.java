package br.com.chesf.projurfinapi.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por mapear o recurso Processo.
 *
 */
@Entity
@Table(name = "TB_PROCESSO", schema = "PROJUR")
@SequenceGenerator(name = "SQ_PROCESSO", sequenceName = "SQ_PROCESSO", allocationSize = 1)
public class Processo implements Serializable{

	private static final long serialVersionUID = 6715709080435860459L;
	
	@Id
	@Column(name = "CD_PROCESSO", nullable = false)
	private Long codigoProcesso;
	
	@Column(name = "NR_PROCESSO", length = 50, nullable = false)
	private String numeroProcesso;
	
	@Column(name = "CD_TIPO_PROCESSO", length = 10, nullable = true)
	private Long codigoTipoProcesso;
	
	@Column(name = "CD_TIPO_ACAO", nullable = true)
	private Long codigoTipoAcao;
	
	@Column(name = "VL_PEDIDO", length = 14, nullable = true)
	private BigDecimal valorPedido;
	
	@Column(name = "VL_ESTIMADO", length = 14, nullable = true)
	private BigDecimal valorEstimado;
	
	@Column(name = "CD_ORGAO_JULGADOR", nullable = true)
	private Long codigoOrgaoJulgador;
	
	@Column(name = "CD_MUNICIPIO", length = 10, nullable = true)
	private Long codigoMunicipio;
	
	@Column(name = "DS_VARA", length = 2000, nullable = true)
	private String descricaoVara;
	
	@Column(name = "DS_ACAO", length = 200, nullable = true)
	private String descricaoAcao;
	
	@Column(name = "ID_RISCO", nullable = true)
	private String idRisco;
	
	@Column(name = "CD_MATR_ADVOGADO", nullable = true)
	private String codigoMatrizAdvogado;
	
	@Column(name = "ID_PRINCIPAL", nullable = true)
	private String idPrincipal;
	
	@Column(name = "CD_ORGAO_REQ", nullable = true)
	private String codigoOrgaoReq;
	
	@Column(name = "ID_STATUS", nullable = true)
	private String idStatus;
	
	@Column(name = "SG_ESTADO", nullable = true)
	private String siglaEstado;
	
	@Column(name = "CD_ESCRITORIO_TERCEIRO", nullable = true)
	private Long codigoEscritorioTerceiro;
	
	@Column(name = "NR_PROCESSO_SIJUS", length = 8, nullable = true)
	private Long numeroProcessoSijus;
	
	@Column(name = "NR_DIG_VERF_SIJUS", length = 1, nullable = true)
	private Long numeroDigVerfSijus;
	
	@Column(name = "DT_AUTUACAO", nullable = true)
	private LocalDate dataAutuacao;
	
	@Column(name = "ID_RELEVANTE", nullable = true)
	private String idRelevante;
	
	@Column(name = "CD_ESCRITORIO_CHESF", nullable = true)
	private String codigoEscritorioChesf;
	
	@Column(name = "DS_ORGAO_JULG_ADM", length = 200, nullable = true)
	private String descricaoOrgaoJulgAdm;
	
	@Column(name = "DT_ANO_PRCS", length = 4, nullable = true)
	private Integer dataAnoPrcs;
	
	@Column(name = "ID_COMPLEXIDADE", nullable = true)
	private Long idComplexidade;
	
	@Column(name = "DS_CRITERIO_VL_RISCO", length = 200, nullable = true)
	private String descricaoCriterioValorRisco;
	
	@Column(name = "DT_CIENTE", nullable = true)
	private LocalDate dataCliente;
	
	@Column(name = "ID_SITUACAO_CIENTE", nullable = true)
	private String idSituacaoCliente;
	
	@Column(name = "ID_MIGRACAO_SIJUS", nullable = true)
	private String idMigracaoSijus;
	
	@Column(name = "ID_FISCAL", nullable = true)
	private String idFiscal;
	
	@Column(name = "ID_SOL_ENCERRAMENTO", nullable = true)
	private String idSolEncerramento;
	
	@Column(name = "DT_AJUIZAMENTO", nullable = true)
	private LocalDate dataAjuizamento;
	
	@Column(name = "VL_SENTENCA", length = 14, nullable = true)
	private BigDecimal valorSentenca;
	
	@Column(name = "NM_APELIDO_PARTE", length = 150, nullable = true)
	private String nomeApelidoParte;
	
	@Column(name = "ID_LOCAD", nullable = true)
	private Long idLocad;
	
	@Column(name = "TP_ENCR", nullable = true)
	private String tipEncr;

	/**
	 * @return the codigoProcesso
	 */
	public Long getCodigoProcesso() {
		return codigoProcesso;
	}

	/**
	 * @param codigoProcesso the codigoProcesso to set
	 */
	public void setCodigoProcesso(Long codigoProcesso) {
		this.codigoProcesso = codigoProcesso;
	}

	/**
	 * @return the numeroProcesso
	 */
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	/**
	 * @param numeroProcesso the numeroProcesso to set
	 */
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	/**
	 * @return the codigoTipoProcesso
	 */
	public Long getCodigoTipoProcesso() {
		return codigoTipoProcesso;
	}

	/**
	 * @param codigoTipoProcesso the codigoTipoProcesso to set
	 */
	public void setCodigoTipoProcesso(Long codigoTipoProcesso) {
		this.codigoTipoProcesso = codigoTipoProcesso;
	}

	/**
	 * @return the codigoTipoAcao
	 */
	public Long getCodigoTipoAcao() {
		return codigoTipoAcao;
	}

	/**
	 * @param codigoTipoAcao the codigoTipoAcao to set
	 */
	public void setCodigoTipoAcao(Long codigoTipoAcao) {
		this.codigoTipoAcao = codigoTipoAcao;
	}

	/**
	 * @return the valorPedido
	 */
	public BigDecimal getValorPedido() {
		return valorPedido;
	}

	/**
	 * @param valorPedido the valorPedido to set
	 */
	public void setValorPedido(BigDecimal valorPedido) {
		this.valorPedido = valorPedido;
	}

	/**
	 * @return the valorEstimado
	 */
	public BigDecimal getValorEstimado() {
		return valorEstimado;
	}

	/**
	 * @param valorEstimado the valorEstimado to set
	 */
	public void setValorEstimado(BigDecimal valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	/**
	 * @return the codigoOrgaoJulgador
	 */
	public Long getCodigoOrgaoJulgador() {
		return codigoOrgaoJulgador;
	}

	/**
	 * @param codigoOrgaoJulgador the codigoOrgaoJulgador to set
	 */
	public void setCodigoOrgaoJulgador(Long codigoOrgaoJulgador) {
		this.codigoOrgaoJulgador = codigoOrgaoJulgador;
	}

	/**
	 * @return the codigoMunicipio
	 */
	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}

	/**
	 * @param codigoMunicipio the codigoMunicipio to set
	 */
	public void setCodigoMunicipio(Long codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	/**
	 * @return the descricaoVara
	 */
	public String getDescricaoVara() {
		return descricaoVara;
	}

	/**
	 * @param descricaoVara the descricaoVara to set
	 */
	public void setDescricaoVara(String descricaoVara) {
		this.descricaoVara = descricaoVara;
	}

	/**
	 * @return the descricaoAcao
	 */
	public String getDescricaoAcao() {
		return descricaoAcao;
	}

	/**
	 * @param descricaoAcao the descricaoAcao to set
	 */
	public void setDescricaoAcao(String descricaoAcao) {
		this.descricaoAcao = descricaoAcao;
	}

	/**
	 * @return the idRisco
	 */
	public String getIdRisco() {
		return idRisco;
	}

	/**
	 * @param idRisco the idRisco to set
	 */
	public void setIdRisco(String idRisco) {
		this.idRisco = idRisco;
	}

	/**
	 * @return the codigoMatrizAdvogado
	 */
	public String getCodigoMatrizAdvogado() {
		return codigoMatrizAdvogado;
	}

	/**
	 * @param codigoMatrizAdvogado the codigoMatrizAdvogado to set
	 */
	public void setCodigoMatrizAdvogado(String codigoMatrizAdvogado) {
		this.codigoMatrizAdvogado = codigoMatrizAdvogado;
	}

	/**
	 * @return the idPrincipal
	 */
	public String getIdPrincipal() {
		return idPrincipal;
	}

	/**
	 * @param idPrincipal the idPrincipal to set
	 */
	public void setIdPrincipal(String idPrincipal) {
		this.idPrincipal = idPrincipal;
	}

	/**
	 * @return the codigoOrgaoReq
	 */
	public String getCodigoOrgaoReq() {
		return codigoOrgaoReq;
	}

	/**
	 * @param codigoOrgaoReq the codigoOrgaoReq to set
	 */
	public void setCodigoOrgaoReq(String codigoOrgaoReq) {
		this.codigoOrgaoReq = codigoOrgaoReq;
	}

	/**
	 * @return the idStatus
	 */
	public String getIdStatus() {
		return idStatus;
	}

	/**
	 * @param idStatus the idStatus to set
	 */
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	/**
	 * @return the siglaEstado
	 */
	public String getSiglaEstado() {
		return siglaEstado;
	}

	/**
	 * @param siglaEstado the siglaEstado to set
	 */
	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	/**
	 * @return the codigoEscritorioTerceiro
	 */
	public Long getCodigoEscritorioTerceiro() {
		return codigoEscritorioTerceiro;
	}

	/**
	 * @param codigoEscritorioTerceiro the codigoEscritorioTerceiro to set
	 */
	public void setCodigoEscritorioTerceiro(Long codigoEscritorioTerceiro) {
		this.codigoEscritorioTerceiro = codigoEscritorioTerceiro;
	}

	/**
	 * @return the numeroProcessoSijus
	 */
	public Long getNumeroProcessoSijus() {
		return numeroProcessoSijus;
	}

	/**
	 * @param numeroProcessoSijus the numeroProcessoSijus to set
	 */
	public void setNumeroProcessoSijus(Long numeroProcessoSijus) {
		this.numeroProcessoSijus = numeroProcessoSijus;
	}

	/**
	 * @return the numeroDigVerfSijus
	 */
	public Long getNumeroDigVerfSijus() {
		return numeroDigVerfSijus;
	}

	/**
	 * @param numeroDigVerfSijus the numeroDigVerfSijus to set
	 */
	public void setNumeroDigVerfSijus(Long numeroDigVerfSijus) {
		this.numeroDigVerfSijus = numeroDigVerfSijus;
	}

	/**
	 * @return the dataAutuacao
	 */
	public LocalDate getDataAutuacao() {
		return dataAutuacao;
	}

	/**
	 * @param dataAutuacao the dataAutuacao to set
	 */
	public void setDataAutuacao(LocalDate dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	/**
	 * @return the idRelevante
	 */
	public String getIdRelevante() {
		return idRelevante;
	}

	/**
	 * @param idRelevante the idRelevante to set
	 */
	public void setIdRelevante(String idRelevante) {
		this.idRelevante = idRelevante;
	}

	/**
	 * @return the codigoEscritorioChesf
	 */
	public String getCodigoEscritorioChesf() {
		return codigoEscritorioChesf;
	}

	/**
	 * @param codigoEscritorioChesf the codigoEscritorioChesf to set
	 */
	public void setCodigoEscritorioChesf(String codigoEscritorioChesf) {
		this.codigoEscritorioChesf = codigoEscritorioChesf;
	}

	/**
	 * @return the descricaoOrgaoJulgAdm
	 */
	public String getDescricaoOrgaoJulgAdm() {
		return descricaoOrgaoJulgAdm;
	}

	/**
	 * @param descricaoOrgaoJulgAdm the descricaoOrgaoJulgAdm to set
	 */
	public void setDescricaoOrgaoJulgAdm(String descricaoOrgaoJulgAdm) {
		this.descricaoOrgaoJulgAdm = descricaoOrgaoJulgAdm;
	}

	/**
	 * @return the dataAnoPrcs
	 */
	public Integer getDataAnoPrcs() {
		return dataAnoPrcs;
	}

	/**
	 * @param dataAnoPrcs the dataAnoPrcs to set
	 */
	public void setDataAnoPrcs(Integer dataAnoPrcs) {
		this.dataAnoPrcs = dataAnoPrcs;
	}

	/**
	 * @return the idComplexidade
	 */
	public Long getIdComplexidade() {
		return idComplexidade;
	}

	/**
	 * @param idComplexidade the idComplexidade to set
	 */
	public void setIdComplexidade(Long idComplexidade) {
		this.idComplexidade = idComplexidade;
	}

	/**
	 * @return the descricaoCriterioValorRisco
	 */
	public String getDescricaoCriterioValorRisco() {
		return descricaoCriterioValorRisco;
	}

	/**
	 * @param descricaoCriterioValorRisco the descricaoCriterioValorRisco to set
	 */
	public void setDescricaoCriterioValorRisco(String descricaoCriterioValorRisco) {
		this.descricaoCriterioValorRisco = descricaoCriterioValorRisco;
	}

	/**
	 * @return the dataCliente
	 */
	public LocalDate getDataCliente() {
		return dataCliente;
	}

	/**
	 * @param dataCliente the dataCliente to set
	 */
	public void setDataCliente(LocalDate dataCliente) {
		this.dataCliente = dataCliente;
	}

	/**
	 * @return the idSituacaoCliente
	 */
	public String getIdSituacaoCliente() {
		return idSituacaoCliente;
	}

	/**
	 * @param idSituacaoCliente the idSituacaoCliente to set
	 */
	public void setIdSituacaoCliente(String idSituacaoCliente) {
		this.idSituacaoCliente = idSituacaoCliente;
	}

	/**
	 * @return the idMigracaoSijus
	 */
	public String getIdMigracaoSijus() {
		return idMigracaoSijus;
	}

	/**
	 * @param idMigracaoSijus the idMigracaoSijus to set
	 */
	public void setIdMigracaoSijus(String idMigracaoSijus) {
		this.idMigracaoSijus = idMigracaoSijus;
	}

	/**
	 * @return the idFiscal
	 */
	public String getIdFiscal() {
		return idFiscal;
	}

	/**
	 * @param idFiscal the idFiscal to set
	 */
	public void setIdFiscal(String idFiscal) {
		this.idFiscal = idFiscal;
	}

	/**
	 * @return the idSolEncerramento
	 */
	public String getIdSolEncerramento() {
		return idSolEncerramento;
	}

	/**
	 * @param idSolEncerramento the idSolEncerramento to set
	 */
	public void setIdSolEncerramento(String idSolEncerramento) {
		this.idSolEncerramento = idSolEncerramento;
	}

	/**
	 * @return the dataAjuizamento
	 */
	public LocalDate getDataAjuizamento() {
		return dataAjuizamento;
	}

	/**
	 * @param dataAjuizamento the dataAjuizamento to set
	 */
	public void setDataAjuizamento(LocalDate dataAjuizamento) {
		this.dataAjuizamento = dataAjuizamento;
	}

	/**
	 * @return the valorSentenca
	 */
	public BigDecimal getValorSentenca() {
		return valorSentenca;
	}

	/**
	 * @param valorSentenca the valorSentenca to set
	 */
	public void setValorSentenca(BigDecimal valorSentenca) {
		this.valorSentenca = valorSentenca;
	}

	/**
	 * @return the nomePelidoParte
	 */
	public String getNomeApelidoParte() {
		return nomeApelidoParte;
	}

	/**
	 * @param nomePelidoParte the nomePelidoParte to set
	 */
	public void setNomeApelidoParte(String nomePelidoParte) {
		this.nomeApelidoParte = nomePelidoParte;
	}

	/**
	 * @return the idLocad
	 */
	public Long getIdLocad() {
		return idLocad;
	}

	/**
	 * @param idLocad the idLocad to set
	 */
	public void setIdLocad(Long idLocad) {
		this.idLocad = idLocad;
	}

	/**
	 * @return the tipEncr
	 */
	public String getTipEncr() {
		return tipEncr;
	}

	/**
	 * @param tipEncr the tipEncr to set
	 */
	public void setTipEncr(String tipEncr) {
		this.tipEncr = tipEncr;
	}
	
}
