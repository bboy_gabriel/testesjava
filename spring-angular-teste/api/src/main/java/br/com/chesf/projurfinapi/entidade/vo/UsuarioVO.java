package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UsuarioVO extends User implements Serializable{


	private static final long serialVersionUID = -3898046574560327906L;
	 
	private Long id;
	private String matricula;
	private String expiration;
	
	
	public String getExpiration() {
		return expiration;
	}


	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}


	public UsuarioVO(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		
	}
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return super.getUsername();
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return super.getPassword();
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		password = password;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

}
