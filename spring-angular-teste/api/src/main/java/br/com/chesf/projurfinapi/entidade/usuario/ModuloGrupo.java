package br.com.chesf.projurfinapi.entidade.usuario;

import java.io.Serializable;

/**
 * Atrela um modulo a um grupo
 * @author isfigueiredo
 *
 */
public class ModuloGrupo implements Serializable{

	private static final long serialVersionUID = 5237909136537112321L;
	
	private Long id;
	private Long numOrdem;
	private Grupo grupo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNumOrdem() {
		return numOrdem;
	}
	public void setNumOrdem(Long numOrdem) {
		this.numOrdem = numOrdem;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}	

}
