package br.com.chesf.projurfinapi.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.chesf.projurfinapi.entidade.Processo;

/**
 * 
 * @author jvneto1
 *
 * Classe responsavel por fornecer a persistencia do recurso Processo.
 *
 */
@Repository
public interface ProcessoRepositorio extends JpaRepository<Processo, Long>, QueryByExampleExecutor<Processo>{

	@Query("select proc from Processo proc where proc.numeroProcessoSijus = :numeroProcessoSijus and proc.numeroDigVerfSijus = :digitoVerificador")
	Processo findByNumeroProcessoSijus(@Param("numeroProcessoSijus") Long numeroProcessoSijus, @Param("digitoVerificador") Long digitoVerificador);
	
	@Query(value = "SELECT * FROM PROJUR.TB_PROCESSO p JOIN PROJUR.TB_PROCESSO_PARTE ppt ON ppt.CD_PROCESSO = p.CD_PROCESSO AND ppt.TP_PARTE = '2' JOIN PROJUR.TB_PARTE pt ON pt.CD_PARTE = ppt.CD_PARTE AND pt.NR_CNPJ LIKE '33541368%' WHERE p.CD_PROCESSO = :codProc", nativeQuery = true)
	List<Processo> consultarProcessoChesfRe(@Param("codProc") Long codProc);
	
}
