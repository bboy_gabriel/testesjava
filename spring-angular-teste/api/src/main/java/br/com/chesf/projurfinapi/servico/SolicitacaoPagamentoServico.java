package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.chesf.projurfinapi.entidade.FornecedorSap;
import br.com.chesf.projurfinapi.entidade.MsgErroGeraPagamento;
import br.com.chesf.projurfinapi.entidade.OrgaoGestor;
import br.com.chesf.projurfinapi.entidade.SolicitacaoPagamento;
import br.com.chesf.projurfinapi.entidade.TipoDeDespesa;
import br.com.chesf.projurfinapi.entidade.usuario.Usuario;
import br.com.chesf.projurfinapi.entidade.vo.DomicilioBancarioVO;
import br.com.chesf.projurfinapi.entidade.vo.MsgErroGeraPagamentoVO;
import br.com.chesf.projurfinapi.entidade.vo.NumeroProcessoVO;
import br.com.chesf.projurfinapi.entidade.vo.PaginacaoVO;
import br.com.chesf.projurfinapi.entidade.vo.SolicitacaoPagamentoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.StatusPagamentoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.SolicitacaoPagamentoRepositorio;
import br.com.chesf.projurfinapi.util.Validador;

@Service
public class SolicitacaoPagamentoServico implements Serializable{

	private static final long serialVersionUID = 5518164853227413950L;
	
	@Autowired
	private SolicitacaoPagamentoRepositorio solicitacaoPagamentoRepositorio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private SapServico sapServico;
	
	@Autowired
	private OrgaoGestorServico orgaoGestorServico;
	
	@Autowired
	private FornecedorSapServico fornecedorSapServico;
	
	@Autowired
	private ProcessoServico processoServico;
	
	@Autowired
	private ListaMensagensErroGeraPagamentoServico listaMensagensErroGeraPagamentoServico;
	
	@Autowired
	private UsuarioServico usuarioServico;	
	
	@Autowired
	private EmailService emailService;
	
	Logger logger = LoggerFactory.getLogger(SolicitacaoPagamentoServico.class);
	
	
	public SolicitacaoPagamentoVO consultarPorID(Long id){
		try {
			SolicitacaoPagamento solic = solicitacaoPagamentoRepositorio.findOne(id);
			SolicitacaoPagamentoVO solicitacaoVO = convertToVO(solic);
			return solicitacaoVO;
		} catch (NegocioException e) {
			logger.error("Erro ao consultar solicitação de pagamento por id! " + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_SOLICITACAO_POR_ID);
		}
	}
	
	public SolicitacaoPagamento consultarSolicitacaoBasePorID(Long id){
		try {
			return solicitacaoPagamentoRepositorio.findOne(id);
		} catch (NegocioException e) {
			logger.error("Erro ao consultar solicitação de pagamento por id! " + e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_SOLICITACAO_POR_ID);
		}
	}
	
	public List<SolicitacaoPagamentoVO> consultarSolicPorUsuario(){
		List<SolicitacaoPagamentoVO> listaRetorno = new ArrayList<>();
		
    	List<String> listaStatusReenvio = Arrays.asList(StatusPagamentoEnum.AGUARDANDO_APROVACAO.getId().toString(), StatusPagamentoEnum.APROVADO.getId().toString(),
    			StatusPagamentoEnum.PENDENTE.getId().toString());

		List<SolicitacaoPagamento> solicitacoesBase = this.consultarSolicitacaoPagamentoStatusPagamento(listaStatusReenvio);
		if(solicitacoesBase != null && !solicitacoesBase.isEmpty()){
			for (SolicitacaoPagamento solicitacaoPagamento : solicitacoesBase) {
				SolicitacaoPagamentoVO solicitacaoVO = convertToVO(solicitacaoPagamento);
				listaRetorno.add(solicitacaoVO);				
			}
		}		
		return listaRetorno;
	}
	
	@Transactional(rollbackOn = Throwable.class)
	public SolicitacaoPagamentoVO cadastrarSolicitacaoPagamento(SolicitacaoPagamentoVO novaSolicitacao) {
		novaSolicitacao.setCodigoStatus(StatusPagamentoEnum.AGUARDANDO_APROVACAO.getId().longValue());
		SolicitacaoPagamento sol = convertToSolicitacaoPagamento(novaSolicitacao);
		//caso de reenvio
		if(sol.getId() != null) {
			sol.setListaMensagensErroGeraPagamento(
					listaMensagensErroGeraPagamentoServico.consultarMsgErroGeraPagamentoPorIDSolicitacao(sol.getId()));
		}
		
		boolean isProcessoPreenchido = novaSolicitacao.getProcesso() != null &&!novaSolicitacao.getProcesso().isEmpty();
		
		if(sol.getTipoDeDespesa().getIndicadorProcesso().toString().equalsIgnoreCase("S") && !isProcessoPreenchido) throw new NegocioException(MensagemEnum.PROCESSO_OBRIGATORIO);
		
		if (isProcessoPreenchido) {
			NumeroProcessoVO processoVO = new NumeroProcessoVO();
			processoVO.setNumeroProcesso(novaSolicitacao.getProcesso());
			processoServico.consultarPorNumero(processoVO);
		}
		
		validarDataSolicitacao(sol);
		
		sol = sapServico.geraPagamento(sol);
		
		sol.setCodigoStatus(sol.getStatusPagamentoEnum().getId().toString());
		if(novaSolicitacao.getTipoDepositoJudicialEnum() != null) {
			sol.setCodigoDepositoJudicial(novaSolicitacao.getTipoDepositoJudicialEnum().getId().longValue());			
		}
		OrgaoGestor orgao = orgaoGestorServico.consultarPorId(sol.getOrgaoGestor().getId());
		sol.setOrgaoGestor(orgao);
		FornecedorSap forn = fornecedorSapServico.consultarPorId(sol.getFornecedorSap().getId());
		sol.setFornecedorSap(forn);
		sol.setDataInsercao(LocalDateTime.now());
		
		sol = solicitacaoPagamentoRepositorio.save(sol);
		if(Validador.isNotNull(sol.getDocumentoSap())) {
			novaSolicitacao.setDocumentoSap(sol.getDocumentoSap());
		}else{
			novaSolicitacao.setListaMensagensErroGeraPagamento(convertListaErroGeraPagamentoVO(sol.getListaMensagensErroGeraPagamento()));
		}
		novaSolicitacao.setId(sol.getId());
		/* Envio de e-mail cancelado pelo cliente
		enviarEmailFornecedorSap(sol, sol.getDocumentoSap() != null
					&& novaSolicitacao.getArquivoPagamento() != null );
					*/
		 
		return novaSolicitacao;
	}
	
	@Transactional(rollbackOn = Throwable.class)
	public SolicitacaoPagamentoVO atualizarSolicitacaoPagamentoPendente(SolicitacaoPagamentoVO solicitacaoPendenteVO) {
		SolicitacaoPagamento solicitacaoBase = consultarSolicitacaoBasePorID(solicitacaoPendenteVO.getId());
		validarSolicitacaoPagamentoPendente(solicitacaoPendenteVO, solicitacaoBase);
		solicitacaoBase  = convertToSolicitacaoPagamento(solicitacaoPendenteVO);
		validarDataSolicitacao(solicitacaoBase);
		try {
			solicitacaoBase.setListaMensagensErroGeraPagamento(listaMensagensErroGeraPagamentoServico.consultarMsgErroGeraPagamentoPorIDSolicitacao(
					solicitacaoBase.getId()));
			
			boolean isProcessoPreenchido = solicitacaoPendenteVO.getProcesso() != null && !solicitacaoPendenteVO.getProcesso().isEmpty();

			if(solicitacaoBase.getTipoDeDespesa().getIndicadorProcesso().toString().equalsIgnoreCase("S") && !isProcessoPreenchido) throw new NegocioException(MensagemEnum.PROCESSO_OBRIGATORIO);
			
			// Caso a o tipo da despesa seja JU01 e o numero do processo nao esteja preenchido
			// o sistema nao vai chama o SAP para consultar o Processo.
			if (isProcessoPreenchido) {
				NumeroProcessoVO processoVO = new NumeroProcessoVO();
				processoVO.setNumeroProcesso(solicitacaoPendenteVO.getProcesso());
				processoServico.consultarPorNumero(processoVO);
			}
			
			solicitacaoBase = sapServico.geraPagamento(solicitacaoBase);
			solicitacaoBase.setCodigoStatus(solicitacaoBase.getStatusPagamentoEnum().getId().toString());
			
			OrgaoGestor orgao = orgaoGestorServico.consultarPorId(solicitacaoBase.getOrgaoGestor().getId());
			solicitacaoBase.setOrgaoGestor(orgao);
			FornecedorSap forn = fornecedorSapServico.consultarPorId(solicitacaoBase.getFornecedorSap().getId());
			solicitacaoBase.setFornecedorSap(forn);
			
			SolicitacaoPagamentoVO resp = convertToVO(solicitacaoPagamentoRepositorio.save(solicitacaoBase));
			
			if(Validador.isNotNull(solicitacaoBase.getDocumentoSap())) {
				resp.setDocumentoSap(resp.getDocumentoSap());
			}else{
				resp.setListaMensagensErroGeraPagamento(convertListaErroGeraPagamentoVO(solicitacaoBase.getListaMensagensErroGeraPagamento()));
			}			 
			return resp;
			
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_SOLICITACAO_PAGAMENTO);
		}
	}
	
	private void validarDataSolicitacao(SolicitacaoPagamento sol) {

		if (sol == null || sol.getDataEmissao() == null || sol.getDataVencimento() == null) {
			logger.error("Datas nula ou inválida!");
			throw new NegocioException(MensagemEnum.DATA_EMISSAO_OU_VENCIMENTO_NULA_OU_INVALIDA);
		}

		if (sol.getDataVencimento().isBefore(sol.getDataEmissao())) {
			logger.error("Data de vencimento não pode ser anterior a data de Emissao!");
			throw new NegocioException(MensagemEnum.ERRO_DATA_VENCIMENTO_MENOR_QUE_DATA_EMISSAO);
		}

		if (sol.getDataVencimento().isBefore(LocalDate.now())) {
			logger.error("Data de vencimento não pode ser anterior a data atual!");
			throw new NegocioException(MensagemEnum.ERRO_DATA_VENCIMENTO_MENOR_QUE_DATA_ATUAL);
		}

		if (sol.getDataEmissao().isAfter(LocalDate.now())) {
			logger.error("Data de emissão não pode ser maior que a data corrente!");
			throw new NegocioException(MensagemEnum.ERRO_DATA_VENCIMENTO_MENOR_QUE_DATA_ATUAL);
		}

	}
	 
	private List<MsgErroGeraPagamentoVO> convertListaErroGeraPagamentoVO(List<MsgErroGeraPagamento> listaErroGeraPagamento){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
		List<MsgErroGeraPagamentoVO> listaVO = new ArrayList<>();
		for (MsgErroGeraPagamento erro : listaErroGeraPagamento) {
			MsgErroGeraPagamentoVO vo = new MsgErroGeraPagamentoVO();
			vo.setId(erro.getId());
			vo.setMensagens(erro.getMensagens());
			vo.setDataInsercao(formatter.format(erro.getDataInsercao()));
			listaVO.add(vo);
		}
		return listaVO;
	}
	
	private void validarSolicitacaoPagamentoPendente(SolicitacaoPagamentoVO solicitacaoPendenteVO, SolicitacaoPagamento solicitacaoBase) {
		if(solicitacaoBase == null) throw new NegocioException(MensagemEnum.SOLICITACAO_NAO_EXISTE_EM_BASE);
		if(solicitacaoPendenteVO.getCodigoStatus().intValue() != StatusPagamentoEnum.PENDENTE.getId()) throw new NegocioException(MensagemEnum.SOLICITACAO_NAO_POSSUI_O_STATUS_PENDENTE);
	}
	
	@Transactional()
	public SolicitacaoPagamentoVO reenviarSolicitacaoPagamento(Long id) {
		try {
			SolicitacaoPagamento solicitacaoPagamentoBase = solicitacaoPagamentoRepositorio.findOne(id);
			validarSolicitacaoConsultadaBase(solicitacaoPagamentoBase);
			if(solicitacaoPagamentoBase.getCodigoStatus().equalsIgnoreCase(StatusPagamentoEnum.PENDENTE.getId().toString())) {
				SolicitacaoPagamento solicitacaoPagamentoAtualizada = sapServico.geraPagamento(solicitacaoPagamentoBase);
				return convertToVO(solicitacaoPagamentoRepositorio.save(solicitacaoPagamentoAtualizada));
			}
			return null;
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_REENVIAR_SOLICITACAO_PAGAMENTO);
		}
	}
	
	public SolicitacaoPagamentoVO consultarSituacaoSolicitacaoPagamento(Long id) {
		try {
			SolicitacaoPagamento solicitacaoPagamentoBase = solicitacaoPagamentoRepositorio.findOne(id);
			solicitacaoPagamentoBase.setStatusPagamentoEnum(StatusPagamentoEnum.getStatusPagamentoEnumPorID(new Long(solicitacaoPagamentoBase.getCodigoStatus())));
			solicitacaoPagamentoBase = sapServico.confirmaPagamento(solicitacaoPagamentoBase);
			return convertToVO(solicitacaoPagamentoRepositorio.save(solicitacaoPagamentoBase));
		} catch (Exception e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_SOLICITACAO_PAGAMENTO);
		}
	}

	/**
	 * Receb uma lista de status para recuperar as solicitações com aqueles status.
	 * @param listaStatus
	 * @return
	 */
	public List<SolicitacaoPagamento> consultarSolicitacaoPagamentoStatusPagamento(List<String> listaStatus){
		try {
			return solicitacaoPagamentoRepositorio.findByCodigoStatusIn(listaStatus);
		} catch (Exception e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_SOLICITACAO_PAGAMENTO_STATUS_PAGAMENTO);
		}
	}
	
	public SolicitacaoPagamento atualizarSolicitacaoPagamentoBase(SolicitacaoPagamento solicitacaoPagamento) {
		try {
			return solicitacaoPagamentoRepositorio.save(solicitacaoPagamento);
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_ATUALIZAR_SOLICITACAO_PAGAMENTO_BASE);
		}
	}
	
	public SolicitacaoPagamento convertToSolicitacaoPagamento(SolicitacaoPagamentoVO solicitacaoVO){
		SolicitacaoPagamento solicitacao = modelMapper.map(solicitacaoVO, SolicitacaoPagamento.class);
		solicitacao.setDataEmissao(LocalDate.parse(solicitacaoVO.getDataEmissao()));
		solicitacao.setDataVencimento(LocalDate.parse(solicitacaoVO.getDataVencimento()));
		if(solicitacaoVO.getDataInsercao() != null) {
			solicitacao.setDataInsercao(LocalDateTime.parse(solicitacaoVO.getDataInsercao()));
		}
		return solicitacao;
	}
	
	private SolicitacaoPagamentoVO convertToVO(SolicitacaoPagamento solicitacao) {
		SolicitacaoPagamentoVO solicitacaoVO = modelMapper.map(solicitacao, SolicitacaoPagamentoVO.class);
		DomicilioBancarioVO dbanc = new DomicilioBancarioVO();
		dbanc.setBancoDomicilio(solicitacao.getDomicilioBancario());
		solicitacaoVO.setDomicilioBancario(dbanc);		
		solicitacaoVO.setStatusPagamentoEnum(StatusPagamentoEnum.getStatusPagamentoEnumPorID(Long.parseLong(solicitacao.getCodigoStatus())).getDesc());
		
	    return solicitacaoVO;
	}
	
	public SolicitacaoPagamentoVO consultarStatusSAPSolicitacaoPagamento(Long id) {
		try {
			SolicitacaoPagamento solicitacaoPagamentoBase = solicitacaoPagamentoRepositorio.findOne(id);
			if(solicitacaoPagamentoBase.getStatusPagamentoEnum().getId().compareTo(StatusPagamentoEnum.PENDENTE.getId()) == 0) {
				SolicitacaoPagamento solicitacaoPagamentoAtualizada = sapServico.geraPagamento(solicitacaoPagamentoBase);
				return convertToVO(solicitacaoPagamentoRepositorio.save(solicitacaoPagamentoAtualizada));
			}
			return null;
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_REENVIAR_SOLICITACAO_PAGAMENTO);
		}
	}
	
	private void validarSolicitacaoConsultadaBase(SolicitacaoPagamento solicitacaoPagamento) {
		if(solicitacaoPagamento == null) throw new NegocioException(MensagemEnum.SOLICITACAO_CONSULTADA_NULA_OU_INVALIDA);
	}
	
	public List<SolicitacaoPagamento> consultarSolicitacaoPorOrgaoGestor(OrgaoGestor orgaoGestor) {
		List<SolicitacaoPagamento> lstSolic = null;
		try {
			lstSolic = solicitacaoPagamentoRepositorio.findByOrgaoGestor(orgaoGestor);
		} catch (Exception e) {
			logger.error("Erro ao consultar solicitações por orgão gestor: ", e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_ORDEM);
		}
		return lstSolic;
	}
	
	public List<SolicitacaoPagamento> consultarSolicitacaoPorTipoDeDespesa(TipoDeDespesa tipoDeDespesa) {
		List<SolicitacaoPagamento> lstSolic = null;
		try {
			lstSolic = solicitacaoPagamentoRepositorio.findByTipoDeDespesa(tipoDeDespesa);
		} catch (Exception e) {
			logger.error("Erro ao consultar solicitações por orgão gestor: ", e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_TIPO_DE_DESPESA);
		}
		return lstSolic;
	}
	
	public List<SolicitacaoPagamento> consultarSolicitacaoPorFornecedorSAP(FornecedorSap fornecedorSap) {
		List<SolicitacaoPagamento> lstSolic = null;
		try {
			lstSolic = solicitacaoPagamentoRepositorio.findByFornecedorSap(fornecedorSap);
		} catch (Exception e) {
			logger.error("Erro ao consultar solicitações por orgão gestor: ", e);
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_FORNECEDOR_SAP);
		}
		return lstSolic;
	}
	
	public PaginacaoVO<SolicitacaoPagamentoVO> consultar(PaginacaoVO<?> paginaFiltros) {
		try {
			HashMap<String, Object> filtros =
					paginaFiltros.getBusca() == null || paginaFiltros.getBusca().isEmpty()? new HashMap<String, Object>(): new ObjectMapper().readValue(paginaFiltros.getBusca(), new TypeReference<HashMap<String, Object>>(){});
			SolicitacaoPagamento busca = new SolicitacaoPagamento();
			if(filtros.containsKey("orgaoGestor") && filtros.get("orgaoGestor") != null) {
				OrgaoGestor orgao = new OrgaoGestor();
				orgao.setId(Long.valueOf(filtros.get("orgaoGestor").toString()));
				busca.setOrgaoGestor(orgao);
			}
			if(filtros.containsKey("processo") && filtros.get("processo") != null) {
				busca.setProcesso(filtros.get("processo").toString());
			}
			if(filtros.containsKey("tipoDeDespesa") && filtros.get("tipoDeDespesa") != null) {
				TipoDeDespesa tipoDespesa = new TipoDeDespesa(); 
				tipoDespesa.setId(Long.valueOf(filtros.get("tipoDeDespesa").toString()));
				busca.setTipoDeDespesa(tipoDespesa);
			}
			if(filtros.containsKey("statusPagamentoEnum") && filtros.get("statusPagamentoEnum") != null) {
				busca.setCodigoStatus(StatusPagamentoEnum.fromDescricao(filtros.get("statusPagamentoEnum").toString()).getId().toString());
			}
			Page<SolicitacaoPagamento> resultado = solicitacaoPagamentoRepositorio.findAll(Example.of(busca), paginaFiltros.getPageableLocal());
			List<SolicitacaoPagamentoVO> itens = resultado.getContent().stream().map(obj->{
				String situacao = StatusPagamentoEnum.fromCodigo(obj.getCodigoStatus()).getDesc();
				SolicitacaoPagamentoVO pagamento = new ModelMapper().map(obj, SolicitacaoPagamentoVO.class);
				pagamento.setStatusPagamentoEnum(situacao);
				Usuario usuarioCriador = usuarioServico.consultarUsuarioByMatricula(pagamento.getUsuario());
				DomicilioBancarioVO domicilio = new DomicilioBancarioVO();
				domicilio.setBancoDomicilio(obj.getDomicilioBancario());
				pagamento.setDomicilioBancario(domicilio);
				pagamento.setLoginSolicitacao(usuarioCriador.getNomeUsuario());
				pagamento.setFormaPagamentoEnumDesc(obj.getFormaPagamentoEnum().getDesc());
				return pagamento;
			}).collect(Collectors.toList());
			return new PaginacaoVO<>(new PageRequest(resultado.getNumber(), resultado.getNumberOfElements()), itens, resultado.getTotalElements());
		}catch (NegocioException e) {
			throw e;
		}catch (Exception e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_SOLICITACAO_PAGAMENTO);
		}
		
	}
	
	public void enviarEmailFornecedorSap(SolicitacaoPagamento solicitacao, boolean enviarComDocAnexo) {	
		if(enviarComDocAnexo) {			
			Usuario usuario = usuarioServico.consultarUsuarioByMatricula(solicitacao.getUsuario());
			String emailRemetente = emailService.getChesfDomineoEmail().concat(usuario.getNomeUsuario());
			emailService.enviarEmailSolicitacaoPagamento(emailRemetente, solicitacao.getDocumentoSap());
		}
	}
	
}

