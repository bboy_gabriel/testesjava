package br.com.chesf.projurfinapi.enums;

public enum IndicadorSimNaoEnum {
	S("SIM"),
	N("NÃO");
	
	private String desc;
	
	private IndicadorSimNaoEnum(String desc){
		this.desc = desc;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
