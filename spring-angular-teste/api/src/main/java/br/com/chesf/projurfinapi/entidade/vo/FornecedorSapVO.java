package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

/**
 * 
 * @author jvneto1
 * 
 * Classe VO usada para não expor a entidade FornecedorSap
 *
 */
public class FornecedorSapVO implements Serializable{

	private static final long serialVersionUID = -2880101702824179413L;
	
    private Long id;
	
    @Min(value = 1)
    @NotNull(message = "Codigo nulo ou inválido!")
	private Long codigo;
	
    @NotEmpty(message = "Descrição nulo ou inválido!")
	private String descricao;
	
    @CNPJ
    @NotEmpty(message = "CNPJ nulo ou inválido!")
	private String cnpj;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj the cnpj to set
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
