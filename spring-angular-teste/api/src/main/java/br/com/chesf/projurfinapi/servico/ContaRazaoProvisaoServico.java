package br.com.chesf.projurfinapi.servico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.chesf.projurfinapi.entidade.ContaRazaoProvisao;
import br.com.chesf.projurfinapi.entidade.vo.ContaRazaoProvisaoVO;
import br.com.chesf.projurfinapi.enums.MensagemEnum;
import br.com.chesf.projurfinapi.enums.TipoProcessoContaRazaoEnum;
import br.com.chesf.projurfinapi.exception.NegocioException;
import br.com.chesf.projurfinapi.repositorio.ContaRazaoProvisaoRepositorio;

/**
 * 
 * Classe responsavel por fornecer uma camada de serviço ao fluxo de Consta Razão Previsão.
 * 
 * @author jvneto1
 *
 */
@Service
public class ContaRazaoProvisaoServico implements Serializable{

	private static final long serialVersionUID = -3039932077378106662L;
	
	@Autowired
	private ContaRazaoProvisaoRepositorio contaRazaoProvisaoRepositorio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	private static Long PREFIXO_CREDITO = 22199L;
	private static Long PREFIXO_DEBITO = 41950L;

	/**
	 * 
	 * Serviço responsavel por listar todas as entidades ContaRazaoProvisao.
	 * 
	 * @return List<ContaRazaoProvisaoVO>
	 * 
	 */
	public List<ContaRazaoProvisaoVO> consultarContaRazaoProvisao() {
		try {
			return convertListToVO(contaRazaoProvisaoRepositorio.findAll());
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CONSULTAR_CONTA_RAZAO_PROVISAO);
		}
	}
	
	/**
	 * 
	 * Serviço responsavel por cadastrar um recurso ContaRazaoProvisão.
	 * 
	 * @param ContaRazaoProvisaoVO
	 * @return ContaRazaoProvisaoVO
	 * 
	 */
	public ContaRazaoProvisaoVO cadastrarContaRazaoProvisao (ContaRazaoProvisaoVO contaVO) {
		ContaRazaoProvisao contaBase = modelMapper.map(contaVO, ContaRazaoProvisao.class);
		validarPrefixoConta(contaBase);
		verificarExistenciaContaRazaoProvisao(contaBase);
		try {
			return convertToVO(contaRazaoProvisaoRepositorio.save(contaBase));
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.ERRO_AO_CADASTRAR_CONTA_RAZAO_PROVISAO);
		}
	}
	
	/**
	 * 
	 * Metodo responsável por fornecer um serviço onde é possível consultar uma lista de ContaRazaoProvisao pelo codigo do tipo de processo.
	 * 
	 * @param codigo
	 * @return List<ContaRazaoProvisaoVO>
	 * 
	 */
	public List<ContaRazaoProvisaoVO> consultarLContaRazaoPorTipoProcesso(String codigo){
		try {
			return convertListToVO(contaRazaoProvisaoRepositorio.findByCodigoTipoProcesso(codigo));
		} catch (NegocioException e) {
			throw new NegocioException(MensagemEnum.NAO_FOI_POSSIVEL_CONSULTAR_LISTA_CONTA_POR_TIPO_PROCESSO);
		}
	}
	
	/**
	 * 
	 * Metodo privado responsavel por validar o prefixo das contas, credito e debito, usadas na inserção da contaRazaoProvisao.
	 * 
	 * @param ContaRazaoProvisao contaBase
	 */
	private void validarPrefixoConta(ContaRazaoProvisao contaBase) {
		Long prefixoCredito = Long.parseLong(montarPrefixoConta(contaBase.getContaCreditoProvisao()));
		Long prefixoDebito = Long.parseLong(montarPrefixoConta(contaBase.getContaDebitoProvisao()));
		if(!prefixoCredito.equals(PREFIXO_CREDITO)) throw new NegocioException(MensagemEnum.PREFIXO_CONTA_CREDITO_PROVISAO_INVALIDA);
		if(!prefixoDebito.equals(PREFIXO_DEBITO)) throw new NegocioException(MensagemEnum.PREFIXO_CONTA_DEBITO_PROVISAO_INVALIDA);
	}
	
	/**
	 * 
	 * Metodo privado responsavel por obter o prefixo da conta, credito ou debito, passada.
	 * 
	 * @param Long conta
	 * @return String prefixo
	 */
	private String montarPrefixoConta(Long conta) {
		char[] arrayPrefixo = conta.toString().toCharArray();
		StringBuilder prefixo = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			prefixo.append(arrayPrefixo[i]);
		}
		return prefixo.toString();
	}
	
	/**
	 * 
	 * Metodo privado que verificar a existencia de uma contaRazaoProvisao cadastrada com a mesma conta de debito e credito.
	 * 
	 * @param ContaRazaoProvisao
	 * 
	 */
	private void verificarExistenciaContaRazaoProvisao(ContaRazaoProvisao contaBase) {
		if(contaRazaoProvisaoRepositorio.existsByContaDebitoProvisaoAndContaCreditoProvisao(contaBase.getContaDebitoProvisao(), contaBase.getContaCreditoProvisao())) 
			throw new NegocioException(MensagemEnum.CONTA_RAZAO_PROVISAO_JA_EXISTE);
	}
	
	/**
	 * 
	 * Metodo privado responsavel por converter uma entidade ContaRazaoProvisao em uma ContaRazaoProvisaoVO.
	 * 
	 * @param ContaRazaoProvisao 
	 * @return ContaRazaoProvisaoVO 
	 * 
	 */
	private ContaRazaoProvisaoVO convertToVO(ContaRazaoProvisao contaBase) {
		ContaRazaoProvisaoVO contaVO = modelMapper.map(contaBase, ContaRazaoProvisaoVO.class);
		contaVO.setTipoProcessoProvisaoEnum(TipoProcessoContaRazaoEnum.getTipoProcessoContaRazaoEnumPorID(Integer.parseInt(contaBase.getCodigoTipoProcesso())));
		return contaVO;
	}
	
	/**
	 * 
	 * Metodo privado responsavel por converter uma lista da entidade ContaRazaoProvisao para uma lista de ContaRazaoProvisaoVO.
	 * 
	 * @param List<ContaRazaoProvisao> listaConta
	 * @return List<ContaRazaoProvisaoVO>
	 * 
	 */
	private List<ContaRazaoProvisaoVO> convertListToVO(List<ContaRazaoProvisao> listaConta) {
		List<ContaRazaoProvisaoVO> listaVO = new ArrayList<>();
		for (ContaRazaoProvisao conta : listaConta) {
			ContaRazaoProvisaoVO vo = new ContaRazaoProvisaoVO();
			vo = modelMapper.map(conta, ContaRazaoProvisaoVO.class);
			vo.setTipoProcessoProvisaoEnum(TipoProcessoContaRazaoEnum.getTipoProcessoContaRazaoEnumPorID(Integer.parseInt(conta.getCodigoTipoProcesso())));
			listaVO.add(vo);
		}
		return listaVO;
	}
	
	
}
