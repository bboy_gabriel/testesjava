package br.com.chesf.projurfinapi.enums;

public enum TipoDepositoJudicialEnum {

	RECURSAL(1, "Recursal"),
	PRINCIPAL(2, "Principal");
	
	private Integer id;
	private String desc;
	
	private TipoDepositoJudicialEnum(Integer id, String desc){
		this.id = id;
		this.desc = desc;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	/**
	 * Retorna o tipo de depósito judicial de acordo com o código passado
	 * @param codigo
	 * @return
	 */
	public static final TipoDepositoJudicialEnum getTipoDepositoJudicialEnumByID(Integer codigo) {
		TipoDepositoJudicialEnum tipo = null;
		switch (codigo) {
		case 1:
			tipo = TipoDepositoJudicialEnum.RECURSAL;
			break;
		case 2:
			tipo = TipoDepositoJudicialEnum.PRINCIPAL;
		}
		return tipo;
	}
}
