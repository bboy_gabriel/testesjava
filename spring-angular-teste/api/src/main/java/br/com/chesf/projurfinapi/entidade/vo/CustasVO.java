package br.com.chesf.projurfinapi.entidade.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class CustasVO implements Serializable{
	
	private static final long serialVersionUID = 7483981943639847271L;
	
	private String descricao;
	private TipoDeDespesaVO tipoDeDespesaVO;
	private BigDecimal valor;
	
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the tipoDeDespesaVO
	 */
	public TipoDeDespesaVO getTipoDeDespesaVO() {
		return tipoDeDespesaVO;
	}
	/**
	 * @param tipoDeDespesaVO the tipoDeDespesaVO to set
	 */
	public void setTipoDeDespesaVO(TipoDeDespesaVO tipoDeDespesaVO) {
		this.tipoDeDespesaVO = tipoDeDespesaVO;
	}
	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
}
